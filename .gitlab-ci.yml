default:
  image: maven:3-eclipse-temurin-17
#  tags:
#    - nms

stages:
  - check
  - build
  - deploy
  - publish-news

variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
  MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
  MAVEN_CLI_OPTS: "--batch-mode"
  LC_CTYPE: "en_US.UTF-8"
  LC_ALL: "en_US.UTF-8"

############ Checks ############

check-changelog-and-version:
  stage: check
  only:
    - main@lasersenigma/le-play-server-utils
  before_script:
    - apt-get -qq update -y
    - apt-get -qq install -y xmlstarlet
  script:
    - git clone https://gitlab.com/lasersenigma/le-play-server-utils.git le-play-server-utils
    - cd le-play-server-utils
    - last_tag=$(git tag -l --sort=-v:refname | sed -n '1 p')
    - echo "The last tag is $last_tag"
    - |
      echo "CHECKING THAT THE VERSION IS DIFFERENT FROM THE LAST TAG"
      revision=$(xmlstarlet sel -t -v "/*[local-name()='project']/*[local-name()='version']" pom.xml | sed 's/-SNAPSHOT//')
      echo "The revision is $revision"
      if [ "$last_tag" == "$revision" ]; then
          >&2 echo "The version is the same as the last tag. Test failed.";
          exit 1;
      else
          echo "The version is different from the last tag. Test passed.";
      fi
    - |
      echo "CHECKING THAT THE CHANGELOG HAS BEEN MODIFIED SINCE THE LAST TAG"
      if ! git diff --unified=0 ${last_tag}..${CI_COMMIT_SHA} -- CHANGELOG.md | grep -Po '(?<=^\+)(?!\+\+).*' > changelog-diff.txt; then
          echo "The file CHANGELOG.md has not be modified. There is no difference since the tag ${last_tag}."
          exit 1;
      fi
      cat changelog-diff.txt
      if [ "$(wc -c < "changelog-diff.txt")" -gt 5 ]; then
          echo "The CHANGELOG.md diff contains more than 5 characters. Test passed.";
      else
          >&2 echo "The file CHANGELOG.md has less than 5 new characters. It should be modified. There is no difference since the tag ${last_tag}.";
          exit 1;
      fi

############ Build ############

build:
  stage: build
  script:
    - mvn $MAVEN_CLI_OPTS $MAVEN_OPTS clean verify
  cache:
    - key: m2
      paths:
        - $CI_PROJECT_DIR/.m2/repository
      policy: pull-push

############ Upload plugin to play server and restart the server ############

update-play-server:
  stage: deploy
  only:
    - tags
  before_script:
    - apt-get -qq update -y
    - apt-get -qq install -y openssh-client sshpass
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan -p ${PLAY_MC_SERVER_SSH_PORT} ${PLAY_MC_SERVER_SSH_HOST} >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - mvn $MAVEN_CLI_OPTS $MAVEN_OPTS clean verify
    - mv target/le-play-server-utils-*.jar target/le-play-server-utils.jar
    - echo "Upload le-play-server-utils-*.jar on the play server"
    - |
      sshpass -p "${PLAY_MC_SERVER_SSH_PASSWORD}" sftp -oPort=${PLAY_MC_SERVER_SSH_PORT} ${PLAY_MC_SERVER_SSH_LOGIN}@${PLAY_MC_SERVER_SSH_HOST} <<EOF
        put $CI_PROJECT_DIR/target/le-play-server-utils.jar /plugins/le-play-server-utils.jar
        chmod 644 /plugins/le-play-server-utils.jar
        bye
      EOF
    - echo "Inform players then restart the play server"
    - 'curl -X POST
      -H "Authorization: Bearer $PLAY_MC_SERVER_PTERO_API_KEY"
      -H "Content-Type: application/json"
      -d ''{"command": "say The server will restart in 10 seconds to update Lasers-Enigma Play Server Utils plugin since a new version was published."}''
       https://${PLAY_MC_SERVER_PTERO_HOST}/api/client/servers/${PLAY_MC_SERVER_PTERO_SERVER_ID}/command'
    - sleep 10
    - echo "Restart the play server"
    - 'curl -X POST
      -H "Authorization: Bearer $PLAY_MC_SERVER_PTERO_API_KEY"
      -H "Content-Type: application/json"
      -d ''{"signal": "restart"}''
       https://${PLAY_MC_SERVER_PTERO_HOST}/api/client/servers/${PLAY_MC_SERVER_PTERO_SERVER_ID}/power'

############ Compute changelog diff and post it to discord webhook ############

publish-discord:
  stage: publish-news
  only:
    - tags
  before_script:
    - apt-get -qq update -y
    - apt-get -qq install -y git jq
  script:
    - git clone https://gitlab.com/lasersenigma/le-play-server-utils.git le-play-server-utils
    - cd le-play-server-utils
    - git diff --unified=0 $(git tag -l --sort=-v:refname | sed -n '2 p')..${CI_COMMIT_SHA} -- CHANGELOG.md | grep -Po '(?<=^\+)(?!\+\+).*' > changelog-diff.txt
    - cat changelog-diff.txt
    - |
      # Add lines of changelog-diff to changelog-diff-tmp until we reached the characters limit
      touch changelog-diff-tmp.txt
      total_length=0
      while IFS= read -r line; do
          line_length=${#line}
          if (( total_length + line_length <= 1800 )); then
              echo "$line" >> changelog-diff-tmp.txt
              total_length=$((total_length + line_length))
          else
              echo $'\n...' >> changelog-diff-tmp.txt
              break
          fi
      done < changelog-diff.txt
      mv changelog-diff-tmp.txt changelog-diff.txt
    - cat changelog-diff.txt
    - cd ..
    - 'jq --rawfile changelog le-play-server-utils/changelog-diff.txt ''.embeds[].description += $changelog'' ci/discord_webhook_body.json > ci/discord_webhook_body_tmp.json'
    - mv ci/discord_webhook_body_tmp.json ci/discord_webhook_body.json
    - cat ci/discord_webhook_body.json
    - 'curl -v -X POST
      -H "Content-Type: application/json"
      -d "@ci/discord_webhook_body.json"
      ${DISCORD_WEBHOOK_URL}'