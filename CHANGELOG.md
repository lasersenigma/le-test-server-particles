[[_TOC_]]

# V1.10.1

- Fix a technical bug preventing the plugin startup

# V1.9.0

## Technical changes

 - Use of reflection to bind enable/disable event handler and listeners

# V1.8.0
- Add new animations in the credits room with a button allowing to change the animation : 
  - A second mandala with a axial symmetry for the half of the points
  - A circuit pulse 2D animation
  - An animation with the lasers enigma logo

# V1.7.0
- Allow players to rate puzzles (rating + difficulty evaluation + comment)
  - It is suggested in the GUI when exiting a puzzle room
  - It is also possible to review a puzzle by using `/puzzle review create` in a puzzle room

- Allow players to see the review of a puzzle
  - By using `/puzzle review view` in a puzzle room
  - In a discord channel
  
# V1.6.0
- Add/set/delete/clear hints in rooms
- Add puzzle hint command to allow players to get hints

# V1.5.5
- Uniformize commands message prefix
- Fix a bug related to teleport menu opener item

# V1.5.4
- Chat messages style uniformization

# V1.5.3
- When filtering by tags, the teleportation menu will display the teleporters having every selected tags
  (instead of teleporters having at least one of the selected tags).

# V1.5.2
- Do not replace 9th slot item (with the the teleportation menu opener) if lasers enigma menu is opened 

# V1.5.1
- Fix portals issue when a player is inside the portal's room

# V1.5.0
- Modify teleport menu colors and icons
- Modify portal particles colors

# V1.4.2
- Multiple bug fixes

# V1.4.1
- Multiple bug fixes

# V1.4.0
- Add colors to portals according to difficulty and players inside
- Display players count in both portals hologram and teleportation menu

# V1.3.0
- Fix Mandala for multiplayer

# V1.2.0
- Add Mandala particle anim easter egg

# V1.1.0
- teleportation menu enhancements

# V1.0.2
- Improve publishing system

# V1.0.0
- Publishing system implementation