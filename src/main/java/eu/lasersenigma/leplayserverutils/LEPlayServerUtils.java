package eu.lasersenigma.leplayserverutils;

import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import eu.lasersenigma.leplayserverutils.common.JavaPluginDisableEventHandler;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import org.bukkit.plugin.java.JavaPlugin;

public final class LEPlayServerUtils extends JavaPlugin {
    private Injector injector;

    @Override
    public void onEnable() {
        LEPlayServerUtilsModule module = new LEPlayServerUtilsModule(this);
        this.injector = module.createInjector();
        injector.injectMembers(this);
        injector.findBindingsByType(new TypeLiteral<JavaPluginEnableEventHandler>() {
                })
                .forEach(
                        javaPluginEventsHandlerBinding -> javaPluginEventsHandlerBinding.getProvider().get().enable()
                );
        injector.findBindingsByType(new TypeLiteral<LEPSUListener>() {
                })
                .forEach(
                        listener -> getServer().getPluginManager().registerEvents(listener.getProvider().get(), this)
                );

    }

    @Override
    public void onDisable() {
        injector.findBindingsByType(new TypeLiteral<JavaPluginDisableEventHandler>() {
                })
                .forEach(
                        javaPluginEventsHandlerBinding -> javaPluginEventsHandlerBinding.getProvider().get().disable()
                );
    }
}
