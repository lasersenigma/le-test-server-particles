package eu.lasersenigma.leplayserverutils;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.clipboard.ClipboardManager;
import eu.lasersenigma.leplayserverutils.common.JavaPluginDisableEventHandler;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.common.command.provider.LPUCommandsProvider;
import eu.lasersenigma.leplayserverutils.common.command.provider.PuzzleCommandsProvider;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.JsonManagerProvider;
import eu.lasersenigma.leplayserverutils.common.messages.MessageFormatter;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.common.utils.WorldEditExecutor;
import eu.lasersenigma.leplayserverutils.lobby.bumpers.BumperAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.bumpers.task.BumperTask;
import eu.lasersenigma.leplayserverutils.lobby.credit.CreditAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.credit.listener.CreditsParticleHitBlockListener;
import eu.lasersenigma.leplayserverutils.lobby.credit.service.CreditsRoomService;
import eu.lasersenigma.leplayserverutils.lobby.credit.task.CreditAreaTask;
import eu.lasersenigma.leplayserverutils.lobby.infinitecorridor.CorridorDialogManager;
import eu.lasersenigma.leplayserverutils.lobby.infinitecorridor.task.CorridorDialogTask;
import eu.lasersenigma.leplayserverutils.lobby.lobby.LobbyController;
import eu.lasersenigma.leplayserverutils.lobby.lobby.command.LobbyCommand;
import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.MandalaNormalAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.mandala.listener.ModChangeButtonListener;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomService;
import eu.lasersenigma.leplayserverutils.lobby.mandala.task.MandalaAreaTask;
import eu.lasersenigma.leplayserverutils.player.LpuPlayerController;
import eu.lasersenigma.leplayserverutils.player.command.LpuPlayerCommand;
import eu.lasersenigma.leplayserverutils.player.listener.NoHungerListener;
import eu.lasersenigma.leplayserverutils.player.listener.RegenerationOnPlayerJoinListener;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import eu.lasersenigma.leplayserverutils.portal.command.PortalCommand;
import eu.lasersenigma.leplayserverutils.portal.graph.PortalGraphService;
import eu.lasersenigma.leplayserverutils.portal.gui.TeleportMenuItemStackProvider;
import eu.lasersenigma.leplayserverutils.portal.gui.TeleportMenuProvider;
import eu.lasersenigma.leplayserverutils.portal.gui.TeleportMenuStateManager;
import eu.lasersenigma.leplayserverutils.portal.listener.PortalHologramDisplayHandler;
import eu.lasersenigma.leplayserverutils.portal.provider.TeleporterDTOsProvider;
import eu.lasersenigma.leplayserverutils.portal.task.PortalTask;
import eu.lasersenigma.leplayserverutils.puzzle.PuzzleController;
import eu.lasersenigma.leplayserverutils.puzzle.download.PuzzleSchematicService;
import eu.lasersenigma.leplayserverutils.puzzle.download.command.PuzzleDownloadCommand;
import eu.lasersenigma.leplayserverutils.puzzle.hint.PlayerHintService;
import eu.lasersenigma.leplayserverutils.puzzle.hint.command.PuzzleHintCommand;
import eu.lasersenigma.leplayserverutils.puzzle.hint.listener.PlayerHintListener;
import eu.lasersenigma.leplayserverutils.puzzle.review.command.PuzzleReviewCommand;
import eu.lasersenigma.leplayserverutils.puzzle.review.listener.DelayedReviewUpdateListener;
import eu.lasersenigma.leplayserverutils.puzzle.review.listener.PuzzleReviewBookSignListener;
import eu.lasersenigma.leplayserverutils.puzzle.solution.PuzzleSolutionService;
import eu.lasersenigma.leplayserverutils.puzzle.teleport.command.PuzzleTeleportCommand;
import eu.lasersenigma.leplayserverutils.puzzle.teleport.listener.TeleportMenuListener;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.room.author.RoomAuthorController;
import eu.lasersenigma.leplayserverutils.room.author.command.RoomAuthorCommand;
import eu.lasersenigma.leplayserverutils.room.colorwheel.listener.ColorWheelListener;
import eu.lasersenigma.leplayserverutils.room.command.RoomCommand;
import eu.lasersenigma.leplayserverutils.room.hint.RoomHintController;
import eu.lasersenigma.leplayserverutils.room.hint.command.RoomHintCommand;
import eu.lasersenigma.leplayserverutils.roomtemplate.RoomTemplateController;
import eu.lasersenigma.leplayserverutils.roomtemplate.command.RoomTemplateCommand;
import eu.lasersenigma.leplayserverutils.tag.TagController;
import eu.lasersenigma.leplayserverutils.tag.command.TagCommand;
import fr.skytale.jsonlib.JsonManager;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ScanResult;

public class LEPlayServerUtilsModule extends AbstractModule {

    private final LEPlayServerUtils plugin;

    public LEPlayServerUtilsModule(LEPlayServerUtils plugin) {
        this.plugin = plugin;
    }

    public Injector createInjector() {
        return Guice.createInjector(this);
    }

    @Override
    protected void configure() {

        //Common
        this.bind(LEPlayServerUtils.class).toInstance(this.plugin);
        this.bind(JsonManager.class).toProvider(JsonManagerProvider.class);

        this.bind(MessageFormatter.class);
        this.bind(MessageSender.class);
        this.bind(ConfigManager.class);
        this.bind(WorldEditExecutor.class);

        //Commands providers
        this.bind(LPUCommandsProvider.class);
        this.bind(PuzzleCommandsProvider.class);

        //lobby
        this.bind(LobbyController.class);
        this.bind(LobbyCommand.class);

        //credits
        this.bind(CreditAnimDisplayHandler.class);
        this.bind(CreditAreaTask.class);
        this.bind(CreditsParticleHitBlockListener.class).asEagerSingleton();
        this.bind(CreditsRoomService.class);

        //mandala
        this.bind(MandalaNormalAnimDisplayHandler.class);
        this.bind(MandalaAreaTask.class);
        this.bind(MandalaRoomService.class);
        this.bind(ModChangeButtonListener.class).asEagerSingleton();

        //portal
        this.bind(PortalGraphService.class);
        this.bind(PortalController.class);
        this.bind(PortalCommand.class);
        this.bind(PortalTask.class);
        this.bind(PortalHologramDisplayHandler.class);

        //room
        this.bind(RoomController.class);
        this.bind(RoomCommand.class);
        this.bind(ColorWheelListener.class).asEagerSingleton();

        //tags
        this.bind(TagController.class);
        this.bind(TagCommand.class);

        //author
        this.bind(RoomAuthorController.class);
        this.bind(RoomAuthorCommand.class);

        //hint
        this.bind(RoomHintCommand.class);
        this.bind(RoomHintController.class);

        //room template
        this.bind(RoomTemplateController.class);
        this.bind(RoomTemplateCommand.class);

        //bumpers
        this.bind(BumperAnimDisplayHandler.class);
        this.bind(BumperTask.class);

        //Player
        this.bind(NoHungerListener.class).asEagerSingleton();
        this.bind(RegenerationOnPlayerJoinListener.class).asEagerSingleton();
        this.bind(LpuPlayerCommand.class);
        this.bind(LpuPlayerController.class);


        //puzzle
        this.bind(TeleporterDTOsProvider.class);
        this.bind(TeleportMenuProvider.class);
        this.bind(TeleportMenuItemStackProvider.class);
        this.bind(TeleportMenuListener.class).asEagerSingleton();
        this.bind(TeleportMenuStateManager.class);

        this.bind(PuzzleController.class);
        this.bind(PuzzleSchematicService.class);
        this.bind(PuzzleTeleportCommand.class);
        this.bind(PuzzleDownloadCommand.class);
        this.bind(PuzzleHintCommand.class);
        this.bind(PuzzleSolutionService.class);
        this.bind(PlayerHintListener.class).asEagerSingleton();
        this.bind(PlayerHintService.class);
        this.bind(PuzzleReviewBookSignListener.class).asEagerSingleton();
        this.bind(PuzzleReviewCommand.class);
        this.bind(DelayedReviewUpdateListener.class).asEagerSingleton();


        // LasersEnigma
        this.bind(Areas.class).toInstance(Areas.getInstance());
        this.bind(ClipboardManager.class).toInstance(ClipboardManager.getInstance());
        this.bind(LasersEnigmaPlugin.class).toInstance(LasersEnigmaPlugin.getInstance());


        //Infinite corridor
        this.bind(CorridorDialogManager.class);
        this.bind(CorridorDialogTask.class);

        // Bind for enable event handler
        createAndPopulateMultiBinder(JavaPluginEnableEventHandler.class);

        // Bind for disable event handler
        createAndPopulateMultiBinder(JavaPluginDisableEventHandler.class);

        // Bind all listeners
        createAndPopulateMultiBinder(LEPSUListener.class);
    }

    private <T> void createAndPopulateMultiBinder(Class<T> interfaceType) {
        Multibinder<T> multibinder = Multibinder.newSetBinder(binder(), interfaceType)
                .permitDuplicates();
        // Bind all listeners
        try (ScanResult scanResult = new ClassGraph()
                .acceptPackages("eu.lasersenigma.leplayserverutils")
                .enableClassInfo()
                .scan()) {
            for (ClassInfo classInfo : scanResult.getClassesImplementing(interfaceType.getCanonicalName())) {
                multibinder.addBinding().to((Class<? extends T>) classInfo.loadClass());
            }
        }
    }
}
