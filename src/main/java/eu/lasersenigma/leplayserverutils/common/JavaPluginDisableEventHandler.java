package eu.lasersenigma.leplayserverutils.common;

/**
 * A {@link JavaPluginDisableEventHandler} is an object that needs to handle certain types of things. It will be created and destroyed by the
 * main class.
 */
public interface JavaPluginDisableEventHandler {

    /**
     * Called on server disable. used to clean up things.
     */
    void disable();


}