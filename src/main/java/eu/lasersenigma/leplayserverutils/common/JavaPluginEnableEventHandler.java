package eu.lasersenigma.leplayserverutils.common;

/**
 * A {@link JavaPluginEnableEventHandler} is an object that needs to handle certain types of things. It will be created and destroyed by the
 * main class.
 */
public interface JavaPluginEnableEventHandler {

    /**
     * Called on server enable
     */
    void enable();


}