package eu.lasersenigma.leplayserverutils.common;

public enum LPUPermission {
    ADMIN("leplayserverutils.admin"),
    CREATOR("leplayserverutils.creator");

    private final String permission;

    LPUPermission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
