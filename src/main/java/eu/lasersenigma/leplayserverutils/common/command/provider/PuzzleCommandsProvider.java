package eu.lasersenigma.leplayserverutils.common.command.provider;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import fr.skytale.commandlib.Commands;
import org.bukkit.ChatColor;

import java.util.Objects;

@Singleton
public class PuzzleCommandsProvider {
    private static final String MAIN_COMMAND = "puzzle";
    private static final String CHAT_PREFIX = ChatColor.GRAY + "[LPU]" + ChatColor.RESET + ChatColor.WHITE + " ";
    @Inject
    private LEPlayServerUtils plugin;

    private Commands commands;

    public Commands get() {
        if (commands == null) {
            commands = Commands.setup(
                    Objects.requireNonNull(plugin.getCommand(MAIN_COMMAND)),
                    CHAT_PREFIX
            );
        }
        return commands;
    }
}
