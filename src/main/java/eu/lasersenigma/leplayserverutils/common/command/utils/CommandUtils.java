package eu.lasersenigma.leplayserverutils.common.command.utils;

import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class CommandUtils {

    private CommandUtils() {
    }

    public static @NotNull List<String> buildStringAutocompleteValues(Set<String> autocompleteValues, String currentArgumentValue) {
        String currentArgumentValueFilteredTmp = currentArgumentValue;
        if (currentArgumentValueFilteredTmp.startsWith("\"")) {
            currentArgumentValueFilteredTmp = currentArgumentValueFilteredTmp.substring(1);
        }
        if (currentArgumentValueFilteredTmp.endsWith("\"")) {
            currentArgumentValueFilteredTmp = currentArgumentValueFilteredTmp.substring(0, currentArgumentValueFilteredTmp.length() - 1);
        }
        final String currentArgumentValueFiltered = currentArgumentValueFilteredTmp.toUpperCase(Locale.ROOT);
        return autocompleteValues
                .stream()
                .filter(strValue -> strValue.toUpperCase(Locale.ROOT).contains(currentArgumentValueFiltered))
                .sorted(getStartsWithThenContainsUpperCaseStringComparator(currentArgumentValueFiltered))
                .map(strValue -> {
                    if (strValue.contains(" ")) {
                        return "\"" + strValue + "\"";
                    }
                    return strValue;

                })
                .toList();
    }

    private static @NotNull Comparator<String> getStartsWithThenContainsUpperCaseStringComparator(String currentArgumentValueUpperCase) {
        return (strValue1, strValue2) -> {
            final String strValue1UpperCase = strValue1.toUpperCase(Locale.ROOT);
            final String strValue2UpperCase = strValue2.toUpperCase(Locale.ROOT);
            final boolean strValue1StartWith = strValue1UpperCase.startsWith(currentArgumentValueUpperCase);
            final boolean strValue2StartWith = strValue2UpperCase.startsWith(currentArgumentValueUpperCase);
            if (strValue1StartWith && strValue2StartWith) {
                return strValue1.compareTo(strValue2);
            }
            if (strValue1StartWith) {
                return -1;
            }
            if (strValue2StartWith) {
                return 1;
            }
            final boolean strValue1Contains = strValue1UpperCase.contains(currentArgumentValueUpperCase);
            final boolean strValue2Contains = strValue2UpperCase.contains(currentArgumentValueUpperCase);
            if (strValue1Contains && strValue2Contains) {
                return strValue1.compareTo(strValue2);
            }
            if (strValue1Contains) {
                return -1;
            }
            if (strValue2Contains) {
                return 1;
            }
            return strValue1.compareTo(strValue2);
        };
    }

    public static List<String> buildIntegerAutocompleteValues(List<Integer> indexesSet, Integer currentArgumentValue) {
        return indexesSet
                .stream()
                .filter(index -> currentArgumentValue == null || index.toString().startsWith(currentArgumentValue.toString()))
                .map(Object::toString)
                .toList();
    }
}
