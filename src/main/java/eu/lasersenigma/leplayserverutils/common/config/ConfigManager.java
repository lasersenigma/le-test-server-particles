package eu.lasersenigma.leplayserverutils.common.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.config.event.ConfigUpdatedEvent;
import eu.lasersenigma.leplayserverutils.common.config.migration.ConfigMigrationExecutor;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import eu.lasersenigma.leplayserverutils.lobby.lobby.LobbyController;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import eu.lasersenigma.leplayserverutils.roomtemplate.RoomTemplateController;
import fr.skytale.jsonlib.JsonManager;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Singleton
public class ConfigManager {

    private static final String CONFIG_FILE_NAME = "config.json";

    @Inject
    private JsonManager jsonManager;

    @Inject
    private LEPlayServerUtils plugin;

    @Inject
    private LobbyController lobbyController;

    @Inject
    private WorldVectorToLocationTransformer locationTransformer;

    @Inject
    private ConfigMigrationExecutor configMigrationExecutor;

    private File configFile;

    private Config config;

    private static int comparePortalOutsideLobby(Portal p1, Portal p2) {
        if (p1.isActive() && p2.isActive()) {
            return Double.compare(p1.getRoom().getDifficulty(), p2.getRoom().getDifficulty());
        } else if (p1.isActive()) {
            return -1;
        } else if (p2.isActive()) {
            return 1;
        } else {
            return 0;
        }
    }

    private static int comparePortalInLobby(Portal p1, Portal p2) {
        final int compareY = Double.compare(p1.getLocation().getY(), p2.getLocation().getY());
        if (compareY != 0) return compareY;
        final int compareX = Double.compare(p2.getLocation().getX(), p1.getLocation().getX());
        if (compareX != 0) return compareX;
        return Double.compare(p1.getLocation().getZ(), p2.getLocation().getZ());
    }

    public void saveConfig(Config config) {
        try {
            //TODO store a long version in the config
            //TODO if the version is different, throw an exception to inform CommandSender that a concurrency issue has occurred and that he may have overrided another player modifications
            recomputePortalOrder(config);
            checkConstraints(config);
            addMissingIds(config);
            this.config = config;

            jsonManager.serialize(config, Config.class, configFile);
            Bukkit.getPluginManager().callEvent(new ConfigUpdatedEvent(config));
        } catch (Exception e) {
            throw new RuntimeException("Failed to save config file", e);
        }
    }

    private void addMissingIds(Config config) {
        addMissingRoomIds(config);
    }

    private static void addMissingRoomIds(Config config) {
        final Set<Room> roomSet = config.getPortals().stream()
                .filter(Portal::isActive)
                .map(Portal::getRoom)
                .collect(Collectors.toSet());

        // find next id available
        AtomicInteger aId = new AtomicInteger(1);
        roomSet.stream()
                .map(Room::getId)
                .filter(Objects::nonNull)
                .max(Integer::compareTo)
                .ifPresent(maxId -> aId.set(maxId + 1));

        // add missing ids
        roomSet.stream()
                .filter(room -> room.getId() == null)
                .forEach(room -> room.setId(aId.getAndIncrement()));
    }

    public void recomputePortalOrder(Config config) {
        Collections.sort(config.getPortals(), getPortalComparator());
        recomputePortalOrderAccordingToSort(config);
    }

    public Config get() {
        if (config == null) {
            loadConfig();
        }
        return new Config(config);
    }

    private void recomputePortalOrderAccordingToSort(Config config) {
        for (int i = 0; i < config.getPortals().size(); i++) {
            final Portal portal = config.getPortals().get(i);
            boolean isPortalInLobby = lobbyController.isInLobby(locationTransformer.transform(portal.getWorldName(), portal.getLocation()));
            if (isPortalInLobby) {
                portal.setOrder(i + 1);
            } else {
                portal.setOrder(null);
            }
        }
    }

    private @NotNull Comparator<Portal> getPortalComparator() {
        return (p1, p2) -> {
            boolean p1IsInLobby = lobbyController.isInLobby(locationTransformer.transform(p1.getWorldName(), p1.getLocation()));
            boolean p2IsInLobby = lobbyController.isInLobby(locationTransformer.transform(p1.getWorldName(), p2.getLocation()));
            if (p1IsInLobby && !p2IsInLobby) return -1;
            if (!p1IsInLobby && p2IsInLobby) return 1;
            if (p1IsInLobby && p2IsInLobby) {
                return comparePortalInLobby(p1, p2);
            } else {
                return comparePortalOutsideLobby(p1, p2);
            }
        };
    }

    private void checkConstraints(Config config) {
        Set<String> duplicatedRoomDescriptions = PortalController.getDuplicatedRoomDescriptions(config);

        if (!duplicatedRoomDescriptions.isEmpty()) {
            // Be careful to check this uniqueness in controller. This exception is thrown in the config manager, but the problem is in the controller.
            throw new IllegalStateException("Room descriptions must be unique. Duplicated room descriptions: " + duplicatedRoomDescriptions.toString() + ". Please check this in controllers.");
        }

        Set<String> duplicatedRoomTemplateNames = RoomTemplateController.getDuplicatedRoomTemplateNames(config);

        if (!duplicatedRoomTemplateNames.isEmpty()) {
            // Be careful to check this uniqueness in controller. This exception is thrown in the config manager, but the problem is in the controller.
            throw new IllegalStateException("Room template names must be unique. Duplicated room template names: " + duplicatedRoomTemplateNames.toString() + ". Please check this in controllers.");
        }
    }

    public void reloadConfig() {
        loadConfig();
    }

    private void loadConfig() {
        if (this.config == null) {
            String configFilePath = plugin.getDataFolder().getAbsolutePath() + "/" + CONFIG_FILE_NAME;
            this.configFile = new File(configFilePath);

            if (configFile.exists()) {
                this.config = jsonManager.deserialize(configFile, Config.class);
            } else {
                this.config = new Config();
            }
            configMigrationExecutor.execute();
            saveConfig(this.config);
        }
    }
}
