package eu.lasersenigma.leplayserverutils.common.config;

import com.google.gson.FieldNamingPolicy;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.serializer.InstantSerializer;
import eu.lasersenigma.leplayserverutils.common.config.serializer.UUIDSerializer;
import eu.lasersenigma.leplayserverutils.common.config.serializer.VectorSerializer;
import fr.skytale.jsonlib.JsonManager;
import org.bukkit.util.Vector;

import java.time.Instant;
import java.util.UUID;

@Singleton
public class JsonManagerProvider implements Provider<JsonManager> {

    private JsonManager jsonManager;

    @Override
    public JsonManager get() {
        if (jsonManager == null) {
            jsonManager = new JsonManager();
            jsonManager.getGsonBuilder().setPrettyPrinting();
            jsonManager.getGsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES);
            jsonManager.registerTypeAdapter(Vector.class, new VectorSerializer());
            jsonManager.registerTypeAdapter(UUID.class, new UUIDSerializer());
            jsonManager.registerTypeAdapter(Instant.class, new InstantSerializer());
            jsonManager.enable();
        }
        return jsonManager;
    }
}
