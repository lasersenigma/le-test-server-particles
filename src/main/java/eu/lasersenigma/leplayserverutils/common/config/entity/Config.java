package eu.lasersenigma.leplayserverutils.common.config.entity;

import com.google.gson.annotations.Expose;
import eu.lasersenigma.leplayserverutils.lobby.credit.entity.CreditRoomEntity;
import eu.lasersenigma.leplayserverutils.lobby.infinitecorridor.entity.InfiniteCorridor;
import eu.lasersenigma.leplayserverutils.lobby.lobby.entity.Lobby;
import eu.lasersenigma.leplayserverutils.lobby.lobby.entity.LobbySectionTemplate;
import eu.lasersenigma.leplayserverutils.lobby.mandala.entity.MandalaRoomEntity;
import eu.lasersenigma.leplayserverutils.player.entity.LpuPlayer;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.puzzle.download.entity.SFTPAuthentication;
import eu.lasersenigma.leplayserverutils.puzzle.solution.entity.GitlabApiAuthentication;
import eu.lasersenigma.leplayserverutils.roomtemplate.entity.RoomTemplate;
import eu.lasersenigma.leplayserverutils.tag.entity.Tag;
import lombok.*;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Config {
    @Expose
    private LobbySectionTemplate lobbyTemplate = new LobbySectionTemplate();

    @Expose
    private Lobby lobby = new Lobby();

    @Expose
    private List<RoomTemplate> roomTemplates = new ArrayList<>();

    @Expose
    private Vector nextRoomMinCorner;

    @Expose
    private SFTPAuthentication sftpAuthentication = new SFTPAuthentication();

    @Expose
    private GitlabApiAuthentication gitlabApiAuthentication = new GitlabApiAuthentication();

    @Expose
    private String solutionRequestDiscordWebhookUrl = "https://discord.com/api/webhooks/CHANGE_ME";

    @Expose
    private String reviewDiscordWebhookUrl = "https://discord.com/api/webhooks/CHANGE_ME";

    @Expose
    private List<Portal> portals = new ArrayList<>();

    @Expose
    private InfiniteCorridor infiniteCorridor = new InfiniteCorridor();

    @Expose
    private CreditRoomEntity creditRoomEntity = new CreditRoomEntity();

    @Expose
    private MandalaRoomEntity mandalaRoomEntity = new MandalaRoomEntity(
            "world",
            -20, 62, 48,
            62, 118, 94,
            -18, 95, 70,
            "ACACIA_BUTTON",
            1.5, 66.5, 70.5,
            1.5, 94, 70.5,
            25,
            60,
            40);

    @Expose
    private List<LpuPlayer> players = new ArrayList<>();

    @Expose
    private List<Tag> tags = new ArrayList<>();

    @Expose
    List<Integer> colorMapsIds = List.of(7, 8, 5, 6);


    public Config(Config config) {
        this.lobbyTemplate = new LobbySectionTemplate(config.getLobbyTemplate());
        this.lobby = new Lobby(config.getLobby());
        this.roomTemplates = new ArrayList<>(config.getRoomTemplates().stream()
                .map(RoomTemplate::new)
                .toList());
        this.nextRoomMinCorner = config.getNextRoomMinCorner().clone();
        this.sftpAuthentication = new SFTPAuthentication(config.getSftpAuthentication());
        this.portals = new ArrayList<>(config.getPortals().stream()
                .map(Portal::new)
                .toList());
        this.infiniteCorridor = new InfiniteCorridor(config.getInfiniteCorridor());
        this.creditRoomEntity = new CreditRoomEntity(config.getCreditRoomEntity());
        this.mandalaRoomEntity = new MandalaRoomEntity(config.getMandalaRoomEntity());
        this.players = new ArrayList<>(config.getPlayers().stream()
                .map(LpuPlayer::new)
                .toList());
        this.gitlabApiAuthentication = new GitlabApiAuthentication(config.getGitlabApiAuthentication());
        this.solutionRequestDiscordWebhookUrl = config.getSolutionRequestDiscordWebhookUrl();
        this.reviewDiscordWebhookUrl = config.getReviewDiscordWebhookUrl();
        this.tags = new ArrayList<>(config.getTags().stream()
                .map(Tag::new)
                .toList());
        this.colorMapsIds = new ArrayList<>(config.getColorMapsIds());

    }
}
