package eu.lasersenigma.leplayserverutils.common.config.event;

import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public class ConfigUpdatedEvent extends Event {
    private static final HandlerList HANDLERS = new HandlerList();
    private Config config;

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public @NotNull HandlerList getHandlers() {
        return HANDLERS;
    }
}
