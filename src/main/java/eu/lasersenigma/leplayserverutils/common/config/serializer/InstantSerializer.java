package eu.lasersenigma.leplayserverutils.common.config.serializer;


import com.google.gson.*;
import fr.skytale.jsonlib.ISerializer;

import java.lang.reflect.Type;
import java.time.Instant;

public class InstantSerializer implements ISerializer<Instant> {
    @Override
    public Instant deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return Instant.ofEpochMilli(json.getAsLong());
    }

    @Override
    public JsonElement serialize(Instant instant, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(instant.toEpochMilli());
    }
}