package eu.lasersenigma.leplayserverutils.common.config.serializer;


import com.google.gson.*;
import fr.skytale.jsonlib.ISerializer;

import java.lang.reflect.Type;
import java.util.UUID;

public class UUIDSerializer implements ISerializer<UUID> {
    @Override
    public UUID deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String uuidStr = json.getAsString();
        return UUID.fromString(uuidStr);
    }

    @Override
    public JsonElement serialize(UUID uuid, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(uuid.toString());
    }
}