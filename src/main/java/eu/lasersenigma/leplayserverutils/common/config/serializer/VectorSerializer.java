package eu.lasersenigma.leplayserverutils.common.config.serializer;

import com.google.gson.*;
import fr.skytale.jsonlib.ISerializer;
import org.bukkit.util.Vector;

import java.lang.reflect.Type;

public class VectorSerializer implements ISerializer<Vector> {
    @Override
    public Vector deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        double x = jsonObject.get("x").getAsDouble();
        double y = jsonObject.get("y").getAsDouble();
        double z = jsonObject.get("z").getAsDouble();
        return new Vector(x, y, z);
    }

    @Override
    public JsonElement serialize(Vector src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("x", src.getX());
        jsonObject.addProperty("y", src.getY());
        jsonObject.addProperty("z", src.getZ());
        return jsonObject;
    }
}
