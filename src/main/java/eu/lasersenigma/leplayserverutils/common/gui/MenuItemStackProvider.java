package eu.lasersenigma.leplayserverutils.common.gui;

import com.google.inject.Singleton;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.leplayserverutils.puzzle.review.dto.IntegerBetweenOneAndFive;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

@Singleton
public class MenuItemStackProvider {

    public static final String CHECKMARK = "✓";

    private static final String TELEPORT_BUTTONS_NEXT_PAGE_SKULL_TEXTURE_URL_SUFFIX = "6395119dd5201a242b86b4866d6f04541b00b92ebdd57ce27919fb5f102a6ddd";
    private static final String TELEPORT_BUTTONS_PREVIOUS_PAGE_SKULL_TEXTURE_URL_SUFFIX = "afa4c82710837480df575ca0d64cef2fcdadaece7091b7076b923c67e5f4e849";


    public static final String EXIT_TEXTURE_URL = "e93b8dc93f018660aa9256b91bb770cbcbcc62ae61e7a671c75dc544569c1a78";

    public static final String BLACK_CONCRETE_5_TEXTURE_URL = "4acc402da6cc22a5c5fdde8e1b236d4757a4184731a3cddbe92e7203c334632";
    public static final String BLACK_CONCRETE_4_TEXTURE_URL = "ec17a569a212dcc2944aefde6e9f1777a1477dc8feeb522049c99d4cb51357de";
    public static final String BLACK_CONCRETE_3_TEXTURE_URL = "59ca8de3a24a9af3627d9a3d7cdf1a6cbe13b25d4afc89ec0e3955239205c55";
    public static final String BLACK_CONCRETE_2_TEXTURE_URL = "3366593dc59bf730bafdf7055cdacfed561562ae5a74f25c3ebec961ebd";
    public static final String BLACK_CONCRETE_1_TEXTURE_URL = "b451c85d13fe90cd46b66e4d39b06a829d6587e782869c86abf47c9cc5b4d";

    private static final String STAR_ACTIVATED_TEXTURE_URL = "d70b7b910a001df70a8e35ab6c33904fd29aa6814bd20c6e7fba8f59bd24892b";
    private static final String STAR_DEACTIVATED_TEXTURE_URL = "75b3cf95de18c13e09667615961cb5fa14e4a03cabf621ea3185e3453e53da35";

    private static final String DECREASE_TEXTURE_URL = "afea2d9be9a82ea8fd7efe54d69f54ac044a772e66220786f1378ab7bb0bdc6";
    private static final String INCREASE_TEXTURE_URL = "f5c140e08429309dd38f1ec7ed3d1765556e5476dc6c3fbf53e2cb2d1b15";


    @NotNull
    public ItemStack getExitItemStack() {
        final ItemStack customHead = ItemStackUtils.getCustomHead(EXIT_TEXTURE_URL);
        ItemStackUtils.setName(customHead, "Exit");
        return customHead;
    }

    @NotNull
    public ItemStack getDecreaseStarItemStack() {
        return ItemStackUtils.getCustomHead(DECREASE_TEXTURE_URL);
    }

    @NotNull
    public ItemStack getIncreaseStarItemStack() {
        return ItemStackUtils.getCustomHead(INCREASE_TEXTURE_URL);
    }

    @NotNull
    public ItemStack getValidateItemStack() {
        final ItemStack customHead = ItemsFactory.getInstance().getItemStack(Item.AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_CHECKED);
        ItemStackUtils.setName(customHead, CHECKMARK + " Validate " + CHECKMARK);
        return customHead;
    }

    @NotNull
    public ItemStack getNumberItemStackFromNote(IntegerBetweenOneAndFive note) {
        String texture = switch (note.getValue()) {
            case 5 -> BLACK_CONCRETE_5_TEXTURE_URL;
            case 4 -> BLACK_CONCRETE_4_TEXTURE_URL;
            case 3 -> BLACK_CONCRETE_3_TEXTURE_URL;
            case 2 -> BLACK_CONCRETE_2_TEXTURE_URL;
            case 1 -> BLACK_CONCRETE_1_TEXTURE_URL;
            default -> throw new IllegalArgumentException("Invalid rating value: " + note.getValue());
        };

        return ItemStackUtils.getCustomHead(texture);
    }

    @NotNull
    public ItemStack getBackgroundItemStack() {
        return new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
    }

    @NotNull
    public ItemStack getStarItemStack(boolean activated) {
        return ItemStackUtils.getCustomHead(
                activated ? STAR_ACTIVATED_TEXTURE_URL : STAR_DEACTIVATED_TEXTURE_URL
        );
    }

    @NotNull
    public ItemStack getPreviousPageItemStack() {
        final ItemStack previousPageItemStack = ItemStackUtils.getCustomHead(TELEPORT_BUTTONS_PREVIOUS_PAGE_SKULL_TEXTURE_URL_SUFFIX);
        ItemStackUtils.setName(previousPageItemStack, "Previous Page");
        ItemStackUtils.setLore(previousPageItemStack, "Go to the previous page");
        return previousPageItemStack;
    }

    @NotNull
    public ItemStack getNextPageItemStack() {
        final ItemStack nextPageItemStack = ItemStackUtils.getCustomHead(TELEPORT_BUTTONS_NEXT_PAGE_SKULL_TEXTURE_URL_SUFFIX);
        ItemStackUtils.setName(nextPageItemStack, "Next Page");
        ItemStackUtils.setLore(nextPageItemStack, "Go to the next page");
        return nextPageItemStack;
    }
}
