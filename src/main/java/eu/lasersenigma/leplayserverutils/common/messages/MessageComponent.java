package eu.lasersenigma.leplayserverutils.common.messages;

import lombok.Getter;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.Style;
import net.kyori.adventure.text.format.TextDecoration;

@Getter
public class MessageComponent {
    private final TextComponent component;

    private MessageComponent(TextComponent component) {
        this.component = component;
    }

    public static MessageComponent of(String text, MessageComponentColor style) {
        return new MessageComponent(text, style);
    }

    public static MessageComponent of(String text, MessageComponentColor style, TextDecoration... decorations) {
        return new MessageComponent(text, style, decorations);
    }

    public MessageComponent(String text, MessageComponentColor color) {
        this.component = Component.text(text).color(color.getColor());
    }

    public MessageComponent(String text, MessageComponentColor color, TextDecoration... decorations) {
        this.component = Component.text(text).style(Style.style(color.getColor(), decorations));
    }

    public MessageComponent append(MessageComponent messageComponent) {
        return new MessageComponent(this.component.append(messageComponent.getComponent()));
    }
}
