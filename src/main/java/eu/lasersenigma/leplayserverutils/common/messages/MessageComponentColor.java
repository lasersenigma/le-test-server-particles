package eu.lasersenigma.leplayserverutils.common.messages;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.kyori.adventure.text.format.TextColor;


@Getter
@RequiredArgsConstructor
public enum MessageComponentColor {
    WARNING(TextColor.color(0xFFFF00)),
    ERROR(TextColor.color(0xFF0000)),
    SUCCESS(TextColor.color(0x00FF00)),
    TITLE(TextColor.color(0xFFFFFF)),
    PARAGRAPH(TextColor.color(0xAAAAAA)),
    LINK(TextColor.color(0x0000FF)),
    HIGHLIGHT(TextColor.color(0xFF00FF)),
    HIGHLIGHT2(TextColor.color(0x00AAAA)),
    DETAILS(TextColor.color(0xAAAAAA)),
    BLACK(TextColor.color(0x000000));
    private final TextColor color;
}
