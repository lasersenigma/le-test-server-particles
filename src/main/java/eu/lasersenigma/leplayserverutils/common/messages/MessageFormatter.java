package eu.lasersenigma.leplayserverutils.common.messages;

import com.google.inject.Singleton;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentLike;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.TextDecoration;
import org.jetbrains.annotations.NotNull;

@Singleton
public class MessageFormatter {

    public ComponentLike format(MessageType style, String text) {
        return format(style, MessageComponent.of(text, style.getColor()));
    }

    public ComponentLike format(MessageType style, MessageComponent component) {
        return format(style, component.getComponent());
    }

    public ComponentLike formatWarning(String text) {
        return format(MessageType.WARNING, text);
    }

    public ComponentLike formatWarning(MessageComponent component) {
        return format(MessageType.WARNING, component);
    }

    public ComponentLike formatError(String text) {
        return format(MessageType.ERROR, text);
    }

    public ComponentLike formatError(MessageComponent component) {
        return format(MessageType.ERROR, component);
    }

    public ComponentLike formatSuccess(String text) {
        return format(MessageType.SUCCESS, text);
    }

    public ComponentLike formatSuccess(MessageComponent component) {
        return format(MessageType.SUCCESS, component);
    }

    public ComponentLike formatTitle(String text) {
        return format(MessageType.TITLE, text);
    }

    public ComponentLike formatTitle(MessageComponent component) {
        return format(MessageType.TITLE, component);
    }

    public ComponentLike formatParagraph(String text) {
        return format(MessageType.PARAGRAPH, text);
    }

    public ComponentLike formatParagraph(MessageComponent component) {
        return format(MessageType.PARAGRAPH, component);
    }

    // This method is protected because we don't want to allow people change the graphical style of the plugin messages
    protected ComponentLike format(MessageType style, Component component1) {
        return getPluginPrefix()
                .append(getIcon(style))
                .append(component1);
    }

    protected static @NotNull TextComponent getIcon(MessageType style) {
        return Component.text(style.getIcon() + " ", style.getColor().getColor(), TextDecoration.BOLD);
    }

    protected TextComponent getPluginPrefix() {
        return Component.text("[" + "LPU" + "] ", MessageComponentColor.DETAILS.getColor());
    }
}
