package eu.lasersenigma.leplayserverutils.common.messages;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.ServerOperator;

@Singleton
public class MessageSender {

    @Inject
    private MessageFormatter formatter;

    public void send(Player player, MessageType type, String content) {
        player.sendMessage(formatter.format(type, content));
    }

    public void send(Player player, MessageType type, MessageComponent component) {
        player.sendMessage(formatter.format(type, component));
    }

    public void sendError(Player player, String content) {
        player.sendMessage(formatter.formatError(content));
    }

    public void sendError(Player player, MessageComponent component) {
        player.sendMessage(formatter.formatError(component));
    }

    public void sendWarning(Player player, String content) {
        player.sendMessage(formatter.formatWarning(content));
    }

    public void sendWarning(Player player, MessageComponent component) {
        player.sendMessage(formatter.formatWarning(component));
    }

    public void sendSuccess(Player player, String content) {
        player.sendMessage(formatter.formatSuccess(content));
    }

    public void sendSuccess(Player player, MessageComponent component) {
        player.sendMessage(formatter.formatSuccess(component));
    }

    public void sendTitle(Player player, String content) {
        player.sendMessage(formatter.formatTitle(content));
    }

    public void sendTitle(Player player, MessageComponent component) {
        player.sendMessage(formatter.formatTitle(component));
    }

    public void sendParagraph(Player player, String content) {
        player.sendMessage(formatter.formatParagraph(content));
    }

    public void sendParagraph(Player player, MessageComponent component) {
        player.sendMessage(formatter.formatParagraph(component));
    }

    public void sendDebugMessageToAdmins(String content) {
        Bukkit.getServer().getOnlinePlayers().stream()
                .filter(ServerOperator::isOp)
                .forEach(player -> player.sendMessage(formatter.formatParagraph(content)));
    }

}
