package eu.lasersenigma.leplayserverutils.common.messages;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum MessageType {
    WARNING("⚠", MessageComponentColor.WARNING),
    ERROR("❌", MessageComponentColor.ERROR),
    SUCCESS("✓", MessageComponentColor.SUCCESS),
    TITLE("●", MessageComponentColor.TITLE),
    PARAGRAPH("♦", MessageComponentColor.PARAGRAPH),;

    private final String icon;
    private final MessageComponentColor color;
}
