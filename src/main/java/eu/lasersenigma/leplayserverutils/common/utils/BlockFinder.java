package eu.lasersenigma.leplayserverutils.common.utils;

import com.google.inject.Singleton;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

import java.util.HashSet;
import java.util.Set;

@Singleton
public class BlockFinder {

    public Set<Location> findBlocks(Location minCorner, Location maxCorner, Material blockType) {
        if (minCorner.getX() > maxCorner.getX() || minCorner.getY() > maxCorner.getY() || minCorner.getZ() > maxCorner.getZ()) {
            throw new IllegalArgumentException("minCorner must be smaller than maxCorner");
        }
        Set<Location> blockLocation = new HashSet<>();
        World w = minCorner.getWorld();
        for (int x = minCorner.getBlockX(); x < maxCorner.getBlockX(); x++) {
            for (int y = minCorner.getBlockY(); y < maxCorner.getBlockY(); y++) {
                for (int z = minCorner.getBlockZ(); z < maxCorner.getBlockZ(); z++) {
                    Location location = new Location(w, x, y, z);
                    if (location.getBlock().getType() == blockType) {
                        blockLocation.add(location);
                    }
                }
            }
        }
        return blockLocation;
    }
}
