package eu.lasersenigma.leplayserverutils.common.utils;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.event.AreaCreatedEvent;
import eu.lasersenigma.area.exception.AreaCrossWorldsException;
import eu.lasersenigma.area.exception.AreaNoDepthException;
import eu.lasersenigma.area.exception.AreaOverlapException;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.clipboard.ClipboardManager;
import eu.lasersenigma.component.DetectionMode;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.player.PlayerInventoryManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.Optional;

@Singleton
public class LasersEnigmaExecutor {

    @Inject
    private LasersEnigmaPlugin lasersEnigmaPlugin;

    @Inject
    private Areas areas;

    @Inject
    private ClipboardManager clipboardManager;

    public void createLEArea(Player player, Location roomMinCorner, Location roomMaxCorner, Location roomSpawnLoc) {
        if (this.lasersEnigmaPlugin == null) {
            throw new IllegalStateException("LasersEnigmaPlugin is not enabled");
        }
        Area createdArea = null;
        try {
            createdArea = areas.createArea(
                    roomMinCorner, roomMaxCorner,
                    -1,
                    true, true, true, true,
                    false, false, false, false, false,
                    roomSpawnLoc, DetectionMode.DETECTION_LASER_RECEIVERS, 1, 10);
        } catch (AreaOverlapException | AreaCrossWorldsException | AreaNoDepthException | NoAreaFoundException e) {
            throw new RuntimeException("Could not create Lasers Enigma Area", e);
        }
        AreaCreatedEvent areaCreationEvent = new AreaCreatedEvent(createdArea, player);
        Bukkit.getServer().getPluginManager().callEvent(areaCreationEvent);
    }

    public File createSchematic(Player player, Location min, Location max, String schematicName) {
        clipboardManager.copyAndSave(player, min, max, schematicName);
        File leDataFolder = lasersEnigmaPlugin.getDataFolder();
        File leSchematicFolder = new File(leDataFolder, "schematics");
        return new File(leSchematicFolder, schematicName + ".leschem");
    }

    public Optional<Integer> getNbPlayersInsideRoom(Location lasersEnigmaRoomSpawn) {
        return Optional.ofNullable(areas.getAreaFromLocation(lasersEnigmaRoomSpawn))
                .map(area -> area.getPlayersInsideArea().size());
    }

    public boolean isAtLeastOneMenuOpened(Player player) {
        final PlayerInventoryManager playerInventoryManager = LEPlayers.getInstance().findLEPlayer(player)
                .getInventoryManager();
        return playerInventoryManager.isInEditionMode() || playerInventoryManager.isRotationShortcutBarOpened();
    }
}
