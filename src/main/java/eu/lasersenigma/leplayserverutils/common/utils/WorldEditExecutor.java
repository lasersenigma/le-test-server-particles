package eu.lasersenigma.leplayserverutils.common.utils;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.clipboard.BlockArrayClipboard;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.session.ClipboardHolder;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldedit.world.block.BlockType;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

@Singleton
public class WorldEditExecutor {

    @Inject
    private LEPlayServerUtils plugin;

    @Inject
    private MessageSender msg;

    public void copy(Player player, Location minCorner, Location maxCorner, Location targetMinCornerLocation) {
        msg.sendParagraph(player, String.format(
                "Copying the area {%d,%d,%d -> %d,%d,%d} to {%d,%d,%d}.",
                minCorner.getBlockX(), minCorner.getBlockY(), minCorner.getBlockZ(),
                maxCorner.getBlockX(), maxCorner.getBlockY(), maxCorner.getBlockZ(),
                targetMinCornerLocation.getBlockX(), targetMinCornerLocation.getBlockY(), targetMinCornerLocation.getBlockZ()));
        WorldEditPlugin worldEditPlugin = getWorldEditPlugin();
        World world = BukkitAdapter.adapt(player.getWorld());
        try (EditSession editSession = worldEditPlugin.createEditSession(player)) {
            Region region = new CuboidRegion(world, BukkitAdapter.asBlockVector(minCorner), BukkitAdapter.asBlockVector(maxCorner));
            BlockArrayClipboard clipboard = new BlockArrayClipboard(region);
            clipboard.setOrigin(BukkitAdapter.asBlockVector(minCorner));
            ForwardExtentCopy copy = new ForwardExtentCopy(editSession, region, clipboard, BukkitAdapter.asBlockVector(minCorner));
            copy.setCopyingEntities(false);
            copy.setCopyingBiomes(false);
            Operations.complete(copy);
            Operation operation = new ClipboardHolder(clipboard)
                    .createPaste(editSession)
                    .copyBiomes(false)
                    .copyEntities(false)
                    .to(BukkitAdapter.asBlockVector(targetMinCornerLocation))
                    // configure here
                    .build();
            Operations.complete(operation);
        } catch (WorldEditException ex) {
            throw new RuntimeException("An error occurred while copying the area using WE API", ex);
        }
    }

    public void set(Player player, Location minCorner, Location maxCorner, Material material) {
        WorldEditPlugin worldEditPlugin = getWorldEditPlugin();
        World world = BukkitAdapter.adapt(player.getWorld());
        try (EditSession editSession = worldEditPlugin.createEditSession(player)) {
            Region region = new CuboidRegion(world, BukkitAdapter.asBlockVector(minCorner), BukkitAdapter.asBlockVector(maxCorner));
            BlockType blockType = BukkitAdapter.asBlockType(material);
            editSession.setBlocks(region, blockType);
        }
    }

    protected WorldEditPlugin getWorldEditPlugin() {
        WorldEditPlugin worldEditPlugin = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
        if (worldEditPlugin == null) {
            throw new RuntimeException("WorldEdit is not available");
        }
        return worldEditPlugin;
    }
}
