package eu.lasersenigma.leplayserverutils.common.utils;

import com.google.inject.Singleton;
import org.bukkit.Location;
import org.bukkit.util.Vector;

@Singleton
public class WorldVectorToLocationTransformer {
    public Location transform(String worldName, Vector vector) {
        return new Location(org.bukkit.Bukkit.getWorld(worldName), vector.getX(), vector.getY(), vector.getZ());
    }
}
