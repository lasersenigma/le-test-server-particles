package eu.lasersenigma.leplayserverutils.lobby.bumpers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import fr.skytale.particleanimlib.animation.animation.helix.HelixBuilder;
import fr.skytale.particleanimlib.animation.animation.helix.HelixTask;
import fr.skytale.particleanimlib.animation.attribute.ParticleTemplate;
import fr.skytale.particleanimlib.animation.attribute.pointdefinition.parent.APointDefinition;
import fr.skytale.particleanimlib.animation.attribute.position.animationposition.DirectedLocationAnimationPosition;
import fr.skytale.particleanimlib.animation.collision.CollisionBuilder;
import fr.skytale.particleanimlib.animation.collision.action.CollisionActionCallback;
import fr.skytale.particleanimlib.animation.collision.processor.ParticleCollisionProcessor;
import fr.skytale.particleanimlib.animation.collision.processor.check.EntityCollisionCheckPreset;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Vector;

@Singleton
public class BumperAnimDisplayHandler {

    public static final int BUMPER_ANIMATION_TICKS_DURATION = 20 * 3;

    @Inject
    private LEPlayServerUtils plugin;

    public HelixTask displayBumperAnim(Location bumperLocation, CollisionActionCallback<Entity, HelixTask> entityCircleTaskCollisionActionCallback) {
        HelixBuilder helixBuilder = new HelixBuilder();
        helixBuilder.setPointDefinition(new ParticleTemplate(Particle.FIREWORKS_SPARK, 1, 0, new Vector(0, 0, 0)));
        helixBuilder.setJavaPlugin(plugin);
        helixBuilder.setCentralPointDefinition((APointDefinition) null);
        helixBuilder.setHelixAngle(Math.toRadians(5.0D));
        helixBuilder.setNbHelix(3);
        helixBuilder.setNbTrailingCentralPoint(0);
        helixBuilder.setNbTrailingHelixPoint(3);
        helixBuilder.setRadius(1.5);
        helixBuilder.setTicksDuration(20 * 3);
        helixBuilder.setShowPeriod(2);
        helixBuilder.setViewers(20);
        helixBuilder.setPosition(new DirectedLocationAnimationPosition(bumperLocation, new Vector(0, 1, 0), 0.02));

        CollisionBuilder<Entity, HelixTask> collisionBuilder = new CollisionBuilder<>();
        collisionBuilder.setJavaPlugin(helixBuilder.getJavaPlugin());
        collisionBuilder.setPotentialCollidingTargetsCollector(helixTask -> {
            Location currentIterationBaseLocation = helixTask.getCurrentIterationBaseLocation();
            return currentIterationBaseLocation.getNearbyEntities(3, 3, 3);
        });
        collisionBuilder.addPotentialCollidingTargetsFilter((entity, lineTask) -> entity.getType().equals(EntityType.PLAYER));

        collisionBuilder.addCollisionProcessor(ParticleCollisionProcessor.useDefault(
                helixBuilder,
                new EntityCollisionCheckPreset<HelixTask>((location, animationTask, target) -> {
                    Location min = animationTask.getCurrentIterationBaseLocation().clone().add(new Vector(-1.5, -0.1, -1.5));
                    Location max = animationTask.getCurrentIterationBaseLocation().clone().add(new Vector(1.5, 3, 1.5));
                    return BoundingBox.of(min, max).contains(target.getLocation().toVector());
                }),
                entityCircleTaskCollisionActionCallback));
        helixBuilder.addCollisionHandler(collisionBuilder.build());
        return helixBuilder.getAnimation().show();
    }
}
