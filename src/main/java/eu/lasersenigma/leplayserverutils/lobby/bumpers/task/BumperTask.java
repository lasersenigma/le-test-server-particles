package eu.lasersenigma.leplayserverutils.lobby.bumpers.task;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.JavaPluginDisableEventHandler;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.config.event.ConfigUpdatedEvent;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import eu.lasersenigma.leplayserverutils.lobby.bumpers.BumperAnimDisplayHandler;
import fr.skytale.particleanimlib.animation.animation.helix.HelixTask;
import fr.skytale.particleanimlib.animation.parent.task.AAnimationTask;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Singleton
public class BumperTask implements JavaPluginEnableEventHandler, JavaPluginDisableEventHandler, LEPSUListener, Runnable {


    private final List<AAnimationTask<?>> tasks = new ArrayList<>();
    @Inject
    private BumperAnimDisplayHandler bumperAnimDisplayHandler;
    @Inject
    private ConfigManager configManager;
    @Inject
    private WorldVectorToLocationTransformer worldVectorToLocationTransformer;
    @Inject
    private LEPlayServerUtils plugin;
    private Integer taskId;
    private Set<Location> bumperLocations = null;

    @Override
    public void enable() {
        startTask();
    }

    @Override
    public void run() {
        startAnimTasks();
    }

    @Override
    public void disable() {
        stopTask();
    }

    @EventHandler
    public void onConfigUpdated(ConfigUpdatedEvent configUpdatedEvent) {
        stopAnimTasks();
        updateBumperData();
        startAnimTasks();
    }

    private void startTask() {
        if (taskId != null) {
            return;
        }
        taskId = Bukkit.getScheduler().runTaskTimer(
                plugin,
                this,
                0,
                BumperAnimDisplayHandler.BUMPER_ANIMATION_TICKS_DURATION
        ).getTaskId();
    }

    private void stopTask() {
        if (taskId != null) {
            Bukkit.getScheduler().cancelTask(taskId);
            taskId = null;
            stopAnimTasks();
        }
    }

    private void startAnimTasks() {
        tasks.clear();
        if (bumperLocations == null) {
            updateBumperData();
        }

        bumperLocations.forEach(bumpLocation -> {
            final HelixTask helixTask = bumperAnimDisplayHandler.displayBumperAnim(
                    bumpLocation,
                    (task, entity) -> {
                        startBump((Player) entity);
                        return 20;
                    });

            tasks.add(helixTask);
        });
    }

    private void stopAnimTasks() {
        tasks.forEach(AAnimationTask::stopAnimation);
        tasks.clear();
    }

    private void updateBumperData() {
        bumperLocations = new HashSet<>();
        final Config config = configManager.get();

        final Vector fromCorridorSectionToPreviousSection = config.getLobbyTemplate().getFromMinCornerToNewMinCorner().clone().multiply(-1);

        Location currentBumperLoc = worldVectorToLocationTransformer.transform(
                config.getLobby().getWorldName(),
                config.getLobby().getCurrentCorridorSectionMinCorner().clone()
                        .add(config.getLobbyTemplate().getFromMinCornerToBumper()).add(new Vector(0.5, 0, 0.5))
        );

        for (int i = config.getLobby().getNbCorridorAdditionalSections(); i > 0; i--) {
            bumperLocations.add(currentBumperLoc);
            currentBumperLoc = currentBumperLoc.clone().add(fromCorridorSectionToPreviousSection);
        }
    }

    private void startBump(Player player) {
        player.removePotionEffect(PotionEffectType.SPEED);
        player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 8, 3, false, false));
        player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * 3, 0, false, false));
        player.playSound(player.getLocation(), Sound.ENTITY_WITHER_SHOOT, 1, 0.4f);
        player.setVelocity(player.getLocation().getDirection().clone().add(new Vector(0, 0.8, 0)));
    }
}
