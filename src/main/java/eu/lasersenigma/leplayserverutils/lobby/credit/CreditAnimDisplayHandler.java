package eu.lasersenigma.leplayserverutils.lobby.credit;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import fr.skytale.particleanimlib.animation.animation.circuitpulse2d.CircuitPulse2DBuilder;
import fr.skytale.particleanimlib.animation.animation.circuitpulse2d.CircuitPulse2DTask;
import fr.skytale.particleanimlib.animation.animation.cuboid.CuboidBuilder;
import fr.skytale.particleanimlib.animation.animation.cuboid.CuboidTask;
import fr.skytale.particleanimlib.animation.attribute.ParticleTemplate;
import fr.skytale.particleanimlib.animation.attribute.area.Sphere;
import fr.skytale.particleanimlib.animation.attribute.pointdefinition.CallbackPointDefinition;
import fr.skytale.particleanimlib.animation.attribute.pointdefinition.ParticlePointDefinition;
import fr.skytale.particleanimlib.animation.attribute.position.animationposition.LocatedAnimationPosition;
import fr.skytale.particleanimlib.animation.attribute.spawner.SimpleSpawner;
import fr.skytale.particleanimlib.animation.attribute.var.CallbackVariable;
import fr.skytale.particleanimlib.animation.attribute.var.CallbackWithPreviousValueVariable;
import fr.skytale.particleanimlib.animation.attribute.var.Constant;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

@Singleton
public class CreditAnimDisplayHandler {
    public static final int CREDITS_ANIMATION_TICKS_DURATION = Integer.MAX_VALUE;

    private static final Location CIRCUIT_PULSE_2D_LOCATION = new Location(Bukkit.getServer().getWorld("world"), 0.5, 38.8, 48.5);

    private static final Location CUBOID_LOCATION = new Location(Bukkit.getServer().getWorld("world"), 3.5, 33, 61.5);
    private static final int CUBOID_MAX_VIEW_DISTANCE = 23;
    private final Random random = new Random(System.currentTimeMillis());

    private CircuitPulse2DBuilder circuitPulse2DBuilder;
    private CuboidBuilder cuboidBuilder;

    @Inject
    public CreditAnimDisplayHandler(LEPlayServerUtils plugin) {
        initCircuitPulse2DBuilder(plugin);
        initCuboidBuilder(plugin);
    }

    private static double computeCuboidHalfSideLengthFromNearestPlayer() {
        //The size of the cuboid will be between 0.4 and 1.3.
        //It will be 0.4 when the distance is 0 and 1.5 when the distance is 23.
        return 0.4 + 1.5 * getCuboidDistanceToNearestPlayerRatio(CUBOID_LOCATION);
    }

    private static @NotNull Double getCuboidDistanceToNearestPlayerRatio(Location baseLocation) {
        double distanceToNearestPlayer = baseLocation.getWorld().getPlayers().stream()
                .map(player -> baseLocation.distance(player.getLocation()))
                // players that are too far away are not taken into account
                .filter(distance -> distance <= CUBOID_MAX_VIEW_DISTANCE)
                // keep the minimum distance
                .min(Double::compareTo)
                .orElse((double) CUBOID_MAX_VIEW_DISTANCE);

        // distanceToNearestPlayer is between 0 and 23 (CUBOID_MAX_VIEW_DISTANCE).
        // We want to return a value between 0 and 1.
        return distanceToNearestPlayer / CUBOID_MAX_VIEW_DISTANCE;
    }

    public CircuitPulse2DTask displayCircuitPulse2DAnim(Color color) {
        circuitPulse2DBuilder.setPosition(new LocatedAnimationPosition(CIRCUIT_PULSE_2D_LOCATION));
        circuitPulse2DBuilder.setPointDefinition(new ParticleTemplate(
                Particle.REDSTONE,
                1,
                0,
                new Vector(0, 0, 0),
                color
        ));
        circuitPulse2DBuilder.setSpawner(
                new SimpleSpawner(
                        new Vector(0, 0, 0),
                        20,
                        Integer.MAX_VALUE,
                        new ParticlePointDefinition(
                                new ParticleTemplate(
                                        Particle.REDSTONE,
                                        color
                                )
                        )
                )
        );
        return circuitPulse2DBuilder.getAnimation().show();
    }

    public CuboidTask displayCuboidAnim() {
        return cuboidBuilder.getAnimation().show();
    }

    private void initCircuitPulse2DBuilder(LEPlayServerUtils plugin) {
        circuitPulse2DBuilder = new CircuitPulse2DBuilder();
        circuitPulse2DBuilder.setJavaPlugin(plugin);
        circuitPulse2DBuilder.setBoundaryArea(new Sphere(16));
        circuitPulse2DBuilder.setShowPeriod(new Constant<>(2));
        circuitPulse2DBuilder.setTicksDuration(CREDITS_ANIMATION_TICKS_DURATION);
        circuitPulse2DBuilder.setViewers(CUBOID_MAX_VIEW_DISTANCE);
    }

    private void initCuboidBuilder(LEPlayServerUtils plugin) {
        cuboidBuilder = new CuboidBuilder();
        cuboidBuilder.setJavaPlugin(plugin);
        cuboidBuilder.setPosition(new LocatedAnimationPosition(CUBOID_LOCATION.clone()));
        cuboidBuilder.setShowPeriod(new Constant<>(2));
        cuboidBuilder.setTicksDuration(CREDITS_ANIMATION_TICKS_DURATION);
        cuboidBuilder.setViewers(CUBOID_MAX_VIEW_DISTANCE);
        cuboidBuilder.setRotation(
                new CallbackWithPreviousValueVariable<>(
                        new Vector(0, 1, 0),
                        (iterationCount, previousValue) ->
                                previousValue.add(new Vector(
                                        Math.random() / 4.0, Math.random() / 4.0, Math.random() / 4.0
                                )).normalize()
                ),
                new CallbackVariable<>(i -> Math.PI * 0.01 + ((1 - getCuboidDistanceToNearestPlayerRatio(CUBOID_LOCATION)) * Math.PI / 10))
        );
        cuboidBuilder.setFromLocationToFirstCorner(new CallbackVariable<>(i -> {
            final double cuboidHalfSideLength = computeCuboidHalfSideLengthFromNearestPlayer();
            return new Vector(-cuboidHalfSideLength, -cuboidHalfSideLength, -cuboidHalfSideLength);
        }));
        cuboidBuilder.setFromLocationToSecondCorner(new CallbackVariable<>(i -> {
            final double cuboidHalfSideLength = computeCuboidHalfSideLengthFromNearestPlayer();
            return new Vector(cuboidHalfSideLength, cuboidHalfSideLength, cuboidHalfSideLength);
        }));
        cuboidBuilder.setDistanceBetweenPoints(new Constant<>(0.4));
        cuboidBuilder.setTicksDuration(CREDITS_ANIMATION_TICKS_DURATION);
        cuboidBuilder.setPointDefinition(new CallbackPointDefinition((location, aAnimation, aAnimationTask, vector, vector1) -> {

            // compute a random hue
            float hue = (random.nextFloat() - 0.5f) * 2000f;
            float saturation = 1f;
            // compute a brightness ratio based on the distance to the nearest player
            float brightness = (float) (1f - getCuboidDistanceToNearestPlayerRatio(location));

            java.awt.Color awtColor = java.awt.Color.getHSBColor(hue, saturation, brightness);
            Color color = Color.fromRGB(awtColor.getRed(), awtColor.getGreen(), awtColor.getBlue());

            ParticleTemplate particleTemplate = new ParticleTemplate(Particle.REDSTONE, color);

            particleTemplate.receivers(aAnimation.getViewers().getPlayers(location)).location(location).spawn();
        }));
        cuboidBuilder.setShowPeriod(new Constant<>(2));
    }
}
