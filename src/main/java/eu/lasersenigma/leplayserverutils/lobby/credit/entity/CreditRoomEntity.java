package eu.lasersenigma.leplayserverutils.lobby.credit.entity;

import com.google.gson.annotations.Expose;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CreditRoomEntity {

    @Expose
    private String worldName;

    @Expose
    private int minX;

    @Expose
    private int minY;

    @Expose
    private int minZ;

    @Expose
    private int maxX;

    @Expose
    private int maxY;

    @Expose
    private int maxZ;


    public CreditRoomEntity(CreditRoomEntity creditRoomEntity) {
        this.worldName = creditRoomEntity.getWorldName();
        this.minX = creditRoomEntity.getMinX();
        this.minY = creditRoomEntity.getMinY();
        this.minZ = creditRoomEntity.getMinZ();
        this.maxX = creditRoomEntity.getMaxX();
        this.maxY = creditRoomEntity.getMaxY();
        this.maxZ = creditRoomEntity.getMaxZ();
    }
}
