package eu.lasersenigma.leplayserverutils.lobby.credit.listener;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginDisableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.lobby.credit.CreditAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.credit.service.CreditsRoomService;
import eu.lasersenigma.particles.event.ParticleTryToHitBlockEvent;
import fr.skytale.particleanimlib.animation.animation.circuitpulse2d.CircuitPulse2DTask;
import fr.skytale.particleanimlib.animation.parent.task.AAnimationTask;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class CreditsParticleHitBlockListener implements LEPSUListener, JavaPluginDisableEventHandler {

    private final List<CircuitPulse2DTask> circuitPulse2DTasks = new ArrayList<>();

    @Inject
    CreditsRoomService creditsRoomService;

    @Inject
    private CreditAnimDisplayHandler creditAnimDisplayHandler;

    private Color lastColorReceived = null;

    /**
     * We intercept the particle hit block event
     * Only for the particle sent by the Laser Sender that is in credits room.
     * To ensure this, we check that:
     * - the hit block is black concrete (the ceiling of the credits room)
     * - the location of the particle is in credits room
     *
     * @param event the particle hit block event
     */
    @EventHandler
    public void onLaserParticleHitBlock(ParticleTryToHitBlockEvent event) {
        //The ceiling of the credits room is black concrete
        if (event.getBlock().getType() != Material.BLACK_CONCRETE) {
            return;
        }

        //The particle is in the credits room
        if (!creditsRoomService.isLocationInCreditsRoom(event.getLaserParticle().getLocation())) {
            return;
        }
        cleanStoppedAnimations();
        final Color color = event.getLaserParticle().getColor().getBukkitColor();

        // Only display 1 pulse per color received
        if (lastColorReceived == null || (!lastColorReceived.equals(color))) {
            circuitPulse2DTasks.add(creditAnimDisplayHandler.displayCircuitPulse2DAnim(color));
            lastColorReceived = color;
        }
    }


    @Override
    public void disable() {
        disableAnimations();
    }

    private void cleanStoppedAnimations() {
        circuitPulse2DTasks.removeIf(task -> !task.isRunning());
    }

    /**
     * Stop all animations on disable
     */
    private void disableAnimations() {
        cleanStoppedAnimations();
        circuitPulse2DTasks.forEach(AAnimationTask::stopAnimation);
        circuitPulse2DTasks.clear();
    }
}
