package eu.lasersenigma.leplayserverutils.lobby.credit.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.config.event.ConfigUpdatedEvent;
import eu.lasersenigma.leplayserverutils.lobby.credit.entity.CreditRoomEntity;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;

@Singleton
public class CreditsRoomService implements LEPSUListener {

    private Location creditsRoomMinLocation;
    private Location creditsRoomMaxLocation;

    @Inject
    public CreditsRoomService(ConfigManager configManager) {
        final Config config = configManager.get();
        onConfigUpdated(config);
    }

    @EventHandler
    public void onConfigUpdated(ConfigUpdatedEvent configUpdatedEvent) {
        onConfigUpdated(configUpdatedEvent.getConfig());
    }

    private void onConfigUpdated(Config config) {
        CreditRoomEntity creditRoom = config.getCreditRoomEntity();
        final World creditWorld = Bukkit.getServer().getWorld(creditRoom.getWorldName());
        creditsRoomMinLocation = new Location(
                creditWorld,
                creditRoom.getMinX(),
                creditRoom.getMinY(),
                creditRoom.getMinZ());
        creditsRoomMaxLocation = new Location(
                creditWorld,
                creditRoom.getMaxX(),
                creditRoom.getMaxY(),
                creditRoom.getMaxZ());
    }

    public boolean isLocationInCreditsRoom(Location location) {
        return location.getWorld().equals(creditsRoomMinLocation.getWorld())
                && location.getX() >= creditsRoomMinLocation.getX()
                && location.getX() <= creditsRoomMaxLocation.getX()
                && location.getY() >= creditsRoomMinLocation.getY()
                && location.getY() <= creditsRoomMaxLocation.getY()
                && location.getZ() >= creditsRoomMinLocation.getZ()
                && location.getZ() <= creditsRoomMaxLocation.getZ();
    }

}
