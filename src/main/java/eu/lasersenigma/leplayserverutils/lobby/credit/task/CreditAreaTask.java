package eu.lasersenigma.leplayserverutils.lobby.credit.task;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.JavaPluginDisableEventHandler;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.lobby.credit.CreditAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.credit.service.CreditsRoomService;
import fr.skytale.particleanimlib.animation.animation.cuboid.CuboidTask;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;

@Singleton
public class CreditAreaTask implements Runnable, JavaPluginEnableEventHandler, JavaPluginDisableEventHandler {

    @Inject
    public ConfigManager configManager;

    @Inject
    private LEPlayServerUtils lePlayServerUtils;

    @Inject
    private CreditAnimDisplayHandler creditAnimDisplayHandler;

    @Inject
    private CreditsRoomService creditsRoomService;

    private CuboidTask cuboidAnimationTask = null;
    private Integer taskId;
    private boolean playersInCreditsRoomPreviously;

    @Override
    public void enable() {
        startTask();
        playersInCreditsRoomPreviously = false;
    }

    @Override
    public void disable() {
        stopTask();
        disableAnimations();
    }

    @Override
    public void run() {
        final boolean playersInCreditsRoom = playersInCreditsRoom();
        if (playersInCreditsRoom && playersInCreditsRoomPreviously) {
            restartAnimationIfNeeded();
        } else if (playersInCreditsRoom) {
            displayAnimation();
        } else {
            disableAnimations();
        }
        this.playersInCreditsRoomPreviously = playersInCreditsRoom;
    }

    private void restartAnimationIfNeeded() {
        if (cuboidAnimationTask == null || (!cuboidAnimationTask.isRunning())) {
            displayAnimation();
        }
    }

    private void startTask() {
        if (taskId != null) {
            return;
        }
        taskId = Bukkit.getScheduler().runTaskTimer(
                lePlayServerUtils,
                this,
                0,
                50
        ).getTaskId();
    }

    private void stopTask() {
        if (taskId != null) {
            Bukkit.getScheduler().cancelTask(taskId);
            taskId = null;
        }
    }

    private boolean playersInCreditsRoom() {
        return Bukkit.getOnlinePlayers().stream().anyMatch(player ->
                player.getGameMode() != GameMode.SPECTATOR
                        && creditsRoomService.isLocationInCreditsRoom(player.getLocation())
        );
    }

    private void displayAnimation() {
        disableAnimations();
        cuboidAnimationTask = creditAnimDisplayHandler.displayCuboidAnim();
    }


    private void disableAnimations() {
        if (cuboidAnimationTask != null) {
            cuboidAnimationTask.stopAnimation();
            cuboidAnimationTask = null;
        }
    }
}
