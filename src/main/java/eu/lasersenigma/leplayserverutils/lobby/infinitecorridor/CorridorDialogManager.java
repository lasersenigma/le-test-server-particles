package eu.lasersenigma.leplayserverutils.lobby.infinitecorridor;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.event.ConfigUpdatedEvent;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class CorridorDialogManager implements LEPSUListener, JavaPluginEnableEventHandler {

    private Map<Integer, List<String>> dialogsPerTick;

    @Inject
    private ConfigManager configManager;

    @Inject
    private WorldVectorToLocationTransformer locationTransformer;

    @Getter
    private Location infiniteCorridorMin;

    @Getter
    private Location infiniteCorridorMax;

    @Override
    public void enable() {
        loadDialogs();
        loadArea();
    }

    @EventHandler
    public void onConfigUpdated(ConfigUpdatedEvent configUpdatedEvent) {
        loadDialogs();
        loadArea();
    }

    private void loadArea() {
        infiniteCorridorMin = locationTransformer.transform(
                configManager.get().getInfiniteCorridor().getWorldName(),
                configManager.get().getInfiniteCorridor().getMin()
        );
        infiniteCorridorMax = locationTransformer.transform(
                configManager.get().getInfiniteCorridor().getWorldName(),
                configManager.get().getInfiniteCorridor().getMax()
        );
    }

    private void loadDialogs() {
        dialogsPerTick = new HashMap<>();

        configManager.get().getInfiniteCorridor().getDialogs().forEach(dialog -> {
            final List<String> messagesForThisTick = dialogsPerTick.get(dialog.getTicksAfterEnter());
            if (messagesForThisTick == null) {
                dialogsPerTick.put(dialog.getTicksAfterEnter(), new ArrayList<>(dialog.getMessages()));
            } else {
                messagesForThisTick.addAll(dialog.getMessages());
            }
        });
    }

    public void sendDialogs(Player player, int tick) {
        List<String> dialogs = dialogsPerTick.get(tick);
        if (dialogs != null) {
            dialogs.forEach(player::sendMessage);
        }
    }

}
