package eu.lasersenigma.leplayserverutils.lobby.infinitecorridor.entity;

import com.google.gson.annotations.Expose;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Dialog {
    @Expose
    private Integer ticksAfterEnter;

    @Expose
    private List<String> messages;


    public Dialog(Dialog dialog) {
        this.ticksAfterEnter = dialog.getTicksAfterEnter();
        this.messages = new ArrayList<>(dialog.getMessages());
    }
}