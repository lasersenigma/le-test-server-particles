package eu.lasersenigma.leplayserverutils.lobby.infinitecorridor.entity;

import com.google.gson.annotations.Expose;
import lombok.*;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class InfiniteCorridor {

    @Expose
    String worldName;

    @Expose
    private Vector min;

    @Expose
    private Vector max;

    @Expose
    private List<Dialog> dialogs = new ArrayList<>();


    public InfiniteCorridor(InfiniteCorridor infiniteCorridor) {
        this.worldName = infiniteCorridor.getWorldName();
        this.min = infiniteCorridor.getMin().clone();
        this.max = infiniteCorridor.getMax().clone();
        this.dialogs = new ArrayList<>(
                infiniteCorridor.getDialogs()
                        .stream()
                        .map(Dialog::new)
                        .toList()
        );
    }
}
