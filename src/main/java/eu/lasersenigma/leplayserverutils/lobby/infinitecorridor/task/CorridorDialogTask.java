package eu.lasersenigma.leplayserverutils.lobby.infinitecorridor.task;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.JavaPluginDisableEventHandler;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.lobby.infinitecorridor.CorridorDialogManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;

@Singleton
public class CorridorDialogTask implements Runnable, JavaPluginEnableEventHandler, JavaPluginDisableEventHandler {

    private final Map<UUID, Integer> entryTickPerPlayer = new HashMap<>();
    @Inject
    private LEPlayServerUtils lePlayServerUtils;
    @Inject
    private CorridorDialogManager corridorDialogManager;
    private Integer taskId;

    @Override
    public void enable() {
        if (taskId != null) {
            return;
        }
        taskId = Bukkit.getScheduler().runTaskTimer(
                lePlayServerUtils,
                this,
                0,
                1
        ).getTaskId();
    }

    @Override
    public void disable() {
        if (taskId != null) {
            Bukkit.getScheduler().cancelTask(taskId);
            taskId = null;
        }
    }

    @Override
    public void run() {

        final List<Player> playersInArea = corridorDialogManager.getInfiniteCorridorMin()
                .getWorld()
                .getPlayers()
                .stream().filter(this::isInArea)
                .toList();

        final List<UUID> playersInAreaUUID = playersInArea.stream()
                .map(Player::getUniqueId)
                .toList();

        Iterator<Map.Entry<UUID, Integer>> it = entryTickPerPlayer.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<UUID, Integer> entry = it.next();
            final UUID playerUUID = entry.getKey();
            final Integer entryTick = entry.getValue();
            if (!playersInAreaUUID.contains(playerUUID)) {
                it.remove();
            } else {
                int ticksAfterEnter = Bukkit.getCurrentTick() - entryTick;
                corridorDialogManager.sendDialogs(Bukkit.getPlayer(playerUUID), ticksAfterEnter);
            }
        }

        playersInArea.forEach(player -> {
            final UUID playerUUID = player.getUniqueId();
            if (!entryTickPerPlayer.containsKey(playerUUID)) {
                entryTickPerPlayer.put(playerUUID, Bukkit.getCurrentTick());
            }
        });
    }

    public boolean isInArea(Player player) {
        Location playerLocation = player.getLocation();
        Location minLocation = corridorDialogManager.getInfiniteCorridorMin();
        Location maxLocation = corridorDialogManager.getInfiniteCorridorMax();
        return minLocation.getX() <= playerLocation.getX() && playerLocation.getX() <= maxLocation.getX()
                && minLocation.getY() <= playerLocation.getY() && playerLocation.getY() <= maxLocation.getY()
                && minLocation.getZ() <= playerLocation.getZ() && playerLocation.getZ() <= maxLocation.getZ();
    }

}
