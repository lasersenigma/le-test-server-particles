package eu.lasersenigma.leplayserverutils.lobby.lobby;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.common.utils.BlockFinder;
import eu.lasersenigma.leplayserverutils.common.utils.WorldEditExecutor;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.Set;
import java.util.stream.Collectors;


@Singleton
public class LobbyController {

    @Inject
    private ConfigManager configManager;

    @Inject
    private WorldEditExecutor worldEditExecutor;

    @Inject
    private LEPlayServerUtils plugin;

    @Inject
    private WorldVectorToLocationTransformer vectorToLocation;

    @Inject
    private BlockFinder blockFinder;

    @Inject
    private MessageSender msg;

    /**
     * Corresponds to command /leutils corridor increase
     * Copy/paste the corridor template to increase its length
     * Creates 4 deactivated portals
     *
     * @param player The CommandSender
     */
    public void increaseCorridor(Player player) {
        Config config = configManager.get();

        final Vector currentCorridorSectionMinCorner = config.getLobby().getCurrentCorridorSectionMinCorner();
        Vector newCorridorSectionMinCorner = currentCorridorSectionMinCorner.clone().add(config.getLobbyTemplate().getFromMinCornerToNewMinCorner());
        final Location targetMinCornerLocation = vectorToLocation.transform(
                config.getLobby().getWorldName(),
                newCorridorSectionMinCorner
        );
        Bukkit.getScheduler().runTaskAsynchronously(
                plugin,
                () -> {
                    corridorTemplateCopyPaste(config, player, targetMinCornerLocation);

                    config.getLobby().setNbCorridorAdditionalSections(config.getLobby().getNbCorridorAdditionalSections() + 1);
                    config.getLobby().setCurrentCorridorSectionMinCorner(newCorridorSectionMinCorner);

                    Set<Portal> portals = getNewPortals(config, targetMinCornerLocation);
                    portals.forEach(portal -> config.getPortals().add(portal));
                    Bukkit.getScheduler().runTask(plugin, () -> configManager.saveConfig(config));
                }
        );
    }


    /* Corresponds to command /leutils corridor decrease
     * Copy/paste the corridor template to decrease its length
     * Deletes 4 deactivated portals
     * Works only if portals are deactivated
     *
     * @param player The CommandSender
     */
    public void decreaseCorridor(Player player) {

        Config config = configManager.get();

        if (config.getLobby().getNbCorridorAdditionalSections() <= 1) {
            msg.sendError(player, "The corridor is already at its minimum length");
            return;
        }


        final Vector currentCorridorSectionMinCorner = config.getLobby().getCurrentCorridorSectionMinCorner().clone();
        final Vector minToMaxCorner = config.getLobbyTemplate().getMaxCorner().clone().subtract(config.getLobbyTemplate().getMinCorner());
        final Vector currentCorridorSectionMaxCorner = currentCorridorSectionMinCorner.clone().add(minToMaxCorner);
        final Vector newCorridorSectionMinCorner = currentCorridorSectionMinCorner.clone().subtract(config.getLobbyTemplate().getFromMinCornerToNewMinCorner());

        final Set<Portal> portalsToDelete = config.getPortals().stream()
                .filter(portal -> portal.getLocation().isInAABB(currentCorridorSectionMinCorner, currentCorridorSectionMaxCorner))
                .collect(Collectors.toSet());

        if (portalsToDelete.stream().anyMatch(Portal::isActive)) {
            msg.sendError(player, "Portals are still active, you can't decrease the corridor length");
            return;
        }

        final Location targetMinCornerLocation = vectorToLocation.transform(
                config.getLobby().getWorldName(),
                newCorridorSectionMinCorner
        );

        corridorTemplateCopyPaste(config, player, targetMinCornerLocation);

        Set<Location> portalExitLocation = blockFinder.findBlocks(
                targetMinCornerLocation.clone(),
                targetMinCornerLocation.clone().add(minToMaxCorner),
                config.getLobbyTemplate().getPortalExitLocationMaterial());

        portalExitLocation.forEach(loc -> loc.getBlock().setType(Material.AIR));

        config.getLobby().setNbCorridorAdditionalSections(config.getLobby().getNbCorridorAdditionalSections() - 1);
        config.getLobby().setCurrentCorridorSectionMinCorner(newCorridorSectionMinCorner);

        config.getPortals().removeAll(portalsToDelete);
        configManager.saveConfig(config);
    }

    private void corridorTemplateCopyPaste(Config config, Player player, Location targetMinCornerLocation) {
        worldEditExecutor.copy(
                player,
                vectorToLocation.transform(
                        config.getLobbyTemplate().getWorldName(),
                        config.getLobbyTemplate().getMinCorner()
                ),
                vectorToLocation.transform(
                        config.getLobbyTemplate().getWorldName(),
                        config.getLobbyTemplate().getMaxCorner()
                ),
                targetMinCornerLocation
        );
    }

    @NotNull
    private Set<Portal> getNewPortals(Config config, Location targetMinCornerLocation) {
        final Vector fromMinCornerToMaxCorner = config.getLobbyTemplate().getMaxCorner().clone().subtract(config.getLobbyTemplate().getMinCorner());

        Set<Location> portalLocations = blockFinder.findBlocks(
                targetMinCornerLocation,
                targetMinCornerLocation.clone().add(fromMinCornerToMaxCorner),
                config.getLobbyTemplate().getPortalMaterial());

        Integer portalExitLocationToPortalMaxDistance = config.getLobbyTemplate().getPortalExitLocationToPortalMaxDistance();

        return portalLocations.stream().map(portalLoc -> {
                    Location exitLoc = blockFinder.findBlocks(
                                    portalLoc.clone().add(-portalExitLocationToPortalMaxDistance, -portalExitLocationToPortalMaxDistance, -portalExitLocationToPortalMaxDistance),
                                    portalLoc.clone().add(portalExitLocationToPortalMaxDistance, portalExitLocationToPortalMaxDistance, portalExitLocationToPortalMaxDistance),
                                    config.getLobbyTemplate().getPortalExitLocationMaterial()
                            )
                            .stream()
                            .findAny()
                            .orElseThrow(() -> new RuntimeException("No exit location found for portal at a range <= " + portalExitLocationToPortalMaxDistance + " from " + portalLoc.toString() + " "));

                    Portal portal = new Portal(
                            -1,
                            portalLoc.getWorld().getName(),
                            portalLoc.toVector(),
                            exitLoc.toVector(),
                            null
                    );

                    Bukkit.getScheduler().runTask(plugin, () ->
                            exitLoc.getBlock().setType(Material.AIR)
                    );
                    return portal;
                })
                .collect(Collectors.toSet());
    }

    public boolean isInLobby(Location location) {
        Config config = configManager.get();
        Vector lobbyCurrentAbsoluteMinCorner = config.getLobby().getCurrentCorridorSectionMinCorner();
        Vector lobbyCurrentAbsoluteMaxCorner = config.getLobby().getCorridorMaxCorner();

        return location.getWorld().getName().equals(config.getLobby().getWorldName())
                && location.getBlockX() >= lobbyCurrentAbsoluteMinCorner.getBlockX()
                && location.getBlockX() <= lobbyCurrentAbsoluteMaxCorner.getBlockX()
                && location.getBlockY() >= lobbyCurrentAbsoluteMinCorner.getBlockY()
                && location.getBlockY() <= lobbyCurrentAbsoluteMaxCorner.getBlockY()
                && location.getBlockZ() >= lobbyCurrentAbsoluteMinCorner.getBlockZ()
                && location.getBlockZ() <= lobbyCurrentAbsoluteMaxCorner.getBlockZ();
    }

    public void teleportToTemplate(Player player) {
        Config config = configManager.get();
        final Location corridorTemplateCenter = vectorToLocation.transform(
                config.getLobbyTemplate().getWorldName(),
                config.getLobbyTemplate().getMinCorner().clone().add(config.getLobbyTemplate().getMaxCorner()).multiply(0.5)
        );
        player.teleport(corridorTemplateCenter);
    }
}
