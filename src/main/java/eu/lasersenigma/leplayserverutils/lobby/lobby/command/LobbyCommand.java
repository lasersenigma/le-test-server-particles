package eu.lasersenigma.leplayserverutils.lobby.lobby.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.provider.LPUCommandsProvider;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class LobbyCommand extends Command<Player> implements JavaPluginEnableEventHandler {

    private final Commands leplayserverutilsCommands;

    @Inject
    public LobbyCommand(
            LPUCommandsProvider lpuCommandsProvider,
            LobbyIncreaseCommand lobbyIncreaseCommand,
            LobbyDecreaseCommand lobbyDecreaseCommand,
            LobbyTeleportToTemplateCommand lobbyTeleportToTemplateCommand) {
        super(Player.class, "lobby");
        this.setPermission(LPUPermission.ADMIN.getPermission());
        super.registerSubCommand(lobbyIncreaseCommand);
        super.registerSubCommand(lobbyDecreaseCommand);
        super.registerSubCommand(lobbyTeleportToTemplateCommand);
        this.leplayserverutilsCommands = lpuCommandsProvider.get();
    }

    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        leplayserverutilsCommands.registerCommand(this);
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return super.defaultProcessForContainer(commands, player, args);
    }

    @Override
    protected String description(Player executor) {
        return "increase or decrease the length of the corridor";
    }

}
