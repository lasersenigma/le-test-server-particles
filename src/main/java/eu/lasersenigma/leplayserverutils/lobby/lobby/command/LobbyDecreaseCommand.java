package eu.lasersenigma.leplayserverutils.lobby.lobby.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.lobby.lobby.LobbyController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class LobbyDecreaseCommand extends Command<Player> {

    private final LobbyController lobbyController;

    @Inject
    public LobbyDecreaseCommand(LobbyController lobbyController) {
        super(Player.class, "decrease");
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.lobbyController = lobbyController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        lobbyController.decreaseCorridor(player);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Decrease the length of the corridor";
    }
}
