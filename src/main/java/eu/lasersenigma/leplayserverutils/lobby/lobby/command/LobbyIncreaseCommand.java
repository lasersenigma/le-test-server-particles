package eu.lasersenigma.leplayserverutils.lobby.lobby.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.lobby.lobby.LobbyController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class LobbyIncreaseCommand extends Command<Player> {

    private final LobbyController lobbyController;

    @Inject
    public LobbyIncreaseCommand(LobbyController lobbyController) {
        super(Player.class, "increase");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.lobbyController = lobbyController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        lobbyController.increaseCorridor(player);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Increase the length of the corridor";
    }
}
