package eu.lasersenigma.leplayserverutils.lobby.lobby.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.lobby.lobby.LobbyController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class LobbyTeleportToTemplateCommand extends Command<Player> {

    private final LobbyController lobbyController;

    @Inject
    public LobbyTeleportToTemplateCommand(LobbyController lobbyController) {
        super(Player.class, "teleportToTemplate");
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.lobbyController = lobbyController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        lobbyController.teleportToTemplate(player);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Teleport to the lobby corridor section template";
    }
}
