package eu.lasersenigma.leplayserverutils.lobby.lobby.entity;

import com.google.gson.annotations.Expose;
import lombok.*;
import org.bukkit.util.Vector;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Lobby {
    @Expose
    private String worldName;

    @Expose
    private Vector currentCorridorSectionMinCorner;

    @Expose
    private Integer nbCorridorAdditionalSections;

    @Expose
    private Vector corridorMaxCorner;

    public Lobby(Lobby lobby) {
        this.worldName = lobby.getWorldName();
        this.currentCorridorSectionMinCorner = lobby.getCurrentCorridorSectionMinCorner().clone();
        this.nbCorridorAdditionalSections = lobby.getNbCorridorAdditionalSections();
        this.corridorMaxCorner = lobby.getCorridorMaxCorner().clone();
    }
}
