package eu.lasersenigma.leplayserverutils.lobby.lobby.entity;

import com.google.gson.annotations.Expose;
import lombok.*;
import org.bukkit.Material;
import org.bukkit.util.Vector;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class LobbySectionTemplate {
    @Expose
    private String worldName;

    @Expose
    private Vector minCorner;

    @Expose
    private Vector fromMinCornerToNewMinCorner;

    @Expose
    private Vector maxCorner;

    @Expose
    private Vector fromMinCornerToBumper;

    @Expose
    private Material portalMaterial = Material.SCULK_SHRIEKER;

    @Expose
    private Material portalExitLocationMaterial = Material.GLOWSTONE;

    @Expose
    private Integer portalExitLocationToPortalMaxDistance = 4;


    public LobbySectionTemplate(LobbySectionTemplate lobbySectionTemplate) {
        this.worldName = lobbySectionTemplate.getWorldName();
        this.minCorner = lobbySectionTemplate.getMinCorner().clone();
        this.fromMinCornerToNewMinCorner = lobbySectionTemplate.getFromMinCornerToNewMinCorner().clone();
        this.maxCorner = lobbySectionTemplate.getMaxCorner().clone();
        this.fromMinCornerToBumper = lobbySectionTemplate.getFromMinCornerToBumper().clone();
        this.portalMaterial = lobbySectionTemplate.getPortalMaterial();
        this.portalExitLocationMaterial = lobbySectionTemplate.getPortalExitLocationMaterial();
        this.portalExitLocationToPortalMaxDistance = lobbySectionTemplate.getPortalExitLocationToPortalMaxDistance();
    }
}
