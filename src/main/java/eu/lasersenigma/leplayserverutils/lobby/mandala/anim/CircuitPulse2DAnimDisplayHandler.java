package eu.lasersenigma.leplayserverutils.lobby.mandala.anim;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.parent.MandalaRoomPlayerAimAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.mandala.dto.MandalaRoomDto;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomService;
import fr.skytale.particleanimlib.animation.animation.circuitpulse2d.CircuitPulse2DBuilder;
import fr.skytale.particleanimlib.animation.attribute.ParticleTemplate;
import fr.skytale.particleanimlib.animation.attribute.area.Sphere;
import fr.skytale.particleanimlib.animation.attribute.pointdefinition.ParticlePointDefinition;
import fr.skytale.particleanimlib.animation.attribute.pointdefinition.parent.APointDefinition;
import fr.skytale.particleanimlib.animation.attribute.position.animationposition.LocatedAnimationPosition;
import fr.skytale.particleanimlib.animation.attribute.spawner.SimpleSpawner;
import fr.skytale.particleanimlib.animation.attribute.var.CallbackVariable;
import fr.skytale.particleanimlib.animation.attribute.var.Constant;
import fr.skytale.particleanimlib.animation.attribute.var.parent.ParametrizedCallback;
import fr.skytale.particleanimlib.animation.parent.task.AAnimationTask;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

@Singleton
public class CircuitPulse2DAnimDisplayHandler extends MandalaRoomPlayerAimAnimDisplayHandler {
    private static final int PULSE_PERIOD = 40;
    private static final int NB_PARTICLE_TO_SPAWN = 20;
    private static final float BRIGHNESS = 1f;
    private static final float SATURATION = 1f;

    private final CircuitPulse2DBuilder circuitPulse2DBuilder;

    @Inject
    public CircuitPulse2DAnimDisplayHandler(LEPlayServerUtils plugin, MandalaRoomService mandalaRoomService) {
        super(plugin, mandalaRoomService);
        this.circuitPulse2DBuilder = initCircuitPulse2DBuilder();
    }

    @Override
    public @NotNull AAnimationTask<?> displayAnimation() {
        return circuitPulse2DBuilder.getAnimation().show();
    }

    protected @NotNull CircuitPulse2DBuilder initCircuitPulse2DBuilder() {
        MandalaRoomDto mandalaRoomDto = mandalaRoomService.getMandalaRoomDto();
        CircuitPulse2DBuilder builder = new CircuitPulse2DBuilder();
        builder.setJavaPlugin(plugin);
        builder.setPosition(new LocatedAnimationPosition(mandalaRoomDto.getAnimationPosition().clone()));
        builder.setBoundaryArea(new Sphere((float) mandalaRoomDto.getAnimationMaxViewDistance()));
        builder.setShowPeriod(new Constant<>(2));
        builder.setTicksDuration(MANDALA_ANIMATION_TICKS_DURATION);
        builder.setViewers(mandalaRoomDto.getAnimationMaxViewDistance());

        final ParametrizedCallback<Vector> playerAimLocationSupplier = iterationCount -> {
            final Vector2D fromAnimationCenterToPlayerAim = getWhereNearestPlayerAims(mandalaRoomDto)
                    .orElse(new Vector2D(0, 0));

            return new Vector(
                    fromAnimationCenterToPlayerAim.getX(),
                    fromAnimationCenterToPlayerAim.getY(),
                    0);
        };
        final ParametrizedCallback<APointDefinition> coloredParticleSupplier = iterationCount -> {
            float hue = getRandomHue();
            java.awt.Color awtColor = java.awt.Color.getHSBColor(hue, SATURATION, BRIGHNESS);
            Color color = Color.fromRGB(awtColor.getRed(), awtColor.getGreen(), awtColor.getBlue());
            return new ParticlePointDefinition(new ParticleTemplate(Particle.REDSTONE, color));
        };
        builder.setSpawner(new SimpleSpawner(
                new CallbackVariable<>(playerAimLocationSupplier),
                new Constant<>(NB_PARTICLE_TO_SPAWN),
                new Constant<>(PULSE_PERIOD),
                new CallbackVariable<>(coloredParticleSupplier)));

        return builder;
    }

    private float getRandomHue() {
        return (random.nextFloat() - 0.5f) * 2000f;
    }
}
