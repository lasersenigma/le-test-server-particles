package eu.lasersenigma.leplayserverutils.lobby.mandala.anim;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.parent.MandalaRoomPlayerAimAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.mandala.dto.MandalaRoomDto;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomService;
import fr.skytale.particleanimlib.animation.animation.image.ImageBuilder;
import fr.skytale.particleanimlib.animation.animation.image.preset.init.ImagePresetInitializer;
import fr.skytale.particleanimlib.animation.attribute.position.animationposition.LocatedRelativeAnimationPosition;
import fr.skytale.particleanimlib.animation.attribute.var.CallbackVariable;
import fr.skytale.particleanimlib.animation.parent.task.AAnimationTask;
import org.bukkit.Location;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@Singleton
public class LogoAnimDisplayHandler extends MandalaRoomPlayerAimAnimDisplayHandler implements JavaPluginEnableEventHandler {

    private ImageBuilder imageBuilder;

    @Override
    public AAnimationTask<?> displayAnimation() {
        if (imageBuilder == null) {
            throw new IllegalStateException("ImageBuilder not initialized");
        }
        return imageBuilder.getAnimation().show();
    }

    @Inject
    public LogoAnimDisplayHandler(LEPlayServerUtils plugin, MandalaRoomService mandalaRoomService) {
        super(plugin, mandalaRoomService);
    }

    private ImageBuilder initImageBuilder() {
        final MandalaRoomDto mandalaRoomDto = mandalaRoomService.getMandalaRoomDto();
        final Location animationPosition = mandalaRoomDto.getAnimationPosition();

        ImageBuilder builder = new ImageBuilder();
        builder.setJavaPlugin(plugin);
        builder.setPosition(new LocatedRelativeAnimationPosition(
                animationPosition,
                new CallbackVariable<>(iterationCount -> new Vector(0, getYOffset(mandalaRoomDto), 0))
        ));
        builder.setTicksDuration(MANDALA_ANIMATION_TICKS_DURATION);
        builder.setViewers(mandalaRoomDto.getAnimationMaxViewDistance());
        builder.setShowPeriod(3);
        builder.setImageFileName("lasers-enigma-logo.png");
        builder.setRotation(
                new Vector(0, 1, 0),
                new CallbackVariable<>(iterationCount -> getRotationAngle(mandalaRoomDto))
        );
        return builder;
    }

    private Double getRotationAngle(MandalaRoomDto mandalaRoomDto) {
        return getMandalaRoomCenterPlayerDistanceRatio(mandalaRoomDto)
                .map(ratio -> ratio * Math.PI/8)
                .orElse(0d);
    }


    private double getYOffset(MandalaRoomDto mandalaRoomDto) {
        return getMandalaRoomCenterPlayerDistanceRatio(mandalaRoomDto)
                .map(ratio -> ratio * 20) // 0 -> 10
                .orElse(0d);
    }

    private @NotNull Optional<@NotNull Double> getMandalaRoomCenterPlayerDistanceRatio(MandalaRoomDto mandalaRoomDto) {
        return getNearestPlayerToMandalaRoomCenter(mandalaRoomDto)
                .map(player -> getMandalaRoomCenterPlayerDistanceRatio(player, mandalaRoomDto));
    }

    @Override
    public void enable() {
        ImagePresetInitializer.initialize(ImagePresetInitializer.class, plugin);
        this.imageBuilder = initImageBuilder();
    }
}
