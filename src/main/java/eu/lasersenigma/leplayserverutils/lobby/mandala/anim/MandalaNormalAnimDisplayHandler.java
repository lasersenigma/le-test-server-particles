package eu.lasersenigma.leplayserverutils.lobby.mandala.anim;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.parent.MandalaRoomPlayerAimAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.mandala.dto.MandalaRoomDto;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomService;
import fr.skytale.particleanimlib.animation.animation.mandala.Mandala2DBuilder;
import fr.skytale.particleanimlib.animation.attribute.ParticleTemplate;
import fr.skytale.particleanimlib.animation.attribute.pointdefinition.CallbackPointDefinition;
import fr.skytale.particleanimlib.animation.attribute.position.animationposition.LocatedAnimationPosition;
import fr.skytale.particleanimlib.animation.attribute.var.CallbackVariable;
import fr.skytale.particleanimlib.animation.attribute.var.Constant;
import fr.skytale.particleanimlib.animation.parent.task.AAnimationTask;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class MandalaNormalAnimDisplayHandler extends MandalaRoomPlayerAimAnimDisplayHandler {
    private final Mandala2DBuilder mandalaBuilder;

    @Inject
    public MandalaNormalAnimDisplayHandler(LEPlayServerUtils plugin, MandalaRoomService mandalaRoomService) {
        super(plugin, mandalaRoomService);
        this.mandalaBuilder = initMandalaBuilder();
    }

    @Override
    public @NotNull AAnimationTask<?> displayAnimation() {
        return mandalaBuilder.getAnimation().show();
    }

    protected @NotNull Mandala2DBuilder initMandalaBuilder() {
        MandalaRoomDto mandalaRoomDto = mandalaRoomService.getMandalaRoomDto();

        Mandala2DBuilder newMandalaBuilder = new Mandala2DBuilder();
        newMandalaBuilder.setJavaPlugin(plugin);
        newMandalaBuilder.setPosition(new LocatedAnimationPosition(mandalaRoomDto.getAnimationPosition().clone()));
        newMandalaBuilder.setTicksDuration(MANDALA_ANIMATION_TICKS_DURATION);
        newMandalaBuilder.setViewers(mandalaRoomDto.getAnimationMaxViewDistance());
        newMandalaBuilder.setPointDefinition(new CallbackPointDefinition((location, aAnimation, aAnimationTask, vector, vector1) -> {
            final Color color = computeColorFromPlayerDistanceToMandalaRoomCenter(mandalaRoomDto);

            ParticleTemplate particleTemplate = new ParticleTemplate(Particle.REDSTONE, color);

            particleTemplate.receivers(aAnimation.getViewers().getPlayers(location)).location(location).spawn();
        }));
        newMandalaBuilder.setShowPeriod(new Constant<>(2));
        newMandalaBuilder.setNbCircleSection(10);

        //because it is outside the callback, previous points will be shared over multiple iterations
        final List<Vector2D> previousPoints = new ArrayList<>();

        newMandalaBuilder.setPoints(new CallbackVariable<>(iterationCount -> {
            getWhereNearestPlayerAims(mandalaRoomDto)
                    .ifPresent(fromOriginToPlayerTargetPoint -> {
                        previousPoints.add(fromOriginToPlayerTargetPoint);
                        if (previousPoints.size() > mandalaRoomDto.getAnimationNbSimultaneousPoints()) {
                            previousPoints.remove(0);
                        }
                    });
            return new ArrayList<>(previousPoints);
        }));
        return newMandalaBuilder;
    }
}
