package eu.lasersenigma.leplayserverutils.lobby.mandala.anim;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomService;
import fr.skytale.particleanimlib.animation.animation.mandala.Mandala2DBuilder;
import org.jetbrains.annotations.NotNull;

@Singleton
public class MandalaSymmetricAnimDisplayHandler extends MandalaNormalAnimDisplayHandler {
    public static final int MANDALA_ANIMATION_TICKS_DURATION = Integer.MAX_VALUE;

    @Inject
    public MandalaSymmetricAnimDisplayHandler(LEPlayServerUtils plugin, MandalaRoomService mandalaRoomService) {
        super(plugin, mandalaRoomService);
    }


    @Override
    protected @NotNull Mandala2DBuilder initMandalaBuilder() {
        Mandala2DBuilder newMandalaBuilder = super.initMandalaBuilder();
        newMandalaBuilder.setAxialSymmetryToHalf(true);
        return newMandalaBuilder;
    }
}
