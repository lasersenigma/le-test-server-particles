package eu.lasersenigma.leplayserverutils.lobby.mandala.anim.parent;

import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.lobby.mandala.dto.MandalaRoomDto;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomService;
import fr.skytale.particleanimlib.animation.parent.task.AAnimationTask;

public abstract class MandalaRoomAnimDisplayHandler {
    public static final int MANDALA_ANIMATION_TICKS_DURATION = Integer.MAX_VALUE;

    protected final LEPlayServerUtils plugin;
    protected final MandalaRoomService mandalaRoomService;

    public abstract AAnimationTask<?> displayAnimation();

    protected MandalaRoomAnimDisplayHandler(LEPlayServerUtils plugin, MandalaRoomService mandalaRoomService) {
        this.plugin = plugin;
        this.mandalaRoomService = mandalaRoomService;
    }

}
