package eu.lasersenigma.leplayserverutils.lobby.mandala.anim.parent;

import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.lobby.mandala.dto.MandalaRoomDto;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomService;
import org.apache.commons.math3.geometry.euclidean.threed.Line;
import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.Optional;
import java.util.Random;

public abstract class MandalaRoomPlayerAimAnimDisplayHandler extends MandalaRoomAnimDisplayHandler {

    protected final Vector3D mandalaCenterVector;
    protected final Plane mandalaPlane;
    protected final Random random;

    protected MandalaRoomPlayerAimAnimDisplayHandler(LEPlayServerUtils plugin, MandalaRoomService mandalaRoomService) {
        super(plugin, mandalaRoomService);
        this.random = new Random(System.currentTimeMillis());
        MandalaRoomDto mandalaRoomDto = mandalaRoomService.getMandalaRoomDto();
        this.mandalaCenterVector = new Vector3D(
                mandalaRoomDto.getAnimationPosition().getX(),
                mandalaRoomDto.getAnimationPosition().getY(),
                mandalaRoomDto.getAnimationPosition().getZ()
        );
        this.mandalaPlane = new Plane(
                mandalaCenterVector,
                new Vector3D(0, 1, 0),
                0.1
        );
    }

    protected @NotNull Color computeColorFromPlayerDistanceToMandalaRoomCenter(MandalaRoomDto mandalaRoomDto) {
        // compute a random hue
        float hue = (random.nextFloat() - 0.5f) * 2000f;
        float saturation = 1f;
        // compute a brightness ratio based on the distance from the mandala room center to the nearest player
        final float brightness = computeBrightnessFromPlayerDistanceToMandalaRoomCenter(mandalaRoomDto);

        java.awt.Color awtColor = java.awt.Color.getHSBColor(hue, saturation, brightness);
        return Color.fromRGB(awtColor.getRed(), awtColor.getGreen(), awtColor.getBlue());
    }

    protected float computeBrightnessFromPlayerDistanceToMandalaRoomCenter(MandalaRoomDto mandalaRoomDto) {
        final Optional<Player> nearestPlayerToMandalaRoomCenter = getNearestPlayerToMandalaRoomCenter(mandalaRoomDto);
        return (float) (1f - getMandalaRoomCenterPlayerDistanceRatio(nearestPlayerToMandalaRoomCenter.orElse(null), mandalaRoomDto));
    }

    protected @NotNull Optional<Vector2D> getWhereNearestPlayerAims(MandalaRoomDto mandalaRoomDto) {
        return getNearestPlayerToMandalaRoomCenter(mandalaRoomDto)
                .map(player -> findWherePlayerAimsInMandalaPlane(player, mandalaRoomDto))
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    protected @NotNull Double getMandalaRoomCenterPlayerDistanceRatio(Player player, MandalaRoomDto mandalaRoomDto) {
        if (player == null) {
            return 1.0;
        } else {
            return player.getLocation().distance(mandalaRoomDto.getPlayerRoomCenter()) / mandalaRoomDto.getPlayerMaxDistanceToPlayerRoomCenter();
        }
    }

    protected @NotNull Optional<Player> getNearestPlayerToMandalaRoomCenter(MandalaRoomDto mandalaRoomDto) {
        return mandalaRoomDto.getPlayerRoomCenter().getWorld().getPlayers().stream()
                .filter(player -> player.getGameMode() != GameMode.SPECTATOR)
                .filter(player -> mandalaRoomService.isLocationInMandalaRoom(player.getLocation()))
                .min(Comparator.comparingDouble(player -> mandalaRoomDto.getPlayerRoomCenter().distance(player.getLocation())));
    }

    protected @NotNull Optional<Vector2D> findWherePlayerAimsInMandalaPlane(Player player, MandalaRoomDto mandalaRoomDto) {
        final Line line = computePlayerLineOfSight(player);

        return Optional.ofNullable(mandalaPlane.intersection(line))
                .map(intersection -> new Vector2D(
                        intersection.getX() - mandalaCenterVector.getX(),
                        intersection.getZ() - mandalaCenterVector.getZ()
                ))
                .filter(intersection -> intersection.getNorm() < mandalaRoomDto.getPlayerMaxDistanceToPlayerRoomCenter() * 2);
    }

    protected @NotNull Line computePlayerLineOfSight(@NotNull Player player) {
        Vector3D lineFirstPoint = new Vector3D(
                player.getEyeLocation().getX(),
                player.getEyeLocation().getY(),
                player.getEyeLocation().getZ());
        Vector3D lineSecondPoint = new Vector3D(
                lineFirstPoint.getX(),
                lineFirstPoint.getY(),
                lineFirstPoint.getZ())
                .add(new Vector3D(
                        player.getLocation().getDirection().getX(),
                        player.getLocation().getDirection().getY(),
                        player.getLocation().getDirection().getZ()));

        return new Line(
                lineFirstPoint,
                lineSecondPoint,
                0.1
        );
    }
}
