package eu.lasersenigma.leplayserverutils.lobby.mandala.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.Material;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MandalaRoomDto {
    private Location mandalaRoomMinLocation;
    private Location mandalaRoomMaxLocation;

    private Location buttonLocation;
    private Material buttonMaterial;

    private Location animationPosition;

    private Location playerRoomCenter;

    private double playerMaxDistanceToPlayerRoomCenter;

    private double animationMaxViewDistance;

    private double animationNbSimultaneousPoints;


    public MandalaRoomDto(MandalaRoomDto mandalaRoomDto) {
        this.mandalaRoomMinLocation = mandalaRoomDto.getMandalaRoomMinLocation().clone();
        this.mandalaRoomMaxLocation = mandalaRoomDto.getMandalaRoomMaxLocation().clone();
        this.buttonLocation = mandalaRoomDto.getButtonLocation().clone();
        this.buttonMaterial = mandalaRoomDto.getButtonMaterial();
        this.animationPosition = mandalaRoomDto.getAnimationPosition().clone();
        this.playerRoomCenter = mandalaRoomDto.getPlayerRoomCenter().clone();
        this.playerMaxDistanceToPlayerRoomCenter = mandalaRoomDto.getPlayerMaxDistanceToPlayerRoomCenter();
        this.animationMaxViewDistance = mandalaRoomDto.getAnimationMaxViewDistance();
        this.animationNbSimultaneousPoints = mandalaRoomDto.getAnimationNbSimultaneousPoints();
    }

    public MandalaRoomDto copy() {
        return new MandalaRoomDto(this);
    }
}
