package eu.lasersenigma.leplayserverutils.lobby.mandala.entity;

import com.google.gson.annotations.Expose;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class MandalaRoomEntity {

    @Expose
    private String worldName;

    @Expose
    private int minX;

    @Expose
    private int minY;

    @Expose
    private int minZ;

    @Expose
    private int maxX;

    @Expose
    private int maxY;

    @Expose
    private int maxZ;

    @Expose
    private int buttonX = -18;

    @Expose
    private int buttonY = 95;

    @Expose
    private int buttonZ = 70;

    @Expose
    private String buttonMaterial = "ACACIA_BUTTON";

    @Expose
    private double animationPositionX = 1.5;

    @Expose
    private double animationPositionY = 66.5;

    @Expose
    private double animationPositionZ = 70.5;

    @Expose
    private double playerRoomCenterX = 1.5;

    @Expose
    private double playerRoomCenterY = 94;

    @Expose
    private double playerRoomCenterZ = 70.5;

    @Expose
    private double playerMaxDistanceToPlayerRoomCenter = 25;

    @Expose
    private double animationMaxViewDistance = 60;

    @Expose
    private int animationNbSimultaneousPoints = 40;


    public MandalaRoomEntity(MandalaRoomEntity mandalaRoomEntity) {
        this.worldName = mandalaRoomEntity.getWorldName();
        this.minX = mandalaRoomEntity.getMinX();
        this.minY = mandalaRoomEntity.getMinY();
        this.minZ = mandalaRoomEntity.getMinZ();
        this.maxX = mandalaRoomEntity.getMaxX();
        this.maxY = mandalaRoomEntity.getMaxY();
        this.maxZ = mandalaRoomEntity.getMaxZ();
        this.buttonX = mandalaRoomEntity.getButtonX();
        this.buttonY = mandalaRoomEntity.getButtonY();
        this.buttonZ = mandalaRoomEntity.getButtonZ();
        this.buttonMaterial = mandalaRoomEntity.getButtonMaterial();
        this.animationPositionX = mandalaRoomEntity.getAnimationPositionX();
        this.animationPositionY = mandalaRoomEntity.getAnimationPositionY();
        this.animationPositionZ = mandalaRoomEntity.getAnimationPositionZ();
        this.playerRoomCenterX = mandalaRoomEntity.getPlayerRoomCenterX();
        this.playerRoomCenterY = mandalaRoomEntity.getPlayerRoomCenterY();
        this.playerRoomCenterZ = mandalaRoomEntity.getPlayerRoomCenterZ();
        this.playerMaxDistanceToPlayerRoomCenter = mandalaRoomEntity.getPlayerMaxDistanceToPlayerRoomCenter();
        this.animationMaxViewDistance = mandalaRoomEntity.getAnimationMaxViewDistance();
        this.animationNbSimultaneousPoints = mandalaRoomEntity.getAnimationNbSimultaneousPoints();
    }
}
