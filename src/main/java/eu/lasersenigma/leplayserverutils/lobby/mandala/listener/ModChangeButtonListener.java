package eu.lasersenigma.leplayserverutils.lobby.mandala.listener;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomModService;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomService;
import eu.lasersenigma.leplayserverutils.lobby.mandala.task.MandalaAreaTask;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Objects;

@Singleton
public class ModChangeButtonListener implements LEPSUListener {

    @Inject
    private MandalaAreaTask mandalaAreaTask;

    @Inject
    private MandalaRoomModService mandalaRoomModService;

    @Inject
    private MandalaRoomService mandalaRoomService;

    @Inject
    private MessageSender msg;

    @EventHandler
    public void onButtonPress(PlayerInteractEvent e) {
        if (e.getAction() != org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        final Block clickedBlock = e.getClickedBlock();
        if (clickedBlock == null || !Objects.equals(clickedBlock.getType(), getButtonType())) {
            return;
        }
        if (!clickedBlock.getLocation().equals(getButtonLocation())) {
            return;
        }
        mandalaRoomModService.nextMod();
        mandalaAreaTask.restartAnimationForce();
        msg.sendSuccess(e.getPlayer(), "Mod changed to " + mandalaRoomModService.getCurrentMod().getDisplayName());
    }

    private Material getButtonType() {
        return mandalaRoomService.getMandalaRoomDto().getButtonMaterial();
    }

    private Location getButtonLocation() {
        return mandalaRoomService.getMandalaRoomDto().getButtonLocation();
    }

}
