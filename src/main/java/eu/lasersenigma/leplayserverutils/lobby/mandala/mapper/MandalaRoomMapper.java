package eu.lasersenigma.leplayserverutils.lobby.mandala.mapper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import eu.lasersenigma.leplayserverutils.lobby.mandala.dto.MandalaRoomDto;
import eu.lasersenigma.leplayserverutils.lobby.mandala.entity.MandalaRoomEntity;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

@Singleton
public class MandalaRoomMapper {

    @Inject
    private WorldVectorToLocationTransformer worldVectorToLocationTransformer;

    public MandalaRoomDto toDto(MandalaRoomEntity mandalaRoomEntity) {
        MandalaRoomDto mandalaRoomDto = new MandalaRoomDto();
        mandalaRoomDto.setMandalaRoomMinLocation(toMinLocation(mandalaRoomEntity));
        mandalaRoomDto.setMandalaRoomMaxLocation(toMaxLocation(mandalaRoomEntity));
        mandalaRoomDto.setButtonLocation(toButtonLocation(mandalaRoomEntity));
        mandalaRoomDto.setButtonMaterial(toButtonMaterial(mandalaRoomEntity));
        mandalaRoomDto.setAnimationPosition(toAnimationPosition(mandalaRoomEntity));
        mandalaRoomDto.setPlayerRoomCenter(toPlayerRoomCenter(mandalaRoomEntity));
        mandalaRoomDto.setPlayerMaxDistanceToPlayerRoomCenter(mandalaRoomEntity.getPlayerMaxDistanceToPlayerRoomCenter());
        mandalaRoomDto.setAnimationMaxViewDistance(mandalaRoomEntity.getAnimationMaxViewDistance());
        mandalaRoomDto.setAnimationNbSimultaneousPoints(mandalaRoomEntity.getAnimationNbSimultaneousPoints());
        return mandalaRoomDto;
    }

    private Location toMinLocation(MandalaRoomEntity mandalaRoomEntity) {
        return worldVectorToLocationTransformer.transform(
                mandalaRoomEntity.getWorldName(),
                new Vector(mandalaRoomEntity.getMinX(), mandalaRoomEntity.getMinY(), mandalaRoomEntity.getMinZ())
        );
    }

    private Location toMaxLocation(MandalaRoomEntity mandalaRoomEntity) {
        return worldVectorToLocationTransformer.transform(
                mandalaRoomEntity.getWorldName(),
                new Vector(mandalaRoomEntity.getMaxX(), mandalaRoomEntity.getMaxY(), mandalaRoomEntity.getMaxZ())
        );
    }

    private Location toButtonLocation(MandalaRoomEntity mandalaRoomEntity) {
        return worldVectorToLocationTransformer.transform(
                mandalaRoomEntity.getWorldName(),
                new Vector(mandalaRoomEntity.getButtonX(), mandalaRoomEntity.getButtonY(), mandalaRoomEntity.getButtonZ())
        );
    }

    private static @NotNull Material toButtonMaterial(MandalaRoomEntity mandalaRoomEntity) {
        return Material.valueOf(mandalaRoomEntity.getButtonMaterial());
    }

    private Location toAnimationPosition(MandalaRoomEntity mandalaRoomEntity) {
        return worldVectorToLocationTransformer.transform(
                mandalaRoomEntity.getWorldName(),
                new Vector(mandalaRoomEntity.getAnimationPositionX(), mandalaRoomEntity.getAnimationPositionY(), mandalaRoomEntity.getAnimationPositionZ())
        );
    }

    private Location toPlayerRoomCenter(MandalaRoomEntity mandalaRoomEntity) {
        return worldVectorToLocationTransformer.transform(
                mandalaRoomEntity.getWorldName(),
                new Vector(mandalaRoomEntity.getPlayerRoomCenterX(), mandalaRoomEntity.getPlayerRoomCenterY(), mandalaRoomEntity.getPlayerRoomCenterZ())
        );
    }
}
