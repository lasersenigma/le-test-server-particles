package eu.lasersenigma.leplayserverutils.lobby.mandala.service;

import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.CircuitPulse2DAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.LogoAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.MandalaNormalAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.MandalaSymmetricAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.parent.MandalaRoomAnimDisplayHandler;
import lombok.Getter;

@Getter
public enum MandalaRoomMod {
    MANDALA_SIMPLE(MandalaNormalAnimDisplayHandler.class, "Simple Mandala"),
    MANDALA_INVERTED_ROTATION(MandalaSymmetricAnimDisplayHandler.class, "Symmetrical Mandala"),
    CIRCUIT_PULSE_2D(CircuitPulse2DAnimDisplayHandler.class, "Circuit Pulse 2D"),
    LOGO(LogoAnimDisplayHandler.class, "Lasers-Enigma logo", true)
    //TODO add more mods
    // - dripping colored ink stain on click: deformed circles spraying out on click
    // - rock in water: circle but interacting with other circles
    // - coordinated bird movement
    // - 8Balls particles: on click spawn particle. It will then takes the movement given by the next seconds mouse movement.
    //   Then when touching a wall, it will bounce and keep the movement during a while and finish by disappearing.
    // - Exploding particles: same as 8Balls but with a explosion into more (smaller?) particles on wall hit.
    ;

    private final boolean defaultMod;

    private final Class<? extends MandalaRoomAnimDisplayHandler> displayHandlerClass;

    private final String displayName;

    private MandalaRoomMod(Class<? extends MandalaRoomAnimDisplayHandler> displayHandlerClass, String displayName, boolean defaultMod) {
        this.defaultMod = defaultMod;
        this.displayHandlerClass = displayHandlerClass;
        this.displayName = displayName;
    }

    private MandalaRoomMod(Class<? extends MandalaRoomAnimDisplayHandler> displayHandlerClass, String displayName) {
        this(displayHandlerClass, displayName, false);
    }

    public static MandalaRoomMod getDefaultMod() {
        for (MandalaRoomMod mod : values()) {
            if (mod.defaultMod) {
                return mod;
            }
        }
        return null;
    }

    public MandalaRoomMod nextMod() {
        MandalaRoomMod[] values = values();
        int ordinal = ordinal();
        return values[(ordinal + 1) % values.length];
    }
}
