package eu.lasersenigma.leplayserverutils.lobby.mandala.service;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.MandalaNormalAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.lobby.mandala.anim.parent.MandalaRoomAnimDisplayHandler;
import lombok.Getter;

@Singleton
public class MandalaRoomModService {

    @Inject
    private MandalaNormalAnimDisplayHandler mandalaNormalAnimDisplayHandler;

    @Inject
    private Injector injector;

    @Getter
    private MandalaRoomMod currentMod = MandalaRoomMod.getDefaultMod();

    public void nextMod() {
        currentMod = currentMod.nextMod();
    }

    public MandalaRoomAnimDisplayHandler getDisplayHandler() {
        return injector.getInstance(currentMod.getDisplayHandlerClass());
    }
}
