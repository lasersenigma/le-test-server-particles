package eu.lasersenigma.leplayserverutils.lobby.mandala.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.config.event.ConfigUpdatedEvent;
import eu.lasersenigma.leplayserverutils.lobby.mandala.dto.MandalaRoomDto;
import eu.lasersenigma.leplayserverutils.lobby.mandala.mapper.MandalaRoomMapper;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;

@Singleton
public class MandalaRoomService implements LEPSUListener {

    private final MandalaRoomMapper mandalaRoomMapper;

    private MandalaRoomDto mandalaRoomDto;

    @Inject
    public MandalaRoomService(ConfigManager configManager, MandalaRoomMapper mandalaRoomMapper) {
        this.mandalaRoomMapper = mandalaRoomMapper;
        final Config config = configManager.get();
        onConfigUpdated(config);
    }

    @EventHandler
    public void onConfigUpdated(ConfigUpdatedEvent configUpdatedEvent) {
        onConfigUpdated(configUpdatedEvent.getConfig());
    }

    private void onConfigUpdated(Config config) {
        this.mandalaRoomDto = mandalaRoomMapper.toDto(config.getMandalaRoomEntity());
    }

    public boolean isLocationInMandalaRoom(Location location) {
        Location mandalaRoomMinLocation = mandalaRoomDto.getMandalaRoomMinLocation();
        Location mandalaRoomMaxLocation = mandalaRoomDto.getMandalaRoomMaxLocation();
        return location.getWorld().equals(mandalaRoomMinLocation.getWorld())
                && location.getX() >= mandalaRoomMinLocation.getX()
                && location.getX() <= mandalaRoomMaxLocation.getX()
                && location.getY() >= mandalaRoomMinLocation.getY()
                && location.getY() <= mandalaRoomMaxLocation.getY()
                && location.getZ() >= mandalaRoomMinLocation.getZ()
                && location.getZ() <= mandalaRoomMaxLocation.getZ();
    }

    public MandalaRoomDto getMandalaRoomDto() {
        return mandalaRoomDto.copy();
    }

}
