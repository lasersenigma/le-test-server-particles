package eu.lasersenigma.leplayserverutils.lobby.mandala.task;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.JavaPluginDisableEventHandler;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomModService;
import eu.lasersenigma.leplayserverutils.lobby.mandala.service.MandalaRoomService;
import fr.skytale.particleanimlib.animation.parent.task.AAnimationTask;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;

@Singleton
public class MandalaAreaTask implements Runnable, JavaPluginEnableEventHandler, JavaPluginDisableEventHandler {

    @Inject
    public ConfigManager configManager;

    @Inject
    private LEPlayServerUtils lePlayServerUtils;

    @Inject
    private MandalaRoomService mandalaRoomService;

    @Inject
    private MandalaRoomModService mandalaRoomModService;

    private AAnimationTask<?> animationTask = null;
    private Integer taskId;
    private boolean playersInMandalaRoomPreviously;

    @Override
    public void enable() {
        startTask();
        playersInMandalaRoomPreviously = false;
    }

    @Override
    public void disable() {
        stopTask();
        disableAnimations();
    }

    @Override
    public void run() {
        final boolean playersInMandalaRoom = playersInMandalaRoom();
        if (playersInMandalaRoom && playersInMandalaRoomPreviously) {
            restartAnimationIfNeeded();
        } else if (playersInMandalaRoom) {
            displayAnimation();
        } else {
            disableAnimations();
        }
        this.playersInMandalaRoomPreviously = playersInMandalaRoom;
    }

    public void restartAnimationForce() {
        disableAnimations();
        displayAnimation();
    }

    private void restartAnimationIfNeeded() {
        if (animationTask == null || (!animationTask.isRunning())) {
            displayAnimation();
        }
    }

    private void startTask() {
        if (taskId != null) {
            return;
        }
        taskId = Bukkit.getScheduler().runTaskTimer(
                lePlayServerUtils,
                this,
                0,
                50
        ).getTaskId();
    }

    private void stopTask() {
        if (taskId != null) {
            Bukkit.getScheduler().cancelTask(taskId);
            taskId = null;
        }
    }

    private boolean playersInMandalaRoom() {
        return Bukkit.getOnlinePlayers().stream().anyMatch(player ->
                player.getGameMode() != GameMode.SPECTATOR
                        && mandalaRoomService.isLocationInMandalaRoom(player.getLocation())
        );
    }

    private void displayAnimation() {
        disableAnimations();
        animationTask = mandalaRoomModService.getDisplayHandler().displayAnimation();
    }


    private void disableAnimations() {
        if (animationTask != null) {
            animationTask.stopAnimation();
            animationTask = null;
        }
    }
}
