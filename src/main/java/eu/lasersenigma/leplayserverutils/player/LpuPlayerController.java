package eu.lasersenigma.leplayserverutils.player;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.player.entity.LpuPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.*;

@Singleton
public class LpuPlayerController {

    @Inject
    private ConfigManager configManager;

    @Inject
    private MessageSender msg;

    public Optional<LpuPlayer> findOrCreatePlayer(String pseudo, Config config) {
        return Optional.ofNullable(Bukkit.getPlayer(pseudo))
                .map(player -> findOrCreatePlayer(player, config))
                .or(() -> config.getPlayers().stream()
                        .filter(p -> p.getDisplayName().equals(pseudo))
                        .findAny());
    }


    public LpuPlayer findOrCreatePlayer(Player player, Config config) {
        LpuPlayer lpuPlayer = config.getPlayers().stream()
                .filter(p -> p.getUuid().equals(player.getUniqueId()))
                .findAny()
                .orElse(null);
        if (lpuPlayer == null) {
            lpuPlayer = new LpuPlayer(player.getUniqueId(), player.getName());
            config.getPlayers().add(lpuPlayer);
        }
        return lpuPlayer;
    }

    public Set<String> findAllPlayersPseudo() {
        return findAllPlayersPseudo(configManager.get());
    }

    public Set<String> findAllPlayersPseudo(Config config) {
        //add pseudo from playersOnlineList and playersSavedList
        // - avoid duplicates using hashset.
        // - This also means a player can have 2 pseudo at the end, one from the saved list and one from the online list
        Set<String> playersPseudoList = new HashSet<>();
        playersPseudoList.addAll(config.getPlayers().stream().map(LpuPlayer::getDisplayName).toList());
        playersPseudoList.addAll(Bukkit.getOnlinePlayers().stream().map(Player::getName).toList());
        return playersPseudoList;
    }

    public Set<String> findPlayerPseudoOrNicknameFromUUID(Collection<UUID> playersUUIDCollection, Config config) {
        Set<String> playersPseudoList = new HashSet<>();
        config.getPlayers().stream()
                .filter(p -> playersUUIDCollection.contains(p.getUuid()))
                .forEach(p -> playersPseudoList.add(p.getDisplayName()));
        playersUUIDCollection.stream()
                .map(Bukkit::getPlayer)
                .filter(Objects::nonNull)
                .map(Player::getName)
                .forEach(playersPseudoList::add);
        return playersPseudoList;
    }


    public boolean setNickname(Player executorAndTargetPlayer, String nickname) {
        Config config = configManager.get();
        findOrCreatePlayer(executorAndTargetPlayer, config).setDisplayName(nickname);
        configManager.saveConfig(config);
        msg.sendSuccess(executorAndTargetPlayer, "Your nickname has been set to " + nickname);
        return true;
    }

    public boolean setNickname(Player executorPlayer, String targetPlayerName, String nickname) {
        Config config = configManager.get();
        Optional<LpuPlayer> lpuPlayer = findOrCreatePlayer(targetPlayerName, config);
        if (lpuPlayer.isEmpty()) {
            msg.sendError(executorPlayer, "Unknown player %s".formatted(targetPlayerName));
            return false;
        }
        lpuPlayer.get().setDisplayName(nickname);
        configManager.saveConfig(config);
        msg.sendSuccess(executorPlayer, "The nickname of %s has been set to %s".formatted(targetPlayerName, nickname));
        return true;
    }
}
