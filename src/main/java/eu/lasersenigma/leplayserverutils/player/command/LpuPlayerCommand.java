package eu.lasersenigma.leplayserverutils.player.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.provider.LPUCommandsProvider;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class LpuPlayerCommand extends Command<Player> implements JavaPluginEnableEventHandler {

    public static final String NICKNAME_ARG = "nickname";
    private static final String COMMAND = "player";
    private final Commands leplayserverutilsCommands;

    @Inject
    public LpuPlayerCommand(LPUCommandsProvider lpuCommandsProvider,
                            LpuPlayerSetMyNicknameCommand lpuPlayerSetMyNicknameCommand,
                            LpuPlayerSetNicknameCommand lpuPlayerSetNicknameCommand) {
        super(Player.class, COMMAND);
        this.setPermission(LPUPermission.CREATOR.getPermission());
        registerSubCommand(lpuPlayerSetMyNicknameCommand);
        registerSubCommand(lpuPlayerSetNicknameCommand);
        this.leplayserverutilsCommands = lpuCommandsProvider.get();
    }

    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        leplayserverutilsCommands.registerCommand(this);
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return super.defaultProcessForContainer(commands, player, args);
    }

    @Override
    protected String description(Player executor) {
        return "Define nicknames to be used as author name in the rooms you create";
    }

}