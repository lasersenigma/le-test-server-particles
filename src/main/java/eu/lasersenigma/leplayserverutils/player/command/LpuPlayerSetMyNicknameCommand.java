package eu.lasersenigma.leplayserverutils.player.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.player.LpuPlayerController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

@Singleton
public class LpuPlayerSetMyNicknameCommand extends Command<Player> {

    private static final String SET_MY_NICKNAME_COMMAND = "setMyNickname";

    private static final String NICKNAME_ARG = LpuPlayerCommand.NICKNAME_ARG;
    private final LpuPlayerController lpuPlayerController;

    @Inject
    public LpuPlayerSetMyNicknameCommand(LpuPlayerController lpuPlayerController) {
        super(Player.class, SET_MY_NICKNAME_COMMAND);
        this.addArgument(NICKNAME_ARG, true, ArgumentType.string());
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.lpuPlayerController = lpuPlayerController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String nickname = this.getArgumentValue(player, NICKNAME_ARG, ArgumentType.string());
        return lpuPlayerController.setNickname(player, nickname);
    }

    @Override
    protected String description(Player executor) {
        return "Defines your nickname. It will be used when displaying room's author.";
    }
}
