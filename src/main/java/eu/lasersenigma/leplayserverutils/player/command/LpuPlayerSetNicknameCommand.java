package eu.lasersenigma.leplayserverutils.player.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.player.LpuPlayerController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Set;

@Singleton
public class LpuPlayerSetNicknameCommand extends Command<Player> {

    private static final String SET_MY_NICKNAME_COMMAND = "setNickname";

    private static final String NICKNAME_ARG = LpuPlayerCommand.NICKNAME_ARG;

    private static final String PLAYER_ARG = "player";
    private static final String PERMISSION = "leplayserverutils.admin";
    private final LpuPlayerController lpuPlayerController;

    @Inject
    public LpuPlayerSetNicknameCommand(LpuPlayerController lpuPlayerController) {
        super(Player.class, SET_MY_NICKNAME_COMMAND);
        this.addArgument(PLAYER_ARG, true, ArgumentType.string());
        this.addArgument(NICKNAME_ARG, true, ArgumentType.string());
        this.setPermission(PERMISSION);
        this.lpuPlayerController = lpuPlayerController;
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(PLAYER_ARG)) {
            String currentArgumentValue = getArgumentValue(executor, PLAYER_ARG, ArgumentType.string());
            final Set<String> playersNameSet = lpuPlayerController.findAllPlayersPseudo();
            super.setAutoCompleteValuesArg(
                    PLAYER_ARG,
                    CommandUtils.buildStringAutocompleteValues(playersNameSet, currentArgumentValue));
        }
    }


    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String targetPlayer = this.getArgumentValue(player, PLAYER_ARG, ArgumentType.string());
        String nickname = this.getArgumentValue(player, NICKNAME_ARG, ArgumentType.string());
        return lpuPlayerController.setNickname(player, targetPlayer, nickname);
    }

    @Override
    protected String description(Player executor) {
        return "Defines a player's nickname. It will be used when displaying room's author.";
    }
}
