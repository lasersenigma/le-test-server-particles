package eu.lasersenigma.leplayserverutils.player.entity;

import com.google.gson.annotations.Expose;
import lombok.*;

import java.util.UUID;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class LpuPlayer {

    @Expose
    private UUID uuid;

    @Expose
    private String displayName;

    public LpuPlayer(LpuPlayer lPUPlayer) {
        this.uuid = lPUPlayer.getUuid();
        this.displayName = lPUPlayer.getDisplayName();
    }
}
