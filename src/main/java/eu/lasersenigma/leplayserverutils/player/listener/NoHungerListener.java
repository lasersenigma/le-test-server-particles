package eu.lasersenigma.leplayserverutils.player.listener;

import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.FoodLevelChangeEvent;

@Singleton
public class NoHungerListener implements LEPSUListener {

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

}
