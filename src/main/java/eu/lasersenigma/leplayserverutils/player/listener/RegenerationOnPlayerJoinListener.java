package eu.lasersenigma.leplayserverutils.player.listener;

import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@Singleton
public class RegenerationOnPlayerJoinListener implements LEPSUListener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 2, false, false, false));
    }
}
