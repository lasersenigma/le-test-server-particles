package eu.lasersenigma.leplayserverutils.player.mapper;

import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.player.entity.LpuPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.UUID;

@Singleton
public class PlayerMapper {

    public Optional<String> getNickname(UUID playerUuid, Config config) {
        final Optional<String> nickname = config
                .getPlayers().stream()
                .filter(p -> p.getUuid().equals(playerUuid))
                .findAny()
                .map(LpuPlayer::getDisplayName);
        if (nickname.isPresent()) {
            return nickname;
        }
        return Optional.ofNullable(Bukkit.getPlayer(playerUuid)).map(Player::getName);
    }
}
