package eu.lasersenigma.leplayserverutils.portal;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import fr.skytale.particleanimlib.animation.animation.circle.CircleBuilder;
import fr.skytale.particleanimlib.animation.animation.circle.CircleTask;
import fr.skytale.particleanimlib.animation.attribute.ParticleTemplate;
import fr.skytale.particleanimlib.animation.attribute.position.animationposition.LocatedAnimationPosition;
import fr.skytale.particleanimlib.animation.attribute.var.CallbackWithPreviousValueVariable;
import fr.skytale.particleanimlib.animation.attribute.var.Constant;
import fr.skytale.particleanimlib.animation.collision.CollisionBuilder;
import fr.skytale.particleanimlib.animation.collision.action.CollisionActionCallback;
import fr.skytale.particleanimlib.animation.collision.processor.ParticleCollisionProcessor;
import fr.skytale.particleanimlib.animation.collision.processor.check.EntityCollisionCheckPresets;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.util.Vector;

@Singleton
public class PortalAnimDisplayHandler {
    public static final int PORTAL_ANIMATION_TICKS_DURATION = 500;
    public static final int CIRCLE_RADIUS = 1;

    private final LEPlayServerUtils plugin;


    @Inject
    public PortalAnimDisplayHandler(LEPlayServerUtils lePlayServerUtils) {
        this.plugin = lePlayServerUtils;
    }

    public CircleTask displayPortalAnim(Location particleAnimationLocation, Color color, CollisionActionCallback<Entity, CircleTask> entityCircleTaskCollisionActionCallback) {
        CircleBuilder circleBuilder = new CircleBuilder();
        circleBuilder.setJavaPlugin(plugin);
        circleBuilder.setNbPoints(7, true);
        circleBuilder.setRadius(CIRCLE_RADIUS);
        circleBuilder.setShowPeriod(new Constant<>(3));
        circleBuilder.setTicksDuration(PORTAL_ANIMATION_TICKS_DURATION);
        circleBuilder.setPointDefinition(new ParticleTemplate(
                Particle.REDSTONE,
                1,
                0,
                new Vector(0, 0, 0),
                color
        ));
        circleBuilder.setViewers(20);
        circleBuilder.setRotation(
                new CallbackWithPreviousValueVariable<>(
                        new Vector(0, 1, 0),
                        (iterationCount, previousValue) -> previousValue.add(new Vector(Math.random() / 4.0, Math.random() / 4.0, Math.random() / 4.0)).normalize()
                ),
                Math.PI / 10
        );
        circleBuilder.setPosition(new LocatedAnimationPosition(particleAnimationLocation));

        CollisionBuilder<Entity, CircleTask> collisionBuilder = new CollisionBuilder<>();
        collisionBuilder.setJavaPlugin(circleBuilder.getJavaPlugin());
        collisionBuilder.setPotentialCollidingTargetsCollector(circleTask -> {
            Location currentIterationBaseLocation = circleTask.getCurrentIterationBaseLocation();
            return currentIterationBaseLocation.getNearbyEntities(CIRCLE_RADIUS, CIRCLE_RADIUS, CIRCLE_RADIUS);
        });
        collisionBuilder.addPotentialCollidingTargetsFilter((entity, lineTask) -> entity.getType().equals(EntityType.PLAYER));

        collisionBuilder.addCollisionProcessor(ParticleCollisionProcessor.useDefault(
                circleBuilder,
                EntityCollisionCheckPresets.TARGET_CENTER_INSIDE_CIRCLE,
                entityCircleTaskCollisionActionCallback));
        circleBuilder.addCollisionHandler(collisionBuilder.build());
        return circleBuilder.getAnimation().show();
    }
}
