package eu.lasersenigma.leplayserverutils.portal;

import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.common.utils.BlockFinder;
import eu.lasersenigma.leplayserverutils.common.utils.LasersEnigmaExecutor;
import eu.lasersenigma.leplayserverutils.common.utils.WorldEditExecutor;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import eu.lasersenigma.leplayserverutils.lobby.lobby.LobbyController;
import eu.lasersenigma.leplayserverutils.portal.dto.EntranceActiveTeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.dto.ExitTeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.dto.TeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.portal.graph.PortalGraphService;
import eu.lasersenigma.leplayserverutils.puzzle.nextlevel.gui.NextLevelMenuProvider;
import eu.lasersenigma.leplayserverutils.puzzle.review.gui.note.NoteLevelMenuProvider;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import eu.lasersenigma.leplayserverutils.room.hint.event.RoomHintUpdatedEvent;
import eu.lasersenigma.leplayserverutils.room.tag.RoomTagController;
import eu.lasersenigma.leplayserverutils.roomtemplate.entity.RoomTemplate;
import eu.lasersenigma.player.LEPlayers;
import net.kyori.adventure.title.Title;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton
public class PortalController {
    public static final String NO_ROOM_FOUND_WITH_DESCRIPTION = "No room has the description \"%s\"";
    private static final double DEFAULT_DIFFICULTY = 0.5;

    @Inject
    private WorldEditExecutor worldEditExecutor;

    @Inject
    private ConfigManager configManager;

    @Inject
    private WorldVectorToLocationTransformer vectorToLocation;

    @Inject
    private LasersEnigmaExecutor lasersEnigmaExecutor;

    @Inject
    private BlockFinder blockFinder;

    @Inject
    private LEPlayServerUtils plugin;

    @Inject
    private RoomController roomController;

    @Inject
    private LobbyController lobbyController;

    @Inject
    private PortalGraphService portalGraphService;

    @Inject
    private WorldVectorToLocationTransformer worldVectorToLocationTransformer;

    @Inject
    private NextLevelMenuProvider nextLevelMenuProvider;

    @Inject
    private NoteLevelMenuProvider noteLevelMenuProvider;

    @Inject
    private MessageSender msg;

    @Inject
    private RoomTagController roomTagController;

    public static Set<String> getDuplicatedRoomDescriptions(Config config) {
        return config.getPortals().stream()
                .filter(Portal::isActive)
                .map(portal -> portal.getRoom().getDescription())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(m -> m.getValue() > 1) // keep only duplicates
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    private boolean checkLobbyPortalHasTeleportExitDestination(Player player, Portal oCurrentPortal) {
        if (oCurrentPortal.getTeleportExitDestination() == null) {
            msg.sendError(
                    player,
                    "In lobby, the portal with order #%d at location %d %d %d must have a teleport exit destination set.".formatted(
                            oCurrentPortal.getOrder(),
                            oCurrentPortal.getLocation().getBlockX(),
                            oCurrentPortal.getLocation().getBlockY(),
                            oCurrentPortal.getLocation().getBlockZ()
                    )
            );
            return false;
        }
        return true;
    }

    private static void swapPortalRoomsUnsecure(Portal firstPortal, Portal secondPortalData) {
        Room firstPortalRoomTemp = firstPortal.getRoom();
        firstPortal.setRoom(secondPortalData.getRoom());
        secondPortalData.setRoom(firstPortalRoomTemp);
    }

    private static Optional<Portal> findPortalByOrder(Config config, int order) {
        return config.getPortals().stream()
                .filter(portal -> portal.getOrder() != null && portal.getOrder().equals(order))
                .findFirst();
    }

    private void displayTitle(Player player, EntranceActiveTeleporterDTO entranceActiveTeleporterDTO) {
        //display title
        player.showTitle(Title.title(
                entranceActiveTeleporterDTO.getTitle(),
                entranceActiveTeleporterDTO.getSubtitle(),
                Title.Times.times(
                        Duration.of(1, ChronoUnit.SECONDS),
                        Duration.of(4, ChronoUnit.SECONDS),
                        Duration.of(1, ChronoUnit.SECONDS)
                )
        ));
    }

    /**
     * Creates a deactivated portal at the player's location
     * - If there is no portal within a 3 blocks range around the player
     *
     * @param player The player using the command
     */
    public void createPortal(Player player) {
        Config config = configManager.get();

        //Find portal
        Optional<Portal> oPortal = findNearPortal(player, config, false, false);
        if (oPortal.isPresent()) {
            msg.sendError(player, "A portal is already present in a 3 blocks range around you.");
            return;
        }

        //Create the portal
        Location portalBlockLocation = player.getLocation().clone().add(0, -1, 0).toBlockLocation();

        Portal newPortal = new Portal(
                -1,
                portalBlockLocation.getWorld().getName(),
                portalBlockLocation.toVector(),
                null,
                null
        );
        config.getPortals().add(newPortal);
        configManager.saveConfig(config);
    }

    /**
     * Deletes an inactive portal if found in a 3 blocks range around the player
     *
     * @param player The player using the command
     */
    public void deletePortal(Player player) {
        Config config = configManager.get();

        //Find portal
        Optional<Portal> oPortal = findNearPortal(player, config, false, false);
        if (oPortal.isEmpty()) {
            msg.sendError(player, "No portal found within a 3 blocks range");
            return;
        }
        Portal portal = oPortal.get();

        if (portal.isActive()) {
            msg.sendError(player, "The portal is activated. Please deactivate it first (Be careful because deactivating a portal deletes the room. You may want to use '/. portal move' command instead.");
            return;
        }

        //Delete the portal
        config.getPortals().remove(portal);
        configManager.saveConfig(config);
        msg.sendSuccess(player, "Portal deleted");
    }

    /**
     * Sets the teleport exit destination of the portal to the player's location
     * Corresponds to command /leutils portal setTeleportExitDestination
     * Must be executed at less than 3 blocks distance from a portal
     *
     * @param player The CommandSender
     */
    public void setTeleportExitDestination(Player player) {
        Config config = configManager.get();

        //Find portal
        Optional<Portal> oPortal = findNearPortal(player, config, false, false);
        if (oPortal.isEmpty()) {
            msg.sendError(player, "No portal found within a 3 blocks range");
            return;
        }

        //Set the teleport exit destination
        oPortal.get().setTeleportExitDestination(player.getLocation().toVector());
        configManager.saveConfig(config);

        msg.sendSuccess(player, "Teleport exit destination set");
    }

    /**
     * Corresponds to command /leutils portal activate <RoomType> <roomDescription>
     * Must be executed in the corridor area at less than 3 blocks distance from a deactivated portal
     * Copy/paste the template of the puzzle area according to the RoomType
     * The fact the portal is activated stats the animation on the next periodic execution of portal animations
     * - Creates the TP:
     * - Portal block (cf corridor template) -> spawn block (cf puzzle template)
     * - Return portal block (cf puzzle template) -> return block of the portal (cf corridor template)
     * - Success portal block (cf puzzle template) -> return block of the portal (cf corridor template)
     * - Creates the Lasers Enigma area
     * - Create the spawn
     * - Create the victory area
     * - Creates the hologram (name="puzzle" + portalId, description = "Puzzle " + portalId + " : " + description of the portal)
     *
     * @param player          The CommandSender
     * @param roomTemplate    The type of the room
     * @param roomDescription The description of the room
     */
    public void activatePortal(Player player, RoomTemplate roomTemplate, String roomDescription) {
        Config config = configManager.get();

        //Find portal
        Optional<Portal> oPortal = findNearPortal(player, config, false, true);
        if (oPortal.isEmpty()) {
            msg.sendError(player, "No deactivated portal found within a 3 blocks range");
            return;
        }
        Portal portal = oPortal.get();

        if (portal.getTeleportExitDestination() == null) {
            msg.sendError(player, Portal.PORTAL_WITH_AN_ACTIVE_ROOM_MUST_HAVE_A_TELEPORT_EXIT_DESTINATION);
            return;
        }

        if (RoomController.roomDescriptionExists(config, roomDescription)) {
            msg.sendError(player, "A room with the description \"" + roomDescription + "\" already exists.");
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously(
                plugin,
                () -> {
                    // Copy/paste the template of the puzzle area according to the RoomType
                    Location roomMinCorner = vectorToLocation.transform(roomTemplate.getWorldName(), config.getNextRoomMinCorner());
                    final Location roomTemplateMinCorner = vectorToLocation.transform(roomTemplate.getWorldName(), roomTemplate.getMinCorner());
                    final Location roomTemplateMaxCorner = vectorToLocation.transform(roomTemplate.getWorldName(), roomTemplate.getMaxCorner());
                    worldEditExecutor.copy(
                            player,
                            roomTemplateMinCorner.clone(),
                            roomTemplateMaxCorner.clone(),
                            roomMinCorner.clone()
                    );
                    Location roomMaxCorner = roomMinCorner.clone().add(roomTemplateMaxCorner.toVector().subtract(roomTemplateMinCorner.toVector()));


                    // Find exit portals in room
                    final Optional<Set<Location>> oRoomExitPortalBlocksLocations = findRoomExitPortalBlockLocations(
                            player,
                            roomTemplate,
                            roomMinCorner,
                            roomMaxCorner);

                    if (oRoomExitPortalBlocksLocations.isEmpty()) {
                        return;
                    }
                    final Set<Location> roomExitPortalBlocksLocations = oRoomExitPortalBlocksLocations.get();


                    // Find the spawn in room
                    final Optional<Location> oRoomSpawnLoc = findRoomSpawnBlockLocation(
                            player,
                            roomTemplate,
                            roomMinCorner,
                            roomMaxCorner);
                    if (oRoomSpawnLoc.isEmpty()) return;
                    final Location roomSpawnLoc = oRoomSpawnLoc.get();

                    // Replace the spawn block by air
                    Bukkit.getScheduler().runTask(plugin, () -> roomSpawnLoc.getBlock().setType(Material.AIR));

                    // Find sub portals in room
                    final Set<Location> roomSubPortalBlocksLocations = findSubPortalBlockLocations(
                            roomTemplate,
                            roomMinCorner,
                            roomMaxCorner);

                    for (Location roomSubPortalBlockLocation : roomSubPortalBlocksLocations) {

                        Portal newPortal = new Portal(
                                null,
                                roomSubPortalBlockLocation.getWorld().getName(),
                                roomSubPortalBlockLocation.toVector(),
                                null,
                                null
                        );
                        config.getPortals().add(newPortal);
                    }

                    // Create the LE area (and define its spawn and victory conditions)
                    Bukkit.getScheduler().runTask(plugin, () -> lasersEnigmaExecutor.createLEArea(player, roomMinCorner, roomMaxCorner, roomSpawnLoc));

                    // Update config and save it
                    Room room = new Room(
                            roomTemplate.getWorldName(),
                            roomMinCorner.toVector(),
                            roomMaxCorner.toVector(),
                            roomSpawnLoc.toVector(),
                            roomExitPortalBlocksLocations.stream()
                                    .map(Location::toVector)
                                    .collect(Collectors.toList()),
                            roomDescription,
                            DEFAULT_DIFFICULTY,
                            new ArrayList<>(List.of(player.getUniqueId())),
                            new HashSet<>(),
                            new ArrayList<>()
                    );

                    config.setNextRoomMinCorner(config.getNextRoomMinCorner().add(new Vector(0, 0, roomMaxCorner.getBlockZ() - roomMinCorner.getBlockZ() + 300)));

                    portal.setRoom(room);

                    Bukkit.getScheduler().runTask(plugin, () -> configManager.saveConfig(config));
                }
        );

    }

    public void deactivatePortal(Player player) {
        Config config = configManager.get();

        //Find portal
        Optional<Portal> oPortal = findNearPortal(player, config, true, false);
        if (oPortal.isEmpty()) {
            msg.sendError(player, "No active portal found within a 3 blocks range");
            return;
        }
        Portal portal = oPortal.get();

        final Room room = portal.getRoom();

        Location minCorner = vectorToLocation.transform(room.getWorldName(), room.getMinCorner());
        Location maxCorner = vectorToLocation.transform(room.getWorldName(), room.getMaxCorner());


        //Check if the room contains sub-portals
        Set<Portal> portalsInRoom = config.getPortals().stream()
                .filter(p -> roomController.isInRoom(p.getLocation().toLocation(player.getWorld()), room))
                .collect(Collectors.toSet());

        if (!portalsInRoom.isEmpty()) {
            boolean activePortalInRoom = portalsInRoom.stream().anyMatch(Portal::isActive);
            if (activePortalInRoom) {
                msg.sendWarning(player, "The room contains active sub-portals. Please deactivate them first.");
                return;
            }
            //delete deactivated sub-portals
            portalsInRoom.forEach(p -> config.getPortals().remove(p));
        }

        //Delete the lasers enigma area
        Location center = minCorner.clone().add(maxCorner).multiply(DEFAULT_DIFFICULTY);
        AreaController.deleteArea(LEPlayers.getInstance().findLEPlayer(player), center);

        //Replace blocks by air
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () ->
                worldEditExecutor.set(player, minCorner, maxCorner, Material.AIR));

        //Deactivate the portal and delete the room, the description
        portal.setRoom(null);
        configManager.saveConfig(config);

        //Send RoomHintUpdatedEvent to remove clear the player received hints for this room
        Bukkit.getPluginManager().callEvent(new RoomHintUpdatedEvent(room.getDescription(), new ArrayList<>()));

        msg.sendSuccess(player, "Portal deactivated");
    }

    public void moveInactivePortalsToSpawnEnd(Player player) {
        Config config = configManager.get();

        for (int i = 1; i < config.getPortals().size() + 1; i++) {
            Optional<Portal> oPortal = findPortalByOrder(config, i);
            if (oPortal.isPresent() && !oPortal.get().isActive()) {
                final Optional<Portal> firstActivePortalHavingIndexEqualOrGreater = findFirstActivePortalHavingOrderEqualOrGreater(config, i);

                if (firstActivePortalHavingIndexEqualOrGreater.isPresent()) {
                    swapPortalRoomsUnsecure(oPortal.get(), firstActivePortalHavingIndexEqualOrGreater.get());
                } else {
                    //no more active portal until the end
                    break;
                }
            }
        }
        configManager.saveConfig(config);
    }

    public void teleport(Player player, TeleporterDTO teleporterDTO) {
        if (teleporterDTO instanceof EntranceActiveTeleporterDTO entranceActiveTeleporterDTO) {
            teleportInRoom(player, entranceActiveTeleporterDTO);
        } else if (teleporterDTO instanceof ExitTeleporterDTO exitTeleporterDTO) {
            teleportOutsideRoom(player, exitTeleporterDTO);
        } else {
            throw new IllegalStateException("Unknown teleporter DTO type");
        }
    }

    public void teleportInRoom(Player player, EntranceActiveTeleporterDTO entranceActiveTeleporterDTO) {
        player.teleport(entranceActiveTeleporterDTO.getDestination());
        displayTitle(player, entranceActiveTeleporterDTO);
    }

    public void teleportOutsideRoom(Player player, ExitTeleporterDTO exitTeleporterDTO) {
        player.teleport(exitTeleporterDTO.getDestination());
        final Optional<EntranceActiveTeleporterDTO> nextPortalEntranceActiveTeleporterDTO = exitTeleporterDTO.getNextPortalEntranceActiveTeleporterDTO();
        boolean hasNextPortal = nextPortalEntranceActiveTeleporterDTO.isPresent();
        if (hasNextPortal) {
            ChestGui gui = nextLevelMenuProvider.getGui(
                    event -> {
                        teleportInRoom(player, nextPortalEntranceActiveTeleporterDTO.get());
                        event.getWhoClicked().closeInventory();
                    },
                    exitTeleporterDTO.getPortal().getRoom().getId()
            );
            gui.show(player);
        } else {

            Config config = configManager.get();
            boolean isPreviousRoomHub = roomTagController.getTags(exitTeleporterDTO.getPortal().getRoom(), config)
                    .stream()
                    .anyMatch(tag -> tag.equalsIgnoreCase("hub"));

            if (!isPreviousRoomHub) {
                ChestGui gui = noteLevelMenuProvider.getGui(
                        exitTeleporterDTO.getPortal().getRoom().getId()
                );
                gui.show(player);
            }
        }
    }

    public Optional<Portal> findNearPortal(Player player, Config config, boolean activePortalOnly, boolean deactivatedPortalOnly) {
        return Optional.ofNullable(
                        config.getPortals().stream()
                                .filter(portal -> {
                                    //check if the portal is in the same world as the player
                                    if (!portal.getWorldName().equals(player.getWorld().getName())) {
                                        return false;
                                    }
                                    //remove deactivated portals if required
                                    if (activePortalOnly && !portal.isActive()) {
                                        return false;
                                    }

                                    //remove active portals if required
                                    if (deactivatedPortalOnly && portal.isActive()) {
                                        return false;
                                    }
                                    return true;
                                })
                                // pair the portal with the distance to player into a Map.Entry<Portal,Double>
                                .map(portal ->
                                        Map.entry(
                                                portal,
                                                player.getLocation().distance(
                                                        portal.getLocation().toLocation(player.getWorld())
                                                )
                                        )
                                )
                                // keep only the portals within a 3 blocks range
                                .filter(entry -> entry.getValue() < 3)

                                // keep only nearest portal's entry
                                .reduce(null, (previous, current) -> {
                                    if (previous == null) {
                                        return current;
                                    }
                                    if (previous.getValue() < current.getValue()) {
                                        return previous;
                                    }
                                    return current;
                                })
                )
                // keep only the portal (discard the distance)
                .map(Map.Entry::getKey);
    }

    private boolean insertAndShiftNextPortalRoomsInLobby(@NotNull Player player, @NotNull Config config, @NotNull Portal originRoomPortal, @NotNull Portal destinationRoomPortal) {

        if (originRoomPortal == destinationRoomPortal) {
            msg.sendError(player, "The two portals are the same.");
            return false;
        }

        final Location destinationRoomPortalLocation = worldVectorToLocationTransformer.transform(destinationRoomPortal.getWorldName(), destinationRoomPortal.getLocation());
        if (!lobbyController.isInLobby(destinationRoomPortalLocation)) {
            msg.sendError(player, "The destination portal should be in lobby. If you want to move a room to a non-lobby portal, please use the '\"/. portal swap \"<room description 1>\" \"<room description 2>\"' or '/. portal swapHere \"<room description>\"' command.");
            return false;
        }

        if (!checkLobbyPortalHasTeleportExitDestination(player, originRoomPortal)) {
            return false;
        }
        if (!checkLobbyPortalHasTeleportExitDestination(player, destinationRoomPortal)) {
            return false;
        }

        int destinationIndex = destinationRoomPortal.getOrder();
        int portalToMoveIndex;
        final Location originRoomPortalLocation = worldVectorToLocationTransformer.transform(originRoomPortal.getWorldName(), originRoomPortal.getLocation());
        if (!lobbyController.isInLobby(originRoomPortalLocation)) {
            // We first swap the origin portal with the first inactive portal in lobby (if any).
            // It is needed because we will have to use order for the room shifting
            Optional<Portal> oFirstInactivePortalInLobby = findFirstInactivePortalHavingOrderEqualOrGreater(config, destinationRoomPortal.getOrder());
            if (oFirstInactivePortalInLobby.isEmpty()) {
                msg.sendError(player, "No inactive portal found in lobby. This operation can only be done if the origin portal is in lobby or if there is an inactive portal in lobby having an order greater than the destination portal.");
                return false;
            }
            Portal firstInactivePortalInLobby = oFirstInactivePortalInLobby.get();
            if (!checkLobbyPortalHasTeleportExitDestination(player, oFirstInactivePortalInLobby.get())) {
                return false;
            }
            swapPortalRoomsUnsecure(originRoomPortal, firstInactivePortalInLobby);
            portalToMoveIndex = Objects.requireNonNull(firstInactivePortalInLobby.getOrder());
        } else {
            portalToMoveIndex = Objects.requireNonNull(originRoomPortal.getOrder());
        }

        int swapStep;
        if (destinationIndex > portalToMoveIndex) {
            swapStep = -1;
        } else {
            swapStep = 1;
        }

        int currentSwapTarget = destinationIndex;
        while (currentSwapTarget != portalToMoveIndex) {
            int finalCurrentSwapTarget = currentSwapTarget;
            Optional<Portal> oCurrentPortal = findPortalByOrder(config, finalCurrentSwapTarget);
            Optional<Portal> oNextPortal = findPortalByOrder(config, portalToMoveIndex);
            if (oCurrentPortal.isEmpty() || oNextPortal.isEmpty()) {
                throw new IllegalStateException("In this method, portals should be found by their order but at least one of them is missing.");
            }
            if (!checkLobbyPortalHasTeleportExitDestination(player, oCurrentPortal.get())) return false;
            if (!checkLobbyPortalHasTeleportExitDestination(player, oNextPortal.get())) return false;
            swapPortalRoomsUnsecure(oCurrentPortal.get(), oNextPortal.get());
            currentSwapTarget = currentSwapTarget + swapStep;
        }
        return true;
    }

    public boolean swapRoomsBetweenPortals(Player player, String firstPortalRoomDescription, String secondPortalRoomDescription) {
        Config config = configManager.get();

        //Find portals
        Optional<Portal> oFirstPortal = roomController.findActivePortalByRoomDescription(config, firstPortalRoomDescription);
        if (oFirstPortal.isEmpty()) {
            msg.sendError(player, NO_ROOM_FOUND_WITH_DESCRIPTION.formatted(firstPortalRoomDescription));
            return false;
        }
        Optional<Portal> oSecondPortal = roomController.findActivePortalByRoomDescription(config, secondPortalRoomDescription);
        if (oSecondPortal.isEmpty()) {
            msg.sendError(player, NO_ROOM_FOUND_WITH_DESCRIPTION.formatted(secondPortalRoomDescription));
            return false;
        }

        //Swap the portals
        boolean success = swapPortalRooms(player, config, oFirstPortal.get(), oSecondPortal.get());
        if (success) {
            configManager.saveConfig(config);
        }
        return success;
    }

    public boolean swapRoomsBetweenFirstPortalAndNearbyPortal(Player player, String firstRoomDescription) {
        Config config = configManager.get();

        //Find portals
        Optional<Portal> oFirstPortal = roomController.findActivePortalByRoomDescription(config, firstRoomDescription);
        if (oFirstPortal.isEmpty()) {
            msg.sendError(player, NO_ROOM_FOUND_WITH_DESCRIPTION.formatted(firstRoomDescription));
            return false;
        }

        Optional<Portal> oSecondPortal = findNearPortal(player, config, false, false);
        if (oSecondPortal.isEmpty()) {
            msg.sendError(player, "No portal has been found within 3 blocks range.");
            return false;
        }

        //Swap the portals
        boolean success = swapPortalRooms(player, config, oFirstPortal.get(), oSecondPortal.get());
        if (success) {
            configManager.saveConfig(config);
        }
        return success;
    }

    public boolean insertRoomInPortalAndShiftNextLobbyPortalsRooms(Player player, String originPortalRoomDescription, String destinationPortalRoomDescription) {
        Config config = configManager.get();

        //Find portals
        Optional<Portal> oOriginPortal = roomController.findActivePortalByRoomDescription(config, originPortalRoomDescription);
        if (oOriginPortal.isEmpty()) {
            msg.sendError(player, NO_ROOM_FOUND_WITH_DESCRIPTION.formatted(originPortalRoomDescription));
            return false;
        }
        Optional<Portal> oDestinationPortal = roomController.findActivePortalByRoomDescription(config, destinationPortalRoomDescription);
        if (oDestinationPortal.isEmpty()) {
            msg.sendError(player, NO_ROOM_FOUND_WITH_DESCRIPTION.formatted(destinationPortalRoomDescription));
            return false;
        }

        //Insert the room in the destination portal and shift the next lobby portals rooms
        boolean result = insertAndShiftNextPortalRoomsInLobby(player, config, oOriginPortal.get(), oDestinationPortal.get());
        if (result) {
            configManager.saveConfig(config);
        }
        return result;
    }

    public boolean insertRoomInNearbyPortalAndShiftNextLobbyPortalsRooms(Player player, String originRoomDescription) {
        Config config = configManager.get();

        //Find portals
        Optional<Portal> oOriginPortal = roomController.findActivePortalByRoomDescription(config, originRoomDescription);
        if (oOriginPortal.isEmpty()) {
            msg.sendError(player, NO_ROOM_FOUND_WITH_DESCRIPTION.formatted(originRoomDescription));
            return false;
        }

        Optional<Portal> oDestinationPortal = findNearPortal(player, config, false, false);
        if (oDestinationPortal.isEmpty()) {
            msg.sendError(player, "No portal has been found within 3 blocks range.");
            return false;
        }

        //Insert the room in the destination portal and shift the next lobby portals rooms
        boolean result = insertAndShiftNextPortalRoomsInLobby(player, config, oOriginPortal.get(), oDestinationPortal.get());
        if (result) {
            configManager.saveConfig(config);
        }
        return result;
    }

    private Optional<Portal> findFirstActivePortalHavingOrderEqualOrGreater(Config config, int order) {
        return config.getPortals().stream()
                .filter(portal -> portal.getOrder() != null && portal.getOrder() >= order && portal.isActive())
                .sorted(Comparator.comparing(Portal::getOrder))
                .findFirst();
    }

    private Optional<Portal> findFirstInactivePortalHavingOrderEqualOrGreater(Config config, Integer order) {
        return config.getPortals().stream()
                .filter(portal -> portal.getOrder() != null && portal.getOrder() >= order && !portal.isActive())
                .min(Comparator.comparing(Portal::getOrder));
    }

    private boolean swapPortalRooms(Player player, Config config, Portal firstPortal, Portal secondPortal) {

        if (firstPortal == secondPortal) {
            msg.sendError(player, "The two rooms are the same. Please enter two different descriptions.");
            return false;
        }
        if (firstPortal.getTeleportExitDestination() == null || secondPortal.getTeleportExitDestination() == null) {
            msg.sendError(player, "Both portals must have a teleport exit destination set.");
            return false;
        }

        swapPortalRoomsUnsecure(firstPortal, secondPortal);

        if (portalGraphService.doesPortalGraphContainsCycle(config, firstPortal) || portalGraphService.doesPortalGraphContainsCycle(config, secondPortal)) {
            msg.sendError(player, "The swap would create a cycle in the portal graph. Please don't move a portal into its own destination room or sub-rooms.");
            return false;
        }

        return true;
    }

    private Optional<Location> findRoomSpawnBlockLocation(Player player, RoomTemplate roomTemplate, Location roomMinCorner, Location roomMaxCorner) {
        final Set<Location> roomSpawnLocations = blockFinder.findBlocks(
                roomMinCorner.clone(),
                roomMaxCorner.clone(),
                roomTemplate.getSpawnMaterial()
        );
        if (roomSpawnLocations.size() != 1) {
            msg.sendError(player, "A room template must have exactly one spawn block. In this room template, the spawn block have the type " + roomTemplate.getSpawnMaterial() + ".");
            return Optional.empty();
        }

        return Optional.of(roomSpawnLocations.stream().findAny().get());
    }

    private Optional<Set<Location>> findRoomExitPortalBlockLocations(Player player, RoomTemplate roomTemplate, Location roomMinCorner, Location roomMaxCorner) {
        final Set<Location> roomPortalBlocksLocations = blockFinder.findBlocks(
                roomMinCorner.clone(),
                roomMaxCorner.clone(),
                roomTemplate.getPortalMaterial()
        );
        if (roomPortalBlocksLocations.size() < 1) {
            msg.sendError(player, "Not enough exit portal in the room template. A room template must have at least 1 exit portal having type " + roomTemplate.getPortalMaterial() + " allowing players to come back to spawn.");
            return Optional.empty();
        }
        if (roomPortalBlocksLocations.size() > 2) {
            msg.sendWarning(player, "More than 2 exit portals in the room template. Are you sure you did not use " + roomTemplate.getPortalMaterial() + " as building block instead of only using this type of block for exit portals ?");
        }
        return Optional.of(roomPortalBlocksLocations);
    }

    private Set<Location> findSubPortalBlockLocations(RoomTemplate roomTemplate, Location roomMinCorner, Location roomMaxCorner) {
        return blockFinder.findBlocks(
                roomMinCorner.clone(),
                roomMaxCorner.clone(),
                roomTemplate.getSubPortalMaterial()
        );
    }
}
