package eu.lasersenigma.leplayserverutils.portal.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import eu.lasersenigma.leplayserverutils.roomtemplate.command.RoomTemplateCommand;
import eu.lasersenigma.leplayserverutils.roomtemplate.entity.RoomTemplate;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class PortalActivateCommand extends Command<Player> {

    private static final String TEMPLATE_NAME = RoomTemplateCommand.TEMPLATE_NAME;
    private static final String ROOM_DESCRIPTION = "roomDescription";
    private final PortalController portalController;

    private final ConfigManager configManager;

    private final MessageSender msg;

    @Inject
    public PortalActivateCommand(PortalController portalController, ConfigManager configManager, MessageSender msg) {
        super(Player.class, "activate");
        this.msg = msg;
        this.addArgument(TEMPLATE_NAME, true, ArgumentType.string());
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.configManager = configManager;
        this.addArgument(ROOM_DESCRIPTION, true, ArgumentType.string());
        this.portalController = portalController;
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(TEMPLATE_NAME)) {
            String currentArgumentValue = getArgumentValue(executor, TEMPLATE_NAME, ArgumentType.string());
            final Set<String> roomTemplatesNameSet = configManager.get()
                    .getRoomTemplates().stream()
                    .map(roomTemplate -> roomTemplate.getName().toUpperCase(Locale.ROOT))
                    .collect(Collectors.toSet());
            super.setAutoCompleteValuesArg(
                    TEMPLATE_NAME,
                    CommandUtils.buildStringAutocompleteValues(roomTemplatesNameSet, currentArgumentValue));
        }
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String roomTemplateStr = this.getArgumentValue(player, TEMPLATE_NAME, ArgumentType.string()).toUpperCase(Locale.ROOT);

        Optional<RoomTemplate> oRoomTemplate = configManager.get()
                .getRoomTemplates().stream()
                .filter(roomTemplate -> roomTemplate.getName().toUpperCase().equals(roomTemplateStr))
                .findFirst();
        if (oRoomTemplate.isEmpty()) {
            msg.sendError(player, "Room template not found");
            return false;
        }


        String roomDescription = this.getArgumentValue(player, ROOM_DESCRIPTION, ArgumentType.string());

        RoomTemplate roomTemplate = oRoomTemplate.get();
        portalController.activatePortal(player, roomTemplate, roomDescription);
        return true;
    }


    @Override
    protected String description(Player executor) {
        return "Activate a portal";
    }
}
