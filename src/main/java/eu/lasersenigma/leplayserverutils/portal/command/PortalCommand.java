package eu.lasersenigma.leplayserverutils.portal.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.provider.LPUCommandsProvider;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class PortalCommand extends Command<Player> implements JavaPluginEnableEventHandler {

    private final Commands leplayserverutilsCommands;

    @Inject
    public PortalCommand(
            LPUCommandsProvider lpuCommandsProvider,
            PortalCreateCommand portalCreateCommand,
            PortalDeleteCommand portalDeleteCommand,
            PortalActivateCommand portalActivateCommand,
            PortalDeactivateCommand portalDeactivateCommand,
            PortalMoveInactivePortalsToTheEndCommand portalMoveInactivePortalsToTheEndCommand,
            PortalTeleportExitDestinationCommand portalTeleportExitDestinationCommand,
            PortalRoomSwapCommand portalRoomSwapCommand,
            PortalRoomSwapHereCommand portalRoomSwapHereCommand,
            PortalRoomOderInsertCommand portalRoomOderInsertCommand,
            PortalRoomOrderInsertHereCommand portalRoomOrderInsertHereCommand
    ) {
        super(Player.class, "portal");
        super.registerSubCommand(portalCreateCommand);
        super.registerSubCommand(portalDeleteCommand);
        super.registerSubCommand(portalActivateCommand);
        super.registerSubCommand(portalDeactivateCommand);
        super.registerSubCommand(portalMoveInactivePortalsToTheEndCommand);
        super.registerSubCommand(portalTeleportExitDestinationCommand);
        super.registerSubCommand(portalRoomSwapCommand);
        super.registerSubCommand(portalRoomSwapHereCommand);
        super.registerSubCommand(portalRoomOderInsertCommand);
        super.registerSubCommand(portalRoomOrderInsertHereCommand);
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.leplayserverutilsCommands = lpuCommandsProvider.get();
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return super.defaultProcessForContainer(commands, player, args);
    }

    @Override
    protected String description(Player executor) {
        return "Portal related commands";
    }

    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        leplayserverutilsCommands.registerCommand(this);
    }

}