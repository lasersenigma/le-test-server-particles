package eu.lasersenigma.leplayserverutils.portal.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class PortalCreateCommand extends Command<Player> {
    private final PortalController portalController;

    @Inject
    public PortalCreateCommand(PortalController portalController) {
        super(Player.class, "create");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.portalController = portalController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        portalController.createPortal(player);
        return true;
    }


    @Override
    protected String description(Player executor) {
        return "Creates a portal at player's location";
    }
}
