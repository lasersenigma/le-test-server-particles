package eu.lasersenigma.leplayserverutils.portal.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

@Singleton
public class PortalDeactivateCommand extends Command<Player> {

    private static final String CONFIRM_ARGUMENT_NAME_AND_VALUE = "confirm";
    private final PortalController portalController;
    private final MessageSender msg;

    @Inject
    public PortalDeactivateCommand(PortalController portalController, MessageSender messageSender) {
        super(Player.class, "deactivate");
        this.msg = messageSender;
        this.addArgument(CONFIRM_ARGUMENT_NAME_AND_VALUE, false, ArgumentType.string());
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.portalController = portalController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        if (this.hasArgumentValue(player, CONFIRM_ARGUMENT_NAME_AND_VALUE) && this.getArgumentValue(player, CONFIRM_ARGUMENT_NAME_AND_VALUE, ArgumentType.string()).equals(CONFIRM_ARGUMENT_NAME_AND_VALUE)) {
            portalController.deactivatePortal(player);
        } else {
            msg.sendWarning(player, "Are you sure you want to deactivate this portal?");
            msg.sendWarning(player, "This action will also delete the LasersEnigma area corresponding to this portal and the blocks related to it. This action is irreversible.");
            msg.sendWarning(player, "Please confirm the deactivation of the portal by adding 'confirm' at the end of the command.");
        }
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Deactivate a portal";
    }
}
