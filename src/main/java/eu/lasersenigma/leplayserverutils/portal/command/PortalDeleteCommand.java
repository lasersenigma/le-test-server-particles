package eu.lasersenigma.leplayserverutils.portal.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class PortalDeleteCommand extends Command<Player> {
    private final PortalController portalController;

    @Inject
    public PortalDeleteCommand(PortalController portalController) {
        super(Player.class, "delete");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.portalController = portalController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        this.portalController.deletePortal(player);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Deletes a portal within 3 blocks range";
    }
}
