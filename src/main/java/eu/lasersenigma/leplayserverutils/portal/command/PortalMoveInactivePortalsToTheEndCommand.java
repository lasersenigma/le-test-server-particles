package eu.lasersenigma.leplayserverutils.portal.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class PortalMoveInactivePortalsToTheEndCommand extends Command<Player> {
    private final PortalController portalController;

    @Inject
    public PortalMoveInactivePortalsToTheEndCommand(PortalController portalController) {
        super(Player.class, "moveInactivePortalsToSpawnEnd");
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.portalController = portalController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        portalController.moveInactivePortalsToSpawnEnd(player);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Move each inactive portal after every active portal";
    }
}
