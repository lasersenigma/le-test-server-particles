package eu.lasersenigma.leplayserverutils.portal.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class PortalRoomOderInsertCommand extends Command<Player> {

    private static final String ORIGIN_PORTAL_ROOM_DESCRIPTION = "originPortalRoomDescription";

    private static final String DESTINATION_PORTAL_ROOM_DESCRIPTION = "destinationPortalRoomDescription";

    private final PortalController portalController;

    private final ConfigManager configManager;

    @Inject
    public PortalRoomOderInsertCommand(PortalController portalController, ConfigManager configManager) {
        super(Player.class, "orderInsert");
        this.addArgument(ORIGIN_PORTAL_ROOM_DESCRIPTION, true, ArgumentType.string());
        this.addArgument(DESTINATION_PORTAL_ROOM_DESCRIPTION, true, ArgumentType.string());
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.portalController = portalController;
        this.configManager = configManager;
    }


    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        final boolean isFirstArg = currentArgumentName.equals(ORIGIN_PORTAL_ROOM_DESCRIPTION);
        final boolean isSecondArg = currentArgumentName.equals(DESTINATION_PORTAL_ROOM_DESCRIPTION);
        if (isFirstArg || isSecondArg) {
            String currentArgumentValue;
            if (isFirstArg) {
                currentArgumentValue = this.getArgumentValue(executor, ORIGIN_PORTAL_ROOM_DESCRIPTION, ArgumentType.string());
            } else {
                currentArgumentValue = this.getArgumentValue(executor, DESTINATION_PORTAL_ROOM_DESCRIPTION, ArgumentType.string());
            }
            final Set<String> roomsDescriptionSet = configManager.get()
                    .getPortals().stream()
                    .filter(Portal::isActive)
                    .map(portal -> portal.getRoom().getDescription())
                    .collect(Collectors.toSet());
            super.setAutoCompleteValuesArg(
                    currentArgumentName,
                    CommandUtils.buildStringAutocompleteValues(roomsDescriptionSet, currentArgumentValue));

        }
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String originPortalRoomDescription = this.getArgumentValue(player, ORIGIN_PORTAL_ROOM_DESCRIPTION, ArgumentType.string());
        String destinationPortalRoomDescription = this.getArgumentValue(player, DESTINATION_PORTAL_ROOM_DESCRIPTION, ArgumentType.string());
        return portalController.insertRoomInPortalAndShiftNextLobbyPortalsRooms(player, originPortalRoomDescription, destinationPortalRoomDescription);
    }

    @Override
    protected String description(Player executor) {
        return "Change the portals destination (the origin room will be the new destination of the destination room's portal). " +
                "The current destination will become the destination of the next lobby's portal and so on until all portal destinations have been shifted. " +
                "This can only be used when destination portal's is in lobby.";
    }
}
