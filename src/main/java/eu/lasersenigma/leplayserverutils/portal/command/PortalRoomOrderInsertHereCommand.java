package eu.lasersenigma.leplayserverutils.portal.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class PortalRoomOrderInsertHereCommand extends Command<Player> {

    private static final String ORIGIN_PORTAL_ROOM_DESCRIPTION = "originPortalRoomDescription";

    private final PortalController portalController;

    private final ConfigManager configManager;

    @Inject
    public PortalRoomOrderInsertHereCommand(PortalController portalController, ConfigManager configManager) {
        super(Player.class, "orderInsertHere");
        this.addArgument(ORIGIN_PORTAL_ROOM_DESCRIPTION, true, ArgumentType.string());
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.portalController = portalController;
        this.configManager = configManager;
    }


    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(ORIGIN_PORTAL_ROOM_DESCRIPTION)) {
            String currentArgumentValue = getArgumentValue(executor, ORIGIN_PORTAL_ROOM_DESCRIPTION, ArgumentType.string());
            final Set<String> roomsDescriptionSet = configManager.get()
                    .getPortals().stream()
                    .filter(Portal::isActive)
                    .map(portal -> portal.getRoom().getDescription())
                    .collect(Collectors.toSet());
            super.setAutoCompleteValuesArg(
                    currentArgumentName,
                    CommandUtils.buildStringAutocompleteValues(roomsDescriptionSet, currentArgumentValue));

        }
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String originRoomDescription = this.getArgumentValue(player, ORIGIN_PORTAL_ROOM_DESCRIPTION, ArgumentType.string());
        return portalController.insertRoomInNearbyPortalAndShiftNextLobbyPortalsRooms(player, originRoomDescription);
    }

    @Override
    protected String description(Player executor) {
        return "Move the given portal at the given destination";
    }
}
