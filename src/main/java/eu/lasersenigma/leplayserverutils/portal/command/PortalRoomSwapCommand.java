package eu.lasersenigma.leplayserverutils.portal.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class PortalRoomSwapCommand extends Command<Player> {
    public static final String FIRST_PORTAL_ROOM_DESCRIPTION = "firstPortalRoomDescription";
    public static final String SECOND_PORTAL_ROOM_DESCRIPTION = "secondPortalRoomDescription";

    private final PortalController portalController;

    private final ConfigManager configManager;

    @Inject
    public PortalRoomSwapCommand(PortalController portalController, ConfigManager configManager) {
        super(Player.class, "swapRoom");
        this.addArgument(FIRST_PORTAL_ROOM_DESCRIPTION, true, ArgumentType.string());
        this.addArgument(SECOND_PORTAL_ROOM_DESCRIPTION, true, ArgumentType.string());
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.portalController = portalController;
        this.configManager = configManager;
    }


    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        final boolean isFirstArg = currentArgumentName.equals(FIRST_PORTAL_ROOM_DESCRIPTION);
        final boolean isSecondArg = currentArgumentName.equals(SECOND_PORTAL_ROOM_DESCRIPTION);
        if (isFirstArg || isSecondArg) {
            String currentArgumentValue;
            if (isFirstArg) {
                currentArgumentValue = this.getArgumentValue(executor, FIRST_PORTAL_ROOM_DESCRIPTION, ArgumentType.string());
            } else {
                currentArgumentValue = this.getArgumentValue(executor, SECOND_PORTAL_ROOM_DESCRIPTION, ArgumentType.string());
            }
            final Set<String> roomsDescriptionSet = configManager.get()
                    .getPortals().stream()
                    .filter(Portal::isActive)
                    .map(portal -> portal.getRoom().getDescription())
                    .collect(Collectors.toSet());
            super.setAutoCompleteValuesArg(
                    currentArgumentName,
                    CommandUtils.buildStringAutocompleteValues(roomsDescriptionSet, currentArgumentValue));

        }
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String firstPortalRoomDescription = this.getArgumentValue(player, FIRST_PORTAL_ROOM_DESCRIPTION, ArgumentType.string());
        String secondPortalRoomDescription = this.getArgumentValue(player, SECOND_PORTAL_ROOM_DESCRIPTION, ArgumentType.string());
        return portalController.swapRoomsBetweenPortals(player, firstPortalRoomDescription, secondPortalRoomDescription);
    }

    @Override
    protected String description(Player executor) {
        return "Swap two portals destination (the room linked to each portal).";
    }
}
