package eu.lasersenigma.leplayserverutils.portal.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class PortalTeleportExitDestinationCommand extends Command<Player> {
    private final PortalController portalController;

    @Inject
    public PortalTeleportExitDestinationCommand(PortalController portalController) {
        super(Player.class, "setTeleportExitDestination");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.portalController = portalController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        this.portalController.setTeleportExitDestination(player);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Defines where players will arrive when comming back from the portal. The portal must be within a 3 blocks range.";
    }
}
