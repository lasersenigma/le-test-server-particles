package eu.lasersenigma.leplayserverutils.portal.dto;

import eu.lasersenigma.leplayserverutils.room.dto.RoomDifficulty;
import lombok.*;
import net.kyori.adventure.text.Component;
import org.bukkit.Location;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EntranceActiveTeleporterDTO extends EntranceTeleporterDTO {

    private Component title;

    private Component subtitle;

    private Location particleAnimLocation;

    private Location destination;

    private String itemName;

    private List<String> itemDescription;

    private RoomDifficulty roomDifficulty;

    private List<String> tagsNameList;

    private int nbPlayersInside;
}
