package eu.lasersenigma.leplayserverutils.portal.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EntranceInactiveTeleporterDTO extends EntranceTeleporterDTO {
}
