package eu.lasersenigma.leplayserverutils.portal.dto;

import lombok.*;
import org.bukkit.Location;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class EntranceTeleporterDTO extends TeleporterDTO {

    private Location hologramEntityLocation;

    private String hologramEntityName;

    private List<String> hologramDescription;
}
