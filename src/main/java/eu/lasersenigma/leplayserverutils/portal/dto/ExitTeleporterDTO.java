package eu.lasersenigma.leplayserverutils.portal.dto;

import lombok.*;
import org.bukkit.Location;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ExitTeleporterDTO extends TeleporterDTO {

    private Optional<EntranceActiveTeleporterDTO> nextPortalEntranceActiveTeleporterDTO;

    private Location particleAnimLocation;

    private Location destination;
}
