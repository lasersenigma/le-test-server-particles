package eu.lasersenigma.leplayserverutils.portal.dto;

import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import lombok.*;
import org.bukkit.Location;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public abstract class TeleporterDTO {

    private Location portalBlockLocation;

    private Portal portal;
}