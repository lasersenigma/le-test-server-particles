package eu.lasersenigma.leplayserverutils.portal.entity;

import com.google.gson.annotations.Expose;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import lombok.*;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Portal {

    public static final String PORTAL_WITH_AN_ACTIVE_ROOM_MUST_HAVE_A_TELEPORT_EXIT_DESTINATION =
            "A portal can not be activated without a teleportExitDestination. Stand where you want the player to be when he comes back from this portal and type \"/. portal setTeleportExitDestination\"";

    @Expose
    @Nullable
    private Integer order;

    @Expose
    private String worldName;

    @Expose
    @EqualsAndHashCode.Include
    private Vector location;

    @Expose
    @Nullable
    private Vector teleportExitDestination;

    @Expose
    private Room room;

    public Portal(Portal portal) {
        this.order = portal.getOrder();
        this.worldName = portal.getWorldName();
        this.location = portal.getLocation().clone();
        this.teleportExitDestination = portal.getTeleportExitDestination() == null ? null : portal.getTeleportExitDestination().clone();
        this.room = portal.getRoom() == null ? null : new Room(portal.getRoom());
    }

    public boolean isActive() {
        return room != null;
    }

    public void setRoom(Room room) {
        if (room != null && teleportExitDestination == null) {
            throw new IllegalArgumentException(PORTAL_WITH_AN_ACTIVE_ROOM_MUST_HAVE_A_TELEPORT_EXIT_DESTINATION);
        }
        this.room = room;
    }

    public void setTeleportExitDestination(Vector teleportExitDestination) {
        if (isActive() && teleportExitDestination == null) {
            throw new IllegalArgumentException(PORTAL_WITH_AN_ACTIVE_ROOM_MUST_HAVE_A_TELEPORT_EXIT_DESTINATION);
        }
        this.teleportExitDestination = teleportExitDestination;
    }


}
