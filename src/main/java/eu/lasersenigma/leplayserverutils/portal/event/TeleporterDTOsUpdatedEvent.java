package eu.lasersenigma.leplayserverutils.portal.event;

import eu.lasersenigma.leplayserverutils.portal.dto.TeleporterDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@AllArgsConstructor
@Getter
public class TeleporterDTOsUpdatedEvent extends Event {
    private static final HandlerList HANDLERS = new HandlerList();
    private final List<TeleporterDTO> teleporterDTOs;

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public @NotNull HandlerList getHandlers() {
        return HANDLERS;
    }
}
