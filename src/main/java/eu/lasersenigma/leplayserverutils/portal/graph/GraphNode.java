package eu.lasersenigma.leplayserverutils.portal.graph;

import java.util.List;

public abstract class GraphNode {

    protected List<GraphNode> nodes;

    protected GraphNode(List<GraphNode> nodes) {
        this.nodes = nodes;
    }

    public List<GraphNode> getSubNodes() {
        return nodes;
    }

}
