package eu.lasersenigma.leplayserverutils.portal.graph;

import java.util.ArrayList;
import java.util.List;

public class GraphNodeLobby extends GraphNode {


    public GraphNodeLobby(List<GraphNode> nodes) {
        super(nodes);
    }

    public GraphNodeLobby() {
        this(new ArrayList<>());
    }
}
