package eu.lasersenigma.leplayserverutils.portal.graph;

import eu.lasersenigma.leplayserverutils.room.entity.Room;

import java.util.ArrayList;
import java.util.List;

public class GraphNodeRoom extends GraphNode {

    private Room room;

    public GraphNodeRoom(List<GraphNode> nodes, Room room) {
        super(nodes);
        this.room = room;
    }

    public GraphNodeRoom(Room room) {
        this(new ArrayList<>(), room);
    }

    public Room getRoom() {
        return room;
    }
}
