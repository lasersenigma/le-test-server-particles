package eu.lasersenigma.leplayserverutils.portal.graph;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.Objects;

@Singleton
public class PortalGraphService {
    @Inject
    private RoomController roomController;

    public boolean doesPortalGraphContainsCycle(Config config, Portal portal) {
        if (portal.isActive()) {
            GraphNodeRoom portalNode = new GraphNodeRoom(portal.getRoom());
            return doesSubGraphContainsCycleRecursive(portalNode, config, portal.getRoom());
        }
        return false;
    }

    private boolean doesSubGraphContainsCycleRecursive(GraphNodeRoom currentNode, Config config, Room originRoom) {
        for (Portal portal : config.getPortals()) {
            if (!portal.isActive()) {
                continue;
            }
            Location portalLocation = portal.getLocation().toLocation(Objects.requireNonNull(Bukkit.getWorld(portal.getWorldName())));
            if (roomController.isInRoom(portalLocation, currentNode.getRoom())) {
                if (portal.getRoom().equals(originRoom)) {
                    return true;
                }
                final GraphNodeRoom subRoom = new GraphNodeRoom(portal.getRoom());
                currentNode.getSubNodes().add(subRoom);
                if (doesSubGraphContainsCycleRecursive(subRoom, config, originRoom)) {
                    return true;
                }
            }
        }
        return false;
    }
}
