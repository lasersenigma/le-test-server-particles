package eu.lasersenigma.leplayserverutils.portal.gui;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum TagItemPreset {
    SOLO(
            "cc8b8a933340819c08504efd39d4717cadab23e2344d3f5e3aa30b5cedb8d21e",
            "e0fe6614d9b78c55a921fb6bbe675ebc80f59e0f31e63ae3889ced5292541ccd"
    ),
    DUO(
            "d27ab35fe88b5f3daed79fbd1f83246c546d4969507abae1184fec5a2a229e79",
            "17756253dd87b5e561a5cdd359aaa71de051471cf7b6c8ab87ef8dab9c670c96"
    ),
    DODGE(
            "87803953fa56acd4244fb47fbb815457fe0fa91066482d511d3b5f88e9bd82f4",
            "5143f02833fa8bc806136745ee57f4415585ae3c026e828b6d4a2e4160793c9e"
    ),
    DEVELOPER(
            "6e67759533f08d837adcce95489bf49110e5ec2e0f1d3b43cc2c8d5966841be5",
            "3d202d6a8f1efed32e179b503496164d6e3e535ad7c15e3e394d7df6edcbd4cc"
    ),
    JUMP(
            "f62b46eaf4207ca4d28b1a8629e3b5fa392cd1b132a461be8bf52e5226d8b180",
            "c16ce5e8eb7310fa89879e799efbec0fe4f8f6d6362135e6fc42860168c9269e"
    );

    private final String headTextureActivated;
    private final String headTextureDeactivated;
}
