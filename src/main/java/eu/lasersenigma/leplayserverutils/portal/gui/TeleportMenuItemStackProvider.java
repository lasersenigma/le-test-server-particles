package eu.lasersenigma.leplayserverutils.portal.gui;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.leplayserverutils.common.gui.MenuItemStackProvider;
import eu.lasersenigma.leplayserverutils.portal.dto.EntranceActiveTeleporterDTO;
import eu.lasersenigma.leplayserverutils.room.dto.RoomDifficulty;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Singleton
public class TeleportMenuItemStackProvider {
    //navigation in teleport paginated pane
    //navigation in tag paginated pane
    private static final String TAGS_BUTTONS_PREVIOUS_PAGE_SKULL_TEXTURE_URL_SUFFIX = "69ea1d86247f4af351ed1866bca6a3040a06c68177c78e42316a1098e60fb7d3";
    private static final String TAGS_BUTTONS_NEXT_PAGE_SKULL_TEXTURE_URL_SUFFIX = "8271a47104495e357c3e8e80f511a9f102b0700ca9b88e88b795d33ff20105eb";

    //default tag textures
    private static final String DEFAULT_DEACTIVATED_TAG_TEXTURE_URL_SUFFIX = "1035c528036b384c53c9c8a1a125685e16bfb369c197cc9f03dfa3b835b1aa55";
    private static final String DEFAULT_ACTIVATED_TAG_TEXTURE_URL_SUFFIX = "fa2064ee92a08f7b8bcd80ecf2b3a581a29d46aad819e403b20055975971239a";

    //item names suffixes and colors
    private static final int CHECKED_COLOR = 0x00FF00;
    private static final int UNCHECKED_COLOR = 0xFF0000;
    private static final String CHECKED_SUFFIX = " ✓";
    private static final String UNCHECKED_SUFFIX = " ✗";

    @Inject
    private MenuItemStackProvider menuItemStackProvider;

    public ItemStack getTagItemStack(String tag, boolean isSelected) {
        final Optional<ItemStack> oItemStack = Arrays.stream(TagItemPreset.values())
                .filter(preset -> tag.toUpperCase(Locale.ROOT).contains(preset.name()))
                .findFirst()
                .map(preset -> isSelected ? preset.getHeadTextureActivated() : preset.getHeadTextureDeactivated())
                .map(ItemStackUtils::getCustomHead);

        final ItemStack itemStack = oItemStack.orElseGet(() -> {
            String headTextureUrl = isSelected ? DEFAULT_ACTIVATED_TAG_TEXTURE_URL_SUFFIX : DEFAULT_DEACTIVATED_TAG_TEXTURE_URL_SUFFIX;
            return ItemStackUtils.getCustomHead(headTextureUrl);
        });
        itemStack.editMeta(meta -> {
            if (isSelected) {
                meta.addEnchant(Enchantment.LUCK, 1, true);
                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                meta.displayName(Component.text(tag + CHECKED_SUFFIX, TextColor.color(CHECKED_COLOR)));
                meta.lore(List.of(Component.text("Click to deselect this tag.")));
            } else {
                meta.displayName(Component.text(tag + UNCHECKED_SUFFIX, TextColor.color(UNCHECKED_COLOR)));
                meta.lore(List.of(Component.text("Click to select this tag.")));
            }
        });

        return itemStack;
    }


    public @NotNull ItemStack getDifficultyItemStack(RoomDifficulty difficulty, boolean isSelected) {
        String headTexture = isSelected ? difficulty.getHeadTextureActivated() : difficulty.getHeadTextureDeactivated();
        final ItemStack item = ItemStackUtils.getCustomHead(headTexture);
        item.editMeta(meta -> {
            if (isSelected) {
                meta.addEnchant(Enchantment.LUCK, 1, true);
                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                meta.displayName(difficulty.getDisplayNameComponent()
                        .append(Component.text(CHECKED_SUFFIX, TextColor.color(CHECKED_COLOR))));
                meta.lore(List.of(Component.text("Click to deselect this difficulty.")));
            } else {
                meta.displayName(difficulty.getDisplayNameComponent()
                        .append(Component.text(UNCHECKED_SUFFIX, TextColor.color(UNCHECKED_COLOR))));
                meta.lore(List.of(Component.text("Click to select this difficulty.")));
            }

        });

        return item;
    }

    public @NotNull ItemStack getTeleportNavNextPage() {
        return menuItemStackProvider.getNextPageItemStack();
    }

    public @NotNull ItemStack getTeleportNavPrevPage() {
        return menuItemStackProvider.getPreviousPageItemStack();
    }

    public @NotNull ItemStack getRoomTeleport(EntranceActiveTeleporterDTO puzzleTeleportData) {
        ItemStack itemStack = ItemsFactory.getInstance().getItemStack(puzzleTeleportData.getRoomDifficulty().getItem());
        ItemStackUtils.setName(itemStack, puzzleTeleportData.getItemName());
        ItemStackUtils.setLore(itemStack, puzzleTeleportData.getItemDescription());
        return itemStack;
    }

    public @NotNull ItemStack getTagNavNextPage() {
        ItemStack nextPageItemStack = ItemStackUtils.getCustomHead(TAGS_BUTTONS_NEXT_PAGE_SKULL_TEXTURE_URL_SUFFIX);
        ItemStackUtils.setName(nextPageItemStack, "Next Tags");
        ItemStackUtils.setLore(nextPageItemStack, "See the next tags");
        return nextPageItemStack;
    }

    public @NotNull ItemStack getTagNavPrevPage() {
        ItemStack previousPageItemStack = ItemStackUtils.getCustomHead(TAGS_BUTTONS_PREVIOUS_PAGE_SKULL_TEXTURE_URL_SUFFIX);
        ItemStackUtils.setName(previousPageItemStack, "Previous Tags");
        ItemStackUtils.setLore(previousPageItemStack, "See the previous tags");
        return previousPageItemStack;
    }
}
