package eu.lasersenigma.leplayserverutils.portal.gui;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.PaginatedPane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.gui.MenuItemStackProvider;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import eu.lasersenigma.leplayserverutils.portal.dto.EntranceActiveTeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.provider.TeleporterDTOsProvider;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.room.dto.RoomDifficulty;
import eu.lasersenigma.leplayserverutils.tag.TagController;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Consumer;

@Singleton
public class TeleportMenuProvider {


    @Inject
    private TeleporterDTOsProvider teleporterDTOsProvider;

    @Inject
    private PortalController portalController;

    @Inject
    private RoomController roomController;

    @Inject
    private TagController tagController;

    @Inject
    private TeleportMenuStateManager teleportMenuStateManager;

    @Inject
    private TeleportMenuItemStackProvider teleportMenuItemStackProvider;

    @Inject
    private MenuItemStackProvider menuItemStackProvider;

    public ChestGui getGui(Player player) {

        ChestGui gui = new ChestGui(6, "Puzzle selection");
        gui.setOnGlobalClick(event -> event.setCancelled(true));
        gui.addPane(getBackground());

        final PaginatedPane paginatedTeleportPane = getPaginatedTeleportPane(player);
        gui.addPane(paginatedTeleportPane);

        gui.addPane(getTeleportNavigationPane(player, paginatedTeleportPane, gui));

        final PaginatedPane paginatedTagPane = getPaginatedTagPane(player, paginatedTeleportPane, gui);
        gui.addPane(paginatedTagPane);

        gui.addPane(getTagNavigationPane(player, paginatedTagPane, gui));

        gui.addPane(getDifficultyPane(player, paginatedTeleportPane, gui));

        return gui;
    }

    private @NotNull PaginatedPane getPaginatedTagPane(Player player, PaginatedPane paginatedTeleportButtons, ChestGui gui) {
        PaginatedPane tagPaginatedPane = new PaginatedPane(1, 0, 7, 1);
        tagPaginatedPane.setPriority(Pane.Priority.HIGHEST);

        final List<GuiItem> tagGuiItems = getTagGuiItems(player, paginatedTeleportButtons, gui, tagPaginatedPane);
        tagPaginatedPane.populateWithGuiItems(tagGuiItems);
        return tagPaginatedPane;
    }

    private @NotNull List<GuiItem> getTagGuiItems(Player player, PaginatedPane paginatedTeleportButtons, ChestGui gui, PaginatedPane tagPaginatedPane) {
        final List<GuiItem> tagGuiItems = new ArrayList<>();
        final List<String> tagsList = tagController.getOrderedTags();

        final TeleportMenuState playerTeleportMenuState = teleportMenuStateManager.getTeleportMenuState(player.getUniqueId());

        for (int i = 0; i < tagsList.size(); i++) {
            String tag = tagsList.get(i);

            boolean isSelected = playerTeleportMenuState.isTagSelected(tag);

            final ItemStack item = teleportMenuItemStackProvider.getTagItemStack(tag, isSelected);
            final GuiItem tagGuiItem = new GuiItem(item, getTagClickConsumer(player, gui, paginatedTeleportButtons, tag, tagPaginatedPane, i));

            tagGuiItems.add(tagGuiItem);
        }
        return tagGuiItems;
    }

    private @NotNull Consumer<InventoryClickEvent> getTagClickConsumer(Player player, ChestGui gui, PaginatedPane paginatedTeleportButtons, String tag, PaginatedPane tagPaginatedPane, int finalI) {
        return event -> {
            teleportMenuStateManager.getTeleportMenuState(player.getUniqueId()).toggleTagSelected(tag);
            //update tags display (to check/uncheck the tag)
            if (!tagPaginatedPane.getItems().isEmpty()) {
                tagPaginatedPane.setPage(0);
            }
            tagPaginatedPane.clear();
            tagPaginatedPane.populateWithGuiItems(getTagGuiItems(player, paginatedTeleportButtons, gui, tagPaginatedPane));
            //update teleport buttons (to filter out teleporters that do not have the selected tags)
            if (!paginatedTeleportButtons.getItems().isEmpty()) {
                paginatedTeleportButtons.setPage(0);
            }
            paginatedTeleportButtons.clear();
            paginatedTeleportButtons.populateWithGuiItems(getTeleportGuiItems(player));
            gui.update();
        };
    }

    private @NotNull Pane getTagNavigationPane(Player player, PaginatedPane paginatedTagPane, ChestGui gui) {
        StaticPane navigation = new StaticPane(0, 0, 9, 1);
        navigation.setPriority(Pane.Priority.NORMAL);

        final ItemStack previousPageItemStack = teleportMenuItemStackProvider.getTagNavPrevPage();
        navigation.addItem(new GuiItem(previousPageItemStack, event -> {
            if (paginatedTagPane.getPage() > 0) {
                paginatedTagPane.setPage(paginatedTagPane.getPage() - 1);

                gui.update();
            }
        }), 0, 0);

        final ItemStack nextPageItemStack = teleportMenuItemStackProvider.getTagNavNextPage();
        navigation.addItem(new GuiItem(nextPageItemStack, event -> {
            if (paginatedTagPane.getPage() < paginatedTagPane.getPages() - 1) {
                paginatedTagPane.setPage(paginatedTagPane.getPage() + 1);

                gui.update();
            }
        }), 8, 0);

        return navigation;
    }


    private @NotNull PaginatedPane getPaginatedTeleportPane(Player player) {
        PaginatedPane teleportPaginatedPane = new PaginatedPane(1, 1, 8, 4);
        teleportPaginatedPane.setPriority(Pane.Priority.HIGHEST);
        teleportPaginatedPane.populateWithGuiItems(getTeleportGuiItems(player));
        return teleportPaginatedPane;
    }

    private @NotNull List<GuiItem> getTeleportGuiItems(Player player) {
        final List<GuiItem> menuItems = new ArrayList<>();

        final List<EntranceActiveTeleporterDTO> teleporterDTOs = teleporterDTOsProvider.getTeleporterDTOs(EntranceActiveTeleporterDTO.class);

        // filter out teleporters that have not a selected difficulty
        final TeleportMenuState playerTeleportMenuState = teleportMenuStateManager.getTeleportMenuState(player.getUniqueId());

        Set<RoomDifficulty> selectedDifficulties = playerTeleportMenuState.getSelectedDifficulties();
        teleporterDTOs.removeIf(teleporterDTO -> !selectedDifficulties.contains(teleporterDTO.getRoomDifficulty()));

        // filter out teleporters that do not have the selected tags
        // Since tags are not mutually exclusive, we need to keep teleporters that have every selected tags
        // Since tags are not mandatory, we need to keep teleporters that have no tags if no tags are selected OR if all tags are selected
        if (!playerTeleportMenuState.areAllTagsSelected()) {
            Set<String> selectedTags = playerTeleportMenuState.getSelectedTags();
            if (!selectedTags.isEmpty()) {
                teleporterDTOs.removeIf(teleporterDTO -> !new HashSet<>(teleporterDTO.getTagsNameList()).containsAll(selectedTags));
            }
        }

        for (EntranceActiveTeleporterDTO puzzleTeleportData : teleporterDTOs) {
            final ItemStack itemStack = teleportMenuItemStackProvider.getRoomTeleport(puzzleTeleportData);
            menuItems.add(new GuiItem(itemStack, event -> {
                portalController.teleportInRoom(
                        player,
                        puzzleTeleportData
                );
                player.closeInventory();
            }));
        }
        return menuItems;
    }

    private @NotNull StaticPane getTeleportNavigationPane(Player player, PaginatedPane pages, ChestGui gui) {
        StaticPane navigation = new StaticPane(1, 5, 8, 1);
        navigation.setPriority(Pane.Priority.NORMAL);

        final ItemStack previousPageItemStack = teleportMenuItemStackProvider.getTeleportNavPrevPage();
        navigation.addItem(new GuiItem(previousPageItemStack, event -> {
            if (pages.getPage() > 0) {
                pages.setPage(pages.getPage() - 1);

                gui.update();
            }
        }), 0, 0);

        final ItemStack nextPageItemStack = teleportMenuItemStackProvider.getTeleportNavNextPage();
        navigation.addItem(new GuiItem(nextPageItemStack, event -> {
            if (pages.getPage() < pages.getPages() - 1) {
                pages.setPage(pages.getPage() + 1);

                gui.update();
            }
        }), 7, 0);

        return navigation;
    }

    private @NotNull OutlinePane getDifficultyPane(Player player, PaginatedPane paginatedTeleportButtons, ChestGui gui) {
        OutlinePane difficultySelector = new OutlinePane(0, 1, 1, 5);
        difficultySelector.setPriority(Pane.Priority.NORMAL);

        final TeleportMenuState playerTeleportMenuState = teleportMenuStateManager.getTeleportMenuState(player.getUniqueId());
        final List<GuiItem> difficultyGuiItems = getDifficultyGuiItems(player, paginatedTeleportButtons, gui, playerTeleportMenuState, difficultySelector);
        difficultyGuiItems.forEach(difficultySelector::addItem);

        return difficultySelector;
    }

    private @NotNull List<GuiItem> getDifficultyGuiItems(Player player, PaginatedPane paginatedTeleportButtons, ChestGui gui, TeleportMenuState playerTeleportMenuState, OutlinePane difficultySelector) {
        return Arrays.stream(RoomDifficulty.values())
                .map(difficulty -> {
                    boolean isSelected = playerTeleportMenuState.isDifficultySelected(difficulty);
                    final ItemStack item = teleportMenuItemStackProvider.getDifficultyItemStack(difficulty, isSelected);
                    return new GuiItem(item, getDifficultyClickConsumer(player, gui, paginatedTeleportButtons, difficulty, difficultySelector));
                })
                .toList();
    }

    private @NotNull Consumer<InventoryClickEvent> getDifficultyClickConsumer(Player player, ChestGui gui, @NotNull PaginatedPane paginatedTeleportButtons, RoomDifficulty difficulty, OutlinePane difficultySelector) {
        return event -> {
            final TeleportMenuState teleportMenuState = teleportMenuStateManager.getTeleportMenuState(player.getUniqueId());
            teleportMenuState.toggleDifficultySelected(difficulty);
            difficultySelector.getItems().clear();

            final TeleportMenuState playerTeleportMenuState = teleportMenuStateManager.getTeleportMenuState(player.getUniqueId());
            final List<GuiItem> difficultyGuiItems = getDifficultyGuiItems(player, paginatedTeleportButtons, gui, playerTeleportMenuState, difficultySelector);
            difficultyGuiItems.forEach(difficultySelector::addItem);

            if (!paginatedTeleportButtons.getItems().isEmpty()) {
                paginatedTeleportButtons.setPage(0);
            }
            paginatedTeleportButtons.clear();
            paginatedTeleportButtons.populateWithGuiItems(getTeleportGuiItems(player));
            gui.update();
        };
    }


    private @NotNull OutlinePane getBackground() {
        OutlinePane background = new OutlinePane(0, 0, 9, 6);
        background.addItem(new GuiItem(new ItemStack(menuItemStackProvider.getBackgroundItemStack())));
        background.setRepeat(true);
        background.setPriority(Pane.Priority.LOWEST);
        return background;
    }
}
