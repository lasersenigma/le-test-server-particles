package eu.lasersenigma.leplayserverutils.portal.gui;

import eu.lasersenigma.leplayserverutils.room.dto.RoomDifficulty;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class TeleportMenuState {
    private final Map<RoomDifficulty, Boolean> roomDifficultyIsSelectedMap;

    private final Map<String, Boolean> tagIsSelectedMap;

    public TeleportMenuState(Collection<String> tags) {
        roomDifficultyIsSelectedMap = Arrays.stream(RoomDifficulty.values())
                .collect(java.util.stream.Collectors.toMap(roomDifficulty -> roomDifficulty, roomDifficulty -> true));
        tagIsSelectedMap = tags.stream()
                .collect(Collectors.toMap(tag -> tag, tag -> true));
    }

    public boolean isDifficultySelected(RoomDifficulty roomDifficulty) {
        return roomDifficultyIsSelectedMap.get(roomDifficulty);
    }

    public boolean isTagSelected(String tag) {
        return tagIsSelectedMap.get(tag);
    }

    public void toggleTagSelected(String tag) {
        if (areAllTagsSelected()) {
            tagIsSelectedMap.replaceAll((k, v) -> false);
        }
        tagIsSelectedMap.put(tag, !tagIsSelectedMap.get(tag));
    }

    public void toggleDifficultySelected(RoomDifficulty roomDifficulty) {
        if (areAllDifficultiesSelected()) {
            roomDifficultyIsSelectedMap.replaceAll((k, v) -> false);
        }
        roomDifficultyIsSelectedMap.put(roomDifficulty, !roomDifficultyIsSelectedMap.get(roomDifficulty));
    }

    public Set<String> getSelectedTags() {
        return tagIsSelectedMap.entrySet().stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    public Set<RoomDifficulty> getSelectedDifficulties() {
        return roomDifficultyIsSelectedMap.entrySet().stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    public boolean areAllTagsSelected() {
        return tagIsSelectedMap.values().stream().allMatch(isSelected -> isSelected);
    }

    public boolean areAllDifficultiesSelected() {
        return roomDifficultyIsSelectedMap.values().stream().allMatch(isSelected -> isSelected);
    }

    public void updateTags(@NotNull Set<String> tags) {
        //add new tags
        tags.forEach(tag -> tagIsSelectedMap.putIfAbsent(tag, true));
        //remove old tags
        tagIsSelectedMap.entrySet().removeIf(tagBooleanEntry -> !tags.contains(tagBooleanEntry.getKey()));
    }
}
