package eu.lasersenigma.leplayserverutils.portal.gui;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.tag.TagController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Singleton
public class TeleportMenuStateManager implements LEPSUListener {

    @Inject
    private TagController tagController;

    private final Map<UUID, TeleportMenuState> teleportMenuStates = new HashMap<>();

    public TeleportMenuState getTeleportMenuState(UUID playerUuid) {
        return teleportMenuStates.computeIfAbsent(playerUuid, uuid -> new TeleportMenuState(tagController.getTags()));
    }

    public void onConfigReload() {
        teleportMenuStates.forEach((uuid, teleportMenuState) -> teleportMenuState.updateTags(tagController.getTags()));
    }

}
