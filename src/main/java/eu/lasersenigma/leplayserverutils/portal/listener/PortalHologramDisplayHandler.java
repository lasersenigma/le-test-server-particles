package eu.lasersenigma.leplayserverutils.portal.listener;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.decentsoftware.holograms.api.DHAPI;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.portal.dto.EntranceTeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.event.TeleporterDTOsUpdatedEvent;
import eu.lasersenigma.leplayserverutils.portal.provider.TeleporterDTOsProvider;
import org.bukkit.event.EventHandler;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class PortalHologramDisplayHandler implements LEPSUListener, JavaPluginEnableEventHandler {

    private final TeleporterDTOsProvider teleporterDTOsProvider;
    Set<String> currentHologramEntityNames = new HashSet<>();

    @Inject
    public PortalHologramDisplayHandler(TeleporterDTOsProvider teleporterDTOsProvider) {
        this.teleporterDTOsProvider = teleporterDTOsProvider;
    }

    private static void createHologram(EntranceTeleporterDTO entranceTeleporterDTO) {
        DHAPI.createHologram(
                entranceTeleporterDTO.getHologramEntityName(),
                entranceTeleporterDTO.getHologramEntityLocation(),
                false,
                entranceTeleporterDTO.getHologramDescription()
        );
    }

    @Override
    public void enable() {
        updateHolograms();
    }

    @EventHandler
    public void onTeleporterDTOsUpdated(TeleporterDTOsUpdatedEvent event) {
        updateHolograms();
    }

    private void updateHolograms() {
        List<EntranceTeleporterDTO> teleporterDTOs = teleporterDTOsProvider.getTeleporterDTOs(EntranceTeleporterDTO.class);
        // find holograms that were here before but are not in new teleporterDTOs
        currentHologramEntityNames.stream()
                .filter(currentHologramEntityName -> teleporterDTOs.stream()
                        .noneMatch(teleporterDTO -> teleporterDTO.getHologramEntityName().equals(currentHologramEntityName)))
                // delete them
                .forEach(DHAPI::removeHologram);

        // find holograms that are in new teleporterDTOs but were not here before
        teleporterDTOs.stream()
                .filter(teleporterDTO -> currentHologramEntityNames.stream()
                        .noneMatch(currentHologramEntityName -> currentHologramEntityName.equals(teleporterDTO.getHologramEntityName())))
                // create them
                .forEach(PortalHologramDisplayHandler::createHologram);

        // find holograms that were here before and are in new teleporterDTOs
        teleporterDTOs.stream()
                .filter(teleporterDTO -> currentHologramEntityNames.stream()
                        .anyMatch(currentHologramEntityName -> currentHologramEntityName.equals(teleporterDTO.getHologramEntityName())))
                // update them : delete then re-create
                .forEach(entranceTeleporterDTO -> {
                    DHAPI.removeHologram(entranceTeleporterDTO.getHologramEntityName());
                    createHologram(entranceTeleporterDTO);
                });

        // Update currentHologramEntityNames
        currentHologramEntityNames = teleporterDTOs.stream()
                .map(EntranceTeleporterDTO::getHologramEntityName)
                .collect(Collectors.toSet());
    }
}
