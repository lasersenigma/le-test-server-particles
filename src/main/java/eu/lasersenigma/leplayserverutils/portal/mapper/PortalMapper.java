package eu.lasersenigma.leplayserverutils.portal.mapper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.utils.LasersEnigmaExecutor;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.room.dto.RoomDifficulty;
import eu.lasersenigma.leplayserverutils.room.mapper.RoomMapper;
import net.kyori.adventure.text.Component;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Singleton
public class PortalMapper {

    private static final Vector BLOCK_CENTER_VECTOR_X_Z = new Vector(0.5, 0, 0.5);
    private static final String SUBTITLE_SEPARATOR = " - ";

    private final RoomMapper roomMapper;

    private final WorldVectorToLocationTransformer vectorToLocation;

    private final LasersEnigmaExecutor lasersEnigmaExecutor;

    @Inject
    public PortalMapper(RoomMapper roomMapper, WorldVectorToLocationTransformer vectorToLocation, LasersEnigmaExecutor lasersEnigmaExecutor) {
        this.roomMapper = roomMapper;
        this.vectorToLocation = vectorToLocation;
        this.lasersEnigmaExecutor = lasersEnigmaExecutor;
    }

    public static float getYaw(Location from, Vector eyeDirection) {
        double angle = Math.atan2(-eyeDirection.getX(), eyeDirection.getZ());
        return (float) Math.toDegrees(angle);
    }

    public List<String> toHologramDescription(Portal portal, Config config) {
        return toHologramDescription(portal, 0, config);
    }

    public List<String> toHologramDescription(Portal portal, int nbPlayersInside, Config config) {
        ArrayList<String> description = new ArrayList<>();
        if (portal.getOrder() != null) {
            description.add("Puzzle #" + portal.getOrder());
        }
        if (!portal.isActive()) {
            description.add("Inactive");
        } else {
            description.addAll(roomMapper.getDescription(portal.getRoom(), nbPlayersInside, config));
        }
        return description;
    }

    public String toPortalHologramEntityName(Portal portal) {
        return String.format("%d-%d-%d", portal.getLocation().getBlockX(), portal.getLocation().getBlockY(), portal.getLocation().getBlockZ());
    }

    public Location toHologramEntityLocation(Portal portal) {
        return vectorToLocation.transform(portal.getWorldName(), portal.getLocation().clone().add(new Vector(0.5, 3.2, 0.5)));
    }

    public Location toPortalBlockLocation(Portal portal) {
        return vectorToLocation.transform(portal.getWorldName(), portal.getLocation());
    }

    public Location toParticleAnimLocation(Portal portal) {
        return fromBlockLocationToParticleAnimLocation(toPortalBlockLocation(portal));
    }

    public Location fromBlockLocationToParticleAnimLocation(Location blockLocation) {
        return blockLocation.clone().add(new Vector(0.5, 2.2, 0.5));
    }

    public Location getPortalExitWithYawPitch(Portal portal) {
        final Vector direction = portal.getTeleportExitDestination().clone().subtract(portal.getLocation()).normalize();
        Location lobbySpawnAfterExit = getPortalExit(portal);
        lobbySpawnAfterExit.setYaw(getYaw(lobbySpawnAfterExit, direction));
        lobbySpawnAfterExit.setPitch(0f);
        return lobbySpawnAfterExit;
    }

    private Location getPortalExit(Portal portal) {
        return vectorToLocation.transform(portal.getWorldName(), portal.getTeleportExitDestination());
    }

    public Location toRoomSpawnLocationWithYawPitch(Portal portal) {
        final Location roomSpawn = vectorToLocation.transform(portal.getRoom().getWorldName(), portal.getRoom().getSpawnLocation().clone().add(BLOCK_CENTER_VECTOR_X_Z));
        final Vector roomMinCorner = portal.getRoom().getMinCorner();
        final Vector roomMaxCorner = portal.getRoom().getMaxCorner();
        final Vector roomCenter = roomMinCorner.clone()
                .add(roomMaxCorner)
                .multiply(0.5);
        final Vector direction = roomCenter.subtract(roomSpawn.toVector());

        return new Location(
                roomSpawn.getWorld(), roomSpawn.getX(), roomSpawn.getY(), roomSpawn.getZ(),
                getYaw(roomSpawn, direction),
                0f);
    }

    public Component toSubtitle(Portal portal, Config config) {
        Component subtitle = toRoomDifficulty(portal).getDisplayNameComponent();

        final Optional<String> oAuthorString = roomMapper.getAuthorString(portal.getRoom(), config);
        if (oAuthorString.isPresent()) {
            subtitle = subtitle.append(Component.text(SUBTITLE_SEPARATOR)).append(Component.text(oAuthorString.get()));
        }

        final Optional<String> oTagsString = roomMapper.getTagsString(portal.getRoom(), config);
        if (oTagsString.isPresent()) {
            subtitle = subtitle.append(Component.text(SUBTITLE_SEPARATOR)).append(Component.text(oTagsString.get()));
        }

        return subtitle;
    }

    public Component toTitle(Portal portal) {
        StringBuilder titleSb = new StringBuilder();
        if (portal.getOrder() != null) {
            titleSb.append("#").append(portal.getOrder()).append(" ");
        }
        titleSb.append(portal.getRoom().getDescription());
        return Component.text(titleSb.toString());
    }

    public List<String> toItemLore(Portal portal, int nbPlayersInside, Config config) {
        List<String> lore = new ArrayList<>();
        if (portal.getOrder() != null) {
            lore.add("Puzzle #" + portal.getOrder() + " " + toRoomDifficulty(portal).getDisplayName());
        }
        roomMapper.getAuthorString(portal.getRoom(), config).ifPresent(lore::add);

        roomMapper.getTagsString(portal.getRoom(), config).ifPresent(lore::add);

        if (nbPlayersInside > 0) {
            lore.add(nbPlayersInside + " player" + (nbPlayersInside > 1 ? "s" : "") + " inside");
        }
        return lore;
    }

    public String toItemName(Portal portal) {
        return portal.getRoom().getDescription();
    }

    public RoomDifficulty toRoomDifficulty(Portal portal) {
        return RoomDifficulty.fromPercent(portal.getRoom().getDifficulty());
    }

    public List<String> toTagsNameList(Portal portal, Config config) {
        return roomMapper.getTagsNameList(portal.getRoom(), config);
    }

    public Optional<Integer> getNbPlayersInside(Location lasersEnigmaRoomSpawn) {
        return lasersEnigmaExecutor.getNbPlayersInsideRoom(lasersEnigmaRoomSpawn);
    }
}
