package eu.lasersenigma.leplayserverutils.portal.mapper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import eu.lasersenigma.leplayserverutils.portal.dto.EntranceActiveTeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.dto.EntranceInactiveTeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.dto.ExitTeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.dto.TeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import org.bukkit.Location;
import org.jetbrains.annotations.NotNull;

import java.util.*;

@Singleton
public class PortalToTeleporterDTOMapper {

    private final PortalMapper portalMapper;

    private final WorldVectorToLocationTransformer vectorToLocationTransformer;

    @Inject
    public PortalToTeleporterDTOMapper(PortalMapper portalMapper, WorldVectorToLocationTransformer vectorToLocationTransformer) {
        this.portalMapper = portalMapper;
        this.vectorToLocationTransformer = vectorToLocationTransformer;
    }

    private static @NotNull Comparator<TeleporterDTO> getTeleporterDTOComparator() {
        return (teleporterDTO1, teleporterDTO2) -> {
            boolean isDTO1EntranceActiveTeleporterDTO = teleporterDTO1 instanceof EntranceActiveTeleporterDTO;
            boolean isDTO2EntranceActiveTeleporterDTO = teleporterDTO2 instanceof EntranceActiveTeleporterDTO;
            if (isDTO1EntranceActiveTeleporterDTO && isDTO2EntranceActiveTeleporterDTO) {
                return compareEntranceActiveTeleporterDTO(teleporterDTO1, teleporterDTO2);
            }
            if (isDTO1EntranceActiveTeleporterDTO) {
                return -1;
            }
            if (isDTO2EntranceActiveTeleporterDTO) {
                return 1;
            }
            return 0;
        };
    }

    private static int compareEntranceActiveTeleporterDTO(TeleporterDTO teleporterDTO1, TeleporterDTO teleporterDTO2) {
        final boolean isDTO1Ordered = teleporterDTO1.getPortal().getOrder() != null;
        final boolean isDTO2Ordered = teleporterDTO2.getPortal().getOrder() != null;
        if (isDTO1Ordered && isDTO2Ordered) {
            return teleporterDTO1.getPortal().getOrder().compareTo(teleporterDTO2.getPortal().getOrder());
        }
        if (isDTO1Ordered) {
            return -1;
        }
        if (isDTO2Ordered) {
            return 1;
        }
        return 0;
    }

    public List<TeleporterDTO> portalToTeleporterDTOs(Config config) {
        Set<TeleporterDTO> teleportersData = new HashSet<>();
        config.getPortals().stream()
                .flatMap(portal -> portalToTeleporterDTO(portal, config).stream())
                .forEach(teleportersData::add);

        // post construct: add next puzzle teleporters to the previous puzzle exit teleporters
        postConstruct(teleportersData);

        return sort(teleportersData);
    }

    private Set<TeleporterDTO> portalToTeleporterDTO(Portal portal, Config config) {
        Set<TeleporterDTO> teleportersData = new HashSet<>();

        if (!portal.isActive()) {
            final EntranceInactiveTeleporterDTO entranceInactiveTeleporterDTO = toEntranceInactiveTeleporterDTO(portal, config);

            teleportersData.add(entranceInactiveTeleporterDTO);
            return teleportersData;
        }

        //entrance portals
        final EntranceActiveTeleporterDTO entranceActiveTeleporterDTO = toEntranceActiveTeleporterDTO(portal, config);

        teleportersData.add(entranceActiveTeleporterDTO);

        //exit portals
        final Location lobbySpawnAfterExit = portalMapper.getPortalExitWithYawPitch(portal);

        portal.getRoom().getExitPortalsLocations().stream()
                .map(roomExitLoc -> {
                    ExitTeleporterDTO exitTeleporterDTO = new ExitTeleporterDTO();
                    exitTeleporterDTO.setPortal(portal);
                    exitTeleporterDTO.setDestination(lobbySpawnAfterExit);
                    exitTeleporterDTO.setPortalBlockLocation(vectorToLocationTransformer.transform(portal.getRoom().getWorldName(), roomExitLoc));
                    exitTeleporterDTO.setParticleAnimLocation(portalMapper.fromBlockLocationToParticleAnimLocation(exitTeleporterDTO.getPortalBlockLocation()));
                    return exitTeleporterDTO;
                })
                .forEach(teleportersData::add);

        return teleportersData;
    }

    private @NotNull EntranceActiveTeleporterDTO toEntranceActiveTeleporterDTO(Portal portal, Config config) {
        EntranceActiveTeleporterDTO entranceActiveTeleporterDTO = new EntranceActiveTeleporterDTO();
        entranceActiveTeleporterDTO.setPortal(portal);
        final Location roomSpawnLocationWithYawPitch = portalMapper.toRoomSpawnLocationWithYawPitch(portal);
        Optional<Integer> oNbPlayersInside = portalMapper.getNbPlayersInside(roomSpawnLocationWithYawPitch);
        int nbPlayersInside = oNbPlayersInside.orElse(0);
        entranceActiveTeleporterDTO.setDestination(roomSpawnLocationWithYawPitch);
        entranceActiveTeleporterDTO.setPortalBlockLocation(portalMapper.toPortalBlockLocation(portal));
        entranceActiveTeleporterDTO.setHologramEntityName(portalMapper.toPortalHologramEntityName(portal));
        entranceActiveTeleporterDTO.setHologramEntityLocation(portalMapper.toHologramEntityLocation(portal));
        entranceActiveTeleporterDTO.setHologramDescription(portalMapper.toHologramDescription(portal, nbPlayersInside, config));
        entranceActiveTeleporterDTO.setParticleAnimLocation(portalMapper.toParticleAnimLocation(portal));
        entranceActiveTeleporterDTO.setSubtitle(portalMapper.toSubtitle(portal, config));
        entranceActiveTeleporterDTO.setTitle(portalMapper.toTitle(portal));
        entranceActiveTeleporterDTO.setItemDescription(portalMapper.toItemLore(portal, nbPlayersInside, config));
        entranceActiveTeleporterDTO.setItemName(portalMapper.toItemName(portal));
        entranceActiveTeleporterDTO.setRoomDifficulty(portalMapper.toRoomDifficulty(portal));
        entranceActiveTeleporterDTO.setTagsNameList(portalMapper.toTagsNameList(portal, config));
        entranceActiveTeleporterDTO.setNbPlayersInside(nbPlayersInside);
        return entranceActiveTeleporterDTO;
    }

    private @NotNull EntranceInactiveTeleporterDTO toEntranceInactiveTeleporterDTO(Portal portal, Config config) {
        final EntranceInactiveTeleporterDTO entranceInactiveTeleporterDTO = new EntranceInactiveTeleporterDTO();
        entranceInactiveTeleporterDTO.setPortal(portal);
        entranceInactiveTeleporterDTO.setPortalBlockLocation(portalMapper.toPortalBlockLocation(portal));
        entranceInactiveTeleporterDTO.setHologramEntityName(portalMapper.toPortalHologramEntityName(portal));
        entranceInactiveTeleporterDTO.setHologramEntityLocation(portalMapper.toHologramEntityLocation(portal));
        entranceInactiveTeleporterDTO.setHologramDescription(portalMapper.toHologramDescription(portal, config));
        return entranceInactiveTeleporterDTO;
    }

    private void postConstruct(Set<TeleporterDTO> teleportersData) {
        //add next puzzle teleporters to the previous puzzle exit teleporters
        teleportersData.stream()
                .filter(teleporterDTO -> teleporterDTO instanceof ExitTeleporterDTO)
                .map(teleporterDTO -> (ExitTeleporterDTO) teleporterDTO)
                .forEach(exitTeleporterDTO -> {
                    if (exitTeleporterDTO.getPortal().getOrder() != null) {
                        final Integer previousPortalOrder = exitTeleporterDTO.getPortal().getOrder();
                        final Integer nextPortalOrder = previousPortalOrder + 1;

                        //find the next portal if exists
                        final Optional<EntranceActiveTeleporterDTO> nextPuzzleTeleporter = teleportersData.stream()
                                .filter(nextTeleporterDTO -> nextTeleporterDTO instanceof EntranceActiveTeleporterDTO)
                                .map(teleporterDTO -> (EntranceActiveTeleporterDTO) teleporterDTO)
                                .filter(entranceActiveTeleporterDTO -> entranceActiveTeleporterDTO.getPortal().getOrder() != null
                                        &&
                                        entranceActiveTeleporterDTO.getPortal().getOrder().equals(nextPortalOrder))
                                .findAny();

                        // add the next puzzle teleporter to the current exit teleporter
                        exitTeleporterDTO.setNextPortalEntranceActiveTeleporterDTO(nextPuzzleTeleporter);
                    } else {
                        exitTeleporterDTO.setNextPortalEntranceActiveTeleporterDTO(Optional.empty());
                    }
                });
    }

    private List<TeleporterDTO> sort(Set<TeleporterDTO> teleportersData) {
        return teleportersData.stream()
                .sorted(getTeleporterDTOComparator())
                .toList();
    }
}
