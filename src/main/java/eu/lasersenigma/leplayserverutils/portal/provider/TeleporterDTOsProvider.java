package eu.lasersenigma.leplayserverutils.portal.provider;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.area.event.PlayerEnteredAreaEvent;
import eu.lasersenigma.area.event.PlayerLeftAreaEvent;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.event.ConfigUpdatedEvent;
import eu.lasersenigma.leplayserverutils.portal.dto.TeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.event.TeleporterDTOsUpdatedEvent;
import eu.lasersenigma.leplayserverutils.portal.mapper.PortalToTeleporterDTOMapper;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class TeleporterDTOsProvider implements LEPSUListener {

    private final ConfigManager configManager;

    private final PortalToTeleporterDTOMapper portalToTeleporterDTOMapper;

    private final LEPlayServerUtils lePlayServerUtils;

    private List<TeleporterDTO> teleportersDTO;

    @Inject
    public TeleporterDTOsProvider(ConfigManager configManager, PortalToTeleporterDTOMapper portalToTeleporterDTOMapper, LEPlayServerUtils lePlayServerUtils) {
        this.configManager = configManager;
        this.portalToTeleporterDTOMapper = portalToTeleporterDTOMapper;
        this.lePlayServerUtils = lePlayServerUtils;
        buildTeleporterDTOs();
    }

    @EventHandler
    public void onConfigUpdated(ConfigUpdatedEvent configUpdatedEvent) {
        buildTeleporterDTOs();
    }

    @EventHandler
    public void onPlayerEnterLEArea(PlayerEnteredAreaEvent playerEnterLEAreaEvent) {
        Bukkit.getScheduler().runTaskLater(
                lePlayServerUtils,
                this::buildTeleporterDTOs,
                1L
        );
    }

    @EventHandler
    public void onPlayerLeaveLEArea(PlayerLeftAreaEvent playerLeftAreaEvent) {
        Bukkit.getScheduler().runTaskLater(
                lePlayServerUtils,
                this::buildTeleporterDTOs,
                1L
        );
    }

    public List<TeleporterDTO> getTeleporterDTOs() {
        if (this.teleportersDTO == null) {
            buildTeleporterDTOs();
        }
        return this.teleportersDTO;
    }

    public <T extends TeleporterDTO> List<T> getTeleporterDTOs(Class<T> clazz) {
        return this.teleportersDTO.stream()
                .filter(clazz::isInstance)
                .map(clazz::cast)
                .collect(Collectors.toList());
    }

    private void buildTeleporterDTOs() {
        this.teleportersDTO = portalToTeleporterDTOMapper.portalToTeleporterDTOs(configManager.get());
        Bukkit.getPluginManager().callEvent(new TeleporterDTOsUpdatedEvent(this.teleportersDTO));
    }

}
