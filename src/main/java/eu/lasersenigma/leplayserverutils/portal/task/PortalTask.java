package eu.lasersenigma.leplayserverutils.portal.task;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.JavaPluginDisableEventHandler;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.portal.PortalAnimDisplayHandler;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import eu.lasersenigma.leplayserverutils.portal.dto.EntranceActiveTeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.dto.ExitTeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.dto.TeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.event.TeleporterDTOsUpdatedEvent;
import eu.lasersenigma.leplayserverutils.portal.provider.TeleporterDTOsProvider;
import fr.skytale.particleanimlib.animation.animation.circle.CircleTask;
import fr.skytale.particleanimlib.animation.collision.action.CollisionActionCallback;
import fr.skytale.particleanimlib.animation.parent.task.AAnimationTask;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class PortalTask implements Runnable, JavaPluginEnableEventHandler, JavaPluginDisableEventHandler, LEPSUListener {

    @Inject
    private LEPlayServerUtils lePlayServerUtils;

    @Inject
    private PortalAnimDisplayHandler portalAnimDisplayHandler;

    @Inject
    private PortalController portalController;

    @Inject
    private TeleporterDTOsProvider teleporterDTOsProvider;

    private Set<TeleporterDTO> teleporterDTOs;

    private final List<AAnimationTask<?>> tasks = new ArrayList<>();

    private Integer taskId;

    @Override
    public void enable() {
        buildTeleportersDTOs(teleporterDTOsProvider.getTeleporterDTOs());
        startTask();
    }

    @Override
    public void run() {
        startAnimTasks();
    }

    @Override
    public void disable() {
        stopTask();
    }

    @EventHandler
    public void onConfigUpdated(TeleporterDTOsUpdatedEvent teleporterDTOsUpdatedEvent) {
        stopAnimTasks();
        buildTeleportersDTOs(teleporterDTOsUpdatedEvent.getTeleporterDTOs());
        startAnimTasks();
    }

    private void buildTeleportersDTOs(List<TeleporterDTO> teleporterDTOs) {
        this.teleporterDTOs = teleporterDTOs.stream()
                .filter(teleporterDTO -> teleporterDTO instanceof EntranceActiveTeleporterDTO || teleporterDTO instanceof ExitTeleporterDTO)
                .collect(Collectors.toSet());
    }

    private void startTask() {
        if (taskId != null) {
            return;
        }
        taskId = Bukkit.getScheduler().runTaskTimer(
                lePlayServerUtils,
                this,
                0,
                PortalAnimDisplayHandler.PORTAL_ANIMATION_TICKS_DURATION
        ).getTaskId();
    }

    private void stopTask() {
        if (taskId != null) {
            Bukkit.getScheduler().cancelTask(taskId);
            taskId = null;
            stopAnimTasks();
        }
    }

    private void startAnimTasks() {
        tasks.clear();

        teleporterDTOs.forEach(teleporterData -> {
            Location particleAnimationLocation;
            Color color;
            if (teleporterData instanceof EntranceActiveTeleporterDTO entranceActiveTeleporterDTO) {
                particleAnimationLocation = entranceActiveTeleporterDTO.getParticleAnimLocation();
                boolean hasPlayersInside = entranceActiveTeleporterDTO.getNbPlayersInside() > 0;
                color = entranceActiveTeleporterDTO.getRoomDifficulty().asColor();
                if (hasPlayersInside) {
                    java.awt.Color awtColor = new java.awt.Color(color.getRed(), color.getGreen(), color.getBlue());
                    awtColor = awtColor.darker();
                    color = Color.fromRGB(awtColor.getRed(), awtColor.getGreen(), awtColor.getBlue());
                }
            } else if (teleporterData instanceof ExitTeleporterDTO exitTeleporterDTO) {
                particleAnimationLocation = exitTeleporterDTO.getParticleAnimLocation();
                color = Color.WHITE;
            } else {
                throw new IllegalStateException("Unknown teleporter DTO type");
            }

            final CollisionActionCallback<Entity, CircleTask> entityCircleTaskCollisionActionCallback = (task, entity) -> {
                portalController.teleport((Player) entity, teleporterData);
                return 20 + 1;
            };
            final CircleTask circleTask = portalAnimDisplayHandler.displayPortalAnim(
                    particleAnimationLocation,
                    color,
                    entityCircleTaskCollisionActionCallback);

            tasks.add(circleTask);
        });
    }

    private void stopAnimTasks() {
        tasks.forEach(AAnimationTask::stopAnimation);
        tasks.clear();
    }
}
