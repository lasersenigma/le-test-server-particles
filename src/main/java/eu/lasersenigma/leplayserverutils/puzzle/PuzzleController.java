package eu.lasersenigma.leplayserverutils.puzzle;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageComponent;
import eu.lasersenigma.leplayserverutils.common.messages.MessageComponentColor;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.common.utils.LasersEnigmaExecutor;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import eu.lasersenigma.leplayserverutils.portal.PortalController;
import eu.lasersenigma.leplayserverutils.portal.dto.EntranceActiveTeleporterDTO;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.portal.gui.TeleportMenuProvider;
import eu.lasersenigma.leplayserverutils.portal.provider.TeleporterDTOsProvider;
import eu.lasersenigma.leplayserverutils.puzzle.download.PuzzleSchematicService;
import eu.lasersenigma.leplayserverutils.puzzle.hint.PlayerHintService;
import eu.lasersenigma.leplayserverutils.puzzle.solution.PuzzleSolutionService;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import eu.lasersenigma.leplayserverutils.room.hint.RoomHintController;
import fr.skytale.itemlib.item.utils.ItemStackTrait;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@Singleton
public class PuzzleController {

    private static final String MENU_BUTTON_TEXTURE_URL = "954752e4dad9b3e3b1232d88bfa722704baea0fff3767349417ee76e13ef6ed2";
    public static final String NOT_IN_A_PUZZLE_ROOM = "You are not in a puzzle room";
    private static final String ASK_ON_DISCORD_FOR_HELP_IF_YOU_ARE_STUCK = "Ask on discord for help if you are stuck";

    @Inject
    TeleporterDTOsProvider teleporterDTOsProvider;

    @Inject
    private TeleportMenuProvider teleportMenuProvider;

    @Inject
    private ConfigManager configManager;

    @Inject
    private WorldVectorToLocationTransformer vectorToLocation;

    @Inject
    private PortalController portalController;

    @Inject
    private RoomController roomController;

    @Inject
    private PuzzleSchematicService puzzleSchematicService;

    @Inject
    private PuzzleSolutionService puzzleSolutionService;

    @Inject
    private LasersEnigmaExecutor lasersEnigmaExecutor;

    @Inject
    private MessageSender msg;

    @Inject
    private PlayerHintService playerHintService;

    private ItemStack teleportMenuOpener;

    /**
     * Corresponds to the command /leutils upload
     * if the player is in an enigma room
     * the room is copied and saved in leschematic (the area is defined by the red wool blocks (cf enigma template))
     * the leschematic file is then uploaded to lasers-enigma.eu/areas/ via sftp
     */
    public void openTeleportMenu(Player player) {
        teleportMenuProvider.getGui(player).show(player);
    }

    public void giveTeleportMenuOpener(Player player) {
        if (lasersEnigmaExecutor.isAtLeastOneMenuOpened(player)) {
            return;
        }
        player.getInventory().setItem(8, getTeleportMenuOpener());
    }

    public void removeTeleportMenuOpenerFromPlayerInventory(Player player) {
        final ItemStack expectedBookItem = player.getInventory().getItem(8);
        if (isTeleportMenuOpener(expectedBookItem)) {
            player.getInventory().setItem(8, new ItemStack(Material.AIR));
        }
    }

    public boolean isTeleportMenuOpener(ItemStack itemStack) {
        return ItemStackUtils.compare(itemStack, getTeleportMenuOpener(), ItemStackTrait.NAME, ItemStackTrait.LORE, ItemStackTrait.SKULL_TEXTURE);
    }

    public void download(Player player) {

        final Config config = configManager.get();
        Optional<Portal> oPortal = portalController.findNearPortal(player, config, true, false);

        if (oPortal.isEmpty()) {
            oPortal = roomController.findPortalFromPlayerInRoom(player, config);
        }

        if (oPortal.isEmpty()) {
            msg.sendError(player, "You are not in a puzzle, nor near an active portal. Which puzzle do you want to download ?");
        } else {
            msg.sendParagraph(player, "Checking if you are in a puzzle or if you have an active puzzle portal near you ...");
            puzzleSchematicService.downloadSchematic(player, oPortal.get());
        }
    }

    public boolean teleportToPuzzle(Player player, String roomDescription) {
        Config config = configManager.get();
        Optional<Portal> oPortal = this.roomController.findActivePortalByRoomDescription(config, roomDescription);

        if (oPortal.isEmpty()) {
            msg.sendError(player, PortalController.NO_ROOM_FOUND_WITH_DESCRIPTION.formatted(roomDescription));
            return false;
        }

        Portal portal = oPortal.get();

        final Optional<EntranceActiveTeleporterDTO> teleporterDTO = teleporterDTOsProvider.getTeleporterDTOs(EntranceActiveTeleporterDTO.class)
                .stream()
                .filter(currentTeleporterDTO -> currentTeleporterDTO.getPortal().equals(portal))
                .findAny();

        if (teleporterDTO.isEmpty()) {
            throw new IllegalStateException("No teleporter found for the portal " + portal + " but this portal exists in config");
        }

        portalController.teleportInRoom(
                player,
                teleporterDTO.get()
        );

        return true;
    }

    public boolean solution(Player player) {
        final Config config = configManager.get();

        Optional<Portal> portal = roomController.findPortalFromPlayerInRoom(player, config);
        if (portal.isEmpty()) {
            msg.sendError(player, NOT_IN_A_PUZZLE_ROOM);
            return false;
        }

        final Room room = portal.get().getRoom();

        boolean pageExists = puzzleSolutionService.doesPageExist(room, config);

        String url = puzzleSolutionService.getPublicUrl(room, config, player);

        if (pageExists) {
            msg.sendSuccess(
                    player,
                    new MessageComponent("Solution wiki page:", MessageComponentColor.SUCCESS)
                            .append(
                                    new MessageComponent(url, MessageComponentColor.LINK, TextDecoration.UNDERLINED)
                            ));
        } else {
            msg.sendWarning(player, "This puzzle's solution wiki page does not exist.");
            boolean solutionRequestSent = puzzleSolutionService.sendSolutionRequestToDiscord(room, config, player);
            if (solutionRequestSent) {
                msg.sendWarning(player,
                        new MessageComponent("A message has been sent on our discord server ( ", MessageComponentColor.WARNING)
                                .append(
                                        new MessageComponent("https://discord.gg/z677bNY84q", MessageComponentColor.LINK, TextDecoration.UNDERLINED)
                                )
                                .append(
                                        new MessageComponent(" ) to ask a puzzle solution.", MessageComponentColor.WARNING)
                                ));
            } else {
                msg.sendWarning(player, new MessageComponent("Please ask for the solution on our Discord server: ", MessageComponentColor.WARNING)
                        .append(
                                new MessageComponent("https://discord.gg/z677bNY84q", MessageComponentColor.LINK, TextDecoration.UNDERLINED)
                        ));
            }
            if (player.isOp()) {
                msg.sendWarning(
                        player,
                        new MessageComponent("Solution wiki page to create : ", MessageComponentColor.WARNING)
                                .append(
                                        new MessageComponent(url, MessageComponentColor.LINK, TextDecoration.UNDERLINED)
                                ));
            }
        }
        return true;
    }

    private ItemStack getTeleportMenuOpener() {
        if (teleportMenuOpener == null) {
            teleportMenuOpener = createTeleportMenuOpener();
        }
        return teleportMenuOpener;
    }

    private static @NotNull ItemStack createTeleportMenuOpener() {
        final ItemStack menuButton = ItemStackUtils.getCustomHead(MENU_BUTTON_TEXTURE_URL);
        ItemStackUtils.setName(menuButton, "Menu");
        ItemStackUtils.setLore(menuButton, "Open the puzzle menu");
        return menuButton;
    }

    public boolean hint(Player player) {
        final Config config = configManager.get();

        Optional<Portal> portal = roomController.findPortalFromPlayerInRoom(player, config);
        if (portal.isEmpty()) {
            msg.sendError(player, NOT_IN_A_PUZZLE_ROOM);
            return false;
        }

        final Room room = portal.get().getRoom();

        if (room.getHints() == null || room.getHints().isEmpty()) {
            msg.sendWarning(player, "No hints available for this room");
            msg.sendParagraph(player, ASK_ON_DISCORD_FOR_HELP_IF_YOU_ARE_STUCK);
            return true;
        }

        Optional<Integer> hintToUseIndex = playerHintService.getNextHintIndex(room, player);
        if (hintToUseIndex.isEmpty()) {
            msg.sendWarning(player, "No more hints available for this room");
            msg.sendWarning(player, "Use the same command again to retrieve the first hint");
            msg.sendParagraph(player, ASK_ON_DISCORD_FOR_HELP_IF_YOU_ARE_STUCK);
            playerHintService.clearPlayerHints(player);
            return true;
        }
        int index = hintToUseIndex.get();

        final String hint = room.getHints().get(index);

        msg.sendSuccess(player, RoomHintController.HINT_MESSAGE.formatted(+(index + 1), hint));

        playerHintService.markRoomHintAsUsed(room, player, index);

        return true;
    }

    public boolean hint(Player player, Integer indexStartingWith1) {
        final Config config = configManager.get();

        Optional<Portal> portal = roomController.findPortalFromPlayerInRoom(player, config);
        if (portal.isEmpty()) {
            msg.sendError(player, NOT_IN_A_PUZZLE_ROOM);
            return false;
        }

        final Room room = portal.get().getRoom();

        if (room.getHints() == null || room.getHints().isEmpty()) {
            msg.sendWarning(player, "No hints available for this room");
            msg.sendParagraph(player, ASK_ON_DISCORD_FOR_HELP_IF_YOU_ARE_STUCK);
            return true;
        }

        if (indexStartingWith1 < 1 || room.getHints().size() < indexStartingWith1) {
            msg.sendError(player, "Hint " + indexStartingWith1 + " does not exist for this room");
            return false;
        }

        final int index = indexStartingWith1 - 1;
        final String hint = room.getHints().get(index);

        msg.sendSuccess(player, RoomHintController.HINT_MESSAGE.formatted(+(indexStartingWith1), hint));

        playerHintService.markRoomHintAsUsed(room, player, index);

        return true;
    }
}
