package eu.lasersenigma.leplayserverutils.puzzle.download;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jcraft.jsch.*;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.messages.MessageComponent;
import eu.lasersenigma.leplayserverutils.common.messages.MessageComponentColor;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.common.utils.LasersEnigmaExecutor;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.puzzle.download.dto.SchematicDTO;
import eu.lasersenigma.leplayserverutils.puzzle.download.dto.SchematicListDTO;
import eu.lasersenigma.leplayserverutils.puzzle.download.entity.SFTPAuthentication;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;

@Singleton
public class PuzzleSchematicService {
    private static final String COULD_NOT_PARSE_SCHEMATIC_LIST_JSON_ERROR_MESSAGE = "Could not parse schematic list json";
    private static final String COULD_NOT_DOWNLOAD_SCHEMATIC_LIST_JSON_ERROR_MESSAGE = "Could not download schematic list json";
    private static final String SCHEM_AVAILABILITY_COULD_NOT_BE_CHECKED_ERROR_MESSAGE = "The schematic availability could not be checked";
    private static final String API_URL = "https://lasers-enigma.eu/schematics/api.php";


    @Inject
    private LEPlayServerUtils plugin;
    @Inject
    private WorldVectorToLocationTransformer vectorToLocation;

    @Inject
    private LasersEnigmaExecutor lasersEnigmaExecutor;

    @Inject
    private ConfigManager configManager;

    @Inject
    private MessageSender msg;

    @NotNull
    private static String getFilename(Portal portalToDownload) {
        String roomDescription = portalToDownload.getRoom().getDescription();
        String safeRoomDescription = java.net.URLEncoder.encode(roomDescription, StandardCharsets.UTF_8);
        return "puzzle_" + safeRoomDescription;
    }

    public void downloadSchematic(Player player, Portal portalToDownload) {

        Bukkit.getScheduler().runTaskAsynchronously(
                plugin,
                () -> {
                    msg.sendParagraph(player, "Checking if the puzzle is already available online ...");
                    SchematicListDTO schematics = getSchematicListDTO(player);
                    if (schematics == null) return;

                    SchematicDTO schematic = getSchematicDTO(portalToDownload, schematics);

                    if (schematic == null) {
                        msg.sendParagraph(player, "The puzzle is not available online yet.");
                        schematic = generateAndUploadSchematic(player, portalToDownload);
                        if (schematic == null) {
                            msg.sendError(player, "The puzzle could not be generated and uploaded.");
                            return;
                        }
                    }
                    msg.sendSuccess(player, "Please click on the link below to download the puzzle:");
                    msg.sendSuccess(player, new MessageComponent(schematic.getLink(), MessageComponentColor.LINK, TextDecoration.UNDERLINED));
                }
        );
    }

    private SchematicDTO generateAndUploadSchematic(Player player, Portal portalToDownload) {
        msg.sendParagraph(player, "Generating Lasers Enigma schematic ...");
        String filename = getFilename(portalToDownload);

        final Location maxLoc = vectorToLocation.transform(
                portalToDownload.getRoom().getWorldName(),
                portalToDownload.getRoom().getMaxCorner()
        );
        final Location minLoc = vectorToLocation.transform(
                portalToDownload.getRoom().getWorldName(),
                portalToDownload.getRoom().getMinCorner()
        );
        File schemFile = lasersEnigmaExecutor.createSchematic(player, minLoc, maxLoc, filename);

        msg.sendParagraph(player,"Uploading Lasers Enigma schematic ...");
        try {
            uploadSchematic(player, schemFile);
        } catch (Exception e) {
            plugin.getLogger().log(Level.SEVERE, "Could not upload schematic", e);
            return null;
        }

        msg.sendParagraph(player, "Checking if the puzzle is now available online ...");
        SchematicListDTO schematics = getSchematicListDTO(player);
        if (schematics == null) return null;
        return getSchematicDTO(portalToDownload, schematics);
    }

    private void uploadSchematic(Player player, File schemFile) throws JSchException, SftpException, IOException {
        final SFTPAuthentication sftpAuthentication = configManager.get().getSftpAuthentication();

        msg.sendParagraph(player, "Uploading the schematic file ...");
        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        plugin.getLogger().log(Level.FINE, "preparing the host information for sftp.");

        try (FileInputStream fis = new FileInputStream(schemFile)) {
            JSch jsch = new JSch();
            File dataFolder = plugin.getDataFolder();
            File idFile = new File(dataFolder, sftpAuthentication.getKeyFile());
            if (!idFile.exists()) {
                throw new IllegalStateException("Could not find the key file: " + idFile.getAbsolutePath());
            }
            jsch.addIdentity(idFile.getAbsolutePath());
            session = jsch.getSession(
                    sftpAuthentication.getUsername(),
                    sftpAuthentication.getHost(),
                    sftpAuthentication.getPort());

            // disabling StrictHostKeyChecking may help to make connection but makes it insecure
            // see http://stackoverflow.com/questions/30178936/jsch-sftp-security-with-session-setconfigstricthostkeychecking-no
            JSch.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            plugin.getLogger().log(Level.FINE, "Host connected.");
            channel = session.openChannel("sftp");
            channel.setInputStream(System.in);
            channel.setOutputStream(System.out);
            channel.connect();
            plugin.getLogger().log(Level.FINE, "sftp channel opened and connected.");
            channelSftp = (ChannelSftp) channel;
            if (sftpAuthentication.getRemoteFolder() != null) {
                channelSftp.cd(sftpAuthentication.getRemoteFolder());
            }
            channelSftp.put(fis, schemFile.getName());
            msg.sendParagraph(player, "Schematic file transferred successfully to lasers-enigma.eu website.");
            plugin.getLogger().log(Level.FINE, "Schematic file transferred successfully to lasers-enigma.eu website.");
        } catch (Exception ex) {
            plugin.getLogger().log(Level.SEVERE, "Exception found while transfer the response.");
            msg.sendError(player, "Could not transfer schematic file to lasers-enigma.eu website.");
            throw ex;
        } finally {
            if (channelSftp != null) channelSftp.exit();
            plugin.getLogger().log(Level.FINE, "sftp Channel exited.");
            if (channel != null) channel.disconnect();
            plugin.getLogger().log(Level.FINE, "Channel disconnected.");
            if (session != null) session.disconnect();
            plugin.getLogger().log(Level.FINE, "Host Session disconnected.");
        }

    }

    @Nullable
    private SchematicListDTO getSchematicListDTO(Player player) {
        HttpClient client = HttpClient.newHttpClient();
        HttpResponse<String> response;
        try {
            response = client.send(
                    HttpRequest.newBuilder()
                            .uri(URI.create(API_URL))
                            .build(),
                    HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            plugin.getLogger().log(Level.SEVERE, COULD_NOT_DOWNLOAD_SCHEMATIC_LIST_JSON_ERROR_MESSAGE, e);
            msg.sendError(player, SCHEM_AVAILABILITY_COULD_NOT_BE_CHECKED_ERROR_MESSAGE);
            return null;
        }
        msg.sendParagraph(player,"Finding the puzzle in the available schematic list ...");
        SchematicListDTO schematics;
        try {
            schematics = new Gson().fromJson(response.body(), SchematicListDTO.class);
        } catch (JsonSyntaxException e) {
            plugin.getLogger().log(Level.SEVERE, COULD_NOT_DOWNLOAD_SCHEMATIC_LIST_JSON_ERROR_MESSAGE, e);
            plugin.getLogger().log(Level.SEVERE, "body:\n %s", response.body());
            msg.sendError(player, SCHEM_AVAILABILITY_COULD_NOT_BE_CHECKED_ERROR_MESSAGE);
            return null;
        }

        if (schematics == null) {
            plugin.getLogger().log(Level.SEVERE, COULD_NOT_PARSE_SCHEMATIC_LIST_JSON_ERROR_MESSAGE);
            msg.sendError(player, SCHEM_AVAILABILITY_COULD_NOT_BE_CHECKED_ERROR_MESSAGE);
            return null;
        }
        return schematics;
    }

    @Nullable
    private SchematicDTO getSchematicDTO(Portal portalToDownload, SchematicListDTO schematics) {
        return schematics.getSchematics().stream()
                .filter(schematic -> schematic.getName().equals(getFilename(portalToDownload) + ".leschem"))
                .findAny()
                .orElse(null);
    }
}
