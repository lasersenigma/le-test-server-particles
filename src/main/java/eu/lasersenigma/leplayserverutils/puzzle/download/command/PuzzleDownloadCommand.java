package eu.lasersenigma.leplayserverutils.puzzle.download.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.command.provider.PuzzleCommandsProvider;
import eu.lasersenigma.leplayserverutils.puzzle.PuzzleController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class PuzzleDownloadCommand extends Command<Player> implements JavaPluginEnableEventHandler {

    private final PuzzleController puzzleController;
    private final Commands puzzleCommands;

    @Inject
    public PuzzleDownloadCommand(PuzzleCommandsProvider puzzleCommands, PuzzleController puzzleController) {
        super(Player.class, "download");

        this.puzzleController = puzzleController;
        this.puzzleCommands = puzzleCommands.get();
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        puzzleController.download(player);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Get the download link of this puzzle schematic";
    }

    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        puzzleCommands.registerCommand(this);
    }
}
