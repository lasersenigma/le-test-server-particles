package eu.lasersenigma.leplayserverutils.puzzle.download.dto;

@lombok.Getter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
@lombok.EqualsAndHashCode
public class SchematicDTO {
    private String name;
    private String link;
}
