package eu.lasersenigma.leplayserverutils.puzzle.download.dto;

import java.util.ArrayList;
import java.util.List;

@lombok.Getter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
@lombok.EqualsAndHashCode
public class SchematicListDTO {
    private List<SchematicDTO> schematics = new ArrayList<>();
}
