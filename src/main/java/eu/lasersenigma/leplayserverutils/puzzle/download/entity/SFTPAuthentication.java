package eu.lasersenigma.leplayserverutils.puzzle.download.entity;

import com.google.gson.annotations.Expose;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SFTPAuthentication {
    @Expose
    private String host = "CHANGE_ME";

    @Expose
    private String username = "CHANGE_ME";

    @Expose
    private int port = 22;

    @Expose
    private String keyFile = ".ssh/id_rsa";

    @Expose
    private String remoteFolder = "web";


    public SFTPAuthentication(SFTPAuthentication sFTPAuthentication) {
        this.host = sFTPAuthentication.getHost();
        this.username = sFTPAuthentication.getUsername();
        this.port = sFTPAuthentication.getPort();
        this.keyFile = sFTPAuthentication.getKeyFile();
        this.remoteFolder = sFTPAuthentication.getRemoteFolder();
    }
}
