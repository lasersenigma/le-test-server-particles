package eu.lasersenigma.leplayserverutils.puzzle.hint;

import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.puzzle.hint.dto.HintUsageState;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Singleton
public class PlayerHintService {

    private final HintUsageState hintUsageState = new HintUsageState();

    public void clearPlayerHints(@NotNull Player player) {
        final UUID playerUUID = player.getUniqueId();
        hintUsageState.getRoomHintUsageStatesPerRoomDescriptions().values()
                .forEach(roomHintUsageState -> roomHintUsageState.getPlayerRoomHintUsageState().remove(playerUUID));
    }

    public void clearPlayerHints(String roomDescription) {
        hintUsageState.getRoomHintUsageStatesPerRoomDescriptions().remove(roomDescription);
    }

    public Optional<Integer> getNextHintIndex(Room room, Player player) {
        final List<Boolean> hintUsed = getPlayerRoomHintUsageState(room, player).getHintUsed();
        for (int i = 0; i < hintUsed.size(); i++) {
            if (Boolean.FALSE.equals(hintUsed.get(i))) {
                return Optional.of(i);
            }
        }
        return Optional.empty();
    }

    public void markRoomHintAsUsed(Room room, Player player, int index) {
        getPlayerRoomHintUsageState(room, player)
                .getHintUsed()
                .set(index, true);
    }

    private HintUsageState.PlayerRoomHintUsageState getPlayerRoomHintUsageState(Room room, Player player) {
        final HintUsageState.PlayerRoomHintUsageState playerRoomHintUsageState = hintUsageState.getRoomHintUsageStatesPerRoomDescriptions()
                .computeIfAbsent(room.getDescription(), roomDescription -> new HintUsageState.RoomHintUsageState())
                .getPlayerRoomHintUsageState()
                .computeIfAbsent(player.getUniqueId(), playerUUID -> new HintUsageState.PlayerRoomHintUsageState());
        if (playerRoomHintUsageState.getHintUsed() == null) {
            List<Boolean> hintUsed = room.getHints().stream()
                    .map(hint -> false)
                    .collect(Collectors.toList());
            playerRoomHintUsageState.setHintUsed(hintUsed);
        }
        return playerRoomHintUsageState;
    }
}
