package eu.lasersenigma.leplayserverutils.puzzle.hint.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.command.provider.PuzzleCommandsProvider;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.puzzle.PuzzleController;
import eu.lasersenigma.leplayserverutils.room.hint.RoomHintController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.List;

@Singleton
public class PuzzleHintCommand extends Command<Player> implements JavaPluginEnableEventHandler {

    private static final String COMMAND = "hint";
    private static final String INDEX_ARG = "index";

    private final PuzzleController puzzleController;

    private final Commands puzzleCommands;

    private final RoomHintController roomHintController;

    @Inject
    public PuzzleHintCommand(PuzzleCommandsProvider puzzleCommands, PuzzleController puzzleController, RoomHintController roomHintController) {
        super(Player.class, COMMAND);
        this.addArgument(INDEX_ARG, false, ArgumentType.integerOnly());
        this.puzzleController = puzzleController;
        this.roomHintController = roomHintController;
        this.puzzleCommands = puzzleCommands.get();
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        if (!this.hasArgumentValue(player, INDEX_ARG)) {
            return puzzleController.hint(player);
        }
        Integer index = this.getArgumentValue(player, INDEX_ARG, ArgumentType.integerOnly());
        return puzzleController.hint(player, index);
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(INDEX_ARG)) {

            Integer currentArgumentValue = getArgumentValue(executor, INDEX_ARG, ArgumentType.integerOnly());

            final List<Integer> indexes = roomHintController.findAllHintsIndexes(executor);
            super.setAutoCompleteValuesArg(
                    INDEX_ARG,
                    CommandUtils.buildIntegerAutocompleteValues(indexes, currentArgumentValue));
        }
    }

    @Override
    protected String description(Player executor) {
        return "get a hint for this puzzle";
    }

    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        puzzleCommands.registerCommand(this);
    }
}