package eu.lasersenigma.leplayserverutils.puzzle.hint.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@RequiredArgsConstructor
public class HintUsageState {
    private final HashMap<String, RoomHintUsageState> roomHintUsageStatesPerRoomDescriptions = new HashMap<>();

    @Getter
    @Setter
    @RequiredArgsConstructor
    public static class RoomHintUsageState {
        private final HashMap<UUID, PlayerRoomHintUsageState> playerRoomHintUsageState = new HashMap<>();
    }

    @Getter
    @Setter
    @RequiredArgsConstructor
    public static class PlayerRoomHintUsageState {
        private List<Boolean> hintUsed = null;
    }
}
