package eu.lasersenigma.leplayserverutils.puzzle.hint.listener;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.area.event.PlayerEnteredAreaEvent;
import eu.lasersenigma.area.event.PlayerLeftAreaEvent;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.puzzle.hint.PlayerHintService;
import eu.lasersenigma.leplayserverutils.room.hint.event.RoomHintUpdatedEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerQuitEvent;

@Singleton
public class PlayerHintListener implements LEPSUListener {

    @Inject
    private PlayerHintService playerHintService;

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        playerHintService.clearPlayerHints(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        playerHintService.clearPlayerHints(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerEnteredArea(PlayerEnteredAreaEvent event) {
        playerHintService.clearPlayerHints(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLeftAreaEvent(PlayerLeftAreaEvent event) {
        playerHintService.clearPlayerHints(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onRoomHintUpdated(RoomHintUpdatedEvent event) {
        playerHintService.clearPlayerHints(event.getRoomDescription());
    }

}
