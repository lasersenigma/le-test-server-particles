package eu.lasersenigma.leplayserverutils.puzzle.nextlevel.gui;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.gui.MenuItemStackProvider;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

@Singleton
public class NextLevelMenuItemStackProvider {

    private static final String NEXT_PUZZLE_TEXTURE_URL = "65a84e6394baf8bd795fe747efc582cde9414fccf2f1c8608f1be18c0e079138";

    @Inject
    private MenuItemStackProvider menuItemStackProvider;

    public @NotNull ItemStack getNextPuzzleItemStack() {
        final ItemStack itemStack = ItemStackUtils.getCustomHead(NEXT_PUZZLE_TEXTURE_URL);
        ItemStackUtils.setName(itemStack, "Next Puzzle");
        ItemStackUtils.setLore(itemStack, "Directly teleport to", "the next puzzle");
        return itemStack;
    }

    public @NotNull ItemStack getRatePuzzleItemStack() {
        final ItemStack itemStack = menuItemStackProvider.getStarItemStack(true);
        ItemStackUtils.setName(itemStack, "Rate Puzzle");
        ItemStackUtils.setLore(itemStack, "Rate the puzzle", "you just completed");
        return itemStack;
    }

}
