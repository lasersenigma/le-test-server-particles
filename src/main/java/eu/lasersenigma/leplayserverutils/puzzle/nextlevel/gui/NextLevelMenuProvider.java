package eu.lasersenigma.leplayserverutils.puzzle.nextlevel.gui;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.gui.MenuItemStackProvider;
import eu.lasersenigma.leplayserverutils.puzzle.review.gui.note.NoteLevelMenuProvider;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

@Singleton
public class NextLevelMenuProvider {

    @Inject
    private NextLevelMenuItemStackProvider nextLevelMenuItemStackProvider;

    @Inject
    private MenuItemStackProvider menuItemStackProvider;

    @Inject
    private NoteLevelMenuProvider noteLevelMenuProvider;

    public @NotNull ChestGui getGui(@NotNull Consumer<InventoryClickEvent> teleportToNextPuzzleAction, int roomId) {

        ChestGui gui = new ChestGui(3, "Go to next puzzle ?");
        gui.setOnGlobalClick(event -> event.setCancelled(true));

        OutlinePane background = new OutlinePane(0, 0, 9, 3, Pane.Priority.LOWEST);
        background.addItem(new GuiItem(menuItemStackProvider.getBackgroundItemStack()));
        background.setRepeat(true);
        gui.addPane(background);

        OutlinePane ratePuzzle = new OutlinePane(3, 1, 1, 1);
        ratePuzzle.addItem(new GuiItem(nextLevelMenuItemStackProvider.getRatePuzzleItemStack(), inventoryClickEvent -> {
            ChestGui chestGui = noteLevelMenuProvider.getGui(roomId);
            chestGui.show(inventoryClickEvent.getWhoClicked());
        }));
        gui.addPane(ratePuzzle);

        OutlinePane goToNextPuzzlePane = new OutlinePane(5, 1, 1, 1);
        goToNextPuzzlePane.addItem(new GuiItem(nextLevelMenuItemStackProvider.getNextPuzzleItemStack(), teleportToNextPuzzleAction));
        gui.addPane(goToNextPuzzlePane);

        OutlinePane cancelPane = new OutlinePane(8, 0, 1, 1);
        cancelPane.addItem(new GuiItem(menuItemStackProvider.getExitItemStack(), event -> event.getWhoClicked().closeInventory()));
        gui.addPane(cancelPane);

        return gui;
    }
}
