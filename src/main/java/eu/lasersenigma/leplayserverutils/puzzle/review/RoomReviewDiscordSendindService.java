package eu.lasersenigma.leplayserverutils.puzzle.review;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookEmbed;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.PlayerRoomReview;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.RoomReviews;
import eu.lasersenigma.leplayserverutils.puzzle.review.mapper.RoomReviewMapper;
import eu.lasersenigma.leplayserverutils.room.entity.Room;

@Singleton
public class RoomReviewDiscordSendindService {

    @Inject
    RoomReviewMapper roomReviewMapper;

    private WebhookClient reviewWebhookClient = null;

    public void send(Room room, RoomReviews roomReviews, PlayerRoomReview playerRoomReview, Config config) {
        final WebhookEmbed embed = roomReviewMapper.toWebhookEmbed(room, roomReviews, playerRoomReview);
        getReviewWebhookClient(config).send(embed);
    }
    public WebhookClient getReviewWebhookClient(Config config) {
        if (this.reviewWebhookClient == null) {
            try {
                this.reviewWebhookClient = WebhookClient.withUrl(config.getReviewDiscordWebhookUrl());
            } catch (IllegalArgumentException e) {
                throw new IllegalStateException("Error while creating the webhook client: %s, Please check the webhook URL in the config. Currently : %s".formatted(e.getMessage(), config.getReviewDiscordWebhookUrl()), e);
            }
        }
        return this.reviewWebhookClient;
    }
}
