package eu.lasersenigma.leplayserverutils.puzzle.review;

import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.google.inject.Inject;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.puzzle.PuzzleController;
import eu.lasersenigma.leplayserverutils.puzzle.review.gui.note.NoteLevelMenuProvider;
import eu.lasersenigma.leplayserverutils.puzzle.review.gui.view.ViewReviewsMenuProvider;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public class RoomsReviewsController {

    private static final String NOT_IN_A_PUZZLE_ROOM = PuzzleController.NOT_IN_A_PUZZLE_ROOM;

    @Inject
    private ConfigManager configManager;

    @Inject
    private RoomsReviewsService roomsReviewsService;

    @Inject
    private NoteLevelMenuProvider noteLevelMenuProvider;

    @Inject
    private ViewReviewsMenuProvider viewReviewsMenuProvider;

    @Inject
    private RoomController roomController;

    @Inject
    private MessageSender msg;

    public boolean createReview(Player player) {
        final Config config = configManager.get();

        Optional<Portal> portal = roomController.findPortalFromPlayerInRoom(player, config);
        if (portal.isEmpty()) {
            msg.sendError(player, NOT_IN_A_PUZZLE_ROOM);
            return false;
        }
        final Room room = portal.get().getRoom();

        ChestGui gui = noteLevelMenuProvider.getGui(room.getId());
        gui.show(player);
        return true;
    }

    public boolean viewReviews(Player player) {
        final Config config = configManager.get();

        Optional<Portal> portal = roomController.findPortalFromPlayerInRoom(player, config);
        if (portal.isEmpty()) {
            msg.sendError(player, NOT_IN_A_PUZZLE_ROOM);
            return false;
        }

        final Room room = portal.get().getRoom();

        viewReviewsMenuProvider.getGui(room).show(player);

        return true;
    }

    public void submitPuzzleNote(@NotNull Player player, int roomId, float percentNote) {
        if (percentNote < 0 || percentNote > 1) {
            throw new IllegalArgumentException("Note must be between 0 and 1");
        }
        Config config = configManager.get();
        Optional<Room> oRoom = roomController.findRoomById(roomId, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, "Room not found with id " + roomId);
            return;
        }
        Room room = oRoom.get();

        roomsReviewsService.submitPuzzleNote(
                player,
                room.getId(),
                percentNote
        );
    }

    public void submitPuzzleDifficulty(@NotNull Player player, int roomId, float percentDifficulty) {
        if (percentDifficulty < 0 || percentDifficulty > 1) {
            throw new IllegalArgumentException("Difficulty must be between 0 and 1");
        }
        Config config = configManager.get();
        Optional<Room> oRoom = roomController.findRoomById(roomId, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, "Room not found with id " + roomId);
            return;
        }
        Room room = oRoom.get();

        roomsReviewsService.submitPuzzleDifficulty(
                player,
                room.getId(),
                percentDifficulty
        );
    }

    public void submitPuzzleComment(@NotNull Player player, int roomId, @Nullable String title, @NotNull List<String> comment) {
        Config config = configManager.get();
        Optional<Room> oRoom = roomController.findRoomById(roomId, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, "Room not found with id " + roomId);
            return;
        }
        Room room = oRoom.get();

        roomsReviewsService.submitPuzzleComment(
                player,
                room.getId(),
                title,
                comment
        );

        msg.sendSuccess(player, "Thank you for your feedback");
    }
}
