package eu.lasersenigma.leplayserverutils.puzzle.review;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.RoomReviews;
import fr.skytale.jsonlib.JsonManager;
import org.jetbrains.annotations.NotNull;

import java.io.File;

@Singleton
public class RoomsReviewsManager {

    private static final String REVIEW_FOLDER = "reviews";
    private static final String REVIEW_FILE_EXT = ".json";
    private static final String PATH_SEPARATOR = "/";

    private final JsonManager jsonManager;

    private final File roomReviewFolder;

    private final LoadingCache<Integer, RoomReviews> roomReviewsPerRoomIdCache;

    @Inject
    public RoomsReviewsManager(JsonManager jsonManager, LEPlayServerUtils plugin) {
        this.jsonManager = jsonManager;
        this.roomReviewFolder = getOrCreateRoomReviewFolder(plugin);
        this.roomReviewsPerRoomIdCache = Caffeine.newBuilder()
                .maximumSize(2000)
                .build(this::buildRoomReviewFromFile);
    }

    public RoomReviews getRoomReviews(int roomId) {
        return roomReviewsPerRoomIdCache.get(roomId);
    }

    public void saveRoomReviews(int roomId, RoomReviews roomReviews) {
        roomReviewsPerRoomIdCache.put(roomId, roomReviews);
        saveRoomReviewToFile(roomId, roomReviews);
    }

    private File getOrCreateRoomReviewFolder(LEPlayServerUtils plugin) {
        String roomReviewFolderPath = plugin.getDataFolder().getAbsolutePath() + PATH_SEPARATOR + REVIEW_FOLDER;
        final File roomReviewFolderTmp;
        roomReviewFolderTmp = new File(roomReviewFolderPath);
        if (!roomReviewFolderTmp.exists()) {
            final boolean created = roomReviewFolderTmp.mkdirs();
            if (!created) {
                throw new RuntimeException("Failed to create room reviews folder");
            }
        }
        return roomReviewFolderTmp;
    }

    private void saveRoomReviewToFile(int roomId, RoomReviews roomReviews) {
        try {
            jsonManager.serialize(roomReviews, RoomReviews.class, getRoomReviewFile(roomId));
        } catch (Exception e) {
            throw new RuntimeException("Failed to save room reviews file %s".formatted(String.valueOf(roomId)), e);
        }
    }

    private @NotNull RoomReviews buildRoomReviewFromFile(@NotNull Integer key) {
        final File roomReviewFile = getRoomReviewFile(key);

        RoomReviews roomReviews;
        if (roomReviewFile.exists()) {
            roomReviews = jsonManager.deserialize(roomReviewFile, RoomReviews.class);
            if (roomReviews == null) {
                roomReviews = new RoomReviews();
            }
        } else {
            roomReviews = new RoomReviews();
        }
        return roomReviews;
    }

    private @NotNull File getRoomReviewFile(@NotNull Integer key) {
        return new File(roomReviewFolder, key + REVIEW_FILE_EXT);
    }
}
