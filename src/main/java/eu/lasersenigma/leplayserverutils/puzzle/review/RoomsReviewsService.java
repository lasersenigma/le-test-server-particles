package eu.lasersenigma.leplayserverutils.puzzle.review;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.PlayerRoomReview;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.RoomReviews;
import eu.lasersenigma.leplayserverutils.puzzle.review.event.PlayerRoomReviewUpdatedEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Instant;
import java.util.List;
import java.util.function.Consumer;

@Singleton
public class RoomsReviewsService {

    @Inject
    private RoomsReviewsManager roomsReviewsManager;

    @Inject
    private LEPlayServerUtils plugin;

    public void submitPuzzleNote(@NotNull Player player, int roomId, float note) {
        Consumer<PlayerRoomReview> roomReviewModifier = playerRoomReview -> {
            playerRoomReview.setAuthorPseudo(player.getName());
            playerRoomReview.setNote(note);
            playerRoomReview.setLastUpdated(Instant.now());
        };
        modifyPlayerRoomReview(player, roomId, roomReviewModifier);
    }

    public void submitPuzzleDifficulty(@NotNull Player player, int roomId, float difficulty) {
        Consumer<PlayerRoomReview> roomReviewModifier = playerRoomReview -> {
            playerRoomReview.setAuthorPseudo(player.getName());
            playerRoomReview.setDifficulty(difficulty);
            playerRoomReview.setLastUpdated(Instant.now());
        };
        modifyPlayerRoomReview(player, roomId, roomReviewModifier);
    }

    public void submitPuzzleComment(@NotNull Player player, int roomId, @Nullable String title, @NotNull List<String> comment) {
        Consumer<PlayerRoomReview> roomReviewModifier = playerRoomReview -> {
            playerRoomReview.setAuthorPseudo(player.getName());
            playerRoomReview.setCommentTitle(title);
            playerRoomReview.setComment(comment);
            playerRoomReview.setLastUpdated(Instant.now());
        };
        modifyPlayerRoomReview(player, roomId, roomReviewModifier);
    }

    private void modifyPlayerRoomReview(@NotNull Player player, int roomId, Consumer<PlayerRoomReview> roomReviewModifier) {
        final RoomReviews roomReviews = roomsReviewsManager.getRoomReviews(roomId);
        final PlayerRoomReview playerRoomReview = roomReviews
                .getReviewPerPlayerUuid()
                .computeIfAbsent(player.getUniqueId(), uuid -> new PlayerRoomReview());
        roomReviewModifier.accept(playerRoomReview);

        //async file save
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> roomsReviewsManager.saveRoomReviews(roomId, roomReviews));

        //event sending
        PlayerRoomReviewUpdatedEvent event = new PlayerRoomReviewUpdatedEvent(player, roomId, playerRoomReview, roomReviews);
        Bukkit.getPluginManager().callEvent(event);
    }

    public RoomReviews getRoomReviews(int roomId) {
        return roomsReviewsManager.getRoomReviews(roomId);
    }
}
