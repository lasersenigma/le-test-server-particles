package eu.lasersenigma.leplayserverutils.puzzle.review.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.command.provider.PuzzleCommandsProvider;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class PuzzleReviewCommand extends Command<Player> implements JavaPluginEnableEventHandler {

    private static final String COMMAND = "review";
    private final Commands puzzleCommand;

    @Inject
    public PuzzleReviewCommand(
            PuzzleCommandsProvider puzzleCommandsProvider,
            PuzzleReviewCreateCommand puzzleReviewCreateCommand,
            PuzzleReviewViewCommand puzzleReviewViewCommand
    ) {
        super(Player.class, COMMAND);
        this.puzzleCommand = puzzleCommandsProvider.get();
        registerSubCommand(puzzleReviewCreateCommand);
        registerSubCommand(puzzleReviewViewCommand);
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return super.defaultProcessForContainer(commands, player, args);
    }

    @Override
    protected String description(Player executor) {
        return "View or create puzzle reviews";
    }


    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        puzzleCommand.registerCommand(this);
    }
}
