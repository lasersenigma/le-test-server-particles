package eu.lasersenigma.leplayserverutils.puzzle.review.command;

import com.google.inject.Inject;
import eu.lasersenigma.leplayserverutils.puzzle.review.RoomsReviewsController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public class PuzzleReviewCreateCommand extends Command<Player> {
    private static final String COMMAND = "create";
    private final RoomsReviewsController roomsReviewsController;

    @Inject
    public PuzzleReviewCreateCommand(RoomsReviewsController roomsReviewsController) {
        super(Player.class, COMMAND);
        this.roomsReviewsController = roomsReviewsController;
    }

    @Override
    protected boolean process(Commands commands, Player executor, String... args) {
        return roomsReviewsController.createReview(executor);
    }

    @Override
    protected String description(Player executor) {
        return "Review the puzzle";
    }
}
