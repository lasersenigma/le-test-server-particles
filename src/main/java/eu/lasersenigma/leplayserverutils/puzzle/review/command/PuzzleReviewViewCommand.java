package eu.lasersenigma.leplayserverutils.puzzle.review.command;

import com.google.inject.Inject;
import eu.lasersenigma.leplayserverutils.puzzle.review.RoomsReviewsController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public class PuzzleReviewViewCommand extends Command<Player> {
    private static final String COMMAND = "view";
    private final RoomsReviewsController roomReviewsController;

    @Inject
    public PuzzleReviewViewCommand(RoomsReviewsController roomReviewsController) {
        super(Player.class, COMMAND);
        this.roomReviewsController = roomReviewsController;
    }

    @Override
    protected boolean process(Commands commands, Player executor, String... args) {
        return roomReviewsController.viewReviews(executor);
    }

    @Override
    protected String description(Player executor) {
        return "View puzzle reviews";
    }
}
