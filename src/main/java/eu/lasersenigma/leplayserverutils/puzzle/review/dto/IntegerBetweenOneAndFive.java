package eu.lasersenigma.leplayserverutils.puzzle.review.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class IntegerBetweenOneAndFive {
    private int value = 3;

    public void setValue(int value) {
        if (value < 1 || value > 5) {
            throw new IllegalArgumentException("Note must be between 1 and 5");
        }
        this.value = value;
    }

    public void increment() {
        if (value < 5) {
            value++;
        }
    }

    public void decrement() {
        if (value > 1) {
            value--;
        }
    }

    public float getPercentNote() {
        return (value - 1) / 4.0f;
    }

    public boolean isMin() {
        return value == 1;
    }

    public boolean isMax() {
        return value == 5;
    }

    public static List<IntegerBetweenOneAndFive> values() {
        return List.of(
                new IntegerBetweenOneAndFive(1),
                new IntegerBetweenOneAndFive(2),
                new IntegerBetweenOneAndFive(3),
                new IntegerBetweenOneAndFive(4),
                new IntegerBetweenOneAndFive(5)
        );
    }
}
