package eu.lasersenigma.leplayserverutils.puzzle.review.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Optional;

@Setter
@NoArgsConstructor
public class ReviewDiscordWebhookDTO {

    @Getter
    private String roomDescription;

    @Getter
    private String author;

    @Getter
    private float note;

    private Float difficulty;

    private List<String> comment;
    private String commentTitle;

    @Getter
    private RoomReviewsStatsDTO stats;

    public Optional<Float> getDifficulty() {
        return Optional.ofNullable(difficulty);
    }

    public Optional<List<String>> getComment() {
        return Optional.ofNullable(comment);
    }

    public Optional<String> getCommentTitle() {
        return Optional.ofNullable(commentTitle);
    }
}
