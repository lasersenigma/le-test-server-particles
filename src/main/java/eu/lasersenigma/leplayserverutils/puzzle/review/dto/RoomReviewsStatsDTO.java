package eu.lasersenigma.leplayserverutils.puzzle.review.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.Optional;

@Setter
@NoArgsConstructor
public class RoomReviewsStatsDTO {

    private Double noteAverage;

    private Double noteMedian;

    private Double noteStandardDeviation;

    @Getter
    private int noteCount;

    private Double difficultyAverage;

    private Double difficultyMedian;

    private Double difficultyStandardDeviation;

    @Getter
    private int difficultyCount;

    private Instant lastReview;

    public Optional<Double> getNoteAverage() {
        return Optional.ofNullable(noteAverage);
    }

    public Optional<Double> getNoteMedian() {
        return Optional.ofNullable(noteMedian);
    }

    public Optional<Double> getNoteStandardDeviation() {
        return Optional.ofNullable(noteStandardDeviation);
    }

    public Optional<Double> getDifficultyAverage() {
        return Optional.ofNullable(difficultyAverage);
    }

    public Optional<Double> getDifficultyMedian() {
        return Optional.ofNullable(difficultyMedian);
    }

    public Optional<Double> getDifficultyStandardDeviation() {
        return Optional.ofNullable(difficultyStandardDeviation);
    }
}
