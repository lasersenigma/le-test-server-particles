package eu.lasersenigma.leplayserverutils.puzzle.review.entity;

import com.google.gson.annotations.Expose;
import lombok.*;

import java.time.Instant;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class PlayerRoomReview {

    public static final String PAGE_BREAK = "%PAGE_BREAK%";

    @Expose
    private String authorPseudo;

    @Expose
    private Float note = null;

    @Expose
    private Float difficulty = null;

    @Expose
    private String commentTitle = null;

    @Expose
    private List<String> comment = null;

    @Expose
    private Instant lastUpdated = Instant.now();

    public PlayerRoomReview(PlayerRoomReview playerRoomReview) {
        this.authorPseudo = playerRoomReview.getAuthorPseudo();
        this.note = playerRoomReview.getNote();
        this.difficulty = playerRoomReview.getDifficulty();
        this.commentTitle = playerRoomReview.getCommentTitle();
        this.comment = playerRoomReview.getComment();
        this.lastUpdated = playerRoomReview.getLastUpdated();
    }
}
