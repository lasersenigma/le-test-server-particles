package eu.lasersenigma.leplayserverutils.puzzle.review.entity;

import com.google.gson.annotations.Expose;
import lombok.*;

import java.util.HashMap;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class RoomReviews {

    @Expose
    private HashMap<UUID, PlayerRoomReview> reviewPerPlayerUuid = new HashMap<>();

    public RoomReviews(RoomReviews other) {
        this.reviewPerPlayerUuid = other.getReviewPerPlayerUuid().entrySet()
                .stream()
                .collect(HashMap::new, (m, e) -> m.put(e.getKey(), new PlayerRoomReview(e.getValue())), HashMap::putAll);
    }
}
