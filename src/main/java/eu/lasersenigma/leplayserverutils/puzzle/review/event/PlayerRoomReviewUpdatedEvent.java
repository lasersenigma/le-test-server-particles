package eu.lasersenigma.leplayserverutils.puzzle.review.event;

import eu.lasersenigma.leplayserverutils.puzzle.review.entity.PlayerRoomReview;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.RoomReviews;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
@Getter
public class PlayerRoomReviewUpdatedEvent extends Event {
    private static final HandlerList HANDLERS = new HandlerList();
    private final Player player;
    private final int roomId;
    private final PlayerRoomReview playerRoomReview;
    private final RoomReviews roomReviews;


    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public @NotNull HandlerList getHandlers() {
        return HANDLERS;
    }
}
