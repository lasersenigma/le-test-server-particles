package eu.lasersenigma.leplayserverutils.puzzle.review.gui.comment;

import com.google.inject.Singleton;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Singleton
public class CommentLevelMenuItemStackProvider {


    private static final String WRITABLE_BOOK_NBT_NAME = "comment-level-writable-book";
    private static final String LE_PLAY_SERVER_UTILS_ITEM_NAME = "le-play-server-utils-item-name";
    private static final String ROOM_ID = "roomId";
    private static final String TITLE = "Write a comment";
    private static final String AUTHOR = "Lasers-Enigma.eu";
    private static final String ROOM_PREFIX = "Room id: ";
    private static final List<String> LORE = List.of(
            "Click to get a writable book. Write your comments,",
            "report bugs or suggest improvements.",
            "Then sign the book to submit your feedback.");

    public @NotNull ItemStack getBookGiverButton() {
        ItemStack itemStack = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta bookMeta = (BookMeta) itemStack.getItemMeta();
        bookMeta.setTitle(TITLE);
        bookMeta.setAuthor(AUTHOR);
        bookMeta.setLore(LORE);
        itemStack.setItemMeta(bookMeta);
        return itemStack;
    }

    public @NotNull ItemStack getWritableBook(int roomId) {
        ItemStack book = new ItemStack(Material.WRITABLE_BOOK);
        BookMeta bookMeta = (BookMeta) book.getItemMeta();
        bookMeta.setTitle(TITLE);
        bookMeta.setAuthor(AUTHOR);
        bookMeta.setLore(getBookLoreWithRoomDescription(roomId));
        book.setItemMeta(bookMeta);
        return book;
    }

    public Optional<Integer> getRoomIdFromWrittenBook(BookMeta bookMeta) {
        if ((!TITLE.equals(bookMeta.getTitle())) || (!AUTHOR.equals(bookMeta.getAuthor()))) {
            return Optional.empty();
        }
        return getRoomDescriptionFromBookLore(bookMeta.getLore());
    }

    private List<String> getBookLoreWithRoomDescription(int roomId) {
        List<String> lore = new ArrayList<>(LORE);
        lore.add(ROOM_PREFIX + roomId);
        return lore;
    }

    private Optional<Integer> getRoomDescriptionFromBookLore(List<String> lore) {
        if (lore == null) {
            return Optional.empty();
        }
        //check size
        if (lore.size() != LORE.size() + 1) {
            return Optional.empty();
        }
        //check every lore line except last one
        if (!lore.subList(0, LORE.size()).equals(LORE)) {
            return Optional.empty();
        }
        //check last line
        String roomDescriptionWithPrefix = lore.get(LORE.size());
        if (!roomDescriptionWithPrefix.startsWith(ROOM_PREFIX)) {
            return Optional.empty();
        }
        //retrieve room description
        String roomIdStr = roomDescriptionWithPrefix.substring(ROOM_PREFIX.length());
        try {
            return Optional.of(Integer.parseInt(roomIdStr));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    public boolean isSignedBook(@Nullable ItemStack itemStack) {
        if (itemStack == null) {
            return false;
        }
        if (itemStack.getType() != Material.WRITTEN_BOOK) {
            return false;
        }
        if (!itemStack.hasItemMeta()) {
            return false;
        }
        BookMeta bookMeta = (BookMeta) itemStack.getItemMeta();

        return getRoomDescriptionFromBookLore(bookMeta.getLore()).isPresent();
    }
}
