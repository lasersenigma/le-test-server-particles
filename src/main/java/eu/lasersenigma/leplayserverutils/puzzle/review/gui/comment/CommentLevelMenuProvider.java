package eu.lasersenigma.leplayserverutils.puzzle.review.gui.comment;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.gui.type.util.Gui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.gui.MenuItemStackProvider;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.Objects;

@Singleton
public class CommentLevelMenuProvider {

    private static final String MENU_TITLE = "Comments, suggestions or bug reports";

    @Inject
    private MenuItemStackProvider menuItemStackProvider;

    @Inject
    private CommentLevelMenuItemStackProvider commentLevelMenuItemStackProvider;

    @Inject
    private MessageSender msg;

    public Gui getGui(Player player, int roomId) {

        ChestGui gui = new ChestGui(5, MENU_TITLE);
        gui.setOnGlobalClick(event -> event.setCancelled(true));

        OutlinePane background = new OutlinePane(0, 0, 9, 5, Pane.Priority.LOWEST);
        background.addItem(new GuiItem(menuItemStackProvider.getBackgroundItemStack()));
        background.setRepeat(true);
        gui.addPane(background);

        OutlinePane cancelPane = new OutlinePane(8, 0, 1, 1);
        cancelPane.addItem(new GuiItem(menuItemStackProvider.getExitItemStack(), event -> event.getWhoClicked().closeInventory()));
        gui.addPane(cancelPane);

        OutlinePane bookPane = new OutlinePane(4, 3, 1, 1);
        bookPane.addItem(new GuiItem(commentLevelMenuItemStackProvider.getBookGiverButton(), event -> {
            event.getWhoClicked().closeInventory();
            giveWritableBook(player, roomId);
        }));
        gui.addPane(bookPane);
        return gui;
    }

    private void giveWritableBook(Player player, int roomId) {
        //Check first free slot in player shortcut bar or send warning if full
        Integer freeSlotInHotbar = null;
        final PlayerInventory inventory = player.getInventory();
        for (int i = 0; i < 9; i++) {
            if (inventory.getItem(i) == null || Objects.requireNonNull(inventory.getItem(i)).getType() == Material.AIR) {
                freeSlotInHotbar = i;
                break;
            }
        }

        if (freeSlotInHotbar == null) {
            msg.sendWarning(player, "Your shortcut bar is full. Please make some space.");
            return;
        }

        //Give writable book to player
        player.getInventory().setItem(freeSlotInHotbar, commentLevelMenuItemStackProvider.getWritableBook(roomId));

        //Send message to player
        msg.sendSuccess(player, "You received a writable book. " +
                "Write your comments, report bugs or suggest improvements. " +
                "Then sign the book to submit your feedback.");
    }
}
