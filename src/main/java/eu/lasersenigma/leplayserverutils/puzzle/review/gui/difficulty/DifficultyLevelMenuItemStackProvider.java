package eu.lasersenigma.leplayserverutils.puzzle.review.gui.difficulty;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.leplayserverutils.common.gui.MenuItemStackProvider;
import eu.lasersenigma.leplayserverutils.puzzle.review.dto.IntegerBetweenOneAndFive;
import eu.lasersenigma.leplayserverutils.room.dto.RoomDifficulty;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@Singleton
public class DifficultyLevelMenuItemStackProvider {

    //item names
    private static final String STAR = "☆";

    @Inject
    private MenuItemStackProvider menuItemStackProvider;


    @NotNull
    public ItemStack getDifficultyEmote(boolean activated, IntegerBetweenOneAndFive note) {
        final RoomDifficulty roomDifficulty = RoomDifficulty.fromNote(note);
        String texture = activated ? roomDifficulty.getHeadTextureActivated() : roomDifficulty.getHeadTextureDeactivated();
        final ItemStack itemStack = ItemStackUtils.getCustomHead(texture);
        ItemStackUtils.setName(itemStack, "Rate Difficulty");
        ItemStackUtils.setLore(itemStack, List.of(
                "Rate " + roomDifficulty.getDisplayName(),
                STAR.repeat(note.getValue())
        ));
        return itemStack;
    }

    @NotNull
    public ItemStack getDecreaseDifficultyItemStack() {
        final ItemStack customHead = menuItemStackProvider.getDecreaseStarItemStack();
        ItemStackUtils.setName(customHead, "Easier");
        ItemStackUtils.setLore(customHead, List.of("Decrease difficulty evaluation"));
        return customHead;
    }

    @NotNull
    public ItemStack getIncreaseDifficultyItemStack() {
        final ItemStack customHead = ItemsFactory.getInstance().getItemStack(Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE);
        ItemStackUtils.setName(customHead, "Harder");
        ItemStackUtils.setLore(customHead, List.of("Increase difficulty evaluation"));
        return customHead;
    }

    @NotNull
    public ItemStack getDifficultyNumberItemStack(IntegerBetweenOneAndFive note) {
        final ItemStack customHead = menuItemStackProvider.getNumberItemStackFromNote(note);
        ItemStackUtils.setName(customHead, note.getValue() + " stars");
        ItemStackUtils.setLore(customHead, List.of(
                STAR.repeat(note.getValue())
        ));
        return customHead;
    }

    @NotNull
    public ItemStack getValidateItemStack(IntegerBetweenOneAndFive note) {
        final RoomDifficulty roomDifficulty = RoomDifficulty.fromNote(note);
        final ItemStack customHead = menuItemStackProvider.getValidateItemStack();
        ItemStackUtils.setLore(customHead, List.of(
                "Rate " + roomDifficulty.getDisplayName(),
                STAR.repeat(note.getValue())
        ));
        return customHead;
    }

}
