package eu.lasersenigma.leplayserverutils.puzzle.review.gui.difficulty;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import eu.lasersenigma.leplayserverutils.puzzle.review.RoomsReviewsController;
import eu.lasersenigma.leplayserverutils.puzzle.review.dto.IntegerBetweenOneAndFive;
import eu.lasersenigma.leplayserverutils.puzzle.review.gui.comment.CommentLevelMenuProvider;
import org.bukkit.entity.Player;

public class DifficultyLevelMenuUpdater {

    private final int roomId;
    private final StaticPane difficultyDependingPane;
    private final ChestGui gui;
    private final DifficultyLevelMenuItemStackProvider difficultyLevelMenuItemStackProvider;
    private final RoomsReviewsController roomsReviewsController;
    private final CommentLevelMenuProvider commentLevelMenuProvider;

    private final IntegerBetweenOneAndFive difficulty;

    public DifficultyLevelMenuUpdater(
            int roomId,
            StaticPane difficultyDependingPane,
            ChestGui gui,
            DifficultyLevelMenuItemStackProvider difficultyLevelMenuItemStackProvider,
            RoomsReviewsController roomsReviewsController,
            CommentLevelMenuProvider commentLevelMenuProvider) {
        this.roomId = roomId;
        this.difficultyDependingPane = difficultyDependingPane;
        this.gui = gui;
        this.difficultyLevelMenuItemStackProvider = difficultyLevelMenuItemStackProvider;
        this.roomsReviewsController = roomsReviewsController;
        this.commentLevelMenuProvider = commentLevelMenuProvider;
        difficulty = new IntegerBetweenOneAndFive();
    }

    public void updateDifficultyDependingPane() {
        updateDifficultyDependingPane(false);
    }

    public void updateDifficultyDependingPane(boolean initialLoading) {
        if (!initialLoading) {
            difficultyDependingPane.clear();
        }
        //Set stars on first line
        IntegerBetweenOneAndFive.values().forEach(curDifficulty -> {
            boolean isActivated = curDifficulty.equals(difficulty);
            GuiItem item = new GuiItem(
                    difficultyLevelMenuItemStackProvider.getDifficultyEmote(isActivated, curDifficulty),
                    event -> {
                        this.difficulty.setValue(curDifficulty.getValue());
                        updateDifficultyDependingPane();
                    });
            difficultyDependingPane.addItem(item, curDifficulty.getValue() - 1, 0);
        });

        //Set increase button on third line
        if (!difficulty.isMin()) {
            final GuiItem decreaseNoteButton = new GuiItem(
                    difficultyLevelMenuItemStackProvider.getDecreaseDifficultyItemStack(),
                    event -> {
                        difficulty.decrement();
                        updateDifficultyDependingPane();
                    });
            difficultyDependingPane.addItem(decreaseNoteButton, 0, 2);
        }

        //Set decrease button on third line
        if (!difficulty.isMax()) {
            final GuiItem increaseNoteButton = new GuiItem(
                    difficultyLevelMenuItemStackProvider.getIncreaseDifficultyItemStack(),
                    event -> {
                        difficulty.increment();
                        updateDifficultyDependingPane();
                    });
            difficultyDependingPane.addItem(increaseNoteButton, 4, 2);
        }

        //Set current note on third line
        difficultyDependingPane.addItem(
                new GuiItem(difficultyLevelMenuItemStackProvider.getDifficultyNumberItemStack(difficulty)),
                2, 2);

        //Set validate button on fourth line
        difficultyDependingPane.addItem(
                new GuiItem(
                        difficultyLevelMenuItemStackProvider.getValidateItemStack(difficulty),
                        event -> {
                            event.getWhoClicked().closeInventory();
                            if (event.getWhoClicked() instanceof Player player) {
                                roomsReviewsController.submitPuzzleDifficulty(player, roomId, difficulty.getPercentNote());

                                commentLevelMenuProvider.getGui(player, roomId).show(player);
                            } else {
                                throw new IllegalStateException("Only player can rate a puzzle");
                            }
                        }
                ),
                2, 4);

        if (!initialLoading) {
            gui.update();
        }
    }
}
