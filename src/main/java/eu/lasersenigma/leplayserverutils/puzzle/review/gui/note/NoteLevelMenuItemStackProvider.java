package eu.lasersenigma.leplayserverutils.puzzle.review.gui.note;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.gui.MenuItemStackProvider;
import eu.lasersenigma.leplayserverutils.puzzle.review.dto.IntegerBetweenOneAndFive;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@Singleton
public class NoteLevelMenuItemStackProvider {

    //item names
    private static final String STAR = "☆";

    @Inject
    private MenuItemStackProvider menuItemStackProvider;


    @NotNull
    public ItemStack getStarItemStack(boolean activated, IntegerBetweenOneAndFive note) {
        final ItemStack itemStack = menuItemStackProvider.getStarItemStack(activated);
        ItemStackUtils.setName(itemStack, "Rate this Puzzle");
        ItemStackUtils.setLore(itemStack, List.of(
                "Rate " + note.getValue() + "/5 stars",
                STAR.repeat(note.getValue())
        ));
        return itemStack;
    }

    @NotNull
    public ItemStack getDecreaseNoteItemStack() {
        final ItemStack customHead = menuItemStackProvider.getDecreaseStarItemStack();
        ItemStackUtils.setName(customHead, "Decrease rating");
        ItemStackUtils.setLore(customHead, List.of("Decrease rating by 1 star"));
        return customHead;
    }

    @NotNull
    public ItemStack getIncreaseNoteItemStack() {
        final ItemStack customHead = menuItemStackProvider.getIncreaseStarItemStack();
        ItemStackUtils.setName(customHead, "Increase rating");
        ItemStackUtils.setLore(customHead, List.of("Increase rating by 1 star"));
        return customHead;
    }

    @NotNull
    public ItemStack getNoteNumberItemStack(IntegerBetweenOneAndFive integerBetweenOneAndFive) {
        ItemStack customHead = menuItemStackProvider.getNumberItemStackFromNote(integerBetweenOneAndFive);
        ItemStackUtils.setName(customHead, integerBetweenOneAndFive.getValue() + " stars");
        ItemStackUtils.setLore(customHead, List.of(
                STAR.repeat(integerBetweenOneAndFive.getValue())
        ));
        return customHead;
    }

    @NotNull
    public ItemStack getValidateItemStack(IntegerBetweenOneAndFive note) {
        final ItemStack customHead = menuItemStackProvider.getValidateItemStack();
        ItemStackUtils.setLore(customHead, List.of(
                "Rate " + note.getValue() + "/5 stars",
                STAR.repeat(note.getValue())
        ));
        return customHead;
    }

}
