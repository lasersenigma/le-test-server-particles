package eu.lasersenigma.leplayserverutils.puzzle.review.gui.note;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.gui.MenuItemStackProvider;
import eu.lasersenigma.leplayserverutils.puzzle.review.RoomsReviewsController;
import eu.lasersenigma.leplayserverutils.puzzle.review.gui.difficulty.DifficultyLevelMenuProvider;

@Singleton
public class NoteLevelMenuProvider {
    private static final String MENU_TITLE = "Rate the puzzle";

    @Inject
    private NoteLevelMenuItemStackProvider noteLevelMenuItemStackProvider;

    @Inject
    private MenuItemStackProvider menuItemStackProvider;

    @Inject
    private RoomsReviewsController roomsReviewsController;

    @Inject
    private DifficultyLevelMenuProvider difficultyLevelMenuProvider;

    public ChestGui getGui(int roomToRateId) {

        ChestGui gui = new ChestGui(5, MENU_TITLE);
        gui.setOnGlobalClick(event -> event.setCancelled(true));

        OutlinePane background = new OutlinePane(0, 0, 9, 5, Pane.Priority.LOWEST);
        background.addItem(new GuiItem(menuItemStackProvider.getBackgroundItemStack()));
        background.setRepeat(true);
        gui.addPane(background);

        OutlinePane cancelPane = new OutlinePane(8, 0, 1, 1);
        cancelPane.addItem(new GuiItem(menuItemStackProvider.getExitItemStack(), event -> event.getWhoClicked().closeInventory()));
        gui.addPane(cancelPane);

        StaticPane noteDependingPane = new StaticPane(2, 0, 5, 5);

        //The variable bind to this instance of the GUI
        final NoteLevelMenuUpdater noteLevelMenuUpdater = new NoteLevelMenuUpdater(
                roomToRateId,
                noteDependingPane,
                gui,
                noteLevelMenuItemStackProvider,
                roomsReviewsController,
                difficultyLevelMenuProvider
        );
        noteLevelMenuUpdater.updateNoteDependingPane(true);

        gui.addPane(noteDependingPane);
        return gui;
    }
}
