package eu.lasersenigma.leplayserverutils.puzzle.review.gui.note;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import eu.lasersenigma.leplayserverutils.puzzle.review.RoomsReviewsController;
import eu.lasersenigma.leplayserverutils.puzzle.review.dto.IntegerBetweenOneAndFive;
import eu.lasersenigma.leplayserverutils.puzzle.review.gui.difficulty.DifficultyLevelMenuProvider;
import org.bukkit.entity.Player;

public class NoteLevelMenuUpdater {

    private final int roomToRateId;
    private final StaticPane noteDependingPane;
    private final ChestGui gui;
    private final NoteLevelMenuItemStackProvider noteLevelMenuItemStackProvider;
    private final RoomsReviewsController roomsReviewsController;
    private final IntegerBetweenOneAndFive note;
    private final DifficultyLevelMenuProvider difficultyLevelMenuProvider;

    public NoteLevelMenuUpdater(
            int roomToRateId,
            StaticPane noteDependingPane,
            ChestGui gui,
            NoteLevelMenuItemStackProvider noteLevelMenuItemStackProvider,
            RoomsReviewsController roomsReviewsController,
            DifficultyLevelMenuProvider difficultyLevelMenuProvider) {
        this.roomToRateId = roomToRateId;
        this.noteDependingPane = noteDependingPane;
        this.gui = gui;
        this.noteLevelMenuItemStackProvider = noteLevelMenuItemStackProvider;
        this.roomsReviewsController = roomsReviewsController;
        this.difficultyLevelMenuProvider = difficultyLevelMenuProvider;
        note = new IntegerBetweenOneAndFive();
    }

    public void updateNoteDependingPane() {
        updateNoteDependingPane(false);
    }

    public void updateNoteDependingPane(boolean initialLoading) {
        if (!initialLoading) {
            noteDependingPane.clear();
        }
        //Set stars on first line
        IntegerBetweenOneAndFive.values().forEach(curNote -> {
            boolean isStarYellow = curNote.getValue() <= note.getValue();
            GuiItem item = new GuiItem(
                    noteLevelMenuItemStackProvider.getStarItemStack(isStarYellow, curNote),
                    event -> {
                        this.note.setValue(curNote.getValue());
                        updateNoteDependingPane();
                    });
            noteDependingPane.addItem(item, curNote.getValue() - 1, 0);
        });

        //Set increase button on third line
        if (!note.isMin()) {
            final GuiItem decreaseNoteButton = new GuiItem(
                    noteLevelMenuItemStackProvider.getDecreaseNoteItemStack(),
                    event -> {
                        note.decrement();
                        updateNoteDependingPane();
                    });
            noteDependingPane.addItem(decreaseNoteButton, 0, 2);
        }

        //Set decrease button on third line
        if (!note.isMax()) {
            final GuiItem increaseNoteButton = new GuiItem(
                    noteLevelMenuItemStackProvider.getIncreaseNoteItemStack(),
                    event -> {
                        note.increment();
                        updateNoteDependingPane();
                    });
            noteDependingPane.addItem(increaseNoteButton, 4, 2);
        }

        //Set current note on third line
        noteDependingPane.addItem(
                new GuiItem(noteLevelMenuItemStackProvider.getNoteNumberItemStack(note)),
                2, 2);

        //Set validate button on fourth line
        noteDependingPane.addItem(
                new GuiItem(
                        noteLevelMenuItemStackProvider.getValidateItemStack(note),
                        event -> {
                            event.getWhoClicked().closeInventory();
                            if (event.getWhoClicked() instanceof Player player) {
                                //save note
                                roomsReviewsController.submitPuzzleNote(player, roomToRateId, note.getPercentNote());
                                //open next rating menu
                                difficultyLevelMenuProvider.getGui(roomToRateId).show(player);
                            } else {
                                throw new IllegalStateException("Only player can rate a puzzle");
                            }
                        }
                ),
                2, 4);

        if (!initialLoading) {
            gui.update();
        }
    }
}
