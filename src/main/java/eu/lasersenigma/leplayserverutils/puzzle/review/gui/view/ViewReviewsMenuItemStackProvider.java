package eu.lasersenigma.leplayserverutils.puzzle.review.gui.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.gui.MenuItemStackProvider;
import eu.lasersenigma.leplayserverutils.puzzle.review.dto.RoomReviewsStatsDTO;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.PlayerRoomReview;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.RoomReviews;
import eu.lasersenigma.leplayserverutils.puzzle.review.mapper.RoomReviewMapper;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import net.kyori.adventure.inventory.Book;
import net.kyori.adventure.text.Component;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Singleton
public class ViewReviewsMenuItemStackProvider {

    public static final String STATS_TEXTURE_URL = "438cf3f8e54afc3b3f91d20a49f324dca1486007fe545399055524c17941f4dc";

    @Inject
    private MenuItemStackProvider menuItemStackProvider;

    @Inject
    private RoomReviewMapper roomReviewMapper;

    public @NotNull ItemStack getNavNextPage() {
        return menuItemStackProvider.getNextPageItemStack();
    }

    public @NotNull ItemStack getNavPrevPage() {
        return menuItemStackProvider.getPreviousPageItemStack();
    }

    public @NotNull ItemStack getStatsButton() {
        ItemStack itemStack = ItemStackUtils.getCustomHead(STATS_TEXTURE_URL);
        ItemStackUtils.setName(itemStack, "Room Stats");
        ItemStackUtils.setLore(itemStack, List.of(
                "View room reviews stats"
                ));
        return itemStack;
    }

    public Book getStatsBook(Room room, RoomReviews roomReviews) {
        final RoomReviewsStatsDTO stats = roomReviewMapper.toStats(roomReviews);
        return Book.builder()
                .title(Component.text("Room Stats"))
                .author(Component.text(room.getDescription()))
                .addPage(roomReviewMapper.toNoteStatsPageComponent(stats))
                .addPage(roomReviewMapper.toDifficultyStatsPageComponent(stats))
                .build();
    }

    public @NotNull List<ReviewBookAndPlayerHead> getReviewBooksAndPlayerHeads(Room room, RoomReviews roomReviews) {
        return roomReviews.getReviewPerPlayerUuid().entrySet().stream()
                .sorted(Comparator.comparingLong(entry -> entry.getValue().getLastUpdated().toEpochMilli()))
                .map(entry -> {
                    UUID playerUUID = entry.getKey();
                    final PlayerRoomReview playerReview = entry.getValue();
                    final Book reviewBook = roomReviewMapper.toReviewBook(room, playerReview);
                    final ItemStack playerHead = getReviewPlayerHead(playerUUID, entry.getValue());
                    return new ReviewBookAndPlayerHead(reviewBook, playerHead);
                })
                .toList();
    }

    private ItemStack getReviewPlayerHead(UUID playerUUID, PlayerRoomReview review) {
            ItemStack itemStack = ItemStackUtils.getPlayerSkull(playerUUID);
            ItemStackUtils.setName(itemStack, review.getAuthorPseudo());
            ItemStackUtils.setLore(itemStack, roomReviewMapper.toReviewPlayerHeadLore(review));
            return itemStack;
    }

    public record ReviewBookAndPlayerHead(Book book, ItemStack playerHead) {}
}
