package eu.lasersenigma.leplayserverutils.puzzle.review.gui.view;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.gui.type.util.Gui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.PaginatedPane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.gui.MenuItemStackProvider;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.puzzle.review.RoomsReviewsService;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.RoomReviews;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import net.kyori.adventure.inventory.Book;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class ViewReviewsMenuProvider {

    @Inject
    private MenuItemStackProvider menuItemStackProvider;

    @Inject
    private ViewReviewsMenuItemStackProvider viewReviewsMenuItemStackProvider;

    @Inject
    private RoomsReviewsService roomsReviewsService;

    @Inject
    private MessageSender msg;

    public Gui getGui(Room room) {

        final RoomReviews roomReviews = roomsReviewsService.getRoomReviews(room.getId());

        ChestGui gui = new ChestGui(5, room.getDescription());
        gui.setOnGlobalClick(event -> event.setCancelled(true));

        final PaginatedPane reviewsPane = getPaginatedPane(room, roomReviews);

        gui.addPane(getBackgroundPane());
        gui.addPane(getCancelPane());
        gui.addPane(getStatsPane(room, roomReviews));
        gui.addPane(reviewsPane);
        gui.addPane(getReviewsNavigationPane(reviewsPane, gui));
        return gui;
    }

    private @NotNull OutlinePane getBackgroundPane() {
        OutlinePane background = new OutlinePane(0, 0, 9, 5, Pane.Priority.LOWEST);
        background.addItem(new GuiItem(menuItemStackProvider.getBackgroundItemStack()));
        background.setRepeat(true);
        return background;
    }

    private @NotNull OutlinePane getCancelPane() {
        OutlinePane cancelPane = new OutlinePane(8, 0, 1, 1);
        cancelPane.addItem(new GuiItem(menuItemStackProvider.getExitItemStack(), event -> event.getWhoClicked().closeInventory()));
        return cancelPane;
    }

    private @NotNull OutlinePane getStatsPane(Room room, RoomReviews roomReviews) {
        OutlinePane statsPane = new OutlinePane(8, 2, 1, 1);
        statsPane.addItem(new GuiItem(
                viewReviewsMenuItemStackProvider.getStatsButton(),
                event -> {
                    Book book = viewReviewsMenuItemStackProvider.getStatsBook(room, roomReviews);
                    event.getWhoClicked().openBook(book);
                }
        ));
        return statsPane;
    }

    private @NotNull PaginatedPane getPaginatedPane(Room room, RoomReviews roomReviews) {
        PaginatedPane reviewsPane = new PaginatedPane(0, 0, 8, 4);

        List<GuiItem> guiItems = viewReviewsMenuItemStackProvider.getReviewBooksAndPlayerHeads(room, roomReviews)
                .stream().map(bookAndPlayerHead -> {
                    if (bookAndPlayerHead.book() == null) {
                        return new GuiItem(bookAndPlayerHead.playerHead());
                    }
                    return new GuiItem(bookAndPlayerHead.playerHead(),
                            event -> event.getWhoClicked().openBook(bookAndPlayerHead.book())
                    );
                })
                .collect(Collectors.toList());

        reviewsPane.populateWithGuiItems(guiItems);
        return reviewsPane;
    }

    private @NotNull StaticPane getReviewsNavigationPane(PaginatedPane pages, ChestGui gui) {
        StaticPane navigation = new StaticPane(0, 4, 8, 1);
        navigation.setPriority(Pane.Priority.NORMAL);

        final ItemStack previousPageItemStack = viewReviewsMenuItemStackProvider.getNavPrevPage();
        navigation.addItem(new GuiItem(previousPageItemStack, event -> {
            if (pages.getPage() > 0) {
                pages.setPage(pages.getPage() - 1);

                gui.update();
            }
        }), 0, 0);

        final ItemStack nextPageItemStack = viewReviewsMenuItemStackProvider.getNavNextPage();
        navigation.addItem(new GuiItem(nextPageItemStack, event -> {
            if (pages.getPage() < pages.getPages() - 1) {
                pages.setPage(pages.getPage() + 1);

                gui.update();
            }
        }), 7, 0);

        return navigation;
    }
}
