package eu.lasersenigma.leplayserverutils.puzzle.review.listener;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.puzzle.review.RoomReviewDiscordSendindService;
import eu.lasersenigma.leplayserverutils.puzzle.review.RoomsReviewsService;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.RoomReviews;
import eu.lasersenigma.leplayserverutils.puzzle.review.event.PlayerRoomReviewUpdatedEvent;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Singleton
public class DelayedReviewUpdateListener implements LEPSUListener {

    private static final int DISCORD_SENDING_DELAY_TICKS = 20 * 60;

    @Inject
    private RoomReviewDiscordSendindService roomsReviewsDiscordSendingService;

    @Inject
    private RoomsReviewsService roomsReviewsService;

    @Inject
    private ConfigManager configManager;

    @Inject
    private RoomController roomController;

    @Inject
    private LEPlayServerUtils plugin;

    public Map<PlayerRoomReviewKey, Integer> taskIdPerPlayerRoomReview = new HashMap<>();

    @EventHandler
    public void onDelayedReviewUpdate(PlayerRoomReviewUpdatedEvent event) {
        PlayerRoomReviewKey playerRoomReviewKey = new PlayerRoomReviewKey(event.getRoomId(), event.getPlayer().getUniqueId());
        if (taskIdPerPlayerRoomReview.containsKey(playerRoomReviewKey)) {
            int taskId = taskIdPerPlayerRoomReview.get(playerRoomReviewKey);
            event.getPlayer().getServer().getScheduler().cancelTask(taskId);
            taskIdPerPlayerRoomReview.remove(playerRoomReviewKey);
        }
        int taskId = Bukkit.getScheduler().runTaskLater(
                        plugin,
                        () -> {
                            RoomReviews currentRoomReviews = roomsReviewsService.getRoomReviews(event.getRoomId());

                            final Config config = configManager.get();
                            final Optional<Room> oRoom = roomController.findRoomById(event.getRoomId(), config);
                            if (oRoom.isEmpty()) {
                                throw new IllegalStateException("Room with id %s not found but this listener were notified that its review was updated".formatted(event.getRoomId()));
                            }
                            final Room room = oRoom.get();


                            roomsReviewsDiscordSendingService.send(
                                    room,
                                    currentRoomReviews,
                                    event.getPlayerRoomReview(),
                                    config
                            );
                            taskIdPerPlayerRoomReview.remove(playerRoomReviewKey);
                        },
                        DISCORD_SENDING_DELAY_TICKS
                )
                .getTaskId();
        taskIdPerPlayerRoomReview.put(playerRoomReviewKey, taskId);
    }

    @Getter
    @AllArgsConstructor
    @EqualsAndHashCode
    @ToString
    public static class PlayerRoomReviewKey {
        private int roomId;
        private UUID playerUUID;
    }

}
