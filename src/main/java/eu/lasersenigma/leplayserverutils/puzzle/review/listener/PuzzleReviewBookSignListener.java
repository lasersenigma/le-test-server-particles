package eu.lasersenigma.leplayserverutils.puzzle.review.listener;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.puzzle.review.RoomsReviewsController;
import eu.lasersenigma.leplayserverutils.puzzle.review.gui.comment.CommentLevelMenuItemStackProvider;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

@Singleton
public class PuzzleReviewBookSignListener implements LEPSUListener {

    @Inject
    private CommentLevelMenuItemStackProvider commentLevelMenuItemStackProvider;

    @Inject
    private RoomsReviewsController roomsReviewsController;

    @Inject
    private LEPlayServerUtils plugin;

    @EventHandler
    public void onPlayerSignBook(PlayerEditBookEvent event) {
        if (!event.isSigning()) {
            return;
        }

        Optional<Integer> oRoomId = commentLevelMenuItemStackProvider.getRoomIdFromWrittenBook(event.getPreviousBookMeta());

        if (oRoomId.isEmpty()) {
            return;
        }
        int roomId = oRoomId.get();

        Player player = event.getPlayer();

        BookMeta bookMeta = event.getNewBookMeta();

        // Récupérer le contenu du livre signé
        List<String> content = bookMeta.getPages();

        String title = bookMeta.getTitle();

        //save comment
        roomsReviewsController.submitPuzzleComment(player, roomId, title, content);

        // Delete the signed book
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            final @Nullable ItemStack[] contents = player.getInventory().getContents();
            for (int i = 0; i < 9; i++) {
                ItemStack itemStack = contents[i];
                if (commentLevelMenuItemStackProvider.isSignedBook(itemStack)) {
                    player.getInventory().setItem(i, null);
                }
            }
        }, 1L);
    }
}
