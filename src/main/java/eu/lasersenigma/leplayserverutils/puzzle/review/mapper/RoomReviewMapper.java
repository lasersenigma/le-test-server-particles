package eu.lasersenigma.leplayserverutils.puzzle.review.mapper;

import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.messages.MessageComponentColor;
import eu.lasersenigma.leplayserverutils.puzzle.review.dto.ReviewDiscordWebhookDTO;
import eu.lasersenigma.leplayserverutils.puzzle.review.dto.RoomReviewsStatsDTO;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.PlayerRoomReview;
import eu.lasersenigma.leplayserverutils.puzzle.review.entity.RoomReviews;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import net.kyori.adventure.inventory.Book;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.Style;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Singleton
public class RoomReviewMapper {

    private static final DecimalFormat STATS_DECIMAL_FORMAT =
            new DecimalFormat("#.###", new DecimalFormatSymbols(Locale.ENGLISH));

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public @NotNull WebhookEmbed toWebhookEmbed(@NotNull Room room, @NotNull RoomReviews roomReviews, @NotNull PlayerRoomReview playerRoomReview) {
        return toWebhookEmbed(
                toDto(room, roomReviews, playerRoomReview)
        );
    }

    public @NotNull Component toNoteStatsPageComponent(@NotNull RoomReviewsStatsDTO stats) {
        Component component = Component.textOfChildren(
                Component.text("Note", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.UNDERLINED, TextDecoration.BOLD)),
                Component.newline());

        if (stats.getNoteAverage().isPresent()) {
            component = Component.textOfChildren(
                    component,
                    Component.text("   "),
                    Component.text("Average:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.UNDERLINED)),
                    Component.text(" "),
                    Component.text(STATS_DECIMAL_FORMAT.format(stats.getNoteAverage().get()), Style.style(MessageComponentColor.HIGHLIGHT.getColor())),
                    Component.newline()
            );
        }

        if (stats.getNoteMedian().isPresent()) {
            component = Component.textOfChildren(
                    component,
                    Component.text("   "),
                    Component.text("Median:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.UNDERLINED)),
                    Component.text(" "),
                    Component.text(STATS_DECIMAL_FORMAT.format(stats.getNoteMedian().get()), Style.style(MessageComponentColor.HIGHLIGHT.getColor())),
                    Component.newline()
            );
        }

        if (stats.getNoteStandardDeviation().isPresent()) {
            component = Component.textOfChildren(
                    component,
                    Component.text("   "),
                    Component.text("Standard deviation:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.UNDERLINED)),
                    Component.text(" "),
                    Component.text(STATS_DECIMAL_FORMAT.format(stats.getNoteStandardDeviation().get()), Style.style(MessageComponentColor.HIGHLIGHT.getColor())),
                    Component.newline()
            );
        }

        return Component.textOfChildren(
                component,
                Component.text("   "),
                Component.text("Note count:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.UNDERLINED)),
                Component.text(" "),
                Component.text(String.valueOf(stats.getNoteCount()), Style.style(MessageComponentColor.HIGHLIGHT.getColor()))
        );
    }

    public @NotNull Component toDifficultyStatsPageComponent(@NotNull RoomReviewsStatsDTO stats) {
        Component component = Component.textOfChildren(
                Component.text("Difficulty", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.UNDERLINED, TextDecoration.BOLD)),
                Component.newline()
        );

        if (stats.getDifficultyAverage().isPresent()) {
            component = Component.textOfChildren(
                    component,
                    Component.text("   "),
                    Component.text("Average:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.UNDERLINED)),
                    Component.text(" "),
                    Component.text(STATS_DECIMAL_FORMAT.format(stats.getDifficultyAverage().get()), Style.style(MessageComponentColor.HIGHLIGHT.getColor())),
                    Component.newline()
            );
        }

        if (stats.getDifficultyMedian().isPresent()) {
            component = Component.textOfChildren(
                    component,
                    Component.text("   "),
                    Component.text("Median:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.UNDERLINED)),
                    Component.text(" "),
                    Component.text(STATS_DECIMAL_FORMAT.format(stats.getDifficultyMedian().get()), Style.style(MessageComponentColor.HIGHLIGHT.getColor())),
                    Component.newline()
            );
        }

        if (stats.getDifficultyStandardDeviation().isPresent()) {
            component = Component.textOfChildren(
                    component,
                    Component.text("   "),
                    Component.text("Standard deviation:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.UNDERLINED)),
                    Component.text(" "),
                    Component.text(STATS_DECIMAL_FORMAT.format(stats.getDifficultyStandardDeviation().get()), Style.style(MessageComponentColor.HIGHLIGHT.getColor())),
                    Component.newline()
            );
        }

        return Component.textOfChildren(
                component,
                Component.text("   "),
                Component.text("Difficulty note count:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.UNDERLINED)),
                Component.text(" "),
                Component.text(String.valueOf(stats.getDifficultyCount()), Style.style(MessageComponentColor.HIGHLIGHT.getColor()))
        );
    }

    public RoomReviewsStatsDTO toStats(@NotNull RoomReviews roomReviews) {
        RoomReviewsStatsDTO stats = new RoomReviewsStatsDTO();
        Optional<DescriptiveStatistics> noteStats = Optional.empty();
        Optional<DescriptiveStatistics> difficultyStats = Optional.empty();
        for (PlayerRoomReview review : roomReviews.getReviewPerPlayerUuid().values()) {
            if (Objects.nonNull(review.getNote())) {
                if (noteStats.isEmpty()) {
                    noteStats = Optional.of(new DescriptiveStatistics());
                }
                noteStats.get().addValue(Double.valueOf(review.getNote()));
            }
            if (Objects.nonNull(review.getDifficulty())) {
                if (difficultyStats.isEmpty()) {
                    difficultyStats = Optional.of(new DescriptiveStatistics());
                }
                difficultyStats.get().addValue(Double.valueOf(review.getDifficulty()));
            }
        }

        Instant lastUpdated = roomReviews.getReviewPerPlayerUuid().values().stream()
                .map(PlayerRoomReview::getLastUpdated)
                .max(Instant::compareTo)
                .orElse(Instant.now());

        stats.setNoteAverage(noteStats.map(DescriptiveStatistics::getMean).orElse(null));
        stats.setNoteStandardDeviation(noteStats.map(DescriptiveStatistics::getStandardDeviation).orElse(null));
        stats.setNoteMedian(noteStats.map(descriptiveStatistics -> descriptiveStatistics.getPercentile(50)).orElse(null));
        stats.setNoteCount(noteStats.map(DescriptiveStatistics::getN).orElse(0L).intValue());

        stats.setDifficultyAverage(difficultyStats.map(DescriptiveStatistics::getMean).orElse(null));
        stats.setDifficultyStandardDeviation(difficultyStats.map(DescriptiveStatistics::getStandardDeviation).orElse(null));
        stats.setDifficultyMedian(difficultyStats.map(descriptiveStatistics -> descriptiveStatistics.getPercentile(50)).orElse(null));
        stats.setDifficultyCount(difficultyStats.map(DescriptiveStatistics::getN).orElse(0L).intValue());

        stats.setLastReview(lastUpdated);

        return stats;
    }

    public Book toReviewBook(Room room, PlayerRoomReview playerReview) {
        if (playerReview.getComment() == null) {
            return null;
        }
        final Book.Builder bookBuilder = Book.builder()
                .title(Component.text("Review"))
                .author(Component.text(playerReview.getAuthorPseudo()))
                .addPage(toReviewBookFirstPage(room, playerReview));

        playerReview.getComment()
                .forEach(page ->
                        bookBuilder.addPage(LegacyComponentSerializer.legacySection().deserialize(page))
                );

        return bookBuilder.build();
    }

    private @NotNull Component toReviewBookFirstPage(Room room, PlayerRoomReview playerReview) {
        Component component = Component.textOfChildren(
                Component.text("Room:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.BOLD, TextDecoration.UNDERLINED)),
                Component.text(" "),
                Component.text(room.getDescription(), MessageComponentColor.HIGHLIGHT.getColor()),
                Component.newline(),
                Component.text("Date:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.BOLD, TextDecoration.UNDERLINED)),
                Component.text(" "),
                Component.text(formatDate(playerReview), MessageComponentColor.HIGHLIGHT.getColor()),
                Component.newline(),
                Component.text("Note:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.BOLD, TextDecoration.UNDERLINED)),
                Component.text(" "),
                Component.text(playerReview.getNote(), MessageComponentColor.HIGHLIGHT.getColor())
        );

        if (playerReview.getDifficulty() != null) {
            component = Component.textOfChildren(
                    component,
                    Component.newline(),
                    Component.text("Difficulty:", Style.style(MessageComponentColor.BLACK.getColor(), TextDecoration.BOLD, TextDecoration.UNDERLINED)),
                    Component.text(" "),
                    Component.text(playerReview.getDifficulty(), MessageComponentColor.HIGHLIGHT.getColor())
            );
        }

        return component;
    }

    private static @NotNull String formatDate(PlayerRoomReview playerReview) {
        final Instant lastUpdated = playerReview.getLastUpdated();
        LocalDateTime dateTime = LocalDateTime.ofInstant(lastUpdated, ZoneOffset.UTC);
        return DATE_TIME_FORMATTER.format(dateTime) + " (UTC)";
    }

    public List<String> toReviewPlayerHeadLore(PlayerRoomReview review) {
        List<String> lore = new ArrayList<>();
        lore.add("Date: " + formatDate(review));
        lore.add("Note: " + review.getNote());
        if (review.getDifficulty() != null) {
            lore.add("Difficulty: " + review.getDifficulty());
        }
        return lore;
    }

    private @NotNull WebhookEmbed toWebhookEmbed(ReviewDiscordWebhookDTO dto) {
        return new WebhookEmbedBuilder()
                .setColor(0xFF00EE)
                .setAuthor(new WebhookEmbed.EmbedAuthor(dto.getAuthor(), null, null))
                .setTitle(new WebhookEmbed.EmbedTitle(dto.getRoomDescription(), null))
                .addField(new WebhookEmbed.EmbedField(true, "Stats", toWebhookStatsStr(dto)))
                .setDescription(toWebhookContentStr(dto))
                .build();
    }

    private @NotNull String toWebhookContentStr(ReviewDiscordWebhookDTO dto) {
        StringBuilder sb = new StringBuilder("\n");
        sb.append("__Note:__ ").append(dto.getNote()).append("\n");
        dto.getDifficulty().ifPresent(difficulty ->
                sb.append("__Difficulty:__ ").append(difficulty).append("\n"));
        dto.getCommentTitle().ifPresent(commentTitle ->
                sb.append("__Comment title:__ ").append(commentTitle).append("\n"));
        dto.getComment().ifPresent(comment ->
                sb.append("__Comment:__ ").append("\n").append(String.join("\n", comment)).append("\n"));
        return sb.toString();
    }

    private @NotNull String toWebhookStatsStr(ReviewDiscordWebhookDTO dto) {
        final RoomReviewsStatsDTO stats = dto.getStats();
        return "\n" + getWebhookDifficultyStatsStr(stats) + "\n\n" + getWebhookNoteStatsStr(stats);
    }

    private String getWebhookNoteStatsStr(RoomReviewsStatsDTO stats) {
        StringBuilder noteSB = new StringBuilder();
        noteSB.append("**__Note__**\n");
        stats.getNoteAverage().ifPresent(average ->
                noteSB.append("__Average:__ ").append(average).append("\n"));
        stats.getNoteMedian().ifPresent(median ->
                noteSB.append("__Median:__ ").append(median).append("\n"));
        stats.getNoteStandardDeviation().ifPresent(standardDeviation ->
                noteSB.append("__Standard deviation:__ ").append(standardDeviation).append("\n"));
        noteSB.append("__Note count:__ ").append(stats.getNoteCount()).append("\n");
        return noteSB.toString();
    }

    private String getWebhookDifficultyStatsStr(RoomReviewsStatsDTO stats) {
        StringBuilder difficultySB = new StringBuilder("**__Difficulty__**\n");
        stats.getDifficultyAverage().ifPresent(average ->
                difficultySB.append("__Average:__ ").append(average).append("\n"));
        stats.getDifficultyMedian().ifPresent(median ->
                difficultySB.append("__Median:__ ").append(median).append("\n"));
        stats.getDifficultyStandardDeviation().ifPresent(standardDeviation ->
                difficultySB.append("__Standard deviation:__ ").append(standardDeviation).append("\n"));
        difficultySB.append("__Difficulty note count:__ ").append(stats.getDifficultyCount());
        return difficultySB.toString();
    }

    private @NotNull ReviewDiscordWebhookDTO toDto(@NotNull Room room, @NotNull RoomReviews roomReviews, @NotNull PlayerRoomReview playerRoomReview) {
        ReviewDiscordWebhookDTO reviewDiscordWebhookDTO = new ReviewDiscordWebhookDTO();

        reviewDiscordWebhookDTO.setRoomDescription(room.getDescription());
        reviewDiscordWebhookDTO.setAuthor(playerRoomReview.getAuthorPseudo());
        reviewDiscordWebhookDTO.setNote(playerRoomReview.getNote());
        reviewDiscordWebhookDTO.setDifficulty(playerRoomReview.getDifficulty());
        reviewDiscordWebhookDTO.setComment(playerRoomReview.getComment());
        reviewDiscordWebhookDTO.setCommentTitle(playerRoomReview.getCommentTitle());
        reviewDiscordWebhookDTO.setStats(toStats(roomReviews));

        return reviewDiscordWebhookDTO;
    }
}
