package eu.lasersenigma.leplayserverutils.puzzle.solution;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import com.google.inject.Inject;
import eu.lasersenigma.leplayserverutils.LEPlayServerUtils;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.puzzle.solution.entity.GitlabApiAuthentication;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import org.bukkit.entity.Player;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.WikiPage;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.logging.Level;

public class PuzzleSolutionService {

    public static final String WIKI_URL_PATH = "/-/wikis/";
    private static final String HOST_URI_SEPARATOR = "/";
    private static final String PROTOCOL_HOST_SEPARATOR = "://";
    private static final String SOLUTION_REQUEST_DESCRIPTION = """
            %s would like the solution to the level "%s".
            
            Please create the corresponding page and upload a screenshot of the solution.
            
            %s""";

    private final LEPlayServerUtils plugin;

    private final MessageSender msg;

    private WebhookClient solutionRequestWebhookClient = null;

    @Inject
    public PuzzleSolutionService(LEPlayServerUtils plugin, MessageSender messageSender) {
        this.plugin = plugin;
        this.msg = messageSender;
    }

    private static String encodeParameter(String solutionPageSlug) {
        return URLEncoder.encode(solutionPageSlug, StandardCharsets.UTF_8);
    }

    public String getPublicUrl(Room room, Config config, Player player) {
        final String solutionPageSlug = getSolutionPageSlug(room, config);
        try {
            URI uri = new URI(
                    config.getGitlabApiAuthentication().getProtocol(),
                    config.getGitlabApiAuthentication().getHost(),
                    HOST_URI_SEPARATOR + config.getGitlabApiAuthentication().getProjectUrlPart() + WIKI_URL_PATH + solutionPageSlug,
                    null);
            return uri.toURL().toString();
        } catch (MalformedURLException | URISyntaxException e) {
            msg.sendError(
                    player,
                    "Error while creating the URL for the wiki page %s. Does it contain specials chars ? Please contact administrator on discord."
                            .formatted(solutionPageSlug));
            throw new RuntimeException("Error while creating the URL for the wiki page %s.".formatted(solutionPageSlug), e);
        }
    }

    public boolean doesPageExist(Room room, Config config) {
        return getWikiPage(room, config).isPresent();
    }

    public Optional<WikiPage> getWikiPage(Room room, Config config) {
        try {
            GitLabApi gitLabApi = getGitLabApi(config);
            return Optional.of(gitLabApi.getWikisApi().getPage(
                    config.getGitlabApiAuthentication().getProjectId(),
                    getEncodedSolutionSlug(room, config))
            );
        } catch (GitLabApiException e) {
            if (!e.getMessage().equals("404 Wiki Page Not Found")) {
                plugin.getLogger().log(Level.SEVERE, "Error while checking if the wiki page exists: %s".formatted(e.getMessage()), e);
            }
        }
        return Optional.empty();
    }

    private String getEncodedSolutionSlug(Room room, Config config) {
        final String solutionPageSlug = getSolutionPageSlug(room, config);
        return encodeParameter(solutionPageSlug);
    }

    private String getSolutionPageSlug(Room room, Config config) {
        return config.getGitlabApiAuthentication().getSolutionPagesSlubPrefix() + room.getDescription().replace(" ", "-");
    }

    private GitLabApi getGitLabApi(Config config) {
        if (GitlabApiAuthentication.GITLAB_ACCESS_TOKEN_DEFAULT_VALUE.equals(config.getGitlabApiAuthentication().getAccessToken())) {
            throw new IllegalStateException("The GitLab access token is not set in the config.");
        }

        return new GitLabApi(
                config.getGitlabApiAuthentication().getProtocol()
                        + PROTOCOL_HOST_SEPARATOR
                        + config.getGitlabApiAuthentication().getHost(),
                config.getGitlabApiAuthentication().getAccessToken()
        );
    }

    public boolean sendSolutionRequestToDiscord(Room room, Config config, Player player) {
        String content = SOLUTION_REQUEST_DESCRIPTION.formatted(player.getName(), room.getDescription(), getPublicUrl(room, config, player));
        WebhookEmbed embed = new WebhookEmbedBuilder()
                .setColor(0xFF00EE)
                .setAuthor(new WebhookEmbed.EmbedAuthor(player.getName(), null, null))
                .setDescription(content)
                .build();
        getSolutionRequestWebhookClient(config).send(embed);
        return true;
    }

    public WebhookClient getSolutionRequestWebhookClient(Config config) {
        if (this.solutionRequestWebhookClient == null) {
            try {
                this.solutionRequestWebhookClient = WebhookClient.withUrl(config.getSolutionRequestDiscordWebhookUrl());
            } catch (IllegalArgumentException e) {
                throw new IllegalStateException("Error while creating the webhook client: %s, Please check the webhook URL in the config. Currently : %s".formatted(e.getMessage(), config.getSolutionRequestDiscordWebhookUrl()), e);
            }
        }
        return this.solutionRequestWebhookClient;
    }
}
