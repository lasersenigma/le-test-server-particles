package eu.lasersenigma.leplayserverutils.puzzle.solution.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.command.provider.PuzzleCommandsProvider;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.puzzle.PuzzleController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class PuzzleSolutionCommand extends Command<Player> implements JavaPluginEnableEventHandler {

    private static final String SOLUTION_COMMAND = "solution";
    private final PuzzleController puzzleController;
    private final Commands puzzleCommands;

    @Inject
    public PuzzleSolutionCommand(PuzzleCommandsProvider puzzleCommands, PuzzleController puzzleController, ConfigManager configManager) {
        super(Player.class, SOLUTION_COMMAND);
        this.puzzleController = puzzleController;
        this.puzzleCommands = puzzleCommands.get();
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return puzzleController.solution(player);
    }

    @Override
    protected String description(Player executor) {
        return "get the solution to the puzzle";
    }

    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        puzzleCommands.registerCommand(this);
    }
}