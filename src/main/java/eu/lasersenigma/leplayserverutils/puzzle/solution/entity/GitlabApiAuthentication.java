package eu.lasersenigma.leplayserverutils.puzzle.solution.entity;

import com.google.gson.annotations.Expose;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class GitlabApiAuthentication {

    public static final String GITLAB_ACCESS_TOKEN_DEFAULT_VALUE = "CHANGE_ME";

    @Expose
    private String accessToken = GITLAB_ACCESS_TOKEN_DEFAULT_VALUE;

    @Expose
    private Integer projectId = 14894958;

    @Expose
    private String solutionPagesSlubPrefix = "Community-server/Solutions/";

    @Expose
    private String protocol = "https";

    @Expose
    private String host = "gitlab.com";

    @Expose
    private String projectUrlPart = "lasersenigma/lasersenigma";


    public GitlabApiAuthentication(GitlabApiAuthentication gitlabApiAuthentication) {
        this.accessToken = gitlabApiAuthentication.getAccessToken();
        this.projectId = gitlabApiAuthentication.getProjectId();
        this.solutionPagesSlubPrefix = gitlabApiAuthentication.getSolutionPagesSlubPrefix();
        this.protocol = gitlabApiAuthentication.getProtocol();
        this.host = gitlabApiAuthentication.getHost();
        this.projectUrlPart = gitlabApiAuthentication.getProjectUrlPart();
    }
}
