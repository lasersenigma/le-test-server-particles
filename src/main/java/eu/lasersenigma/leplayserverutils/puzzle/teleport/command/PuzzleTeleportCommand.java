package eu.lasersenigma.leplayserverutils.puzzle.teleport.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.command.provider.PuzzleCommandsProvider;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.puzzle.PuzzleController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class PuzzleTeleportCommand extends Command<Player> implements JavaPluginEnableEventHandler {

    private static final String TELEPORT_COMMAND = "teleport";
    private static final String ROOM_DESCRIPTION_ARG = "roomDescription";
    private final PuzzleController puzzleController;
    private final Commands puzzleCommands;

    private final ConfigManager configManager;

    @Inject
    public PuzzleTeleportCommand(PuzzleCommandsProvider puzzleCommands, PuzzleController puzzleController, ConfigManager configManager) {
        super(Player.class, TELEPORT_COMMAND);
        addArgument(ROOM_DESCRIPTION_ARG, false, ArgumentType.string());
        this.puzzleController = puzzleController;
        this.puzzleCommands = puzzleCommands.get();
        this.configManager = configManager;
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(ROOM_DESCRIPTION_ARG)) {
            String currentArgumentValue = getArgumentValue(executor, ROOM_DESCRIPTION_ARG, ArgumentType.string());
            final Set<String> roomsDescriptionSet = configManager.get()
                    .getPortals().stream()
                    .filter(Portal::isActive)
                    .map(portal -> portal.getRoom().getDescription())
                    .collect(Collectors.toSet());
            super.setAutoCompleteValuesArg(
                    currentArgumentName,
                    CommandUtils.buildStringAutocompleteValues(
                            roomsDescriptionSet,
                            currentArgumentValue)
            );
        }
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String roomDescription = null;
        if (super.hasArgumentValue(player, ROOM_DESCRIPTION_ARG)) {
            roomDescription = this.getArgumentValue(player, ROOM_DESCRIPTION_ARG, ArgumentType.string());
            if (roomDescription != null && !roomDescription.isBlank()) {
                return puzzleController.teleportToPuzzle(player, roomDescription);
            }
        }
        puzzleController.openTeleportMenu(player);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "puzzle teleportation menu";
    }

    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        puzzleCommands.registerCommand(this);
    }
}
