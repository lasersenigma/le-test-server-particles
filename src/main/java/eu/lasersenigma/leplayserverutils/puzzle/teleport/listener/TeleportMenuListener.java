package eu.lasersenigma.leplayserverutils.puzzle.teleport.listener;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.area.event.PlayerEnteredAreaEvent;
import eu.lasersenigma.area.event.PlayerLeftAreaEvent;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.puzzle.PuzzleController;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.*;

@Singleton
public class TeleportMenuListener implements LEPSUListener {

    @Inject
    private PuzzleController puzzleController;

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        puzzleController.giveTeleportMenuOpener(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        puzzleController.removeTeleportMenuOpenerFromPlayerInventory(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        puzzleController.giveTeleportMenuOpener(event.getPlayer());
    }

    @EventHandler
    public void onTeleportMenuOpenerDrop(PlayerDropItemEvent event) {
        // prevents the player from dropping the item to open the menu
        if (puzzleController.isTeleportMenuOpener(event.getItemDrop().getItemStack())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onTeleportMenuOpenerInventoryClick(InventoryClickEvent event) {
        // If the teleport menu opener is missing from the player's inventory after the click event, give the teleport menu opener back to the player.
        if (puzzleController.isTeleportMenuOpener(event.getCurrentItem())) {
            if (event.getWhoClicked() instanceof Player player) {
                puzzleController.openTeleportMenu(player);
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onTeleportMenuOpenerClick(PlayerInteractEvent event) {
        if (puzzleController.isTeleportMenuOpener(event.getItem())) {
            puzzleController.openTeleportMenu(event.getPlayer());
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onTeleportMenuOpenerDrag(InventoryDragEvent event) {
        // prevents the player from dragging the teleport menu opener in its inventory
        if (puzzleController.isTeleportMenuOpener(event.getOldCursor())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerEnteredArea(PlayerEnteredAreaEvent event) {
        // gives the player the item to open the menu
        puzzleController.giveTeleportMenuOpener(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLeftAreaEvent(PlayerLeftAreaEvent event) {
        // gives the player the item to open the menu
        puzzleController.giveTeleportMenuOpener(event.getPlayer());
    }

}
