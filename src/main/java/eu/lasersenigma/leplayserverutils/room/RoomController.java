package eu.lasersenigma.leplayserverutils.room;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageComponent;
import eu.lasersenigma.leplayserverutils.common.messages.MessageComponentColor;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.common.utils.WorldVectorToLocationTransformer;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.puzzle.solution.PuzzleSolutionService;
import eu.lasersenigma.leplayserverutils.room.dto.RoomDifficulty;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import eu.lasersenigma.leplayserverutils.room.hint.event.RoomHintUpdatedEvent;
import eu.lasersenigma.leplayserverutils.room.mapper.RoomMapper;
import fr.skytale.itemlib.item.utils.ItemStackTrait;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;
import org.gitlab4j.api.models.WikiPage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Singleton
public class RoomController {

    public static final String PLAYER_NOT_IN_ROOM = "You are not in a room";
    private static final String DIFFICULTY_BETWEEN_0_AND_1 = "Difficulty must be between 0.0 and 1.0.";

    private final ConfigManager configManager;

    private final WorldVectorToLocationTransformer locationTransformer;

    private final PuzzleSolutionService puzzleSolutionService;

    private final RoomMapper roomMapper;

    private final MessageSender msg;

    @Inject
    public RoomController(WorldVectorToLocationTransformer locationTransformer, ConfigManager configManager, PuzzleSolutionService puzzleSolutionService, RoomMapper roomMapper, MessageSender messageSender) {
        this.locationTransformer = locationTransformer;
        this.configManager = configManager;
        this.puzzleSolutionService = puzzleSolutionService;
        this.roomMapper = roomMapper;
        this.msg = messageSender;
    }

    public static boolean roomDescriptionExists(Config config, String roomDescription) {
        return config.getPortals().stream()
                .filter(Portal::isActive)
                .map(portal -> portal.getRoom().getDescription())
                .anyMatch(description -> description.equals(roomDescription));
    }

    public boolean createExitPortal(Player player) {
        Config config = configManager.get();

        Optional<Room> oRoom = findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, PLAYER_NOT_IN_ROOM);
            return false;
        }

        Room room = oRoom.get();

        Vector portalLocationVector = player.getLocation()
                .clone()
                .add(0, -1, 0)
                .getBlock()
                .getLocation()
                .toVector();

        //mutable copy of the list
        List<Vector> exitPortalsLocations = new ArrayList<>(room.getExitPortalsLocations());

        exitPortalsLocations.add(portalLocationVector);
        room.setExitPortalsLocations(exitPortalsLocations);

        configManager.saveConfig(config);
        msg.sendSuccess(player, "Exit portal created");
        return true;
    }

    public boolean deleteExitPortal(Player player) {
        Config config = configManager.get();

        Optional<Room> oRoom = findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, PLAYER_NOT_IN_ROOM);
            return false;
        }

        Room room = oRoom.get();

        // find the exit portal that is less than 3 blocks from the player and keep only the nearest one
        Optional<Vector> oNearestExitPortal = room.getExitPortalsLocations().stream()
                .filter(exitPortal -> locationTransformer.transform(room.getWorldName(), exitPortal).distance(player.getLocation()) < 3)
                .min((exitPortal1, exitPortal2) -> {
                    Location location1 = locationTransformer.transform(room.getWorldName(), exitPortal1);
                    Location location2 = locationTransformer.transform(room.getWorldName(), exitPortal2);
                    return Double.compare(location1.distance(player.getLocation()), location2.distance(player.getLocation()));
                });

        if (oNearestExitPortal.isEmpty()) {
            msg.sendError(player, "No exit portal found in the vicinity");
            return false;
        }

        //create a mutable copy of the list
        final List<Vector> exitPortalsLocations = new ArrayList<>(room.getExitPortalsLocations());
        exitPortalsLocations.remove(oNearestExitPortal.get());
        room.setExitPortalsLocations(exitPortalsLocations);

        configManager.saveConfig(config);
        msg.sendSuccess(player, "Exit portal deleted");
        return true;
    }

    public boolean setSpawnPoint(Player player) {
        Config config = configManager.get();

        Optional<Room> oRoom = findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, PLAYER_NOT_IN_ROOM);
            return false;
        }

        Room room = oRoom.get();
        room.setSpawnLocation(player.getLocation().toVector());
        configManager.saveConfig(config);
        msg.sendSuccess(player, "Spawn point set");
        return true;
    }

    public void setDescription(Player player, String roomDescription) {
        Config config = configManager.get();

        Optional<Room> oRoom = findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, PLAYER_NOT_IN_ROOM);
            return;
        }

        Room room = oRoom.get();

        String previousDescription = room.getDescription();

        if (roomDescriptionExists(config, roomDescription)) {
            msg.sendError(player, "A room with the description (\"" + roomDescription + "\") already exists");
            return;
        }

        // Check if page exists
        Optional<WikiPage> wikiPage = puzzleSolutionService.getWikiPage(room, config);

        boolean shouldRenamePage = false;
        if (wikiPage.isPresent()) {
            shouldRenamePage = true;
            msg.sendWarning(player, "The link to this puzzle solution will be broken, please rename the page on the wiki to match the new room description");
            msg.sendWarning(player,
                    new MessageComponent("Current room url:", MessageComponentColor.WARNING)
                            .append(
                                    new MessageComponent(
                                            " " + puzzleSolutionService.getPublicUrl(room, config, player),
                                            MessageComponentColor.LINK,
                                            TextDecoration.UNDERLINED)
                            )
            );
        }


        room.setDescription(roomDescription);
        if (shouldRenamePage) {
            msg.sendWarning(player,
                    new MessageComponent("New room url:", MessageComponentColor.WARNING)
                            .append(
                                    new MessageComponent(
                                            " " + puzzleSolutionService.getPublicUrl(room, config, player),
                                            MessageComponentColor.LINK,
                                            TextDecoration.UNDERLINED)
                            )
            );
        }
        configManager.saveConfig(config);

        Bukkit.getPluginManager().callEvent(new RoomHintUpdatedEvent(previousDescription, new ArrayList<>()));
        Bukkit.getPluginManager().callEvent(new RoomHintUpdatedEvent(room.getDescription(), room.getHints()));

        msg.sendSuccess(player, "Description set");
    }

    public void setDifficulty(Player player, double difficulty) {
        Config config = configManager.get();

        Optional<Room> oRoom = findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, PLAYER_NOT_IN_ROOM);
            return;
        }
        if (difficulty < 0 || difficulty > 1) {
            msg.sendError(player, DIFFICULTY_BETWEEN_0_AND_1);
            return;
        }

        Room room = oRoom.get();
        room.setDifficulty(difficulty);
        configManager.saveConfig(config);
        msg.sendSuccess(player, "Difficulty set");
    }

    public Optional<Room> findRoomFromPlayerInRoom(Player player, Config config) {
        return findPortalFromPlayerInRoom(player, config)
                .map(Portal::getRoom);
    }

    public Optional<Portal> findPortalFromPlayerInRoom(Player player, Config config) {
        return config.getPortals().stream()
                .filter(portal -> {
                    if (!portal.isActive()) return false;
                    return isInRoom(player.getLocation(), portal.getRoom());
                })
                .findAny();
    }

    public boolean isInRoom(Location location, Room room) {
        Location min = locationTransformer.transform(
                room.getWorldName(),
                room.getMinCorner()
        );
        Location max = locationTransformer.transform(
                room.getWorldName(),
                room.getMaxCorner()
        );
        return min.getWorld().getUID().equals(location.getWorld().getUID())
                && min.getX() <= location.getX()
                && location.getX() <= max.getX()
                && min.getY() <= location.getY()
                && location.getY() <= max.getY()
                && min.getZ() <= location.getZ()
                && location.getZ() <= max.getZ();
    }

    public Optional<Portal> findActivePortalByRoomDescription(Config config, String firstPortalRoomDescription) {
        return config.getPortals().stream()
                .filter(portal -> portal.isActive() && portal.getRoom().getDescription().equals(firstPortalRoomDescription))
                .findAny();
    }

    public boolean setCuboid(Player player, Vector minCorner, Vector maxCorner) {
        Config config = configManager.get();

        Optional<Room> oRoom = findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, PLAYER_NOT_IN_ROOM);
            return false;
        }

        Room room = oRoom.get();
        room.setMinCorner(minCorner);
        room.setMaxCorner(maxCorner);

        configManager.saveConfig(config);
        msg.sendSuccess(player, "Room coordinates set");

        return true;
    }

    public void info(Player player) {
        Config config = configManager.get();

        Optional<Room> oRoom = findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, PLAYER_NOT_IN_ROOM);
            return;
        }

        Room room = oRoom.get();
        msg.sendTitle(player, "Room description: " + room.getDescription());
        msg.sendParagraph(player, "Room coordinates: " + room.getMinCorner() + " " + room.getMaxCorner());
        msg.sendParagraph(player, "Room spawn point: " + room.getSpawnLocation());
        msg.sendParagraph(player, "Exit portals: " + room.getExitPortalsLocations());
        msg.sendParagraph(player, "Room difficulty: " + room.getDifficulty() + " (" + RoomDifficulty.fromPercent(room.getDifficulty()).getDisplayName() + ")");
        msg.sendParagraph(player, "Room authors: " + roomMapper.getAuthorString(room, config).orElse(""));
        msg.sendParagraph(player, "Room " + roomMapper.getTagsString(room, config).orElse(""));
    }

    public boolean giveColorWheel(Player player) {

        ItemStack mapPlacerItemStack = getMapPlacerItemStack();

        player.getInventory().addItem(mapPlacerItemStack);
        return true;
    }

    private ItemStack getMapPlacerItemStack() {
        ItemStack mapPlacerItemStack = new ItemStack(Material.MAP);

        final ItemMeta itemMeta = mapPlacerItemStack.getItemMeta();

        itemMeta.displayName(Component.text("Color Wheel Placer"));
        itemMeta.lore(List.of(
                Component.text("Right click the wall to place a color wheel")
        ));
        itemMeta.addEnchant(Enchantment.DURABILITY, 1, true);
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        mapPlacerItemStack.setItemMeta(itemMeta);
        return mapPlacerItemStack;
    }

    public boolean isMapPlacerItemStack(ItemStack itemStack) {
        return ItemStackUtils.compare(itemStack, getMapPlacerItemStack(), ItemStackTrait.NAME, ItemStackTrait.LORE, ItemStackTrait.TYPE, ItemStackTrait.ENCHANTMENTS, ItemStackTrait.FLAGS);
    }

    public Optional<Room> findRoomById(int roomId, Config config) {
        return config.getPortals().stream()
                .filter(Portal::isActive)
                .map(Portal::getRoom)
                .filter(room -> room.getId() == roomId)
                .findAny();
    }
}
