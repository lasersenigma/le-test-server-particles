package eu.lasersenigma.leplayserverutils.room.author;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.player.LpuPlayerController;
import eu.lasersenigma.leplayserverutils.player.entity.LpuPlayer;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Singleton
public class RoomAuthorController {
    private static final String UNKNOWN_PLAYER = "Unknown player %s";
    private final RoomController roomController;

    private final ConfigManager configManager;

    private final LpuPlayerController lpuPlayerController;

    private final MessageSender msg;

    @Inject
    public RoomAuthorController(RoomController roomController, ConfigManager configManager, LpuPlayerController lpuPlayerController, MessageSender messageSender) {
        this.roomController = roomController;
        this.configManager = configManager;
        this.lpuPlayerController = lpuPlayerController;
        this.msg = messageSender;
    }

    public boolean addAuthor(Player player, String author) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, RoomController.PLAYER_NOT_IN_ROOM);
            return false;
        }

        Optional<LpuPlayer> oLpuPlayer = lpuPlayerController.findOrCreatePlayer(author, config);
        if (oLpuPlayer.isEmpty()) {
            msg.sendError(player, UNKNOWN_PLAYER.formatted(author));
            return false;
        }

        if (oRoom.get().getAuthors().contains(oLpuPlayer.get().getUuid())) {
            msg.sendError(player, "Author already added");
            return false;
        }

        oRoom.get().getAuthors().add(oLpuPlayer.get().getUuid());
        msg.sendSuccess(player, "Author added");
        configManager.saveConfig(config);
        return true;
    }

    public boolean deleteAuthor(Player player, String author) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, RoomController.PLAYER_NOT_IN_ROOM);
            return false;
        }

        final Optional<LpuPlayer> oLpuPlayer = lpuPlayerController.findOrCreatePlayer(author, config);

        if (oLpuPlayer.isEmpty()) {
            msg.sendError(player, UNKNOWN_PLAYER.formatted(author));
            return false;
        }

        if (oRoom.get().getAuthors().remove(oLpuPlayer.get().getUuid())) {
            msg.sendSuccess(player, "Author deleted");
        } else {
            msg.sendError(player, "Author not found");
            return false;
        }

        configManager.saveConfig(config);
        return true;
    }

    public boolean setAuthor(Player player, String author) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, RoomController.PLAYER_NOT_IN_ROOM);
            return false;
        }
        Room room = oRoom.get();

        final Optional<LpuPlayer> oLpuPlayer = lpuPlayerController.findOrCreatePlayer(author, config);

        if (oLpuPlayer.isEmpty()) {
            msg.sendError(player, UNKNOWN_PLAYER.formatted(author));
            return false;
        }
        LpuPlayer lpuPlayer = oLpuPlayer.get();

        room.getAuthors().clear();
        room.getAuthors().add(lpuPlayer.getUuid());

        configManager.saveConfig(config);
        msg.sendSuccess(player, "Author set");
        return true;
    }

    public Set<String> findAllRoomAuthorsPseudoOrNickname(Player executorPlayer) {
        Config config = configManager.get();
        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(executorPlayer, config);
        if (oRoom.isEmpty()) {
            return Set.of();
        }
        return lpuPlayerController.findPlayerPseudoOrNicknameFromUUID(oRoom.get().getAuthors(), config);
    }

    public Set<String> findAllPlayersOnlineOrOfflinePseudoOrNickname() {
        Config config = configManager.get();
        return lpuPlayerController.findAllPlayersPseudo(config);
    }

    public Set<String> findAllPossibleRoomAdditionalAuthorsPseudoOrNickname(Player executorPlayer) {
        Config config = configManager.get();

        Set<UUID> possibleAuthorsUuidSet = Bukkit.getOnlinePlayers().stream().map(Player::getUniqueId).collect(Collectors.toSet());
        //merge online and saved players
        possibleAuthorsUuidSet.addAll(config.getPlayers().stream().map(LpuPlayer::getUuid).collect(Collectors.toSet()));


        //exclude players already in the room
        Set<UUID> roomAuthorsUuidSet = new HashSet<>();
        roomController.findRoomFromPlayerInRoom(executorPlayer, config)
                .ifPresent(room -> roomAuthorsUuidSet.addAll(room.getAuthors()));
        possibleAuthorsUuidSet.removeIf(roomAuthorsUuidSet::contains);

        return lpuPlayerController.findPlayerPseudoOrNicknameFromUUID(possibleAuthorsUuidSet, config);
    }
}