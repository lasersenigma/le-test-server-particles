package eu.lasersenigma.leplayserverutils.room.author.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class RoomAuthorCommand extends Command<Player> {

    @Inject
    public RoomAuthorCommand(
            RoomAuthorAddCommand roomAuthorAddCommand,
            RoomAuthorDeleteCommand roomAuthorDeleteCommand,
            RoomAuthorSetCommand roomAuthorSetCommand) {
        super(Player.class, "author");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        registerSubCommand(roomAuthorAddCommand);
        registerSubCommand(roomAuthorDeleteCommand);
        registerSubCommand(roomAuthorSetCommand);
    }


    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return super.defaultProcessForContainer(commands, player, args);
    }

    @Override
    protected String description(Player executor) {
        return "Define room's authors";
    }
}
