package eu.lasersenigma.leplayserverutils.room.author.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.room.author.RoomAuthorController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Set;

@Singleton
public class RoomAuthorSetCommand extends Command<Player> {
    private static final String SET_AUTHOR_COMMAND = "set";
    private static final String AUTHOR_ARG = "author";
    private final RoomAuthorController roomAuthorController;

    @Inject
    public RoomAuthorSetCommand(RoomAuthorController roomAuthorController) {
        super(Player.class, SET_AUTHOR_COMMAND);
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.addArgument(AUTHOR_ARG, true, ArgumentType.string());
        this.roomAuthorController = roomAuthorController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String author = this.getArgumentValue(player, AUTHOR_ARG, ArgumentType.string());
        return roomAuthorController.setAuthor(player, author);
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(AUTHOR_ARG)) {

            String currentArgumentValue = getArgumentValue(executor, AUTHOR_ARG, ArgumentType.string());
            final Set<String> playersNameSet = roomAuthorController.findAllPlayersOnlineOrOfflinePseudoOrNickname();
            super.setAutoCompleteValuesArg(
                    AUTHOR_ARG,
                    CommandUtils.buildStringAutocompleteValues(playersNameSet, currentArgumentValue));
        }
    }

    @Override
    protected String description(Player executor) {
        return "Add a author to the room";
    }
}
