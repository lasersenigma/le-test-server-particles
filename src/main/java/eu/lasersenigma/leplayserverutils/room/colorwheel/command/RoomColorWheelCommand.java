package eu.lasersenigma.leplayserverutils.room.colorwheel.command;

import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

import javax.inject.Inject;

@Singleton
public class RoomColorWheelCommand extends Command<Player> {

    private RoomController roomController;

    @Inject
    public RoomColorWheelCommand(RoomController roomController) {
        super(Player.class, "colorWheel");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.roomController = roomController;
    }

    @Override
    public boolean process(Commands commands, Player player, String... args) {
        return roomController.giveColorWheel(player);
    }

    @Override
    public String description(Player executor) {
        return "Gives an item allowing to play the color wheel on a wall";
    }

}