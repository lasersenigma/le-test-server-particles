package eu.lasersenigma.leplayserverutils.room.colorwheel.listener;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LEPSUListener;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ItemFrame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.MapMeta;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class ColorWheelListener implements LEPSUListener {


    @Inject
    private RoomController roomController;

    @Inject
    private ConfigManager configManager;

    @Inject
    private MessageSender msg;

    @EventHandler
    public void onTeleportMenuOpenerClick(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        // Check if the player is holding the required item
        if (!roomController.isMapPlacerItemStack(event.getItem())) {
            return;
        }

        final Block clickedBlock = event.getClickedBlock();
        if (clickedBlock == null) {
            return;
        }

        final BlockFace blockFaceClicked = event.getBlockFace();

        final List<Location> mapFutureLocations = getMapLocations(clickedBlock, blockFaceClicked);

        // check if all the blocks are air
        if (!areMapFutureLocationsEmpty(mapFutureLocations)) {

            msg.sendError(event.getPlayer(), "Not enough space to place the maps on the wall!");
            return;
        }


        // place the maps on the wall
        List<Integer> mapIds = configManager.get().getColorMapsIds();

        for (int i = 0; i < 4; i++) {
            final Integer mapId = mapIds.get(i);
            Location location = mapFutureLocations.get(i);


            spawnNewMap(mapId, location, blockFaceClicked);
        }
    }

    private static boolean areMapFutureLocationsEmpty(List<Location> mapFutureLocations) {
        boolean areMapFutureLocationsEmpty = true;
        for (Location mapFutureLocation : mapFutureLocations) {
            if (!mapFutureLocation.getBlock().isEmpty()) {
                areMapFutureLocationsEmpty = false;
                break;
            }
        }
        return areMapFutureLocationsEmpty;
    }

    private static @NotNull List<Location> getMapLocations(Block clickedBlock, BlockFace blockFaceClicked) {
        List<Location> mapFutureLocations = new ArrayList<>();
        Location firstMapLocation = clickedBlock.getLocation().clone().add(blockFaceClicked.getModX(), 0, blockFaceClicked.getModZ()).getBlock().getLocation();
        mapFutureLocations.add(firstMapLocation);
        //second map location is at the right according to the player's facing (at left according to the block face clicked)
        Vector leftDirectionFromBlockFaceClicked = blockFaceClicked.getDirection().rotateAroundAxis(new Vector(0, 1, 0), -Math.PI / 2);
        Location secondMapLocation = firstMapLocation.clone().add(leftDirectionFromBlockFaceClicked);
        mapFutureLocations.add(secondMapLocation);
        //third and fourth map locations are below the first and second map locations
        Location thirdMapLocation = firstMapLocation.clone().add(0, -1, 0);
        mapFutureLocations.add(thirdMapLocation);
        Location fourthMapLocation = secondMapLocation.clone().add(0, -1, 0);
        mapFutureLocations.add(fourthMapLocation);
        return mapFutureLocations;
    }

    private static void spawnNewMap(Integer mapId, Location location, BlockFace blockFaceClicked) {
        ItemStack map = new ItemStack(Material.FILLED_MAP, 1);
        MapMeta meta = (MapMeta) map.getItemMeta();
        meta.setMapId(mapId);
        map.setItemMeta(meta);

        ItemFrame frame = location.getWorld().spawn(location, ItemFrame.class);
        frame.setFacingDirection(blockFaceClicked, true);
        frame.setItem(map);
        frame.setFixed(true);
        frame.setInvulnerable(true);
        frame.setVisible(false);
    }
}
