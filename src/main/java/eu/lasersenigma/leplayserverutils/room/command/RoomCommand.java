package eu.lasersenigma.leplayserverutils.room.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.provider.LPUCommandsProvider;
import eu.lasersenigma.leplayserverutils.room.author.command.RoomAuthorCommand;
import eu.lasersenigma.leplayserverutils.room.colorwheel.command.RoomColorWheelCommand;
import eu.lasersenigma.leplayserverutils.room.hint.command.RoomHintCommand;
import eu.lasersenigma.leplayserverutils.room.tag.command.RoomTagCommand;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class RoomCommand extends Command<Player> implements JavaPluginEnableEventHandler {

    private final Commands leplayserverutilsCommands;

    @Inject
    public RoomCommand(LPUCommandsProvider lpuCommandsProvider,
                       RoomExitPortalCommand roomExitPortalCommand,
                       RoomSpawnPointCommand roomSpawnPointCommand,
                       RoomRenameCommand roomRenameCommand,
                       RoomDifficultyCommand roomDifficultyCommand,
                       RoomResizeCommand roomResizeCommand,
                       RoomAuthorCommand roomAuthorCommand,
                       RoomTagCommand roomTagCommand,
                       RoomInfoCommand roomInfoCommand,
                       RoomColorWheelCommand roomColorWheelCommand,
                       RoomHintCommand roomHintCommand
    ) {
        super(Player.class, "room");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        registerSubCommand(roomExitPortalCommand);
        registerSubCommand(roomSpawnPointCommand);
        registerSubCommand(roomRenameCommand);
        registerSubCommand(roomDifficultyCommand);
        registerSubCommand(roomResizeCommand);
        registerSubCommand(roomAuthorCommand);
        registerSubCommand(roomTagCommand);
        registerSubCommand(roomInfoCommand);
        registerSubCommand(roomColorWheelCommand);
        registerSubCommand(roomHintCommand);

        this.leplayserverutilsCommands = lpuCommandsProvider.get();
    }

    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        leplayserverutilsCommands.registerCommand(this);
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return super.defaultProcessForContainer(commands, player, args);
    }

    @Override
    protected String description(Player executor) {
        return "modify a room spawn point or exit portals";
    }

}
