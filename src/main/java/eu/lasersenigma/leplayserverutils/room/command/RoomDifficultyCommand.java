package eu.lasersenigma.leplayserverutils.room.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

@Singleton
public class RoomDifficultyCommand extends Command<Player> {
    private static final String DIFFICULTY_PARAM = "difficulty";
    private final RoomController roomController;

    @Inject
    public RoomDifficultyCommand(RoomController roomController) {
        super(Player.class, "setDifficulty");
        this.addArgument(DIFFICULTY_PARAM, true, ArgumentType.doubleOnly());
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.roomController = roomController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        double difficulty = this.getArgumentValue(player, DIFFICULTY_PARAM, ArgumentType.doubleOnly());
        roomController.setDifficulty(player, difficulty);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Set the difficulty of a portal (between 0 and 1)";
    }
}
