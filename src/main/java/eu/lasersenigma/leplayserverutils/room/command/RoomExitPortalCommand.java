package eu.lasersenigma.leplayserverutils.room.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.apache.commons.lang.NotImplementedException;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.stream.Collectors;

@Singleton
public class RoomExitPortalCommand extends Command<Player> {

    private static final String ACTION_ARG = "action";
    private final RoomController roomController;

    private final MessageSender msg;

    @Inject
    public RoomExitPortalCommand(RoomController roomController, MessageSender messageSender) {
        super(Player.class, "exitportal");
        this.roomController = roomController;
        this.msg = messageSender;
        this.addArgument(ACTION_ARG, true, ArgumentType.string());
        this.addAutoCompleteValuesArg(ACTION_ARG, Arrays.stream(ExitPortalAction.values())
                .map(action -> action.name().toLowerCase())
                .collect(Collectors.toList()));
        this.setPermission(LPUPermission.CREATOR.getPermission());
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        final String actionStr = this.getArgumentValue(player, ACTION_ARG, ArgumentType.string());
        ExitPortalAction action = null;
        try {
            action = ExitPortalAction.valueOf(actionStr.toUpperCase());
        } catch (IllegalArgumentException e) {
            final String actions = Arrays.stream(ExitPortalAction.values())
                    .map(Enum::name)
                    .map(String::toLowerCase)
                    .collect(Collectors.joining(", "));
            msg.sendError(player, "Invalid action: %s. Allowed actions are %s".formatted(actionStr, actions));
            return false;
        }
        switch (action) {
            case CREATE -> {
                return roomController.createExitPortal(player);
            }
            case DELETE -> {
                return roomController.deleteExitPortal(player);
            }
            default -> {
                throw new NotImplementedException("Action %s is not implemented".formatted(action));
            }
        }
    }

    @Override
    protected String description(Player executor) {
        return "Create an exit portal in the room (allowing players to come back to the lobby)";
    }

    public enum ExitPortalAction {
        CREATE, DELETE
    }
}
