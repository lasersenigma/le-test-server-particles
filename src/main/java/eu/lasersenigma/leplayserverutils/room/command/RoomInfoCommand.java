package eu.lasersenigma.leplayserverutils.room.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class RoomInfoCommand extends Command<Player> {
    private final RoomController roomController;

    @Inject
    public RoomInfoCommand(RoomController roomController) {
        super(Player.class, "info");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.roomController = roomController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        roomController.info(player);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Retrieves room informations";
    }
}
