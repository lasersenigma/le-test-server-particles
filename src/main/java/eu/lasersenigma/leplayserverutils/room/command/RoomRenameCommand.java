package eu.lasersenigma.leplayserverutils.room.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

@Singleton
public class RoomRenameCommand extends Command<Player> {
    private final RoomController roomController;

    @Inject
    public RoomRenameCommand(RoomController roomController) {
        super(Player.class, "rename");
        this.addArgument("roomDescription", true, ArgumentType.string());
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.roomController = roomController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String roomDescription = this.getArgumentValue(player, "roomDescription", ArgumentType.string());
        roomController.setDescription(player, roomDescription);
        return true;
    }

    @Override
    protected String description(Player executor) {
        return "Rename a portal";
    }
}
