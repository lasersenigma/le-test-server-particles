package eu.lasersenigma.leplayserverutils.room.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.roomtemplate.command.RoomTemplateCommand;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

@Singleton
public class RoomResizeCommand extends Command<Player> {
    public static final String MIN_CORNER = "minCorner";
    public static final String MAX_CORNER = "maxCorner";
    private final RoomController roomController;

    @Inject
    public RoomResizeCommand(RoomController roomController) {
        super(Player.class, "resize");
        this.addArgument(MIN_CORNER, true, ArgumentType.bukkitVector());
        this.addArgument(MAX_CORNER, true, ArgumentType.bukkitVector());
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.roomController = roomController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        Vector minCorner = this.getArgumentValue(player, RoomTemplateCommand.MIN_CORNER, ArgumentType.bukkitVector());
        Vector maxCorner = this.getArgumentValue(player, RoomTemplateCommand.MAX_CORNER, ArgumentType.bukkitVector());
        return roomController.setCuboid(player, minCorner, maxCorner);
    }

    @Override
    protected String description(Player executor) {
        return "Set the the min and max coordinates of the room";
    }
}
