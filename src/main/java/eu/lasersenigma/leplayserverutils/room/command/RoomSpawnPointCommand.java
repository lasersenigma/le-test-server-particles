package eu.lasersenigma.leplayserverutils.room.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

@Singleton
public class RoomSpawnPointCommand extends Command<Player> {

    private final RoomController roomController;

    @Inject
    public RoomSpawnPointCommand(RoomController roomController) {
        super(Player.class, "setSpawnPoint");
        this.addArgument("action", true, ArgumentType.fromEnum(SpawnPointAction.class));
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.roomController = roomController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        final SpawnPointAction action = this.getArgumentValue(
                player,
                "action",
                ArgumentType.fromEnum(SpawnPointAction.class)
        );
        if (action == SpawnPointAction.SET) {
            return roomController.setSpawnPoint(player);
        }
        return false;
    }

    @Override
    protected String description(Player executor) {
        return "Set the room's spawn point";
    }

    public enum SpawnPointAction {
        SET
    }
}
