package eu.lasersenigma.leplayserverutils.room.dto;

import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.leplayserverutils.puzzle.review.dto.IntegerBetweenOneAndFive;
import lombok.Getter;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Color;

@Getter
public enum RoomDifficulty {

    EASY(
            0.2,
            "Easy",
            TextColor.color(0x00FFFF),
            "2657e0a570b48c9a16df665ed157b6dfa3e1b42e6a74daf2e2d3c98c0667f933",
            "1896d392d3bee2697453ad12a300db9735d9accfb7e3492c25a86627590479f5",
            Item.LASER_SENDER_CYAN_ALWAYS_ON),
    MEDIUM(0.4,
            "Medium",
            TextColor.color(0x00FF00),
            "ddf00d2f3d495c0a34f4e71d92854ea0cc0ec5a670cab119e615fa246fa3f2f8",
            "5a56a0b5eb5af5b46570809849c6d91eac83353e6a06ab1522047e34d3d81910",
            Item.LASER_SENDER_GREEN_ALWAYS_ON),
    HARD(0.6,
            "Hard",
            TextColor.color(0xFFFF00),
            "8d72a3c35e3c991e50243bfe4c37fd34fe5303faa379555d011c706d46ccff1b",
            "2678960e78331acf129894169b61416fba1ad741907534cd7b5f22c713ed17b6",
            Item.LASER_SENDER_YELLOW_ALWAYS_ON),
    LEGENDARY(0.8,
            "Legendary",
            TextColor.color(0xFF0000),
            "2c819525b3dee21a5cb5c7c7eb381b7786272b035ebfe37d02c6d3fe3e1782eb",
            "fd50637c0de237f05ef5123831aef97076b7d5e147a425fe104c9b944db04de0",
            Item.LASER_SENDER_RED_ALWAYS_ON),
    MYTHIC(1.0,
            "Mythic",
            TextColor.color(0xFF00FF),
            "467b5ce772e900456ba3214542399c62a45c9ced02d0dee4f4494ab1e82d4c19",
    "fed13f47cea830b69378dd38bf68e96dc3ca1346c2cdd5f889e79d734e1e518",
            Item.LASER_SENDER_MAGENTA_ALWAYS_ON);

    private final double rangeMax;

    private final String displayName;

    private final TextColor color;

    private final String headTextureActivated;

    private final String headTextureDeactivated;

    private final Item item;

    RoomDifficulty(double rangeMax, String displayName, TextColor color, String headTextureActivated, String headTextureDeactivated, Item item) {
        this.rangeMax = rangeMax;
        this.displayName = displayName;
        this.color = color;
        this.headTextureActivated = headTextureActivated;
        this.headTextureDeactivated = headTextureDeactivated;
        this.item = item;
    }

    public static RoomDifficulty fromPercent(double difficulty) {
        for (RoomDifficulty roomDifficulty : RoomDifficulty.values()) {
            if (difficulty <= roomDifficulty.rangeMax) {
                return roomDifficulty;
            }
        }
        throw new IllegalStateException("RoomDifficulty enum is not complete");
    }

    public static RoomDifficulty fromNote(IntegerBetweenOneAndFive note) {
        return RoomDifficulty.values()[note.getValue() - 1];
    }

    public TextComponent getDisplayNameComponent() {
        return Component.text(displayName).color(color);
    }

    public Color asColor() {
        return Color.fromRGB(color.value());
    }
}
