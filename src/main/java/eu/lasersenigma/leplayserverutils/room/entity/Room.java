package eu.lasersenigma.leplayserverutils.room.entity;

import com.google.gson.annotations.Expose;
import lombok.*;
import org.bukkit.util.Vector;

import java.util.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Room {

    @Expose
    private Integer id = null;

    @Expose
    private String worldName;

    @Expose
    private Vector minCorner;

    @Expose
    private Vector maxCorner;

    @Expose
    private Vector spawnLocation;

    @Expose
    private List<Vector> exitPortalsLocations = new ArrayList<>();

    @Expose
    private String description;

    @Expose
    private Double difficulty = 0.5;

    @Expose
    private List<UUID> authors = new ArrayList<>();

    @Expose
    Set<Integer> tagsId = new HashSet<>();

    @Expose
    private List<String> hints = new ArrayList<>();

    public Room(String worldName, Vector minCorner, Vector maxCorner, Vector spawnLocation, List<Vector> exitPortalsLocations, String description, Double difficulty, List<UUID> authors, Set<Integer> tagsId, List<String> hints) {
        this.worldName = worldName;
        this.minCorner = minCorner;
        this.maxCorner = maxCorner;
        this.spawnLocation = spawnLocation;
        this.exitPortalsLocations = exitPortalsLocations;
        this.description = description;
        this.difficulty = difficulty;
        this.authors = authors;
        this.tagsId = tagsId;
        this.hints = hints;
    }

    public Room(Room room) {
        this.id = room.getId();
        this.worldName = room.getWorldName();
        this.minCorner = room.getMinCorner().clone();
        this.maxCorner = room.getMaxCorner().clone();
        this.spawnLocation = room.getSpawnLocation().clone();
        this.exitPortalsLocations = new ArrayList<>(room.getExitPortalsLocations().stream()
                .map(Vector::clone)
                .toList());
        this.description = room.getDescription();
        this.difficulty = room.getDifficulty();
        this.authors = new ArrayList<>(room.getAuthors());
        this.tagsId = new HashSet<>(room.getTagsId());
        this.hints = new ArrayList<>(room.getHints());
    }
}
