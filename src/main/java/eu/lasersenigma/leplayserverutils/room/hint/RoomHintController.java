package eu.lasersenigma.leplayserverutils.room.hint;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.player.LpuPlayerController;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import eu.lasersenigma.leplayserverutils.room.hint.event.RoomHintUpdatedEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Singleton
public class RoomHintController {
    public static final String HINT_MESSAGE = "Hint %d : %s";
    private final RoomController roomController;

    private final ConfigManager configManager;

    private final MessageSender msg;

    @Inject
    public RoomHintController(RoomController roomController, ConfigManager configManager, LpuPlayerController lpuPlayerController, MessageSender messageSender) {
        this.roomController = roomController;
        this.configManager = configManager;
        this.msg = messageSender;
    }

    public boolean addHint(Player player, String text) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, RoomController.PLAYER_NOT_IN_ROOM);
            return false;
        }
        Room room = oRoom.get();

        room.getHints().add(text);
        configManager.saveConfig(config);

        Bukkit.getPluginManager().callEvent(new RoomHintUpdatedEvent(room.getDescription(), room.getHints()));

        msg.sendSuccess(player, "Hint added");
        return true;
    }

    public boolean clearHints(Player player) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, RoomController.PLAYER_NOT_IN_ROOM);
            return false;
        }
        Room room = oRoom.get();

        room.getHints().clear();

        Bukkit.getPluginManager().callEvent(new RoomHintUpdatedEvent(room.getDescription(), room.getHints()));

        configManager.saveConfig(config);
        msg.sendSuccess(player, "Hints cleared");

        return true;
    }

    public boolean deleteHint(Player player, Integer index) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, RoomController.PLAYER_NOT_IN_ROOM);
            return false;
        }
        Room room = oRoom.get();

        if (index == null || index < 1 || index > room.getHints().size()) {
            msg.sendError(player, "Invalid index");
            return false;
        }

        room.getHints().remove(index - 1);
        configManager.saveConfig(config);

        Bukkit.getPluginManager().callEvent(new RoomHintUpdatedEvent(room.getDescription(), room.getHints()));

        msg.sendSuccess(player, "Hint deleted");
        return true;
    }

    public boolean setHint(Player player, Integer index, String text) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, RoomController.PLAYER_NOT_IN_ROOM);
            return false;
        }
        Room room = oRoom.get();

        if (index == null || index < 1 || index > room.getHints().size()) {
            msg.sendError(player, "Invalid index");
            return false;
        }

        room.getHints().set(index - 1, text);
        configManager.saveConfig(config);

        Bukkit.getPluginManager().callEvent(new RoomHintUpdatedEvent(room.getDescription(), room.getHints()));

        msg.sendSuccess(player, "Hint set");

        return true;
    }

    public boolean listHints(Player player) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, RoomController.PLAYER_NOT_IN_ROOM);
            return false;
        }
        Room room = oRoom.get();

        if (room.getHints().isEmpty()) {
            msg.sendTitle(player, "No hints");
        } else {
            msg.sendTitle(player, "Hints");
            List<String> hints = room.getHints();
            for (int i = 0; i < hints.size(); i++) {
                String hint = hints.get(i);
                msg.sendParagraph(player, HINT_MESSAGE.formatted(i + 1, hint));
            }
        }

        return true;
    }

    public List<Integer> findAllHintsIndexes(Player executor) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(executor, config);
        if (oRoom.isEmpty()) {
            return List.of();
        }
        Room room = oRoom.get();

        List<Integer> result;
        if (room.getHints().isEmpty()) {
            result = List.of();
        } else {
            result = IntStream.range(1, room.getHints().size() + 1)
                    .boxed()
                    .collect(Collectors.toList());
        }
        return result;
    }
}