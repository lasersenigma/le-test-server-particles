package eu.lasersenigma.leplayserverutils.room.hint.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.room.hint.RoomHintController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

@Singleton
public class RoomHintAddCommand extends Command<Player> {
    private static final String COMMAND = "add";
    private static final String TEXT_ARG = "text";

    private final RoomHintController roomHintController;

    @Inject
    public RoomHintAddCommand(RoomHintController roomHintController) {
        super(Player.class, COMMAND);
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.addArgument(TEXT_ARG, true, ArgumentType.string());
        this.roomHintController = roomHintController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String text = this.getArgumentValue(player, TEXT_ARG, ArgumentType.string());
        return roomHintController.addHint(player, text);
    }

    @Override
    protected String description(Player executor) {
        return "Add a hint to the room";
    }
}
