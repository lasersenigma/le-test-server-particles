package eu.lasersenigma.leplayserverutils.room.hint.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.room.hint.RoomHintController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class RoomHintClearCommand extends Command<Player> {
    private static final String COMMAND = "clear";

    private final RoomHintController roomHintController;

    @Inject
    public RoomHintClearCommand(RoomHintController roomHintController) {
        super(Player.class, COMMAND);
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.roomHintController = roomHintController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return roomHintController.clearHints(player);
    }

    @Override
    protected String description(Player executor) {
        return "Delete every hint from the room";
    }
}
