package eu.lasersenigma.leplayserverutils.room.hint.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class RoomHintCommand extends Command<Player> {

    @Inject
    public RoomHintCommand(
            RoomHintAddCommand roomHintAddCommand,
            RoomHintDeleteCommand roomHintDeleteCommand,
            RoomHintSetCommand roomHintSetCommand,
            RoomHintClearCommand roomHintClearCommand,
            RoomHintListCommand roomHintListCommand
    ) {
        super(Player.class, "hint");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        registerSubCommand(roomHintAddCommand);
        registerSubCommand(roomHintDeleteCommand);
        registerSubCommand(roomHintSetCommand);
        registerSubCommand(roomHintClearCommand);
        registerSubCommand(roomHintListCommand);
    }


    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return super.defaultProcessForContainer(commands, player, args);
    }

    @Override
    protected String description(Player executor) {
        return "Define room's hints";
    }
}
