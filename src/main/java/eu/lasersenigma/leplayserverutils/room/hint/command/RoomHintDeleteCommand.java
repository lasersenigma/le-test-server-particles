package eu.lasersenigma.leplayserverutils.room.hint.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.room.hint.RoomHintController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.List;

@Singleton
public class RoomHintDeleteCommand extends Command<Player> {
    private static final String COMMAND = "delete";
    private static final String INDEX_ARG = "index";

    private final RoomHintController roomHintController;

    @Inject
    public RoomHintDeleteCommand(RoomHintController roomHintController) {
        super(Player.class, COMMAND);
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.addArgument(INDEX_ARG, true, ArgumentType.integerOnly());
        this.roomHintController = roomHintController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        Integer index = this.getArgumentValue(player, INDEX_ARG, ArgumentType.integerOnly());
        return roomHintController.deleteHint(player, index);
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(INDEX_ARG)) {

            Integer currentArgumentValue = getArgumentValue(executor, INDEX_ARG, ArgumentType.integerOnly());

            final List<Integer> indexes = roomHintController.findAllHintsIndexes(executor);
            super.setAutoCompleteValuesArg(
                    INDEX_ARG,
                    CommandUtils.buildIntegerAutocompleteValues(indexes, currentArgumentValue));
        }
    }

    @Override
    protected String description(Player executor) {
        return "Delete an hint using its index in the room hints";
    }
}
