package eu.lasersenigma.leplayserverutils.room.hint.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class RoomHintUpdatedEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final String roomDescription;

    private final List<String> roomHints;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
