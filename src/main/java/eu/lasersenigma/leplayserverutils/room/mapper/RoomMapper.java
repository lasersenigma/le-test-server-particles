package eu.lasersenigma.leplayserverutils.room.mapper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.player.mapper.PlayerMapper;
import eu.lasersenigma.leplayserverutils.room.dto.RoomDifficulty;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import eu.lasersenigma.leplayserverutils.tag.entity.Tag;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Singleton
public class RoomMapper {

    @Inject
    private PlayerMapper playerMapper;

    private static @NotNull String toAuthorDisplayStr(List<String> authorNicknames) {
        StringBuilder authorStringBuilder = new StringBuilder("By ");
        if (authorNicknames.size() == 1) {
            authorStringBuilder.append(authorNicknames.get(0));
        } else {
            for (int i = 0; i < authorNicknames.size(); i++) {
                authorStringBuilder.append(authorNicknames.get(i));
                if (i < authorNicknames.size() - 2) {
                    authorStringBuilder.append(", ");
                } else if (i == authorNicknames.size() - 2) {
                    authorStringBuilder.append(" and ");
                }
            }
        }
        return authorStringBuilder.toString();
    }

    public List<String> getDescription(Room room, int nbPlayersInside, Config config) {
        List<String> description = new ArrayList<>();
        if (room.getDescription() != null && !room.getDescription().isBlank()) {
            description.add(room.getDescription());
        }

        RoomDifficulty difficulty = RoomDifficulty.fromPercent(room.getDifficulty());
        description.add(difficulty.getDisplayName());

        Optional<String> oAuthorString = getAuthorString(room, config);
        oAuthorString.ifPresent(description::add);

        Optional<String> oTagsString = getTagsString(room, config);
        oTagsString.ifPresent(description::add);

        if (nbPlayersInside > 0) {
            description.add(nbPlayersInside + " player" + (nbPlayersInside > 1 ? "s" : "") + " inside");
        }

        return description;
    }

    public Optional<String> getAuthorString(Room room, Config config) {
        if (room.getAuthors().isEmpty()) {
            return Optional.empty();
        }
        List<String> authorNicknames = room.getAuthors().stream()
                .map(authorUuid -> playerMapper.getNickname(authorUuid, config))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
        if (authorNicknames.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(toAuthorDisplayStr(authorNicknames));
    }


    public Optional<String> getTagsString(Room room, Config config) {
        final List<String> tagsNames = getTagsNameList(room, config);
        if (tagsNames.isEmpty()) {
            return Optional.empty();
        }

        String tagsPrefix = config.getTags().size() > 1 ? "Tags : " : "Tag : ";

        return Optional.of(tagsPrefix + String.join(", ", tagsNames));
    }

    public @NotNull List<String> getTagsNameList(Room room, Config config) {
        return room.getTagsId().stream()
                .map(tagId -> config.getTags()
                        .stream()
                        .filter(tag -> tag.getId() == tagId)
                        .findFirst()
                        .orElseThrow())
                .map(Tag::getName)
                .toList();
    }
}
