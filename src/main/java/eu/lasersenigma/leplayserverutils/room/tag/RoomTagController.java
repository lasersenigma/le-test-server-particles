package eu.lasersenigma.leplayserverutils.room.tag;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.room.RoomController;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import eu.lasersenigma.leplayserverutils.tag.TagController;
import eu.lasersenigma.leplayserverutils.tag.entity.Tag;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class RoomTagController {
    private final RoomController roomController;
    private final ConfigManager configManager;

    private final MessageSender msg;

    @Inject
    public RoomTagController(RoomController roomController, ConfigManager configManager, MessageSender messageSender) {
        this.roomController = roomController;
        this.configManager = configManager;
        this.msg = messageSender;
    }

    public boolean addTag(Player player, String tag) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, RoomController.PLAYER_NOT_IN_ROOM);
            return false;
        }

        Room room = oRoom.get();

        //find the tag to add
        Optional<Tag> tagEntity = TagController.findTagByName(tag, config);
        if (tagEntity.isEmpty()) {
            msg.sendError(player, "Tag %s not found".formatted(tag));
            return false;
        }


        room.getTagsId().add(tagEntity.get().getId());

        configManager.saveConfig(config);
        msg.sendSuccess(player, "Tag %s added".formatted(tag));
        return true;
    }

    public boolean deleteTag(Player player, String tag) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isEmpty()) {
            msg.sendError(player, RoomController.PLAYER_NOT_IN_ROOM);
            return false;
        }

        Room room = oRoom.get();

        //find the tag to delete
        Optional<Tag> tagEntity = TagController.findTagByName(tag, config);
        if (tagEntity.isEmpty()) {
            msg.sendError(player, "Tag %s not found".formatted(tag));
            return false;
        }

        if (room.getTagsId().remove(tagEntity.get().getId())) {
            msg.sendSuccess(player, "Tag %s deleted".formatted(tag));
        } else {
            msg.sendError(player, "Tag %s not found".formatted(tag));
            return false;
        }

        configManager.saveConfig(config);
        return true;
    }

    public Set<String> getTagsNotOnRoom(Player player) {
        Config config = configManager.get();

        List<Tag> allTags = config.getTags();

        List<Tag> tagsToDisplay;

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);
        if (oRoom.isPresent()) {
            Set<Integer> roomTagIds = oRoom.get().getTagsId();

            //keep only the tags that are not on the room
            tagsToDisplay = allTags.stream()
                    .filter(tag -> !roomTagIds.contains(tag.getId()))
                    .toList();
        } else {
            tagsToDisplay = allTags;
        }

        return tagsToDisplay.stream()
                .map(Tag::getName)
                .collect(Collectors.toSet());

    }

    public Set<String> getTagsOnRoom(Player player) {
        Config config = configManager.get();

        Optional<Room> oRoom = roomController.findRoomFromPlayerInRoom(player, config);

        if (oRoom.isEmpty()) {
            return new HashSet<>();
        }
        Room room = oRoom.get();

        return getTags(room, config);
    }

    public @NotNull Set<String> getTags(Room room, Config config) {
        return room.getTagsId().stream()
                .map(tagId -> config.getTags().stream()
                        .filter(tag -> tag.getId() == tagId)
                        .map(Tag::getName)
                        .findAny()
                        .orElseThrow())
                .collect(Collectors.toSet());
    }
}