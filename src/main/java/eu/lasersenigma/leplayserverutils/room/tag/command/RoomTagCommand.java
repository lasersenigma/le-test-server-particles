package eu.lasersenigma.leplayserverutils.room.tag.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class RoomTagCommand extends Command<Player> {

    @Inject
    public RoomTagCommand(RoomTagAddCommand roomTagAddCommand, RoomTagDeleteCommand roomTagDeleteCommand) {
        super(Player.class, "tag");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        registerSubCommand(roomTagAddCommand);
        registerSubCommand(roomTagDeleteCommand);
    }


    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return super.defaultProcessForContainer(commands, player, args);
    }

    @Override
    protected String description(Player executor) {
        return "Defines room's tags";
    }

}
