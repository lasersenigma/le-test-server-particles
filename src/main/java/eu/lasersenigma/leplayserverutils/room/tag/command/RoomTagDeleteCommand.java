package eu.lasersenigma.leplayserverutils.room.tag.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.room.tag.RoomTagController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Set;

@Singleton
public class RoomTagDeleteCommand extends Command<Player> {
    private static final String TAG_PARAM = "tag";
    private final RoomTagController roomTagController;

    @Inject
    public RoomTagDeleteCommand(RoomTagController roomTagController) {
        super(Player.class, "delete");
        this.setPermission(LPUPermission.CREATOR.getPermission());
        this.addArgument(TAG_PARAM, true, ArgumentType.string());
        this.roomTagController = roomTagController;
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(TAG_PARAM)) {
            String currentArgumentValue = getArgumentValue(executor, TAG_PARAM, ArgumentType.string());
            Set<String> tags = roomTagController.getTagsOnRoom(executor);
            super.setAutoCompleteValuesArg(TAG_PARAM, CommandUtils.buildStringAutocompleteValues(tags, currentArgumentValue));
        }
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String tag = this.getArgumentValue(player, TAG_PARAM, ArgumentType.string());
        return roomTagController.deleteTag(player, tag);
    }

    @Override
    protected String description(Player executor) {
        return "Delete a tag from the room";
    }
}
