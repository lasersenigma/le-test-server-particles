package eu.lasersenigma.leplayserverutils.roomtemplate;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.roomtemplate.entity.RoomTemplate;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton
public class RoomTemplateController {

    private static final String PUZZLE_TEMPLATE_NOT_FOUND = "Puzzle template %s not found";
    @Inject
    ConfigManager configManager;

    @Inject
    MessageSender msg;

    public static Set<String> getDuplicatedRoomTemplateNames(Config config) {
        return config.getRoomTemplates().stream()
                .map(roomTemplate -> roomTemplate.getName().toUpperCase(Locale.ROOT))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(entry -> entry.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    public boolean createPuzzleTemplate(Player player, String templateName, Vector minCorner, Vector maxCorner, Material spawnMaterial, Material portalMaterial, Material subPortalMaterial) {
        Config config = configManager.get();

        //Check if no other room templates have the same name
        for (RoomTemplate roomTemplate : config.getRoomTemplates()) {
            if (roomTemplate.getName().equalsIgnoreCase(templateName)) {
                msg.sendError(player, "A room template with the name %s already exists".formatted(templateName.toUpperCase(Locale.ROOT)));
                return false;
            }
        }

        //Add room template
        RoomTemplate roomTemplate = new RoomTemplate(
                templateName,
                player.getWorld().getName(),
                minCorner,
                maxCorner,
                spawnMaterial,
                portalMaterial,
                subPortalMaterial
        );
        config.getRoomTemplates().add(roomTemplate);
        configManager.saveConfig(config);
        msg.sendSuccess(player, "Puzzle template %s created".formatted(templateName.toUpperCase(Locale.ROOT)));
        return true;
    }

    public boolean deletePuzzleTemplate(Player player, String roomTemplateName) {
        Config config = configManager.get();

        Optional<RoomTemplate> oRoomTemplate = getRoomTemplate(config, roomTemplateName);
        if (oRoomTemplate.isEmpty()) {
            msg.sendError(player, PUZZLE_TEMPLATE_NOT_FOUND.formatted(roomTemplateName.toUpperCase(Locale.ROOT)));
            return false;
        }

        RoomTemplate roomTemplate = oRoomTemplate.get();

        if (config.getRoomTemplates().remove(roomTemplate)) {
            configManager.saveConfig(config);
            msg.sendSuccess(player, "Puzzle template %s deleted".formatted(roomTemplateName.toUpperCase(Locale.ROOT)));
            return true;
        } else {
            throw new IllegalStateException("Room template %s not found during its deletion".formatted(roomTemplateName.toUpperCase(Locale.ROOT)));
        }
    }

    public boolean edit(Player player, String roomTemplateName, Vector minCorner, Vector maxCorner, Optional<Material> spawnMaterial, Optional<Material> portalMaterial, Optional<Material> subPortalMaterial) {
        Config config = configManager.get();

        Optional<RoomTemplate> oRoomTemplate = getRoomTemplate(config, roomTemplateName);
        if (oRoomTemplate.isEmpty()) {
            msg.sendSuccess(player, PUZZLE_TEMPLATE_NOT_FOUND.formatted(roomTemplateName.toUpperCase(Locale.ROOT)));
            return false;
        }
        RoomTemplate roomTemplate = oRoomTemplate.get();

        config.getRoomTemplates().stream()
                .filter(rt -> rt.equals(roomTemplate))
                .findFirst()
                .ifPresent(rt -> {
                    rt.setWorldName(player.getWorld().getName());
                    rt.setMinCorner(minCorner);
                    rt.setMaxCorner(maxCorner);
                    spawnMaterial.ifPresent(rt::setSpawnMaterial);
                    portalMaterial.ifPresent(rt::setPortalMaterial);
                    subPortalMaterial.ifPresent(rt::setSubPortalMaterial);
                });
        configManager.saveConfig(config);
        msg.sendSuccess(player, "Puzzle template %s updated".formatted(roomTemplateName.toUpperCase(Locale.ROOT)));
        return true;
    }

    public boolean teleport(Player player, RoomTemplate roomTemplate) {
        World world = Bukkit.getWorld(roomTemplate.getWorldName());
        if (world == null) {
            msg.sendError(player, "World %s not found".formatted(roomTemplate.getWorldName()));
            return false;
        }

        Location roomTemplateCenter = roomTemplate.getMinCorner().clone().add(roomTemplate.getMaxCorner()).multiply(0.5).toLocation(world);
        player.teleport(roomTemplateCenter);
        return true;
    }

    public boolean info(Player player, RoomTemplate roomTemplate) {
        msg.sendTitle(player, "Room template %s".formatted(roomTemplate.getName().toUpperCase(Locale.ROOT)));
        msg.sendParagraph(player, "World: %s".formatted(roomTemplate.getWorldName()));
        msg.sendParagraph(player, "Min corner: %d, %d, %d".formatted(roomTemplate.getMinCorner().getBlockX(), roomTemplate.getMinCorner().getBlockY(), roomTemplate.getMinCorner().getBlockZ()));
        msg.sendParagraph(player, "Max corner: %d, %d, %d".formatted(roomTemplate.getMaxCorner().getBlockX(), roomTemplate.getMaxCorner().getBlockY(), roomTemplate.getMaxCorner().getBlockZ()));
        msg.sendParagraph(player, "Spawn material: %s".formatted(roomTemplate.getSpawnMaterial().name()));
        msg.sendParagraph(player, "Portal material: %s".formatted(roomTemplate.getPortalMaterial().name()));
        msg.sendParagraph(player, "Sub portal material: %s".formatted(roomTemplate.getSubPortalMaterial().name()));
        return true;
    }

    private Optional<RoomTemplate> getRoomTemplate(Config config, String templateName) {
        return config.getRoomTemplates().stream()
                .filter(rt -> rt.getName().toLowerCase(Locale.ROOT).equalsIgnoreCase(templateName))
                .findFirst();
    }
}
