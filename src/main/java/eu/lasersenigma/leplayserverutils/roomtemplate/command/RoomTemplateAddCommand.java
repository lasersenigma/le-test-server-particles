package eu.lasersenigma.leplayserverutils.roomtemplate.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.roomtemplate.RoomTemplateController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

@Singleton
public class RoomTemplateAddCommand extends Command<Player> {

    private static final String ADD_COMMAND = "add";
    private final RoomTemplateController roomTemplateController;

    @Inject
    public RoomTemplateAddCommand(RoomTemplateController roomTemplateController) {
        super(Player.class, ADD_COMMAND);
        this.addArgument(RoomTemplateCommand.TEMPLATE_NAME, true, ArgumentType.string());
        this.addArgument(RoomTemplateCommand.MIN_CORNER, true, ArgumentType.bukkitVector());
        this.addArgument(RoomTemplateCommand.MAX_CORNER, true, ArgumentType.bukkitVector());
        this.addArgument(RoomTemplateCommand.SPAWN_MATERIAL, false, ArgumentType.material());
        this.addArgument(RoomTemplateCommand.PORTAL_MATERIAL, false, ArgumentType.material());
        this.addArgument(RoomTemplateCommand.SUB_PORTAL_MATERIAL, false, ArgumentType.material());
        this.setPermission(RoomTemplateCommand.PERMISSION);
        this.roomTemplateController = roomTemplateController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String templateName = this.getArgumentValue(player, RoomTemplateCommand.TEMPLATE_NAME, ArgumentType.string());
        Vector minCorner = this.getArgumentValue(player, RoomTemplateCommand.MIN_CORNER, ArgumentType.bukkitVector());
        Vector maxCorner = this.getArgumentValue(player, RoomTemplateCommand.MAX_CORNER, ArgumentType.bukkitVector());
        Material spawnMaterial = this.getArgumentValue(player, RoomTemplateCommand.SPAWN_MATERIAL, ArgumentType.material(), Material.GLOWSTONE);
        Material portalMaterial = this.getArgumentValue(player, RoomTemplateCommand.PORTAL_MATERIAL, ArgumentType.material(), Material.SCULK_SHRIEKER);
        Material subPortalMaterial = this.getArgumentValue(player, RoomTemplateCommand.SUB_PORTAL_MATERIAL, ArgumentType.material(), Material.SCULK_CATALYST);
        return roomTemplateController.createPuzzleTemplate(player, templateName, minCorner, maxCorner, spawnMaterial, portalMaterial, subPortalMaterial);
    }

    @Override
    protected String description(Player executor) {
        return "Create a room template";
    }
}
