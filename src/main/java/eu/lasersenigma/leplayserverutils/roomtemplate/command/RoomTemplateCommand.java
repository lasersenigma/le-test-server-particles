package eu.lasersenigma.leplayserverutils.roomtemplate.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.command.provider.LPUCommandsProvider;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class RoomTemplateCommand extends Command<Player> implements JavaPluginEnableEventHandler {

    public static final String TEMPLATE_NAME = "templateName";
    public static final String MIN_CORNER = "minCorner";
    public static final String MAX_CORNER = "maxCorner";
    public static final String SPAWN_MATERIAL = "spawnMaterial";
    public static final String PORTAL_MATERIAL = "portalMaterial";

    public static final String SUB_PORTAL_MATERIAL = "subPortalMaterial";
    public static final String PERMISSION = "leplayserverutils.creator";
    private static final String COMMAND = "roomtemplate";
    private final Commands leplayserverutilsCommands;

    @Inject
    public RoomTemplateCommand(LPUCommandsProvider lpuCommandsProvider,
                               RoomTemplateAddCommand roomTemplateAddCommand,
                               RoomTemplateDeleteCommand roomTemplateDeleteCommand,
                               RoomTemplateEditCommand roomTemplateEditCommand,
                               RoomTemplateTeleportCommand roomTemplateTeleportCommand,
                               RoomTemplateInfoCommand roomTemplateInfoCommand) {
        super(Player.class, COMMAND);
        this.setPermission(PERMISSION);
        registerSubCommand(roomTemplateAddCommand);
        registerSubCommand(roomTemplateDeleteCommand);
        registerSubCommand(roomTemplateEditCommand);
        registerSubCommand(roomTemplateTeleportCommand);
        registerSubCommand(roomTemplateInfoCommand);
        this.leplayserverutilsCommands = lpuCommandsProvider.get();
    }

    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        leplayserverutilsCommands.registerCommand(this);
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return super.defaultProcessForContainer(commands, player, args);
    }

    @Override
    protected String description(Player executor) {
        return "create, delete or update a room template";
    }

}