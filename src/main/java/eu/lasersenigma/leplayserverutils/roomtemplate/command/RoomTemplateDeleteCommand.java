package eu.lasersenigma.leplayserverutils.roomtemplate.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.roomtemplate.RoomTemplateController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class RoomTemplateDeleteCommand extends Command<Player> {

    private static final String DELETE_COMMAND = "delete";
    private final RoomTemplateController roomTemplateController;

    private final ConfigManager configManager;

    @Inject
    public RoomTemplateDeleteCommand(RoomTemplateController roomTemplateController, ConfigManager configManager) {
        super(Player.class, DELETE_COMMAND);
        this.addArgument(RoomTemplateCommand.TEMPLATE_NAME, true, ArgumentType.string());
        this.setPermission(RoomTemplateCommand.PERMISSION);
        this.roomTemplateController = roomTemplateController;
        this.configManager = configManager;
    }


    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(RoomTemplateCommand.TEMPLATE_NAME)) {
            String currentArgumentValue = getArgumentValue(executor, RoomTemplateCommand.TEMPLATE_NAME, ArgumentType.string());
            final Set<String> roomTemplatesNameSet = configManager.get()
                    .getRoomTemplates().stream()
                    .map(roomTemplate -> roomTemplate.getName().toUpperCase(Locale.ROOT))
                    .collect(Collectors.toSet());
            super.setAutoCompleteValuesArg(
                    RoomTemplateCommand.TEMPLATE_NAME,
                    CommandUtils.buildStringAutocompleteValues(roomTemplatesNameSet, currentArgumentValue));
        }
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String roomTemplateName = this.getArgumentValue(player, RoomTemplateCommand.TEMPLATE_NAME, ArgumentType.string()).toUpperCase(Locale.ROOT);

        return roomTemplateController.deletePuzzleTemplate(player, roomTemplateName);
    }

    @Override
    protected String description(Player executor) {
        return "Delete a room template";
    }
}
