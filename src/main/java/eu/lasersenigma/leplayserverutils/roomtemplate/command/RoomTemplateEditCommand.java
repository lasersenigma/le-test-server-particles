package eu.lasersenigma.leplayserverutils.roomtemplate.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.roomtemplate.RoomTemplateController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class RoomTemplateEditCommand extends Command<Player> {

    private static final String EDIT_COMMAND = "edit";
    private final RoomTemplateController roomTemplateController;

    private final ConfigManager configManager;

    @Inject
    public RoomTemplateEditCommand(RoomTemplateController roomTemplateController, ConfigManager configManager) {
        super(Player.class, EDIT_COMMAND);
        this.addArgument(RoomTemplateCommand.TEMPLATE_NAME, true, ArgumentType.string());
        this.addArgument(RoomTemplateCommand.MIN_CORNER, true, ArgumentType.bukkitVector());
        this.addArgument(RoomTemplateCommand.MAX_CORNER, true, ArgumentType.bukkitVector());
        this.addArgument(RoomTemplateCommand.SPAWN_MATERIAL, false, ArgumentType.material());
        this.addArgument(RoomTemplateCommand.PORTAL_MATERIAL, false, ArgumentType.material());
        this.addArgument(RoomTemplateCommand.SUB_PORTAL_MATERIAL, false, ArgumentType.material());
        this.setPermission(RoomTemplateCommand.PERMISSION);
        this.roomTemplateController = roomTemplateController;
        this.configManager = configManager;
    }


    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(RoomTemplateCommand.TEMPLATE_NAME)) {
            String currentArgumentValue = getArgumentValue(executor, RoomTemplateCommand.TEMPLATE_NAME, ArgumentType.string());
            final Set<String> roomTemplatesNameSet = configManager.get()
                    .getRoomTemplates().stream()
                    .map(roomTemplate -> roomTemplate.getName().toUpperCase(Locale.ROOT))
                    .collect(Collectors.toSet());
            super.setAutoCompleteValuesArg(
                    RoomTemplateCommand.TEMPLATE_NAME,
                    CommandUtils.buildStringAutocompleteValues(roomTemplatesNameSet, currentArgumentValue));
        }
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String roomTemplateName = this.getArgumentValue(player, RoomTemplateCommand.TEMPLATE_NAME, ArgumentType.string()).toUpperCase(Locale.ROOT);

        Vector minCorner = this.getArgumentValue(player, RoomTemplateCommand.MIN_CORNER, ArgumentType.bukkitVector());
        Vector maxCorner = this.getArgumentValue(player, RoomTemplateCommand.MAX_CORNER, ArgumentType.bukkitVector());
        Optional<Material> oSpawnMaterial = Optional.ofNullable(this.getArgumentValue(player, RoomTemplateCommand.SPAWN_MATERIAL, ArgumentType.material(), null));
        Optional<Material> oPortalMaterial = Optional.ofNullable(this.getArgumentValue(player, RoomTemplateCommand.PORTAL_MATERIAL, ArgumentType.material(), null));
        Optional<Material> oSubPortalMaterial = Optional.ofNullable(this.getArgumentValue(player, RoomTemplateCommand.SUB_PORTAL_MATERIAL, ArgumentType.material(), null));

        return roomTemplateController.edit(player, roomTemplateName, minCorner, maxCorner, oSpawnMaterial, oPortalMaterial, oSubPortalMaterial);
    }

    @Override
    protected String description(Player executor) {
        return "Edit a room template";
    }
}
