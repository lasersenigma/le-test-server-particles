package eu.lasersenigma.leplayserverutils.roomtemplate.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.roomtemplate.RoomTemplateController;
import eu.lasersenigma.leplayserverutils.roomtemplate.entity.RoomTemplate;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class RoomTemplateTeleportCommand extends Command<Player> {

    private static final String TELEPORT_COMMAND = "teleport";
    private final RoomTemplateController roomTemplateController;

    private final ConfigManager configManager;

    private final MessageSender msg;

    @Inject
    public RoomTemplateTeleportCommand(RoomTemplateController roomTemplateController, ConfigManager configManager, MessageSender messageSender) {
        super(Player.class, TELEPORT_COMMAND);
        this.addArgument(RoomTemplateCommand.TEMPLATE_NAME, true, ArgumentType.string());
        this.setPermission(RoomTemplateCommand.PERMISSION);
        this.roomTemplateController = roomTemplateController;
        this.configManager = configManager;
        this.msg = messageSender;
    }


    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(RoomTemplateCommand.TEMPLATE_NAME)) {
            String currentArgumentValue = getArgumentValue(executor, RoomTemplateCommand.TEMPLATE_NAME, ArgumentType.string());
            final Set<String> roomTemplatesNameSet = configManager.get()
                    .getRoomTemplates().stream()
                    .map(roomTemplate -> roomTemplate.getName().toUpperCase(Locale.ROOT))
                    .collect(Collectors.toSet());

            super.setAutoCompleteValuesArg(
                    RoomTemplateCommand.TEMPLATE_NAME,
                    CommandUtils.buildStringAutocompleteValues(roomTemplatesNameSet, currentArgumentValue));
        }
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String roomTemplateStr = this.getArgumentValue(player, RoomTemplateCommand.TEMPLATE_NAME, ArgumentType.string()).toUpperCase(Locale.ROOT);
        Optional<RoomTemplate> oRoomTemplate = configManager.get()
                .getRoomTemplates().stream()
                .filter(roomTemplate -> roomTemplate.getName().toUpperCase().equals(roomTemplateStr))
                .findFirst();
        if (oRoomTemplate.isEmpty()) {
            msg.sendError(player, "Room template not found");
            return false;
        }

        RoomTemplate roomTemplate = oRoomTemplate.get();
        return roomTemplateController.teleport(player, roomTemplate);
    }

    @Override
    protected String description(Player executor) {
        return "Teleport to a room template";
    }
}
