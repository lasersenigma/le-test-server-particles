package eu.lasersenigma.leplayserverutils.roomtemplate.entity;

import com.google.gson.annotations.Expose;
import lombok.*;
import org.bukkit.Material;
import org.bukkit.util.Vector;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class RoomTemplate {

    @Expose
    @EqualsAndHashCode.Include
    private String name;

    @Expose
    private String worldName;

    @Expose
    private Vector minCorner;

    @Expose
    private Vector maxCorner;

    @Expose
    private Material spawnMaterial = Material.GLOWSTONE;

    @Expose
    private Material portalMaterial = Material.SCULK_SHRIEKER;

    @Expose
    private Material subPortalMaterial = Material.SCULK_CATALYST;


    public RoomTemplate(RoomTemplate roomTemplate) {
        this.name = roomTemplate.getName();
        this.worldName = roomTemplate.getWorldName();
        this.minCorner = roomTemplate.getMinCorner().clone();
        this.maxCorner = roomTemplate.getMaxCorner().clone();
        this.spawnMaterial = roomTemplate.getSpawnMaterial();
        this.portalMaterial = roomTemplate.getPortalMaterial();
        this.subPortalMaterial = roomTemplate.getSubPortalMaterial();
    }
}
