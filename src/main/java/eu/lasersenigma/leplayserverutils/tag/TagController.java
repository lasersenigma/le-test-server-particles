package eu.lasersenigma.leplayserverutils.tag;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.common.config.entity.Config;
import eu.lasersenigma.leplayserverutils.common.messages.MessageComponent;
import eu.lasersenigma.leplayserverutils.common.messages.MessageComponentColor;
import eu.lasersenigma.leplayserverutils.common.messages.MessageSender;
import eu.lasersenigma.leplayserverutils.portal.entity.Portal;
import eu.lasersenigma.leplayserverutils.room.entity.Room;
import eu.lasersenigma.leplayserverutils.tag.entity.Tag;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;

@Singleton
public class TagController {

    private static final List<String> PRIORITY_TAGS = List.of("SOLO", "DUO", "TRIO", "JUMP", "RACE");
    private static final String TAG_ALREADY_EXISTS = "Tag %s already exists";
    private static final String TAG_DOES_NOT_EXIST = "Tag %s does not exist";
    private static final String AVAILABLE_TAGS_TITLE = "Available tags:";

    @Inject
    ConfigManager configManager;

    @Inject
    MessageSender msg;

    public static Optional<Tag> findTagByName(String tagName, Config config) {
        return config.getTags().stream()
                .filter(tagEntity -> tagEntity.getName().equals(tagName))
                .findFirst();
    }

    public boolean create(Player player, String tagName) {
        Config config = configManager.get();

        Optional<Tag> tag = findTagByName(tagName, config);

        if (tag.isPresent()) {
            msg.sendError(player, String.format(TAG_ALREADY_EXISTS, tagName));
            return false;
        }

        config.getTags().add(new Tag(config.getTags().size(), tagName));

        configManager.saveConfig(config);
        return true;
    }

    public boolean delete(Player player, String tagName, boolean deleteTagsIfUsedInRoom) {
        Config config = configManager.get();

        Optional<Tag> oTag = findTagByName(tagName, config);

        if (oTag.isEmpty()) {
            msg.sendError(player, String.format(TAG_DOES_NOT_EXIST, tagName));
            return false;
        }

        final Tag tag = oTag.get();

        // check if tag is used in a room,
        List<Room> roomsHavingThisTag = config.getPortals().stream()
                .filter(Portal::isActive)
                .map(Portal::getRoom)
                .filter(room -> room.getTagsId().contains(tag.getId()))
                .toList();

        // send an error message if it is used and if deleteTagsIfUsedInRoom is false
        if (!roomsHavingThisTag.isEmpty() && !deleteTagsIfUsedInRoom) {
            String roomHavingThisTagDescriptions = roomsHavingThisTag.stream()
                    .map(Room::getDescription)
                    .collect(Collectors.joining(", "));
            msg.sendWarning(player, "Tag is used in rooms: %s".formatted(roomHavingThisTagDescriptions));
            msg.sendWarning(player, "Use '/. room delete %s true' to delete the tag anyway".formatted(tagName));
            return true;
        }

        config.getTags().remove(tag);
        configManager.saveConfig(config);

        return true;
    }

    public boolean list(Player player) {

        MessageComponent component = new MessageComponent(
                AVAILABLE_TAGS_TITLE,
                MessageComponentColor.TITLE,
                TextDecoration.UNDERLINED);
        component.append(new MessageComponent(
                " " + String.join(", ", getOrderedTags()),
                MessageComponentColor.DETAILS
        ));

        msg.sendParagraph(player, component);
        return true;
    }

    public @NotNull Set<String> getTags() {
        return configManager.get().getTags().stream()
                .map(Tag::getName)
                .collect(Collectors.toSet());
    }



    public @NotNull List<String> getOrderedTags() {
        final Set<String> tags = getTags();

        //order tags
        List<String> tagsList = new ArrayList<>(tags);
        tagsList.sort((tag1, tag2) -> {
            final String upperCaseTag1 = tag1.toUpperCase(Locale.ROOT);
            final boolean isTag1Priority = PRIORITY_TAGS.contains(upperCaseTag1);
            final String upperCaseTag2 = tag2.toUpperCase(Locale.ROOT);
            final boolean isTag2Priority = PRIORITY_TAGS.contains(upperCaseTag2);
            if (isTag1Priority && isTag2Priority) {
                return PRIORITY_TAGS.indexOf(upperCaseTag1) - PRIORITY_TAGS.indexOf(upperCaseTag2);
            }
            if (isTag1Priority) {
                return -1;
            }
            if (isTag2Priority) {
                return 1;
            }
            return tag1.compareTo(tag2);
        });
        return tagsList;
    }

    public boolean rename(Player player, String tagName, String newTagName) {
        Config config = configManager.get();

        Optional<Tag> tag = findTagByName(tagName, config);

        if (tag.isEmpty()) {
            msg.sendError(player, String.format(TAG_DOES_NOT_EXIST, tagName));
            return false;
        }

        tag.get().setName(newTagName);

        configManager.saveConfig(config);
        return true;
    }
}
