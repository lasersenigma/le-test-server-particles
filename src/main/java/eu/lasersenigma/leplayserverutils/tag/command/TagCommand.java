package eu.lasersenigma.leplayserverutils.tag.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.JavaPluginEnableEventHandler;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.provider.LPUCommandsProvider;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class TagCommand extends Command<Player> implements JavaPluginEnableEventHandler {
    private final Commands leplayserverutilsCommands;

    @Inject
    public TagCommand(LPUCommandsProvider lpuCommandsProvider,
                      TagCreateCommand tagCreateCommand,
                      TagDeleteCommand tagDeleteCommand,
                      TagRenameCommand tagRenameCommand,
                      TagListCommand tagListCommand) {
        super(Player.class, "tag");

        this.setPermission(LPUPermission.CREATOR.getPermission());
        registerSubCommand(tagCreateCommand);
        registerSubCommand(tagDeleteCommand);
        registerSubCommand(tagRenameCommand);
        registerSubCommand(tagListCommand);
        this.leplayserverutilsCommands = lpuCommandsProvider.get();
    }

    @Override
    public void enable() {
        //Ensure the command is created because an instance will be built on enable
        leplayserverutilsCommands.registerCommand(this);
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return super.defaultProcessForContainer(commands, player, args);
    }

    @Override
    protected String description(Player executor) {
        return "Configure the tags that can then be applied to rooms";
    }

}
