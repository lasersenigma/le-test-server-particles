package eu.lasersenigma.leplayserverutils.tag.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.tag.TagController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

@Singleton
public class TagCreateCommand extends Command<Player> {
    private static final String TAG_NAME_PARAM = "tagName";
    private final TagController tagController;

    @Inject
    public TagCreateCommand(TagController tagController) {
        super(Player.class, "create");
        this.addArgument(TAG_NAME_PARAM, true, ArgumentType.string());
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.tagController = tagController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String tagName = this.getArgumentValue(player, TAG_NAME_PARAM, ArgumentType.string());
        return tagController.create(player, tagName);
    }

    @Override
    protected String description(Player executor) {
        return "Creates a new tag";
    }
}
