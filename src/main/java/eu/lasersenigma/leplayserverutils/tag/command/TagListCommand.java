package eu.lasersenigma.leplayserverutils.tag.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.tag.TagController;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

@Singleton
public class TagListCommand extends Command<Player> {
    private final TagController tagController;

    @Inject
    public TagListCommand(TagController tagController) {
        super(Player.class, "list");
        this.setPermission(LPUPermission.ADMIN.getPermission());
        this.tagController = tagController;
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        return tagController.list(player);
    }

    @Override
    protected String description(Player executor) {
        return "List tags";
    }
}