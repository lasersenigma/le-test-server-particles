package eu.lasersenigma.leplayserverutils.tag.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import eu.lasersenigma.leplayserverutils.common.LPUPermission;
import eu.lasersenigma.leplayserverutils.common.command.utils.CommandUtils;
import eu.lasersenigma.leplayserverutils.common.config.ConfigManager;
import eu.lasersenigma.leplayserverutils.tag.TagController;
import eu.lasersenigma.leplayserverutils.tag.entity.Tag;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class TagRenameCommand extends Command<Player> {
    private static final String TAG_NAME_PARAM = "tagName";
    private static final String NEW_TAG_NAME_PARAM = "newTagName";

    private final TagController tagController;

    private final ConfigManager configManager;

    @Inject
    public TagRenameCommand(TagController tagController, ConfigManager configManager) {
        super(Player.class, "rename");
        this.configManager = configManager;
        this.tagController = tagController;
        this.addArgument(TAG_NAME_PARAM, true, ArgumentType.string());
        this.addArgument(NEW_TAG_NAME_PARAM, true, ArgumentType.string());
        this.setPermission(LPUPermission.ADMIN.getPermission());
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        if (currentArgumentName.equals(TAG_NAME_PARAM)) {
            String currentArgumentValue = getArgumentValue(executor, TAG_NAME_PARAM, ArgumentType.string());
            final Set<String> tagsNameSet = configManager.get().getTags().stream()
                    .map(Tag::getName)
                    .collect(Collectors.toSet());

            super.setAutoCompleteValuesArg(
                    TAG_NAME_PARAM,
                    CommandUtils.buildStringAutocompleteValues(tagsNameSet, currentArgumentValue));
        }
    }

    @Override
    protected boolean process(Commands commands, Player player, String... args) {
        String tagName = this.getArgumentValue(player, TAG_NAME_PARAM, ArgumentType.string());
        String newTagName = this.getArgumentValue(player, NEW_TAG_NAME_PARAM, ArgumentType.string());
        return tagController.rename(player, tagName, newTagName);
    }

    @Override
    protected String description(Player executor) {
        return "Rename a tag";
    }
}