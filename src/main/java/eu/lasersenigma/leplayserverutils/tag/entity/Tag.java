package eu.lasersenigma.leplayserverutils.tag.entity;

import com.google.gson.annotations.Expose;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Tag {

    @Expose
    private int id;

    @Expose
    private String name;

    public Tag(Tag tag) {
        this.id = tag.getId();
        this.name = tag.getName();
    }
}
